-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 01, 2020 at 01:27 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `esame`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin@saltu.in', '$2y$10$MSQzhq41AzktB03bAZWxNeOPvYp0YB2Zyu99fds78FSbxRmpqEy9q', 'oFLxmaS5ZfIjLvKpj9xO8mupnwxUiJFc4K27W7bdMIVQM9sCReKADW7VKoS9', '2019-11-12 10:55:48', '2020-01-31 11:06:30');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `class_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `added_by` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `class_id`, `name`, `description`, `image`, `added_by`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 'Engineering', 'The branch of science and technology concerned with the design, building, and use of engines, machines, and structures. With XI Syllabus', '', 1, 0, '2020-01-22 05:55:39', '2020-01-22 05:55:39'),
(2, 1, 'Medical', 'Biomedical engineering or medical engineering is the application of engineering principles and design concepts to medicine and biology for healthcare purposes. On XI Syllabus\r\n', '', 1, 0, '2020-01-22 05:56:24', '2020-01-22 05:56:24'),
(3, 2, 'Medical ', 'Biomedical engineering or medical engineering is the application of engineering principles and design concepts to medicine and biology for healthcare purposes. On XII Syllabus', '', 1, 0, '2020-01-22 05:57:31', '2020-01-22 05:57:31'),
(4, 2, 'Engineering', 'The branch of science and technology concerned with the design, building, and use of engines, machines, and structures. With XII Syllabus', '', 1, 0, '2020-01-22 05:58:57', '2020-01-22 05:58:57');

-- --------------------------------------------------------

--
-- Table structure for table `centers`
--

CREATE TABLE `centers` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `registration_no` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `place` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(10) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `centers`
--

INSERT INTO `centers` (`id`, `name`, `registration_no`, `email`, `place`, `address`, `phone`, `password`, `status`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'saltu', '1234', 'admin@saltu.in', 'Nangiarkulangara', 'Karthikapally,Nagyarkulangara,Allapuzha', '7012525741', '$2y$10$gsS8o.MN/rHo27GsnqU4/.iq1yNg1cgHW/Gd7td73r19SamPgU/ua', 0, 'ioYf8veTedyRCcqbzw7DVUa5En8kAsgHWEDcCZbKo5P4omhCghlCQ9eiSmIt', '2019-09-16 13:54:42', '2020-01-28 12:03:09'),
(5, 'testing', '101', 'nithinpvarghese12@gmail.com', 'teat', 'dfbg', '1234567890', '$2y$10$dfrKnBvEYcUm5afb6Wcl/e3PdSx7cKsAxFxPWvm.hc73XS0YZX0fC', 0, NULL, '2019-11-14 19:25:55', '2019-11-14 19:25:55');

-- --------------------------------------------------------

--
-- Table structure for table `chapters`
--

CREATE TABLE `chapters` (
  `id` int(10) UNSIGNED NOT NULL,
  `subject_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `added_by` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `chapters`
--

INSERT INTO `chapters` (`id`, `subject_id`, `name`, `description`, `added_by`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 'Some Basic Concepts of Chemistry', 'dummy', 1, 0, '2020-01-24 07:52:43', '2020-01-24 07:52:43'),
(2, 1, 'Structure of Atom', '', 1, 0, '2020-01-28 07:23:53', '2020-01-28 07:23:53'),
(3, 1, 'Classification of Elements and Periodicity in Properties', '', 1, 0, '2020-01-28 07:24:10', '2020-01-28 07:24:10'),
(4, 1, 'Chemical Bonding and Molecular Structure', '', 1, 0, '2020-01-28 07:24:34', '2020-01-28 07:24:34'),
(5, 2, 'Physical World', '', 1, 0, '2020-01-28 08:04:18', '2020-01-28 08:04:18'),
(7, 2, 'Units and Measurement', '', 1, 0, '2020-01-28 08:04:34', '2020-01-28 08:04:34'),
(8, 2, 'Motion in A Straight Line', '', 1, 0, '2020-01-28 08:04:47', '2020-01-28 08:04:47'),
(9, 2, 'Motion in A Plane', '', 1, 0, '2020-01-28 08:05:01', '2020-01-28 08:05:01'),
(10, 3, 'Sets', '', 1, 0, '2020-01-28 08:06:30', '2020-01-28 08:06:30'),
(11, 3, 'Relations and Functions', '', 1, 0, '2020-01-28 08:06:47', '2020-01-28 08:06:47'),
(12, 3, 'Trigonometric Functions', '', 1, 0, '2020-01-28 08:07:02', '2020-01-28 08:07:02'),
(13, 3, 'Principle of Mathematical Induction', '', 1, 0, '2020-01-28 08:07:24', '2020-01-28 08:07:24'),
(14, 4, 'The Living World', '', 1, 0, '2020-01-28 08:07:51', '2020-01-28 08:07:51'),
(15, 4, 'Biological Classification', '', 1, 0, '2020-01-28 08:08:04', '2020-01-28 08:08:04'),
(16, 4, 'Plant Kingdom', '', 1, 0, '2020-01-28 08:08:18', '2020-01-28 08:08:18'),
(17, 4, 'Animal Kingdom', '', 1, 0, '2020-01-28 08:08:31', '2020-01-28 08:08:31'),
(18, 7, 'The Solid State', '', 1, 0, '2020-02-01 06:28:41', '2020-02-01 06:28:41'),
(19, 7, 'Solutions', '', 1, 0, '2020-02-01 06:43:14', '2020-02-01 06:43:14'),
(20, 7, 'ElectroChemistry', '', 1, 0, '2020-02-01 11:11:20', '2020-02-01 11:11:20');

-- --------------------------------------------------------

--
-- Table structure for table `classes`
--

CREATE TABLE `classes` (
  `id` int(10) UNSIGNED NOT NULL,
  `class` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `added_by` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `classes`
--

INSERT INTO `classes` (`id`, `class`, `added_by`, `status`, `created_at`, `updated_at`) VALUES
(1, 'XI', 1, 0, '2020-01-13 06:46:47', '2020-01-13 06:46:47'),
(2, 'XII', 1, 0, '2020-01-13 06:47:15', '2020-01-13 06:47:15');

-- --------------------------------------------------------

--
-- Table structure for table `exam_fees`
--

CREATE TABLE `exam_fees` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ex_package_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `student_amount` int(11) NOT NULL,
  `center_amount` int(11) NOT NULL,
  `added_by` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `exam_fees`
--

INSERT INTO `exam_fees` (`id`, `title`, `ex_package_id`, `student_amount`, `center_amount`, `added_by`, `status`, `created_at`, `updated_at`) VALUES
(1, 'plus one jee', '1', 500, 300, 1, 0, '2020-01-24 08:19:57', '2020-01-24 08:19:57');

-- --------------------------------------------------------

--
-- Table structure for table `exam_names`
--

CREATE TABLE `exam_names` (
  `id` int(10) UNSIGNED NOT NULL,
  `category_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `added_by` int(11) NOT NULL,
  `subject_ids` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `exam_names`
--

INSERT INTO `exam_names` (`id`, `category_id`, `name`, `description`, `image`, `added_by`, `subject_ids`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 'JEE', 'Joint Entrance Examination – Main, formerly All India Engineering Entrance Examination, is an examination organised by the National Testing Agency in India. ', '', 1, ',1,2,3', 0, '2020-01-22 06:07:54', '2020-01-28 07:20:01'),
(2, 2, 'NEET', 'The National Eligibility cum Entrance Test-Under Graduate, succeeded from All India Pre-Medical Test is an entrance examination in India for students who wish to study undergraduate medical courses and dental courses in government or private medical colle', '', 1, ',1,2,4', 0, '2020-01-22 06:08:10', '2020-01-28 07:20:24'),
(3, 4, 'JEE', 'Joint Entrance Examination – Main, formerly All India Engineering Entrance Examination, is an examination organised by the National Testing Agency in India. ', '', 1, ',6,7,8', 0, '2020-01-22 06:08:27', '2020-01-28 07:22:23'),
(4, 3, 'NEET', 'The National Eligibility cum Entrance Test-Under Graduate, succeeded from All India Pre-Medical Test is an entrance examination in India for students who wish to study undergraduate medical courses and dental courses in government or private medical colle', '', 1, ',5,7,8', 0, '2020-01-22 06:08:40', '2020-01-28 07:22:23');

-- --------------------------------------------------------

--
-- Table structure for table `exam_type_packages`
--

CREATE TABLE `exam_type_packages` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `class_ids` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `ex_type_ids` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `added_by` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `exam_type_packages`
--

INSERT INTO `exam_type_packages` (`id`, `title`, `class_ids`, `ex_type_ids`, `added_by`, `status`, `created_at`, `updated_at`) VALUES
(1, 'plus one jee', '1', '1', 1, 0, '2020-01-24 08:16:55', '2020-01-24 08:16:55');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2020_01_09_114905_create_admins_table', 1),
('2020_01_09_114953_create_students_table', 1),
('2020_01_09_115007_create_centers_table', 1),
('2020_01_09_121329_create_questions_table', 1),
('2020_01_09_122232_create_students_approvals_table', 1),
('2020_01_09_123931_create_classes_table', 1),
('2020_01_09_124120_create_categories_table', 1),
('2020_01_09_124148_create_exam_names_table', 1),
('2020_01_09_124203_create_subjects_table', 1),
('2020_01_09_124221_create_chapters_table', 1),
('2020_01_10_121331_create_exam_type_packages_table', 1),
('2020_01_10_121422_create_exam_fees_table', 1),
('2020_01_10_121457_create_question_details_table', 1),
('2020_01_10_121511_create_question_options_table', 1),
('2020_01_14_133832_create_student_payments_table', 2),
('2020_01_14_133900_create_student_exam_paids_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions` (
  `id` int(10) UNSIGNED NOT NULL,
  `question` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `solution` int(11) NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `q_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `added_by` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`id`, `question`, `solution`, `description`, `q_image`, `added_by`, `status`, `created_at`, `updated_at`) VALUES
(1, 'First Question?`', 1, 'test', NULL, 0, 0, '2020-01-31 09:50:05', '2020-01-31 09:50:05'),
(2, 'First Question2?', 5, 'test', NULL, 0, 0, '2020-01-31 09:50:55', '2020-01-31 09:50:55'),
(3, 'The interpaticle force in solid hydrogen are', 9, 'The forces of attraction in molecular  solids are van der Waals’ forces. ', NULL, 0, 0, '2020-02-01 06:30:35', '2020-02-01 06:30:35'),
(4, 'Crystalline solid is not', 13, 'Crystalline solids are not isotropic, actually refers to anisotropic. ', NULL, 0, 0, '2020-02-01 06:32:11', '2020-02-01 06:32:11'),
(5, 'Which of the following statements about amorphous solid is incorrect?', 17, 'Amorphous solids are isotropic.', NULL, 0, 0, '2020-02-01 06:34:38', '2020-02-01 06:34:38'),
(6, 'Which type of solid crystals will conduct  heat and electricity', 21, 'Metallic crystals are good conductor of heat and electricity due to the presence of free electrons in them. ', NULL, 0, 0, '2020-02-01 06:35:29', '2020-02-01 06:35:29'),
(7, 'Which of the following is not correct for  ionic crystals?', 25, ' Ionic crystals have no directional bonds as the oppositely charged ions are attracted by electrostatic forces and arranged in a definite ratio.', NULL, 0, 0, '2020-02-01 06:36:32', '2020-02-01 06:36:32'),
(8, 'Graphitre is a soft solid lubricant extremely difficult to melt. The reason for this  anomalous behaviour is that graphite', 29, 'Graphite has a two-dimensional sheet like structure and each carbon atom is sp2 hybridised. The layer structure of graphite is much less compact than diamond. Since, the bonding between the layers involving only van der Waals’ forces is weak, these layers', NULL, 0, 0, '2020-02-01 06:37:51', '2020-02-01 06:37:51'),
(9, 'Among solids, the highest melting point is established by ', 33, 'Covalent or network solids have extremely high melting points and may even decompose before melting. ', NULL, 0, 0, '2020-02-01 06:39:07', '2020-02-01 06:39:07'),
(10, 'For two ionic solids CaO and KI, identify the wrong statement among the following', 37, ' KI is an ionic solid while benzene is not. Therefore, it cannot be soluble in benzene.', NULL, 0, 0, '2020-02-01 06:40:12', '2020-02-01 06:40:12'),
(11, 'Which one of the following forms a molecular solid when solidified', 41, '', NULL, 0, 0, '2020-02-01 06:41:06', '2020-02-01 06:41:06'),
(12, 'A unit cell of diomond consists of ', 45, ' Diamond is like ZnS (zinc blende).  Carbon in diamond forms ccp (fcc)  and also occupies half of tetrahedral  voids. Hence, there are 4 units per  unit cell of diamond. ', NULL, 0, 0, '2020-02-01 06:42:35', '2020-02-01 06:42:35'),
(13, 'Sucrose dissoved in water is a type', 49, ' Sucrose dissolved in water, is a  solid in liquid type solution. ', NULL, 0, 0, '2020-02-01 06:53:18', '2020-02-01 06:53:18'),
(14, 'Concentration terms like mass percentage, ppm, mole fraction and molarity do not depend on temperature. However, molarity is a function of temperature because', 53, '', NULL, 0, 0, '2020-02-01 06:56:37', '2020-02-01 06:56:37'),
(15, 'On dissolving sugar in water at room temperature, solution feels cool to touch. Under which of the following cases dissolution of sugar will be most rapid?', 57, 'As dissolution of sugar in water is  an endothermic process. Hence,  solubility increases with temperature,  also increased surface area of sugar increases the solubility.', NULL, 0, 0, '2020-02-01 06:58:12', '2020-02-01 06:58:12'),
(16, 'which of the following solutions shows positive deviation from Raoult\'s law?', 61, 'Acetone + ethanol, is an example of  solutions showing positive deviation  from Raoult’s law. Since, acetone- ethanol attractions are weaker than  acetone-acetone and  ethanolethanol attractions.', NULL, 0, 0, '2020-02-01 06:59:38', '2020-02-01 06:59:38'),
(17, 'If two liquids A and B form minimum boiling azeotrope at some specific compression then', 65, 'If A – B interactions is smaller than A – A or B – B  interactions, then the vapour pressure will be  more and the result will be positive deviation. The  solutions which show positive deviation form  minimum boiling azeotropes.', NULL, 0, 0, '2020-02-01 07:00:32', '2020-02-01 07:00:32'),
(18, 'A plant cell shrinks when it is kept in a ', 69, ' Hypertonic solution has high  osmotic pressure. When a plant cell  is placed in hypertonic solution,  water will diffuse out of the cell  resulting in shrinking of the cell. ', NULL, 0, 0, '2020-02-01 07:01:50', '2020-02-01 07:01:50'),
(19, 'which of the following is colligative property?', 73, 'Relative lowering of vapour pressure  is a colligative property and not  lowering of vapour pressure.  Out of given properties, only  osmotic pressure is a colligative  property.', NULL, 0, 0, '2020-02-01 07:02:54', '2020-02-01 07:02:54'),
(20, 'what happends to freezing point of benzene when naphthelene is added', 77, 'When a non-volatile solid is added  to the solvent its vapour pressure  decreases.', NULL, 0, 0, '2020-02-01 07:03:41', '2020-02-01 07:03:41'),
(21, 'Which of the following statemennt about the composition of the vapour over an ideal 1:1 molar mixture of benzene and toluene is correct at 250 C.', 81, '', NULL, 0, 0, '2020-02-01 07:04:52', '2020-02-01 07:04:52'),
(22, 'What is the mole fraction of the solute in a 1.00 m aqueous solution?', 85, '', NULL, 0, 0, '2020-02-01 07:07:02', '2020-02-01 07:07:02'),
(23, 'In a Daniell cell,', 89, '', NULL, 0, 0, '2020-02-01 11:13:04', '2020-02-01 11:13:04'),
(24, 'In the standard hydrogen electrode, the inert metal used is', 93, '', NULL, 0, 0, '2020-02-01 11:14:59', '2020-02-01 11:14:59'),
(25, 'Which one of the following statement is wrong?', 97, 'It is used as primary reference  electrode.', NULL, 0, 0, '2020-02-01 11:16:23', '2020-02-01 11:16:23'),
(26, 'In an electrochemical cell, the electrons flow from', 101, 'The electrons liberated at anode  during oxidation, move towards  cathode where they are used up. ', NULL, 0, 0, '2020-02-01 11:17:11', '2020-02-01 11:17:11'),
(27, 'Mark the incorrect statement', 105, '', NULL, 0, 0, '2020-02-01 11:18:25', '2020-02-01 11:18:25'),
(28, 'In an electrolytic cell, the flow of electrons is ', 109, 'In electrolytic cell, electrons flow  from cathode to anode through  internal supply', NULL, 0, 0, '2020-02-01 11:19:50', '2020-02-01 11:19:50'),
(29, 'During the electrolysis of molten sodium ch,loride, the time required to produce 0.10 mol of chlorine gas using a current of 3 ampere is', 113, '', NULL, 0, 0, '2020-02-01 11:23:42', '2020-02-01 11:23:42'),
(30, 'Zinc can be coated on iron to produce galvanized iron but the reverse is not possible. It is because', 117, '', NULL, 0, 0, '2020-02-01 11:24:56', '2020-02-01 11:24:56'),
(31, '1 . Two students X and Y report the weight of the same substances as 4.0 g and 4.00g respectively .which of the following staement is coorect ?', 121, '', NULL, 0, 0, '2020-02-01 11:28:23', '2020-02-01 11:28:23'),
(32, ' Which set of figures will be obtained after rounding up the  following upmto three significant figures?', 125, '', NULL, 0, 0, '2020-02-01 11:31:33', '2020-02-01 11:31:33'),
(33, 'Mark the rule which is not correctly stated about the determination of significant figures  ', 129, '', NULL, 0, 0, '2020-02-01 11:32:34', '2020-02-01 11:32:34'),
(34, 'The following data are obtained when dinitrogen and dioxygen react together to form different compounds:', 133, '', NULL, 0, 0, '2020-02-01 11:33:26', '2020-02-01 11:33:26'),
(35, 'Which of the following statement indicates that law of multiple  proportion is being followed?', 137, '', NULL, 0, 0, '2020-02-01 12:06:10', '2020-02-01 12:06:10'),
(36, 'If one gram of a metal carnate gave 0.56 g of its  oxide on heating ,then equivalent  weight of the metal will be', 141, '', NULL, 0, 0, '2020-02-01 12:07:55', '2020-02-01 12:07:55'),
(37, 'Oxygen occurs in nature as a mixture of isotopes 16O having  atomic masses of 15.995 u,16.999u and 17.999 u and relative  abundance of 99.763% ,0.037% and 0.200% respectively . what is the average attomic mass of oxygen', 145, '', NULL, 0, 0, '2020-02-01 12:09:46', '2020-02-01 12:09:46'),
(38, 'How much water is needed to dilute 10 ML hydrochlroric acid  to make it exactly decinormal (0.1 an)?  ', 149, '', NULL, 0, 0, '2020-02-01 12:10:37', '2020-02-01 12:10:37'),
(39, 'What will be the molarity of the solution in Which 0.365 g of HCl gas is dissolved in 100 mL of solution', 153, '', NULL, 0, 0, '2020-02-01 12:11:48', '2020-02-01 12:11:48'),
(40, '46.suppose the element X andY combine to from two compounds xy2 and x3y2.when 0.1 mole of xy2 weight 10g and 0.05 mole x3y2 weight  9g ,the atomic weight X of  and Y', 157, '', NULL, 0, 0, '2020-02-01 12:16:11', '2020-02-01 12:16:11');

-- --------------------------------------------------------

--
-- Table structure for table `question_details`
--

CREATE TABLE `question_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `question_id` int(11) NOT NULL,
  `chapter_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `question_details`
--

INSERT INTO `question_details` (`id`, `question_id`, `chapter_id`, `created_at`, `updated_at`) VALUES
(1, 1, '4', '2020-01-31 09:50:06', '2020-01-31 09:50:06'),
(2, 2, '4', '2020-01-31 09:50:56', '2020-01-31 09:50:56'),
(3, 3, '18', '2020-02-01 06:30:35', '2020-02-01 06:30:35'),
(4, 4, '18', '2020-02-01 06:32:12', '2020-02-01 06:32:12'),
(5, 5, '18', '2020-02-01 06:34:39', '2020-02-01 06:34:39'),
(6, 6, '18', '2020-02-01 06:35:29', '2020-02-01 06:35:29'),
(7, 7, '', '2020-02-01 06:36:33', '2020-02-01 06:36:33'),
(8, 8, '18', '2020-02-01 06:37:51', '2020-02-01 06:37:51'),
(9, 9, '18', '2020-02-01 06:39:07', '2020-02-01 06:39:07'),
(10, 10, '18', '2020-02-01 06:40:12', '2020-02-01 06:40:12'),
(11, 11, '18', '2020-02-01 06:41:07', '2020-02-01 06:41:07'),
(12, 12, '18', '2020-02-01 06:42:36', '2020-02-01 06:42:36'),
(13, 13, '19', '2020-02-01 06:53:18', '2020-02-01 06:53:18'),
(14, 14, '19', '2020-02-01 06:56:38', '2020-02-01 06:56:38'),
(15, 15, '', '2020-02-01 06:58:13', '2020-02-01 06:58:13'),
(16, 16, '', '2020-02-01 06:59:38', '2020-02-01 06:59:38'),
(17, 17, '19', '2020-02-01 07:00:33', '2020-02-01 07:00:33'),
(18, 18, '19', '2020-02-01 07:01:51', '2020-02-01 07:01:51'),
(19, 19, '19', '2020-02-01 07:02:54', '2020-02-01 07:02:54'),
(20, 20, '', '2020-02-01 07:03:42', '2020-02-01 07:03:42'),
(21, 21, '19', '2020-02-01 07:04:53', '2020-02-01 07:04:53'),
(22, 22, '19', '2020-02-01 07:07:02', '2020-02-01 07:07:02'),
(23, 23, '20', '2020-02-01 11:13:05', '2020-02-01 11:13:05'),
(24, 24, '20', '2020-02-01 11:14:59', '2020-02-01 11:14:59'),
(25, 25, '', '2020-02-01 11:16:23', '2020-02-01 11:16:23'),
(26, 26, '', '2020-02-01 11:17:11', '2020-02-01 11:17:11'),
(27, 27, '20', '2020-02-01 11:18:26', '2020-02-01 11:18:26'),
(28, 28, '20', '2020-02-01 11:19:50', '2020-02-01 11:19:50'),
(29, 29, '20', '2020-02-01 11:23:43', '2020-02-01 11:23:43'),
(30, 30, '20', '2020-02-01 11:24:57', '2020-02-01 11:24:57'),
(31, 31, '1', '2020-02-01 11:28:24', '2020-02-01 11:28:24'),
(32, 32, '1', '2020-02-01 11:31:34', '2020-02-01 11:31:34'),
(33, 33, '1', '2020-02-01 11:32:34', '2020-02-01 11:32:34'),
(34, 34, '1', '2020-02-01 11:33:27', '2020-02-01 11:33:27'),
(35, 35, '1', '2020-02-01 12:06:10', '2020-02-01 12:06:10'),
(36, 36, '1', '2020-02-01 12:07:55', '2020-02-01 12:07:55'),
(37, 37, '1', '2020-02-01 12:09:46', '2020-02-01 12:09:46'),
(38, 38, '1', '2020-02-01 12:10:38', '2020-02-01 12:10:38'),
(39, 39, '1', '2020-02-01 12:11:48', '2020-02-01 12:11:48'),
(40, 40, '', '2020-02-01 12:16:11', '2020-02-01 12:16:11');

-- --------------------------------------------------------

--
-- Table structure for table `question_options`
--

CREATE TABLE `question_options` (
  `id` int(10) UNSIGNED NOT NULL,
  `question_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `options` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `opt_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `question_options`
--

INSERT INTO `question_options` (`id`, `question_id`, `options`, `opt_image`, `created_at`, `updated_at`) VALUES
(1, '1', 'answer`', NULL, '2020-01-31 09:50:05', '2020-01-31 09:50:05'),
(2, '1', 'option 1', NULL, '2020-01-31 09:50:05', '2020-01-31 09:50:05'),
(3, '1', 'option 2', NULL, '2020-01-31 09:50:05', '2020-01-31 09:50:05'),
(4, '1', 'option 3', NULL, '2020-01-31 09:50:06', '2020-01-31 09:50:06'),
(5, '2', 'answer 2', NULL, '2020-01-31 09:50:55', '2020-01-31 09:50:55'),
(6, '2', 'option 1', NULL, '2020-01-31 09:50:55', '2020-01-31 09:50:55'),
(7, '2', 'option 2', NULL, '2020-01-31 09:50:56', '2020-01-31 09:50:56'),
(8, '2', 'option 3', NULL, '2020-01-31 09:50:56', '2020-01-31 09:50:56'),
(9, '3', 'Vander walls\' forces', NULL, '2020-02-01 06:30:35', '2020-02-01 06:30:35'),
(10, '3', 'Covalent bonds', NULL, '2020-02-01 06:30:35', '2020-02-01 06:30:35'),
(11, '3', 'Coordinate bonds', NULL, '2020-02-01 06:30:35', '2020-02-01 06:30:35'),
(12, '3', 'Hydrogen bonds', NULL, '2020-02-01 06:30:35', '2020-02-01 06:30:35'),
(13, '4', 'Isotropic', NULL, '2020-02-01 06:32:11', '2020-02-01 06:32:11'),
(14, '4', 'Anisotropic', NULL, '2020-02-01 06:32:11', '2020-02-01 06:32:11'),
(15, '4', 'Hard', NULL, '2020-02-01 06:32:11', '2020-02-01 06:32:11'),
(16, '4', 'Dense', NULL, '2020-02-01 06:32:12', '2020-02-01 06:32:12'),
(17, '5', 'They are anisotropic', NULL, '2020-02-01 06:34:38', '2020-02-01 06:34:38'),
(18, '5', 'They melt over a range  of temperature', NULL, '2020-02-01 06:34:38', '2020-02-01 06:34:38'),
(19, '5', 'There is no ordered  arrangement of particles', NULL, '2020-02-01 06:34:38', '2020-02-01 06:34:38'),
(20, '5', 'They are rigid and  incopressible', NULL, '2020-02-01 06:34:39', '2020-02-01 06:34:39'),
(21, '6', 'Metalic', NULL, '2020-02-01 06:35:29', '2020-02-01 06:35:29'),
(22, '6', 'Ionic', NULL, '2020-02-01 06:35:29', '2020-02-01 06:35:29'),
(23, '6', 'Molecular', NULL, '2020-02-01 06:35:29', '2020-02-01 06:35:29'),
(24, '6', 'Covalent', NULL, '2020-02-01 06:35:29', '2020-02-01 06:35:29'),
(25, '7', 'They exhibit directional properties of the bond', NULL, '2020-02-01 06:36:32', '2020-02-01 06:36:32'),
(26, '7', 'They posses high  melting points and  boiling point', NULL, '2020-02-01 06:36:32', '2020-02-01 06:36:32'),
(27, '7', '`All are electrolytes', NULL, '2020-02-01 06:36:32', '2020-02-01 06:36:32'),
(28, '7', 'They exhibit the property of isomorphism', NULL, '2020-02-01 06:36:32', '2020-02-01 06:36:32'),
(29, '8', 'Has carbon atom arranged in large plates of ring of strong bound carbon atoms with weak interplate bonds', NULL, '2020-02-01 06:37:51', '2020-02-01 06:37:51'),
(30, '8', 'Is an allotropic form  of diomond', NULL, '2020-02-01 06:37:51', '2020-02-01 06:37:51'),
(31, '8', 'Has molecules of  variable molecular masses', NULL, '2020-02-01 06:37:51', '2020-02-01 06:37:51'),
(32, '8', 'Is a non-crystaline  substance', NULL, '2020-02-01 06:37:51', '2020-02-01 06:37:51'),
(33, '9', 'Covalent solids', NULL, '2020-02-01 06:39:07', '2020-02-01 06:39:07'),
(34, '9', 'Psudo solids', NULL, '2020-02-01 06:39:07', '2020-02-01 06:39:07'),
(35, '9', 'Ionic solid', NULL, '2020-02-01 06:39:07', '2020-02-01 06:39:07'),
(36, '9', 'Molecular solid', NULL, '2020-02-01 06:39:07', '2020-02-01 06:39:07'),
(37, '10', 'KI is soluble in benzene', NULL, '2020-02-01 06:40:12', '2020-02-01 06:40:12'),
(38, '10', 'CaO has high melting point', NULL, '2020-02-01 06:40:12', '2020-02-01 06:40:12'),
(39, '10', ' Lattice energy of CaO is much larger than that of KI', NULL, '2020-02-01 06:40:12', '2020-02-01 06:40:12'),
(40, '10', 'Both are electricsal insulators', NULL, '2020-02-01 06:40:12', '2020-02-01 06:40:12'),
(41, '11', 'Methane', NULL, '2020-02-01 06:41:06', '2020-02-01 06:41:06'),
(42, '11', 'Calcium fluoride', NULL, '2020-02-01 06:41:06', '2020-02-01 06:41:06'),
(43, '11', 'Rock salt', NULL, '2020-02-01 06:41:06', '2020-02-01 06:41:06'),
(44, '11', 'Silicon carbide', NULL, '2020-02-01 06:41:06', '2020-02-01 06:41:06'),
(45, '12', '4 Unit', NULL, '2020-02-01 06:42:35', '2020-02-01 06:42:35'),
(46, '12', '6 Units', NULL, '2020-02-01 06:42:35', '2020-02-01 06:42:35'),
(47, '12', '8 Units', NULL, '2020-02-01 06:42:35', '2020-02-01 06:42:35'),
(48, '12', '10 Units', NULL, '2020-02-01 06:42:35', '2020-02-01 06:42:35'),
(49, '13', 'solid in liquid solution', NULL, '2020-02-01 06:53:18', '2020-02-01 06:53:18'),
(50, '13', 'solid in solid solution', NULL, '2020-02-01 06:53:18', '2020-02-01 06:53:18'),
(51, '13', 'solid in gas solution', NULL, '2020-02-01 06:53:18', '2020-02-01 06:53:18'),
(52, '13', 'gas in solid solution', NULL, '2020-02-01 06:53:18', '2020-02-01 06:53:18'),
(53, '14', 'Volume depends on temperature and molarity involves volume', NULL, '2020-02-01 06:56:37', '2020-02-01 06:56:37'),
(54, '14', 'Molarity involves non-volatile solute while all other terms involve volatile solute', NULL, '2020-02-01 06:56:37', '2020-02-01 06:56:37'),
(55, '14', 'number of moles of solute changes with change in temperature', NULL, '2020-02-01 06:56:37', '2020-02-01 06:56:37'),
(56, '14', 'molarity is used for polar solvent only', NULL, '2020-02-01 06:56:37', '2020-02-01 06:56:37'),
(57, '15', 'Powdered sugar in hot water', NULL, '2020-02-01 06:58:13', '2020-02-01 06:58:13'),
(58, '15', 'sugar crystals in hot water', NULL, '2020-02-01 06:58:13', '2020-02-01 06:58:13'),
(59, '15', 'powdered sugar in cold water', NULL, '2020-02-01 06:58:13', '2020-02-01 06:58:13'),
(60, '15', 'powdered sugar in hot water', NULL, '2020-02-01 06:58:13', '2020-02-01 06:58:13'),
(61, '16', 'Water + Nitric acid', NULL, '2020-02-01 06:59:38', '2020-02-01 06:59:38'),
(62, '16', 'Acetone + Aniline', NULL, '2020-02-01 06:59:38', '2020-02-01 06:59:38'),
(63, '16', 'Acetone + Ethanol', NULL, '2020-02-01 06:59:38', '2020-02-01 06:59:38'),
(64, '16', 'Chloroform + Benzene', NULL, '2020-02-01 06:59:38', '2020-02-01 06:59:38'),
(65, '17', 'A-B interactions are weaker than those between A-A or B-B.', NULL, '2020-02-01 07:00:32', '2020-02-01 07:00:32'),
(66, '17', 'A-B interactions are  stronger than those  between A-A or B-B', NULL, '2020-02-01 07:00:32', '2020-02-01 07:00:32'),
(67, '17', 'A-B interactions are equal to A-A and B-B interactions', NULL, '2020-02-01 07:00:32', '2020-02-01 07:00:32'),
(68, '17', 'vapour pressure of solution decreases because less number of molecules of only one of the liqyuids escape from the solution', NULL, '2020-02-01 07:00:32', '2020-02-01 07:00:32'),
(69, '18', 'hypertonic solution', NULL, '2020-02-01 07:01:50', '2020-02-01 07:01:50'),
(70, '18', 'hypotonic solution', NULL, '2020-02-01 07:01:50', '2020-02-01 07:01:50'),
(71, '18', 'isotonic solution', NULL, '2020-02-01 07:01:50', '2020-02-01 07:01:50'),
(72, '18', 'pure water', NULL, '2020-02-01 07:01:50', '2020-02-01 07:01:50'),
(73, '19', 'Osmotic pressure', NULL, '2020-02-01 07:02:54', '2020-02-01 07:02:54'),
(74, '19', 'Lowering of vapour pressure', NULL, '2020-02-01 07:02:54', '2020-02-01 07:02:54'),
(75, '19', 'boiling point', NULL, '2020-02-01 07:02:54', '2020-02-01 07:02:54'),
(76, '19', 'change entropy', NULL, '2020-02-01 07:02:54', '2020-02-01 07:02:54'),
(77, '20', 'Decreases', NULL, '2020-02-01 07:03:41', '2020-02-01 07:03:41'),
(78, '20', 'Increases', NULL, '2020-02-01 07:03:41', '2020-02-01 07:03:41'),
(79, '20', 'Remains unchanged', NULL, '2020-02-01 07:03:41', '2020-02-01 07:03:41'),
(80, '20', 'First decrease and then increase', NULL, '2020-02-01 07:03:42', '2020-02-01 07:03:42'),
(81, '21', 'The vapour will contain a  higher percentage of  benzene', NULL, '2020-02-01 07:04:52', '2020-02-01 07:04:52'),
(82, '21', 'The vapour will contain  equal amount of benzene  and toluene', NULL, '2020-02-01 07:04:52', '2020-02-01 07:04:52'),
(83, '21', 'Not enough information is given to make a prediction', NULL, '2020-02-01 07:04:53', '2020-02-01 07:04:53'),
(84, '21', 'The vapour will contain a higher percentage of toluence', NULL, '2020-02-01 07:04:53', '2020-02-01 07:04:53'),
(85, '22', '0.0177', NULL, '2020-02-01 07:07:02', '2020-02-01 07:07:02'),
(86, '22', '1.77', NULL, '2020-02-01 07:07:02', '2020-02-01 07:07:02'),
(87, '22', '0.0354', NULL, '2020-02-01 07:07:02', '2020-02-01 07:07:02'),
(88, '22', '0.177', NULL, '2020-02-01 07:07:02', '2020-02-01 07:07:02'),
(89, '23', 'the chemical energy liberated during the redox reaction is converted to electrical energy', NULL, '2020-02-01 11:13:04', '2020-02-01 11:13:04'),
(90, '23', 'the electrical energy of the  cell is converted to  chemical energy ', NULL, '2020-02-01 11:13:05', '2020-02-01 11:13:05'),
(91, '23', 'the energy of the cell is utilised in conduction of the redox reaction', NULL, '2020-02-01 11:13:05', '2020-02-01 11:13:05'),
(92, '23', 'the potential energy of the cell is converted into electrical energy', NULL, '2020-02-01 11:13:05', '2020-02-01 11:13:05'),
(93, '24', 'Pt', NULL, '2020-02-01 11:14:59', '2020-02-01 11:14:59'),
(94, '24', 'Pb', NULL, '2020-02-01 11:14:59', '2020-02-01 11:14:59'),
(95, '24', 'Pd', NULL, '2020-02-01 11:14:59', '2020-02-01 11:14:59'),
(96, '24', 'Ni', NULL, '2020-02-01 11:14:59', '2020-02-01 11:14:59'),
(97, '25', 'Standard hydrogen  electrode is used as a  secondary reference electrode', NULL, '2020-02-01 11:16:23', '2020-02-01 11:16:23'),
(98, '25', 'The electrode potential is  called standard electrode  potential if the electrode  is set up in ! M solution  at 298 K', NULL, '2020-02-01 11:16:23', '2020-02-01 11:16:23'),
(99, '25', 'The electrode potential of Pt, H2 | H+ is taken as zero', NULL, '2020-02-01 11:16:23', '2020-02-01 11:16:23'),
(100, '25', 'Greater the oxidation potential of a metal, more active is the metal', NULL, '2020-02-01 11:16:23', '2020-02-01 11:16:23'),
(101, '26', 'anode to cathode', NULL, '2020-02-01 11:17:11', '2020-02-01 11:17:11'),
(102, '26', 'cathod to anode', NULL, '2020-02-01 11:17:11', '2020-02-01 11:17:11'),
(103, '26', 'anode to solution', NULL, '2020-02-01 11:17:11', '2020-02-01 11:17:11'),
(104, '26', 'solution to cathode', NULL, '2020-02-01 11:17:11', '2020-02-01 11:17:11'),
(105, '27', 'For cell reaction to occur spontaneously, the EMF of the cell should be negative', NULL, '2020-02-01 11:18:25', '2020-02-01 11:18:25'),
(106, '27', 'The limiting equivalent conductance for weak electrolytes can be computed with the help of Kohlraushs\'s Law', NULL, '2020-02-01 11:18:25', '2020-02-01 11:18:25'),
(107, '27', 'EMF of a cell is the difference in the reduction potentials of cathode and anode', NULL, '2020-02-01 11:18:25', '2020-02-01 11:18:25'),
(108, '27', 'Fluorine is the strongest oxiding agent as its reducing potential is very high', NULL, '2020-02-01 11:18:26', '2020-02-01 11:18:26'),
(109, '28', 'From cathode to anode through internal supply', NULL, '2020-02-01 11:19:50', '2020-02-01 11:19:50'),
(110, '28', 'From cathode to anode in the solution', NULL, '2020-02-01 11:19:50', '2020-02-01 11:19:50'),
(111, '28', 'From cathode to anode through external supply', NULL, '2020-02-01 11:19:50', '2020-02-01 11:19:50'),
(112, '28', 'from anode to cathode through internal supply', NULL, '2020-02-01 11:19:50', '2020-02-01 11:19:50'),
(113, '29', '110 minutes', NULL, '2020-02-01 11:23:42', '2020-02-01 11:23:42'),
(114, '29', '55 minutes', NULL, '2020-02-01 11:23:42', '2020-02-01 11:23:42'),
(115, '29', '220 minutes', NULL, '2020-02-01 11:23:43', '2020-02-01 11:23:43'),
(116, '29', '330 minutes', NULL, '2020-02-01 11:23:43', '2020-02-01 11:23:43'),
(117, '30', 'Zinc has higher negative electrode potential than iron', NULL, '2020-02-01 11:24:56', '2020-02-01 11:24:56'),
(118, '30', 'Zinc is lighter than iron', NULL, '2020-02-01 11:24:56', '2020-02-01 11:24:56'),
(119, '30', 'Zinc has lower melting point than iron', NULL, '2020-02-01 11:24:56', '2020-02-01 11:24:56'),
(120, '30', 'Zinc has lower negative electrode potential than iron', NULL, '2020-02-01 11:24:56', '2020-02-01 11:24:56'),
(121, '31', 'Y is more accurate than x', NULL, '2020-02-01 11:28:23', '2020-02-01 11:28:23'),
(122, '31', 'Both are equally  accurate', NULL, '2020-02-01 11:28:23', '2020-02-01 11:28:23'),
(123, '31', 'X is more accurate than Y', NULL, '2020-02-01 11:28:24', '2020-02-01 11:28:24'),
(124, '31', 'Both are inaccurate scientifically.', NULL, '2020-02-01 11:28:24', '2020-02-01 11:28:24'),
(125, '32', '34.2,0.0460,10.4', NULL, '2020-02-01 11:31:33', '2020-02-01 11:31:33'),
(126, '32', '34.3,0.0461,10.4', NULL, '2020-02-01 11:31:33', '2020-02-01 11:31:33'),
(127, '32', '34.20,0.460,10.40', NULL, '2020-02-01 11:31:33', '2020-02-01 11:31:33'),
(128, '32', '34.21,4.597,1.04', NULL, '2020-02-01 11:31:33', '2020-02-01 11:31:33'),
(129, '33', 'Zero between two non-zero digits are not significant.', NULL, '2020-02-01 11:32:34', '2020-02-01 11:32:34'),
(130, '33', 'Zero preceding to first non-zero digit are not significant. ', NULL, '2020-02-01 11:32:34', '2020-02-01 11:32:34'),
(131, '33', 'Zero at the end or right of the number are significant if they are on the eright side of decimal point.', NULL, '2020-02-01 11:32:34', '2020-02-01 11:32:34'),
(132, '33', 'All non zero digit are significant', NULL, '2020-02-01 11:32:34', '2020-02-01 11:32:34'),
(133, '34', 'Law of multiple proportions', NULL, '2020-02-01 11:33:26', '2020-02-01 11:33:26'),
(134, '34', 'Law of conservation of mass', NULL, '2020-02-01 11:33:27', '2020-02-01 11:33:27'),
(135, '34', 'Law of definite proportions', NULL, '2020-02-01 11:33:27', '2020-02-01 11:33:27'),
(136, '34', 'Avogadro\'s Law', NULL, '2020-02-01 11:33:27', '2020-02-01 11:33:27'),
(137, '35', 'carbon forms two oxides namely CO2 and CO, Where masses of oxygen which combine with  fixed mass of carbon are in the simple ratio 2:1.', NULL, '2020-02-01 12:06:10', '2020-02-01 12:06:10'),
(138, '35', 'sample of carbon dioxide take from  any source will always have carbon  and oxygen in the 1:2', NULL, '2020-02-01 12:06:10', '2020-02-01 12:06:10'),
(139, '35', 'when magnesium burns in oxygen , the amount of magnesium take for  the amount of magnesium in  magnesium oxide formed', NULL, '2020-02-01 12:06:10', '2020-02-01 12:06:10'),
(140, '35', 'At constant temperature and pressure ,200 ML of hydrogen will  combine with 100 Ml of oxygen to produce 200 mL of water vapour', NULL, '2020-02-01 12:06:10', '2020-02-01 12:06:10'),
(141, '36', '20', NULL, '2020-02-01 12:07:55', '2020-02-01 12:07:55'),
(142, '36', '30', NULL, '2020-02-01 12:07:55', '2020-02-01 12:07:55'),
(143, '36', '40', NULL, '2020-02-01 12:07:55', '2020-02-01 12:07:55'),
(144, '36', '25', NULL, '2020-02-01 12:07:55', '2020-02-01 12:07:55'),
(145, '37', '15.999 u', NULL, '2020-02-01 12:09:46', '2020-02-01 12:09:46'),
(146, '37', '16.999u', NULL, '2020-02-01 12:09:46', '2020-02-01 12:09:46'),
(147, '37', '17.999u', NULL, '2020-02-01 12:09:46', '2020-02-01 12:09:46'),
(148, '37', '18.999u', NULL, '2020-02-01 12:09:46', '2020-02-01 12:09:46'),
(149, '38', '990 mL', NULL, '2020-02-01 12:10:37', '2020-02-01 12:10:37'),
(150, '38', '1000mL', NULL, '2020-02-01 12:10:37', '2020-02-01 12:10:37'),
(151, '38', '1010mL', NULL, '2020-02-01 12:10:38', '2020-02-01 12:10:38'),
(152, '38', '100mL', NULL, '2020-02-01 12:10:38', '2020-02-01 12:10:38'),
(153, '39', '0.1 M', NULL, '2020-02-01 12:11:48', '2020-02-01 12:11:48'),
(154, '39', '2 M', NULL, '2020-02-01 12:11:48', '2020-02-01 12:11:48'),
(155, '39', '0.2 M ', NULL, '2020-02-01 12:11:48', '2020-02-01 12:11:48'),
(156, '39', '0.1 M', NULL, '2020-02-01 12:11:48', '2020-02-01 12:11:48'),
(157, '40', '40,30', NULL, '2020-02-01 12:16:11', '2020-02-01 12:16:11'),
(158, '40', '60,40', NULL, '2020-02-01 12:16:11', '2020-02-01 12:16:11'),
(159, '40', '20,30', NULL, '2020-02-01 12:16:11', '2020-02-01 12:16:11'),
(160, '40', '30,20', NULL, '2020-02-01 12:16:11', '2020-02-01 12:16:11');

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE `students` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `class` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `place` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `dob` date NOT NULL,
  `yoa` int(11) NOT NULL,
  `center` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `school` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `parent_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `rws` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `parent_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `parent_phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) DEFAULT NULL,
  `added_by` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`id`, `name`, `email`, `class`, `place`, `address`, `phone`, `dob`, `yoa`, `center`, `school`, `parent_name`, `rws`, `parent_email`, `parent_phone`, `password`, `status`, `added_by`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Student', 'admin@saltu.in', 'Plus Two', '', 'Kochukalakattu House,Muthukulam,Choolatheruve PO,Allapuzha', '7012525741', '0000-00-00', 0, '', '', '', '', '', '', '$2y$10$gnp2q6gW4gwkd.Rxn3NWIu/6ZwuXgKec4bT7JRIER1Kf6Q6eFvfBe', 1, NULL, 'fjb52a6V30sqIx5oDxTfxrRTHGtykBf8qjWczMB7xiDUpUyF7uWP3qKxw4fU', '2019-09-16 14:23:33', '2020-02-01 06:23:08'),
(2, 'Prithviraj.K.R', 'prithviraj@saltu.in', 'plus two', 'muthukulam', 'muthukulam', '7012525742', '1996-08-15', 0, 'creative suite', 'KVSHSS', 'Rajendran', 'son', 'rajendran@gmail.com', '9495307770', '$2y$10$c/gl/kEZLO.a47AVoJY8ou6FZYrPD1zRWlx1gL6O0M1b9r6lHPkCq', 0, '', 'DrZOnQTAMCa8LkqGi116n0vdZHVqDJZ1EFOT0wzBrpvgIjy2Fos9jDV49uWK', '2019-09-20 08:33:57', '2019-09-24 11:51:32'),
(3, 'Akash Sanjeev', 'akashsanjeev@saltu.in', 'plus two', 'nangiarkulangara', 'Akkubhavan, Harripad, pallipad po kerala', '9446854520', '1995-10-12', 0, 'saltu', 'Amritha', 'Sanjeev Shankar', 'son', 'SanjeevShankar@gmail.com', '9495305555', '$2y$10$GSf/PbycgQnvLh/e6Q25h.sJtjGzLyKQih8M2ea.F1JR08rxnGAdu', 0, 'center', NULL, '2019-11-02 07:43:45', '2019-11-02 07:43:45'),
(5, 'noufal', 'noufal@saltu.in', 'plus two', 'Haripad', 'Majeed house', '7012525743', '1996-01-15', 2019, 'student', 'Holy trinity', 'Majeedh', 'son', 'majeed@gmail.com', '9495305555', '$2y$10$NLIhgtpdG4jjB4lE8AMQ9OBpweqaG2AqkDOyMK.37dYj4.qGP8mGW', 0, 'student', NULL, '2019-12-17 06:37:25', '2019-12-17 06:37:25');

-- --------------------------------------------------------

--
-- Table structure for table `students_approvals`
--

CREATE TABLE `students_approvals` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `added_from` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `school` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `center` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dob` date NOT NULL,
  `yoa` int(11) NOT NULL,
  `place` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `class` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `parent_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `rws` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `parent_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent_phone` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `student_exam_paids`
--

CREATE TABLE `student_exam_paids` (
  `id` int(10) UNSIGNED NOT NULL,
  `package_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `student_payments`
--

CREATE TABLE `student_payments` (
  `id` int(10) UNSIGNED NOT NULL,
  `amount` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `package_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `student_payments`
--

INSERT INTO `student_payments` (`id`, `amount`, `student_id`, `package_id`, `date`, `status`, `created_at`, `updated_at`) VALUES
(1, 500, 1, 1, '2020-01-27', 0, '2020-01-27 09:42:00', '2020-01-27 09:42:00');

-- --------------------------------------------------------

--
-- Table structure for table `subjects`
--

CREATE TABLE `subjects` (
  `id` int(10) UNSIGNED NOT NULL,
  `class_id` int(11) NOT NULL,
  `examname_id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `added_by` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `subjects`
--

INSERT INTO `subjects` (`id`, `class_id`, `examname_id`, `name`, `description`, `added_by`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, '1,2', 'Chemistry', 'dummy', 1, 0, '2020-01-27 07:44:19', '2020-01-27 07:44:19'),
(2, 1, '1,2', 'Physics', '', 1, 0, '2020-01-28 07:19:39', '2020-01-28 07:19:39'),
(3, 1, '1', 'Maths', '', 1, 0, '2020-01-28 07:20:01', '2020-01-28 07:20:01'),
(4, 1, '2', 'Biology', '', 1, 0, '2020-01-28 07:20:24', '2020-01-28 07:20:24'),
(5, 2, '4', 'Biology', '', 1, 0, '2020-01-28 07:21:18', '2020-01-28 07:21:18'),
(6, 2, '3', 'Maths', '', 1, 0, '2020-01-28 07:21:32', '2020-01-28 07:21:32'),
(7, 2, '4,3', 'Chemistry', '', 1, 0, '2020-01-28 07:21:55', '2020-01-28 07:21:55'),
(8, 2, '4,3', 'Physics', '', 1, 0, '2020-01-28 07:22:22', '2020-01-28 07:22:22');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admins_email_unique` (`email`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `centers`
--
ALTER TABLE `centers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `centers_email_unique` (`email`);

--
-- Indexes for table `chapters`
--
ALTER TABLE `chapters`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `classes`
--
ALTER TABLE `classes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `exam_fees`
--
ALTER TABLE `exam_fees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `exam_names`
--
ALTER TABLE `exam_names`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `exam_type_packages`
--
ALTER TABLE `exam_type_packages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `question_details`
--
ALTER TABLE `question_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `question_options`
--
ALTER TABLE `question_options`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `students_email_unique` (`email`);

--
-- Indexes for table `students_approvals`
--
ALTER TABLE `students_approvals`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `students_approvals_email_unique` (`email`);

--
-- Indexes for table `student_exam_paids`
--
ALTER TABLE `student_exam_paids`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student_payments`
--
ALTER TABLE `student_payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subjects`
--
ALTER TABLE `subjects`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `centers`
--
ALTER TABLE `centers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `chapters`
--
ALTER TABLE `chapters`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `classes`
--
ALTER TABLE `classes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `exam_fees`
--
ALTER TABLE `exam_fees`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `exam_names`
--
ALTER TABLE `exam_names`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `exam_type_packages`
--
ALTER TABLE `exam_type_packages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `question_details`
--
ALTER TABLE `question_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `question_options`
--
ALTER TABLE `question_options`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=161;

--
-- AUTO_INCREMENT for table `students`
--
ALTER TABLE `students`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `students_approvals`
--
ALTER TABLE `students_approvals`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `student_exam_paids`
--
ALTER TABLE `student_exam_paids`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `student_payments`
--
ALTER TABLE `student_payments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `subjects`
--
ALTER TABLE `subjects`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
