-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 24, 2020 at 12:33 PM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.1.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `esame`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin@saltu.in', '$2y$10$MSQzhq41AzktB03bAZWxNeOPvYp0YB2Zyu99fds78FSbxRmpqEy9q', 'nEdMP846HfxepxEhygBqJ0MpHfQ99hTQKDSfZzBVeb25CMhlze0o3stD314C', '2019-11-12 10:55:48', '2020-04-15 08:01:19');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `class_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `added_by` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `class_id`, `name`, `description`, `image`, `added_by`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 'Engineering', 'The branch of science and technology concerned with the design, building, and use of engines, machines, and structures. With XI Syllabus', '', 1, 0, '2020-01-22 05:55:39', '2020-01-22 05:55:39'),
(2, 1, 'Medical', 'Biomedical engineering or medical engineering is the application of engineering principles and design concepts to medicine and biology for healthcare purposes. On XI Syllabus\r\n', '', 1, 0, '2020-01-22 05:56:24', '2020-01-22 05:56:24'),
(3, 2, 'Medical ', 'Biomedical engineering or medical engineering is the application of engineering principles and design concepts to medicine and biology for healthcare purposes. On XII Syllabus', '', 1, 0, '2020-01-22 05:57:31', '2020-01-22 05:57:31'),
(4, 2, 'Engineering', 'The branch of science and technology concerned with the design, building, and use of engines, machines, and structures. With XII Syllabus', '', 1, 0, '2020-01-22 05:58:57', '2020-01-22 05:58:57');

-- --------------------------------------------------------

--
-- Table structure for table `centers`
--

CREATE TABLE `centers` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `registration_no` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `place` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(10) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `centers`
--

INSERT INTO `centers` (`id`, `name`, `registration_no`, `email`, `place`, `address`, `phone`, `password`, `status`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'saltu', '1234', 'admin@saltu.in', 'Nangiarkulangara', 'Karthikapally,Nagyarkulangara,Allapuzha', '7012525741', '$2y$10$gsS8o.MN/rHo27GsnqU4/.iq1yNg1cgHW/Gd7td73r19SamPgU/ua', 0, 'L7vg9dc7RHgYTuT5k36w0Hi2OQ0WxrLJhvODaokehnTMZSVFlpIzW31KO1xY', '2019-09-16 13:54:42', '2020-04-15 08:15:48'),
(5, 'testing', '101', 'nithinpvarghese12@gmail.com', 'teat', 'dfbg', '1234567890', '$2y$10$dfrKnBvEYcUm5afb6Wcl/e3PdSx7cKsAxFxPWvm.hc73XS0YZX0fC', 0, NULL, '2019-11-14 19:25:55', '2019-11-14 19:25:55');

-- --------------------------------------------------------

--
-- Table structure for table `chapters`
--

CREATE TABLE `chapters` (
  `id` int(10) UNSIGNED NOT NULL,
  `subject_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `added_by` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `chapters`
--

INSERT INTO `chapters` (`id`, `subject_id`, `name`, `description`, `added_by`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 'Some Basic Concepts of Chemistry', 'dummy', 1, 0, '2020-01-24 07:52:43', '2020-01-24 07:52:43'),
(2, 1, 'Structure of Atom', '', 1, 0, '2020-01-28 07:23:53', '2020-01-28 07:23:53'),
(3, 1, 'Classification of Elements and Periodicity in Properties', '', 1, 0, '2020-01-28 07:24:10', '2020-01-28 07:24:10'),
(4, 1, 'Chemical Bonding and Molecular Structure', '', 1, 0, '2020-01-28 07:24:34', '2020-01-28 07:24:34'),
(5, 2, 'Physical World', '', 1, 0, '2020-01-28 08:04:18', '2020-01-28 08:04:18'),
(7, 2, 'Units and Measurement', '', 1, 0, '2020-01-28 08:04:34', '2020-01-28 08:04:34'),
(8, 2, 'Motion in A Straight Line', '', 1, 0, '2020-01-28 08:04:47', '2020-01-28 08:04:47'),
(9, 2, 'Motion in A Plane', '', 1, 0, '2020-01-28 08:05:01', '2020-01-28 08:05:01'),
(10, 3, 'Sets', '', 1, 0, '2020-01-28 08:06:30', '2020-01-28 08:06:30'),
(11, 3, 'Relations and Functions', '', 1, 0, '2020-01-28 08:06:47', '2020-01-28 08:06:47'),
(12, 3, 'Trigonometric Functions', '', 1, 0, '2020-01-28 08:07:02', '2020-01-28 08:07:02'),
(13, 3, 'Principle of Mathematical Induction', '', 1, 0, '2020-01-28 08:07:24', '2020-01-28 08:07:24'),
(14, 4, 'The Living World', '', 1, 0, '2020-01-28 08:07:51', '2020-01-28 08:07:51'),
(15, 4, 'Biological Classification', '', 1, 0, '2020-01-28 08:08:04', '2020-01-28 08:08:04'),
(16, 4, 'Plant Kingdom', '', 1, 0, '2020-01-28 08:08:18', '2020-01-28 08:08:18'),
(17, 4, 'Animal Kingdom', '', 1, 0, '2020-01-28 08:08:31', '2020-01-28 08:08:31'),
(18, 7, 'The Solid State', '', 1, 0, '2020-02-01 06:28:41', '2020-02-01 06:28:41'),
(19, 7, 'Solutions', '', 1, 0, '2020-02-01 06:43:14', '2020-02-01 06:43:14'),
(20, 7, 'ElectroChemistry', '', 1, 0, '2020-02-01 11:11:20', '2020-02-01 11:11:20');

-- --------------------------------------------------------

--
-- Table structure for table `classes`
--

CREATE TABLE `classes` (
  `id` int(10) UNSIGNED NOT NULL,
  `class` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `added_by` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `classes`
--

INSERT INTO `classes` (`id`, `class`, `added_by`, `status`, `created_at`, `updated_at`) VALUES
(1, 'XI', 1, 0, '2020-01-13 06:46:47', '2020-01-13 06:46:47'),
(2, 'XII', 1, 0, '2020-01-13 06:47:15', '2020-01-13 06:47:15');

-- --------------------------------------------------------

--
-- Table structure for table `exam_fees`
--

CREATE TABLE `exam_fees` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ex_package_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `student_amount` int(11) NOT NULL,
  `center_amount` int(11) NOT NULL,
  `added_by` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `exam_fees`
--

INSERT INTO `exam_fees` (`id`, `title`, `ex_package_id`, `student_amount`, `center_amount`, `added_by`, `status`, `created_at`, `updated_at`) VALUES
(1, 'plus one jee', '1', 500, 300, 1, 0, '2020-01-24 08:19:57', '2020-01-24 08:19:57');

-- --------------------------------------------------------

--
-- Table structure for table `exam_names`
--

CREATE TABLE `exam_names` (
  `id` int(10) UNSIGNED NOT NULL,
  `category_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `added_by` int(11) NOT NULL,
  `subject_ids` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `exam_names`
--

INSERT INTO `exam_names` (`id`, `category_id`, `name`, `description`, `image`, `added_by`, `subject_ids`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 'JEE', 'Joint Entrance Examination – Main, formerly All India Engineering Entrance Examination, is an examination organised by the National Testing Agency in India. ', 'jee.jpg', 1, ',1,2,3', 0, '2020-01-22 06:07:54', '2020-01-28 07:20:01'),
(2, 2, 'NEET', 'The National Eligibility cum Entrance Test-Under Graduate, succeeded from All India Pre-Medical Test is an entrance examination in India for students who wish to study undergraduate medical courses and dental courses in government or private medical colle', 'neet.jpg', 1, ',1,2,4', 0, '2020-01-22 06:08:10', '2020-01-28 07:20:24'),
(3, 4, 'JEE', 'Joint Entrance Examination – Main, formerly All India Engineering Entrance Examination, is an examination organised by the National Testing Agency in India. ', 'jee.jpg', 1, ',6,7,8', 0, '2020-01-22 06:08:27', '2020-01-28 07:22:23'),
(4, 3, 'NEET', 'The National Eligibility cum Entrance Test-Under Graduate, succeeded from All India Pre-Medical Test is an entrance examination in India for students who wish to study undergraduate medical courses and dental courses in government or private medical colle', 'neet.jpg', 1, ',5,7,8', 0, '2020-01-22 06:08:40', '2020-01-28 07:22:23');

-- --------------------------------------------------------

--
-- Table structure for table `exam_results`
--

CREATE TABLE `exam_results` (
  `id` int(10) UNSIGNED NOT NULL,
  `exam_id` int(11) NOT NULL,
  `test_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `chapter_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `stud_id` int(11) DEFAULT NULL,
  `q_id` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `op_id` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `result` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `p_id` int(11) DEFAULT NULL,
  `t_attend` int(11) DEFAULT NULL,
  `t_true` int(11) DEFAULT NULL,
  `t_question` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `exam_results`
--

INSERT INTO `exam_results` (`id`, `exam_id`, `test_type`, `chapter_id`, `date`, `stud_id`, `q_id`, `op_id`, `result`, `p_id`, `t_attend`, `t_true`, `t_question`, `created_at`, `updated_at`) VALUES
(1, 1, '0', '', '2020-03-06', 1, '1,9,7,38,2,3,16,13,18,4,34,14,23,36,10,31,20,21,5,19,24,32,25,6,12,22,17,8,33,35', '1,33,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0', '0', NULL, NULL, NULL, NULL, '2020-03-06 06:39:55', '2020-03-06 06:39:55'),
(2, 1, '0', '', '2020-03-06', 1, '17,22,24,37,9,35,19,5,8,31,7,21,18,3,4,34,33,10,39,20,15,2,6,32,16,14,36,38,11,25', '0', '0', NULL, NULL, NULL, NULL, '2020-03-06 07:14:32', '2020-03-06 07:14:32'),
(3, 1, '0', '', '2020-03-06', 1, '17,22,24,37,9,35,19,5,8,31,7,21,18,3,4,34,33,10,39,20,15,2,6,32,16,14,36,38,11,25', '0', '0', NULL, NULL, NULL, NULL, '2020-03-06 07:15:09', '2020-03-06 07:15:09'),
(4, 1, '0', '', '2020-03-06', 1, '39,15,24,3,25,38,6,37,11,21,7,9,1,10,4,19,35,23,18,14,16,33,5,13,31,22,2,34,20,36', '155,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0', '0', NULL, NULL, NULL, 30, '2020-03-06 07:18:18', '2020-03-06 07:18:18'),
(5, 1, '0', '', '2020-03-06', 1, '39,15,24,3,25,38,6,37,11,21,7,9,1,10,4,19,35,23,18,14,16,33,5,13,31,22,2,34,20,36', '0', '0', NULL, NULL, NULL, NULL, '2020-03-06 07:21:25', '2020-03-06 07:21:25'),
(6, 1, '0', '', '2020-03-06', 1, '16,13,14,8,12,35,33,15,2,37,34,5,21,10,4,39,36,31,18,9,38,19,25,24,7,32,6,20,3,11', '62,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0', '0', NULL, NULL, NULL, 30, '2020-03-06 07:21:54', '2020-03-06 07:21:54'),
(7, 1, '0', '', '2020-03-07', 1, '37,16,36,19,33,25,9,35,1,6,17,13,15,18,7,14,23,24,3,22,32,39,8,21,2,11,10,12,4,31', '146,62,141,73,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0', '-1,-1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0', NULL, 4, 2, 30, '2020-03-07 07:05:46', '2020-03-07 07:05:46'),
(8, 1, 'Cumulative_Test', '1,2,3,4', '2020-03-09', 1, '14,3,23,39,35,18,24,15,25,19,37,34,20,22,17,9,10,12,38,5,8,13,16,33,6,21,7,4,2,1', '53,10,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0', '1,-1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0', NULL, 2, 1, 30, '2020-03-09 07:08:05', '2020-03-09 07:08:05'),
(9, 1, 'Cumulative_Test', '1,2,3,4', '2020-03-11', 1, '21,8,25,36,39,20,15,5,6,1,22,18,33,23,38,3,4,12,14,11,9,17,37,16,2,13,19,7,34,32', '0', '0', NULL, 0, 0, 30, '2020-03-11 08:39:07', '2020-03-11 08:39:07'),
(11, 1, 'Cumulative_Test', '1,2,3,4', '2020-03-12', 1, '16,7,38,21,12,11,9,22,3,10,33,15,17,23,37,35,8,19,2,5,13,34,14,39,25,32,4,24,20,36', '64,28,152,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0', '-1,-1,-1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0', NULL, 3, 0, 30, '2020-03-12 18:06:04', '2020-03-12 18:06:04'),
(12, 1, 'Cumulative_Test', '1,2,3,4', '2020-03-17', 1, '9,38,15,34,20,2,22,19,5,12,35,21,1,17,33,31,18,3,32,39,8,13,4,16,37,23,7,14,25,11', '34,151,57,136,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0', '-1,-1,1,-1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0', NULL, 4, 1, 30, '2020-03-17 11:43:19', '2020-03-17 11:43:19'),
(13, 1, 'Cumulative_Test', '1,2,3,4', '2020-03-18', 1, '12,16,23,39,25,17,22,34,20,5,3,8,1,9,19,21,36,37,10,38,13,18,11,7,33,35,14,4,24,32', '46,61,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0', '-1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0', 1, 2, 1, 30, '2020-03-18 10:59:25', '2020-03-18 10:59:25'),
(14, 1, 'Cumulative_Test', '1,2,3,4', '2020-03-19', 1, '2,38,25,24,36,17,32,4,31,10,33,15,39,13,9,20,21,12,1,8,6,7,11,19,3,5,18,35,14,22', '5,151,97,94,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0', '1,-1,1,-1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0', 1, 4, 2, 30, '2020-03-19 13:10:51', '2020-03-19 13:10:51'),
(15, 1, 'Cumulative_Test', '1,2,3,4', '2020-03-20', 1, '1,10,22,35,18,12,37,13,5,31,3,20,25,17,14,19,8,21,34,33,24,11,4,15,23,16,38,32,2,39', '1,38,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0', '1,-1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0', 1, 2, 1, 30, '2020-03-20 07:41:20', '2020-03-20 07:41:20'),
(16, 1, 'Cumulative_Test', '1,2,3,4', '2020-03-23', 1, '13,35,15,5,25,31,23,6,39,34,4,14,20,7,10,12,21,8,16,36,19,38,17,18,37,9,24,3,32,22', '49,138,60,18,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0', '1,-1,-1,-1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0', 1, NULL, NULL, NULL, '2020-03-23 12:40:56', '2020-03-23 12:40:56'),
(17, 1, 'Cumulative_Test', '1,2,3,4', '2020-03-23', 1, '5,7,4,33,17,6,38,31,39,37,16,25,22,1,2,14,19,12,35,13,36,23,21,3,9,18,15,11,8,24', '17,27,15,132,66,21,152,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0', '1,-1,-1,-1,-1,1,-1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0', 1, 7, 2, 30, '2020-03-23 12:43:11', '2020-03-23 12:43:11'),
(29, 1, 'Cumulative_Test', '1,2,3,4', '2020-03-24', 1, '9,5,25,13,1,15,20,12,22,35,10,39,17,24,6,4,2,3,21,19,11,34,14,33,37,16,23,8,7,38', '33,18,97,49,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0', '0', 1, NULL, NULL, NULL, '2020-03-24 13:21:07', '2020-03-24 13:21:07'),
(30, 1, 'Cumulative_Test', '1,2,3,4', '2020-03-24', 1, '8,18,32,7,23,16,5,9,12,15,25,6,34,1,38,4,39,33,14,13,3,20,19,35,17,22,2,36,24,21', '30,69,125,28,90,62,18,33,45,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0', '0', 1, NULL, NULL, NULL, '2020-03-24 13:25:11', '2020-03-24 13:25:11'),
(31, 1, 'Cumulative_Test', '1,2,3,4', '2020-04-02', 8, '11,24,15,39,38,37,33,13,25,8,20,18,17,22,19,6,9,21,5,34,35,36,14,1,7,32,16,23,4,2', '43,93,60,153,152,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0', '-1,1,-1,1,-1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0', 1, 5, 2, 30, '2020-04-02 18:28:50', '2020-04-02 18:28:50');

-- --------------------------------------------------------

--
-- Table structure for table `exam_type_packages`
--

CREATE TABLE `exam_type_packages` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `class_ids` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `ex_type_ids` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `added_by` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `exam_type_packages`
--

INSERT INTO `exam_type_packages` (`id`, `title`, `class_ids`, `ex_type_ids`, `added_by`, `status`, `created_at`, `updated_at`) VALUES
(1, 'plus one jee', '1', '1', 1, 0, '2020-01-24 08:16:55', '2020-01-24 08:16:55'),
(2, 'Plus One & PLus Two JEE', '1,2', '1,3', 1, 0, '2020-03-02 11:20:36', '2020-03-02 11:20:36');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2020_01_09_114905_create_admins_table', 1),
('2020_01_09_114953_create_students_table', 1),
('2020_01_09_115007_create_centers_table', 1),
('2020_01_09_121329_create_questions_table', 1),
('2020_01_09_122232_create_students_approvals_table', 1),
('2020_01_09_123931_create_classes_table', 1),
('2020_01_09_124120_create_categories_table', 1),
('2020_01_09_124148_create_exam_names_table', 1),
('2020_01_09_124203_create_subjects_table', 1),
('2020_01_09_124221_create_chapters_table', 1),
('2020_01_10_121331_create_exam_type_packages_table', 1),
('2020_01_10_121422_create_exam_fees_table', 1),
('2020_01_10_121457_create_question_details_table', 1),
('2020_01_10_121511_create_question_options_table', 1),
('2020_01_14_133832_create_student_payments_table', 2),
('2020_01_14_133900_create_student_exam_paids_table', 2),
('2020_02_06_111913_create_exam_results_table', 3),
('2020_03_24_191902_create_notifications_table', 4);

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` int(10) UNSIGNED NOT NULL,
  `student_id` int(11) DEFAULT NULL,
  `center_id` int(11) DEFAULT NULL,
  `date` date NOT NULL,
  `from` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `message` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `view` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `student_id`, `center_id`, `date`, `from`, `message`, `view`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, '2020-03-25', 'center', 'hai', 1, '2020-03-25 11:48:14', '2020-03-25 11:48:14'),
(2, 7, NULL, '2020-03-25', 'center', 'hai', 0, '2020-03-25 11:48:14', '2020-03-25 11:48:14'),
(3, NULL, 1, '2020-04-03', 'admin', 'hai', 1, '2020-04-03 17:42:23', '2020-04-03 17:42:23'),
(4, NULL, 5, '2020-04-03', 'admin', 'hai', 0, '2020-04-03 17:42:23', '2020-04-03 17:42:23'),
(5, 8, NULL, '2020-04-03', 'admin', 'hi', 0, '2020-04-03 17:42:30', '2020-04-03 17:42:30');

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions` (
  `id` int(10) UNSIGNED NOT NULL,
  `question` text COLLATE utf8_unicode_ci NOT NULL,
  `solution` int(11) NOT NULL,
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `q_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `added_by` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`id`, `question`, `solution`, `description`, `q_image`, `added_by`, `status`, `created_at`, `updated_at`) VALUES
(1, 'First Question?`', 1, 'test', NULL, 0, 0, '2020-01-31 09:50:05', '2020-01-31 09:50:05'),
(2, 'First Question2?', 5, 'test', NULL, 0, 0, '2020-01-31 09:50:55', '2020-01-31 09:50:55'),
(3, 'The interpaticle force in solid hydrogen are', 9, 'The forces of attraction in molecular  solids are van der Waals’ forces. ', NULL, 0, 0, '2020-02-01 06:30:35', '2020-02-01 06:30:35'),
(4, 'Crystalline solid is not', 13, 'Crystalline solids are not isotropic, actually refers to anisotropic. ', NULL, 0, 0, '2020-02-01 06:32:11', '2020-02-01 06:32:11'),
(5, 'Which of the following statements about amorphous solid is incorrect?', 17, 'Amorphous solids are isotropic.', NULL, 0, 0, '2020-02-01 06:34:38', '2020-02-01 06:34:38'),
(6, 'Which type of solid crystals will conduct  heat and electricity', 21, 'Metallic crystals are good conductor of heat and electricity due to the presence of free electrons in them. ', NULL, 0, 0, '2020-02-01 06:35:29', '2020-02-01 06:35:29'),
(7, 'Which of the following is not correct for  ionic crystals?', 25, ' Ionic crystals have no directional bonds as the oppositely charged ions are attracted by electrostatic forces and arranged in a definite ratio.', NULL, 0, 0, '2020-02-01 06:36:32', '2020-02-01 06:36:32'),
(8, 'Graphitre is a soft solid lubricant extremely difficult to melt. The reason for this  anomalous behaviour is that graphite', 29, 'Graphite has a two-dimensional sheet like structure and each carbon atom is sp2 hybridised. The layer structure of graphite is much less compact than diamond. Since, the bonding between the layers involving only van der Waals’ forces is weak, these layers', NULL, 0, 0, '2020-02-01 06:37:51', '2020-02-01 06:37:51'),
(9, 'Among solids, the highest melting point is established by ', 33, 'Covalent or network solids have extremely high melting points and may even decompose before melting. ', NULL, 0, 0, '2020-02-01 06:39:07', '2020-02-01 06:39:07'),
(10, 'For two ionic solids CaO and KI, identify the wrong statement among the following', 37, ' KI is an ionic solid while benzene is not. Therefore, it cannot be soluble in benzene.', NULL, 0, 0, '2020-02-01 06:40:12', '2020-02-01 06:40:12'),
(11, 'Which one of the following forms a molecular solid when solidified', 41, '', NULL, 0, 0, '2020-02-01 06:41:06', '2020-02-01 06:41:06'),
(12, 'A unit cell of diomond consists of ', 45, ' Diamond is like ZnS (zinc blende).  Carbon in diamond forms ccp (fcc)  and also occupies half of tetrahedral  voids. Hence, there are 4 units per  unit cell of diamond. ', NULL, 0, 0, '2020-02-01 06:42:35', '2020-02-01 06:42:35'),
(13, 'Sucrose dissoved in water is a type', 49, ' Sucrose dissolved in water, is a  solid in liquid type solution. ', NULL, 0, 0, '2020-02-01 06:53:18', '2020-02-01 06:53:18'),
(14, 'Concentration terms like mass percentage, ppm, mole fraction and molarity do not depend on temperature. However, molarity is a function of temperature because', 53, '', NULL, 0, 0, '2020-02-01 06:56:37', '2020-02-01 06:56:37'),
(15, 'On dissolving sugar in water at room temperature, solution feels cool to touch. Under which of the following cases dissolution of sugar will be most rapid?', 57, 'As dissolution of sugar in water is  an endothermic process. Hence,  solubility increases with temperature,  also increased surface area of sugar increases the solubility.', NULL, 0, 0, '2020-02-01 06:58:12', '2020-02-01 06:58:12'),
(16, 'which of the following solutions shows positive deviation from Raoult\'s law?', 61, 'Acetone + ethanol, is an example of  solutions showing positive deviation  from Raoult’s law. Since, acetone- ethanol attractions are weaker than  acetone-acetone and  ethanolethanol attractions.', NULL, 0, 0, '2020-02-01 06:59:38', '2020-02-01 06:59:38'),
(17, 'If two liquids A and B form minimum boiling azeotrope at some specific compression then', 65, 'If A – B interactions is smaller than A – A or B – B  interactions, then the vapour pressure will be  more and the result will be positive deviation. The  solutions which show positive deviation form  minimum boiling azeotropes.', NULL, 0, 0, '2020-02-01 07:00:32', '2020-02-01 07:00:32'),
(18, 'A plant cell shrinks when it is kept in a ', 69, ' Hypertonic solution has high  osmotic pressure. When a plant cell  is placed in hypertonic solution,  water will diffuse out of the cell  resulting in shrinking of the cell. ', NULL, 0, 0, '2020-02-01 07:01:50', '2020-02-01 07:01:50'),
(19, 'which of the following is colligative property?', 73, 'Relative lowering of vapour pressure  is a colligative property and not  lowering of vapour pressure.  Out of given properties, only  osmotic pressure is a colligative  property.', NULL, 0, 0, '2020-02-01 07:02:54', '2020-02-01 07:02:54'),
(20, 'what happends to freezing point of benzene when naphthelene is added', 77, 'When a non-volatile solid is added  to the solvent its vapour pressure  decreases.', NULL, 0, 0, '2020-02-01 07:03:41', '2020-02-01 07:03:41'),
(21, 'Which of the following statemennt about the composition of the vapour over an ideal 1:1 molar mixture of benzene and toluene is correct at 250 C.', 81, '', NULL, 0, 0, '2020-02-01 07:04:52', '2020-02-01 07:04:52'),
(22, 'What is the mole fraction of the solute in a 1.00 m aqueous solution?', 85, '', NULL, 0, 0, '2020-02-01 07:07:02', '2020-02-01 07:07:02'),
(23, 'In a Daniell cell,', 89, '', NULL, 0, 0, '2020-02-01 11:13:04', '2020-02-01 11:13:04'),
(24, 'In the standard hydrogen electrode, the inert metal used is', 93, '', NULL, 0, 0, '2020-02-01 11:14:59', '2020-02-01 11:14:59'),
(25, 'Which one of the following statement is wrong?', 97, 'It is used as primary reference  electrode.', NULL, 0, 0, '2020-02-01 11:16:23', '2020-02-01 11:16:23'),
(26, 'In an electrochemical cell, the electrons flow from', 101, 'The electrons liberated at anode  during oxidation, move towards  cathode where they are used up. ', NULL, 0, 0, '2020-02-01 11:17:11', '2020-02-01 11:17:11'),
(27, 'Mark the incorrect statement', 105, '', NULL, 0, 0, '2020-02-01 11:18:25', '2020-02-01 11:18:25'),
(28, 'In an electrolytic cell, the flow of electrons is ', 109, 'In electrolytic cell, electrons flow  from cathode to anode through  internal supply', NULL, 0, 0, '2020-02-01 11:19:50', '2020-02-01 11:19:50'),
(29, 'During the electrolysis of molten sodium ch,loride, the time required to produce 0.10 mol of chlorine gas using a current of 3 ampere is', 113, '', NULL, 0, 0, '2020-02-01 11:23:42', '2020-02-01 11:23:42'),
(30, 'Zinc can be coated on iron to produce galvanized iron but the reverse is not possible. It is because', 117, '', NULL, 0, 0, '2020-02-01 11:24:56', '2020-02-01 11:24:56'),
(31, '1 . Two students X and Y report the weight of the same substances as 4.0 g and 4.00g respectively .which of the following staement is coorect ?', 121, '', NULL, 0, 0, '2020-02-01 11:28:23', '2020-02-01 11:28:23'),
(32, ' Which set of figures will be obtained after rounding up the  following upmto three significant figures?', 125, '', NULL, 0, 0, '2020-02-01 11:31:33', '2020-02-01 11:31:33'),
(33, 'Mark the rule which is not correctly stated about the determination of significant figures  ', 129, '', NULL, 0, 0, '2020-02-01 11:32:34', '2020-02-01 11:32:34'),
(34, 'The following data are obtained when dinitrogen and dioxygen react together to form different compounds:', 133, '', NULL, 0, 0, '2020-02-01 11:33:26', '2020-02-01 11:33:26'),
(35, 'Which of the following statement indicates that law of multiple  proportion is being followed?', 137, '', NULL, 0, 0, '2020-02-01 12:06:10', '2020-02-01 12:06:10'),
(36, 'If one gram of a metal carnate gave 0.56 g of its  oxide on heating ,then equivalent  weight of the metal will be', 141, '', NULL, 0, 0, '2020-02-01 12:07:55', '2020-02-01 12:07:55'),
(37, 'Oxygen occurs in nature as a mixture of isotopes 16O having  atomic masses of 15.995 u,16.999u and 17.999 u and relative  abundance of 99.763% ,0.037% and 0.200% respectively . what is the average attomic mass of oxygen', 145, '', NULL, 0, 0, '2020-02-01 12:09:46', '2020-02-01 12:09:46'),
(38, 'How much water is needed to dilute 10 ML hydrochlroric acid  to make it exactly decinormal (0.1 an)?  ', 149, '', NULL, 0, 0, '2020-02-01 12:10:37', '2020-02-01 12:10:37'),
(39, 'What will be the molarity of the solution in Which 0.365 g of HCl gas is dissolved in 100 mL of solution', 153, '', NULL, 0, 0, '2020-02-01 12:11:48', '2020-02-01 12:11:48'),
(40, '46.suppose the element X andY combine to from two compounds xy2 and x3y2.when 0.1 mole of xy2 weight 10g and 0.05 mole x3y2 weight  9g ,the atomic weight X of  and Y', 157, '', NULL, 0, 0, '2020-02-01 12:16:11', '2020-02-01 12:16:11'),
(41, '<p><span style=\"font-size:11pt\"><span style=\"font-family:Calibri,sans-serif\"><span style=\"color:black\">Following reactions are taking place in a Galvanic cell,<br />\r\nZn -&gt; Zn<sup>2+ </sup>+ 2e<sup>-</sup>; Ag<sup>+ </sup>+ e<sup>-</sup> -&gt; Ag<br />\r\nWhich of the given representation is the correct method of depiciting the cell?</span></span></span></p>\r\n', 161, '<p><span style=\"font-size:11pt\"><span style=\"font-family:Calibri,sans-serif\"><img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAgAAAAiCAIAAADtWg+MAAAAAXNSR0IArs4c6QAAAAlwSFlzAAAOxAAADsQBlSsOGwAAAL9JREFUOE+9UtsNwyAMhM5S5asT0H1gmP7QEboELJKvwC6ubUKKEUh9SPVXYt+dzpw1AKhRnYZdbDaDfHdXFw8gSgGk4A13bOB/LGJEd9uWBwQrZCsCeCAZX7uSxDf2yNvau0remrIGlTGGvOlf3upjV93+0z1mOam/2c0R70CXOu4BLwEPwYZUjoJy53zVK3P+oonxiBoteFnOyBMMUmW8lAqYV+m2AwRzcrV2KQS33coQ7WAt6SGju9nd7vR1n0h02j3R2T0FAAAAAElFTkSuQmCC\" style=\"height:34px; width:8px\" />&nbsp;&nbsp;<img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB8AAAApCAIAAABC72XeAAAAAXNSR0IArs4c6QAAAAlwSFlzAAAOxAAADsQBlSsOGwAAAdlJREFUWEfFl8t1gzAQRUVqsb3wcQW0kH1KEBWkCm9owVvvgQpCBT4sLHohow8gpNGPT8LKR6Crpzea0TgbhoEc9nyg5KYomj5pTccM0G48Nc1pbQ+HRrBpZCc2x9h8g15Tskb3KNGcvqDDy7xkIQt871mZ6/J0+nb4MCzxM91YVijkY0Tsh4Gp6mdgb7rGiY7Aa0prJsYpLWsGUYsKioYf6RyCeg7fJgZDQym6E+7wyxv8GSZzta+eLbldTlZ68hf59ZyUtuR0uZH2WUGySzrrWoJC4EX+9Wmv6l/ufM1J2zH4aDoaWBZx0+dxeYIWj8OhMViCnhg5WCWQdEIHyOLO9O8XQV1Ps1v7mjtPXu8er8CrsebEdGNi6pCy5ljt/0fPoh9XnHzaYwyW36yhbz85XPucuNt5iqBKy/FRHTNrN+VT9gvtu1sz1lxB31v8XLjkkVqWWuMoJt/dE01FlXsDNQ1zvinulwcs0Hb3oiLfPzAVu8W0qVy6uouUTufFOu8ttpGyb22zzUG8ie4AkY7Diw9sbCkF7ZZkaPHdJ7SArk5PNnYY3nuglsINwh922HiLH12Ig/8ONvCj/tmo1I0+f2pnol+2dmn9bwp351NWhhVknntre0X+BQiw3Nymc1loAAAAAElFTkSuQmCC\" style=\"height:41px; width:31px\" />&nbsp;<img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAgAAAAiCAIAAADtWg+MAAAAAXNSR0IArs4c6QAAAAlwSFlzAAAOxAAADsQBlSsOGwAAAM1JREFUOE+9UssNwjAMTZmlYggzQcWFEyMkw3AJI7BEuginprsYP8e0tEolPhK+2cmzn59fw8yuFrtqVYovD+M1HEI/fZRWzDlF0opPmksA0YfLsL9x8ou2zx+sD0vE16yWwDf2GIf7mlWOnsoaCCICt+YXrT5mtdp/c4+tO7l/0HXl5LNYRDFl6FROKjdFZq6gmMUMKrc/dS2Yt93xbELLL1jHEJoAgFboYLbCOVAtM8w3NkMGGmICF4dZY3ViPdZ0C3cbjgUnKxi/TXUftlW7Qzs+h9wAAAAASUVORK5CYII=\" style=\"height:34px; width:8px\" />&nbsp;<img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAgAAAAiCAIAAADtWg+MAAAAAXNSR0IArs4c6QAAAAlwSFlzAAAOxAAADsQBlSsOGwAAAL9JREFUOE+9UtsNwyAMhM5S5asT0H1gmP7QEboELJKvwC6ubUKKEUh9SPVXYt+dzpw1AKhRnYZdbDaDfHdXFw8gSgGk4A13bOB/LGJEd9uWBwQrZCsCeCAZX7uSxDf2yNvau0remrIGlTGGvOlf3upjV93+0z1mOam/2c0R70CXOu4BLwEPwYZUjoJy53zVK3P+oonxiBoteFnOyBMMUmW8lAqYV+m2AwRzcrV2KQS33coQ7WAt6SGju9nd7vR1n0h02j3R2T0FAAAAAElFTkSuQmCC\" style=\"height:34px; width:8px\" />&nbsp;<img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADYAAAAiCAIAAACbYL0HAAAAAXNSR0IArs4c6QAAAAlwSFlzAAAOxAAADsQBlSsOGwAAAntJREFUWEftmD1y6jAQxwVngRQZTiBOwKRJRUsnl8/N6yjTpRHlS/daKprYJwgnYFJg38X570o28occcJSZMEEV8a53f7ur3UgeFUUhfvYa/2w8orOI+Saaj2jNozT3UZNSlHZL8zTi90eskG+stU3TmFWbV4LWg7Z9FLpIlBAqyYoio19S41djZYmW/K5KWjL7ICONUmz/aBsiZ1iuE/jscln5EeCC7Uqn9kepBasaEZhIfIgcqRUnSvq8ZlppyoTrsZewAGLdcw/HZ4hkCd4Qpd8nhBSGw4hwamVDDKZgUmlWHov8eGiW/3D07sfe7lo8qn08Xb2v3/5MPIrZu7ifCkGa21fyku5eZneVNjbxNBZr3iBrEcekHLSj0x1FO3tceAOBxvKBgKb3khmRIEnMvPLNKt6r5N+iRJakPBaTu1nTohPW+TMJCXhC/Fq+7DxdD4bjwdqePCyZEVk1zET4ut1L/dcGSNVlZWQRAYmqtCQ4hXU+YBqttsv/iB+uvYxg4DLTMozPu5LZuqqykz4joaYeKPppQrhDpzZEaG+0HlS9TZKq1X1zyxhwJgLbaz7g1sGI02YwAUgRIs9D00Voo3KqOETUZFZOKvKkVJLbuXqyxGO2tgyRy9QecAYDrZyZhPBoGt3+R5+/m72aQYdOAJ4OEzfEEHm9giyGCPObbdyGTogEX8Fe/FWIeVpe0fiSFqLE1ob/KnKRxJxi+OjAx6XeS85FlunuEn75z2NDfH3bXhx0cu/eHUPi6n+n8577BTfBC91zhR6IGRaRrtB9HwMGQYZERAKD89GlYFBgXS/VAOkTS9cHnSHeQiHyLHRX/3eaS1Cv4KTzAc/9vI28xUO4AAAAAElFTkSuQmCC\" style=\"height:34px; width:54px\" />&nbsp;&nbsp;<img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAiCAIAAADZI86QAAAAAXNSR0IArs4c6QAAAAlwSFlzAAAOxAAADsQBlSsOGwAAARhJREFUOE/dVMERhCAM1KtFfThWECuwEXzawBWBTxvxOrACx4fQC0cEVBAcHWfucfuCZBPCEhILIaI7eN0hI3cXwNu6rD92ArTFCOnhyiVLEoL1FJYd6Ze9Rk/QwtAvV0DlSmBATwiVZuXe6EwmUSRMaTZYUtV1TZUcLsOmISoybU+yIhomZt3BieDz6OYYZ76/9DW9wrJiEQ6wwJN3SHOIsIgFWCDkqZHV6GDLisp4ZWWUgHoGBACs4kr9tWO1xb/spaeyBuKftPe1km6z/lBW3qo/v6Fsdb9qeY7vYH4x/nXZrO/G+bz7KWGtdXO7fjVmPLBmxs4fCgjxl7nkgTujNoo/IMz3nhAsB8/xnHDK9wSc88Xtbv0CuAEUgWc3u0EAAAAASUVORK5CYII=\" style=\"height:34px; width:16px\" />&nbsp;<img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAATCAIAAADJS2CHAAAAAXNSR0IArs4c6QAAAAlwSFlzAAAOxAAADsQBlSsOGwAAAIRJREFUKFNj/P//PwNewIRfGihLVxV3JlozMjJap2+/g+Su7RNBXLA77kzsZlj4///tsCtequnboWruTNxwjUEFyAH6dlta2jYgBQK3J1gxWE3YdhssOAFIAcMCKLptAkwBTBEDgxVUHqwCWRikGw6AOoFcKrnj/7Zt+NzBONRiDk86AgAA/qJGNusFDQAAAABJRU5ErkJggg==\" style=\"height:19px; width:11px\" />&nbsp;<sup><span style=\"font-size:16.5pt\"><span style=\"color:black\">2</span></span></sup></span></span></p>\r\n', NULL, 0, 0, '2020-04-24 09:17:13', '2020-04-24 09:17:13'),
(42, '<p>Following reactions are taking place in a Galvanic cell,<br />Zn -&gt; Zn<sup>2+ </sup>+ 2e<sup>-</sup>; Ag<sup>+ </sup>+ e<sup>-</sup> -&gt; Ag<br />Which of the given representation is the correct method of depiciting the cell?</p>', 165, '<p><img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAgAAAAiCAIAAADtWg+MAAAAAXNSR0IArs4c6QAAAAlwSFlzAAAOxAAADsQBlSsOGwAAAL9JREFUOE+9UtsNwyAMhM5S5asT0H1gmP7QEboELJKvwC6ubUKKEUh9SPVXYt+dzpw1AKhRnYZdbDaDfHdXFw8gSgGk4A13bOB/LGJEd9uWBwQrZCsCeCAZX7uSxDf2yNvau0remrIGlTGGvOlf3upjV93+0z1mOam/2c0R70CXOu4BLwEPwYZUjoJy53zVK3P+oonxiBoteFnOyBMMUmW8lAqYV+m2AwRzcrV2KQS33coQ7WAt6SGju9nd7vR1n0h02j3R2T0FAAAAAElFTkSuQmCC\" width=\"8\" height=\"34\" />&nbsp;&nbsp;<img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB8AAAApCAIAAABC72XeAAAAAXNSR0IArs4c6QAAAAlwSFlzAAAOxAAADsQBlSsOGwAAAdlJREFUWEfFl8t1gzAQRUVqsb3wcQW0kH1KEBWkCm9owVvvgQpCBT4sLHohow8gpNGPT8LKR6Crpzea0TgbhoEc9nyg5KYomj5pTccM0G48Nc1pbQ+HRrBpZCc2x9h8g15Tskb3KNGcvqDDy7xkIQt871mZ6/J0+nb4MCzxM91YVijkY0Tsh4Gp6mdgb7rGiY7Aa0prJsYpLWsGUYsKioYf6RyCeg7fJgZDQym6E+7wyxv8GSZzta+eLbldTlZ68hf59ZyUtuR0uZH2WUGySzrrWoJC4EX+9Wmv6l/ufM1J2zH4aDoaWBZx0+dxeYIWj8OhMViCnhg5WCWQdEIHyOLO9O8XQV1Ps1v7mjtPXu8er8CrsebEdGNi6pCy5ljt/0fPoh9XnHzaYwyW36yhbz85XPucuNt5iqBKy/FRHTNrN+VT9gvtu1sz1lxB31v8XLjkkVqWWuMoJt/dE01FlXsDNQ1zvinulwcs0Hb3oiLfPzAVu8W0qVy6uouUTufFOu8ttpGyb22zzUG8ie4AkY7Diw9sbCkF7ZZkaPHdJ7SArk5PNnYY3nuglsINwh922HiLH12Ig/8ONvCj/tmo1I0+f2pnol+2dmn9bwp351NWhhVknntre0X+BQiw3Nymc1loAAAAAElFTkSuQmCC\" width=\"31\" height=\"41\" />&nbsp;<img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAgAAAAiCAIAAADtWg+MAAAAAXNSR0IArs4c6QAAAAlwSFlzAAAOxAAADsQBlSsOGwAAAM1JREFUOE+9UssNwjAMTZmlYggzQcWFEyMkw3AJI7BEuginprsYP8e0tEolPhK+2cmzn59fw8yuFrtqVYovD+M1HEI/fZRWzDlF0opPmksA0YfLsL9x8ou2zx+sD0vE16yWwDf2GIf7mlWOnsoaCCICt+YXrT5mtdp/c4+tO7l/0HXl5LNYRDFl6FROKjdFZq6gmMUMKrc/dS2Yt93xbELLL1jHEJoAgFboYLbCOVAtM8w3NkMGGmICF4dZY3ViPdZ0C3cbjgUnKxi/TXUftlW7Qzs+h9wAAAAASUVORK5CYII=\" width=\"8\" height=\"34\" />&nbsp;<img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAgAAAAiCAIAAADtWg+MAAAAAXNSR0IArs4c6QAAAAlwSFlzAAAOxAAADsQBlSsOGwAAAL9JREFUOE+9UtsNwyAMhM5S5asT0H1gmP7QEboELJKvwC6ubUKKEUh9SPVXYt+dzpw1AKhRnYZdbDaDfHdXFw8gSgGk4A13bOB/LGJEd9uWBwQrZCsCeCAZX7uSxDf2yNvau0remrIGlTGGvOlf3upjV93+0z1mOam/2c0R70CXOu4BLwEPwYZUjoJy53zVK3P+oonxiBoteFnOyBMMUmW8lAqYV+m2AwRzcrV2KQS33coQ7WAt6SGju9nd7vR1n0h02j3R2T0FAAAAAElFTkSuQmCC\" width=\"8\" height=\"34\" />&nbsp;<img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADYAAAAiCAIAAACbYL0HAAAAAXNSR0IArs4c6QAAAAlwSFlzAAAOxAAADsQBlSsOGwAAAntJREFUWEftmD1y6jAQxwVngRQZTiBOwKRJRUsnl8/N6yjTpRHlS/daKprYJwgnYFJg38X570o28occcJSZMEEV8a53f7ur3UgeFUUhfvYa/2w8orOI+Saaj2jNozT3UZNSlHZL8zTi90eskG+stU3TmFWbV4LWg7Z9FLpIlBAqyYoio19S41djZYmW/K5KWjL7ICONUmz/aBsiZ1iuE/jscln5EeCC7Uqn9kepBasaEZhIfIgcqRUnSvq8ZlppyoTrsZewAGLdcw/HZ4hkCd4Qpd8nhBSGw4hwamVDDKZgUmlWHov8eGiW/3D07sfe7lo8qn08Xb2v3/5MPIrZu7ifCkGa21fyku5eZneVNjbxNBZr3iBrEcekHLSj0x1FO3tceAOBxvKBgKb3khmRIEnMvPLNKt6r5N+iRJakPBaTu1nTohPW+TMJCXhC/Fq+7DxdD4bjwdqePCyZEVk1zET4ut1L/dcGSNVlZWQRAYmqtCQ4hXU+YBqttsv/iB+uvYxg4DLTMozPu5LZuqqykz4joaYeKPppQrhDpzZEaG+0HlS9TZKq1X1zyxhwJgLbaz7g1sGI02YwAUgRIs9D00Voo3KqOETUZFZOKvKkVJLbuXqyxGO2tgyRy9QecAYDrZyZhPBoGt3+R5+/m72aQYdOAJ4OEzfEEHm9giyGCPObbdyGTogEX8Fe/FWIeVpe0fiSFqLE1ob/KnKRxJxi+OjAx6XeS85FlunuEn75z2NDfH3bXhx0cu/eHUPi6n+n8577BTfBC91zhR6IGRaRrtB9HwMGQYZERAKD89GlYFBgXS/VAOkTS9cHnSHeQiHyLHRX/3eaS1Cv4KTzAc/9vI28xUO4AAAAAElFTkSuQmCC\" width=\"54\" height=\"34\" />&nbsp;&nbsp;<img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAiCAIAAADZI86QAAAAAXNSR0IArs4c6QAAAAlwSFlzAAAOxAAADsQBlSsOGwAAARhJREFUOE/dVMERhCAM1KtFfThWECuwEXzawBWBTxvxOrACx4fQC0cEVBAcHWfucfuCZBPCEhILIaI7eN0hI3cXwNu6rD92ArTFCOnhyiVLEoL1FJYd6Ze9Rk/QwtAvV0DlSmBATwiVZuXe6EwmUSRMaTZYUtV1TZUcLsOmISoybU+yIhomZt3BieDz6OYYZ76/9DW9wrJiEQ6wwJN3SHOIsIgFWCDkqZHV6GDLisp4ZWWUgHoGBACs4kr9tWO1xb/spaeyBuKftPe1km6z/lBW3qo/v6Fsdb9qeY7vYH4x/nXZrO/G+bz7KWGtdXO7fjVmPLBmxs4fCgjxl7nkgTujNoo/IMz3nhAsB8/xnHDK9wSc88Xtbv0CuAEUgWc3u0EAAAAASUVORK5CYII=\" width=\"16\" height=\"34\" />&nbsp;<sup><img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAATCAIAAADJS2CHAAAAAXNSR0IArs4c6QAAAAlwSFlzAAAOxAAADsQBlSsOGwAAAIRJREFUKFNj/P//PwNewIRfGihLVxV3JlozMjJap2+/g+Su7RNBXLA77kzsZlj4///tsCtequnboWruTNxwjUEFyAH6dlta2jYgBQK3J1gxWE3YdhssOAFIAcMCKLptAkwBTBEDgxVUHqwCWRikGw6AOoFcKrnj/7Zt+NzBONRiDk86AgAA/qJGNusFDQAAAABJRU5ErkJggg==\" width=\"11\" height=\"19\" /></sup>&nbsp;<span style=\"font-size: 24pt;\"><sup>2</sup></span></p>', NULL, 0, 0, '2020-04-24 09:24:30', '2020-04-24 09:24:30'),
(43, '<table style=\"width: 298pt;\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><colgroup><col style=\"width: 298pt;\" /></colgroup>\r\n<tbody>\r\n<tr style=\"height: 33.75pt;\">\r\n<td style=\"width: 298pt;\">The cell reaction of the following Galvanic cell:<br />Cu<span><sub>(s) </sub></span><span>| Cu</span><span><sup>2+</sup></span><span><sub>(aq)</sub></span><span> || Hg</span><span><sup>2+</sup></span><span><sub>(aq) </sub></span><span>| Hg</span><span><sub>(i)</sub></span><span> is</span></td>\r\n</tr>\r\n</tbody>\r\n</table>', 169, '', NULL, 0, 0, '2020-04-24 09:34:17', '2020-04-24 09:34:17'),
(49, '<table style=\"border-collapse: collapse; width: 298pt;\" border=\"0\" width=\"397\" cellspacing=\"0\" cellpadding=\"0\">\r\n<tbody>\r\n<tr style=\"height: 33.75pt;\">\r\n<td class=\"xl65\" style=\"height: 33.75pt; width: 298pt;\" width=\"397\" height=\"45\">The cell reaction of the following Galvanic cell:<br />Cu<span class=\"font6\" style=\"\"><sub>(s) </sub></span><span class=\"font0\" style=\"\">| Cu</span><span class=\"font5\" style=\"\"><sup>2+</sup></span><span class=\"font6\" style=\"\"><sub>(aq)</sub></span><span class=\"font0\" style=\"\"> || Hg</span><span class=\"font5\" style=\"\"><sup>2+</sup></span><span class=\"font6\" style=\"\"><sub>(aq) </sub></span><span class=\"font0\" style=\"\">| Hg</span><span class=\"font6\" style=\"\"><sub>(i)</sub></span><span class=\"font0\" style=\"\"> is</span></td>\r\n</tr>\r\n</tbody>\r\n</table>', 193, '', NULL, 0, 0, '2020-04-24 10:29:43', '2020-04-24 10:29:43');

-- --------------------------------------------------------

--
-- Table structure for table `question_details`
--

CREATE TABLE `question_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `question_id` int(11) NOT NULL,
  `chapter_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `question_details`
--

INSERT INTO `question_details` (`id`, `question_id`, `chapter_id`, `created_at`, `updated_at`) VALUES
(1, 1, '4', '2020-01-31 09:50:06', '2020-01-31 09:50:06'),
(2, 2, '4', '2020-01-31 09:50:56', '2020-01-31 09:50:56'),
(3, 3, '4', '2020-02-01 06:30:35', '2020-02-01 06:30:35'),
(4, 4, '4', '2020-02-01 06:32:12', '2020-02-01 06:32:12'),
(5, 5, '4', '2020-02-01 06:34:39', '2020-02-01 06:34:39'),
(6, 6, '4', '2020-02-01 06:35:29', '2020-02-01 06:35:29'),
(7, 7, '4', '2020-02-01 06:36:33', '2020-02-01 06:36:33'),
(8, 8, '4', '2020-02-01 06:37:51', '2020-02-01 06:37:51'),
(9, 9, '4', '2020-02-01 06:39:07', '2020-02-01 06:39:07'),
(10, 10, '4', '2020-02-01 06:40:12', '2020-02-01 06:40:12'),
(11, 11, '4', '2020-02-01 06:41:07', '2020-02-01 06:41:07'),
(12, 12, '4', '2020-02-01 06:42:36', '2020-02-01 06:42:36'),
(13, 13, '4', '2020-02-01 06:53:18', '2020-02-01 06:53:18'),
(14, 14, '4', '2020-02-01 06:56:38', '2020-02-01 06:56:38'),
(15, 15, '4', '2020-02-01 06:58:13', '2020-02-01 06:58:13'),
(16, 16, '2', '2020-02-01 06:59:38', '2020-02-01 06:59:38'),
(17, 17, '2', '2020-02-01 07:00:33', '2020-02-01 07:00:33'),
(18, 18, '2', '2020-02-01 07:01:51', '2020-02-01 07:01:51'),
(19, 19, '2', '2020-02-01 07:02:54', '2020-02-01 07:02:54'),
(20, 20, '2', '2020-02-01 07:03:42', '2020-02-01 07:03:42'),
(21, 21, '2', '2020-02-01 07:04:53', '2020-02-01 07:04:53'),
(22, 22, '2', '2020-02-01 07:07:02', '2020-02-01 07:07:02'),
(23, 23, '2', '2020-02-01 11:13:05', '2020-02-01 11:13:05'),
(24, 24, '2', '2020-02-01 11:14:59', '2020-02-01 11:14:59'),
(25, 25, '2', '2020-02-01 11:16:23', '2020-02-01 11:16:23'),
(26, 26, '', '2020-02-01 11:17:11', '2020-02-01 11:17:11'),
(27, 27, '20', '2020-02-01 11:18:26', '2020-02-01 11:18:26'),
(28, 28, '20', '2020-02-01 11:19:50', '2020-02-01 11:19:50'),
(29, 29, '20', '2020-02-01 11:23:43', '2020-02-01 11:23:43'),
(30, 30, '20', '2020-02-01 11:24:57', '2020-02-01 11:24:57'),
(31, 31, '1', '2020-02-01 11:28:24', '2020-02-01 11:28:24'),
(32, 32, '1', '2020-02-01 11:31:34', '2020-02-01 11:31:34'),
(33, 33, '1', '2020-02-01 11:32:34', '2020-02-01 11:32:34'),
(34, 34, '1', '2020-02-01 11:33:27', '2020-02-01 11:33:27'),
(35, 35, '1', '2020-02-01 12:06:10', '2020-02-01 12:06:10'),
(36, 36, '1', '2020-02-01 12:07:55', '2020-02-01 12:07:55'),
(37, 37, '1', '2020-02-01 12:09:46', '2020-02-01 12:09:46'),
(38, 38, '1', '2020-02-01 12:10:38', '2020-02-01 12:10:38'),
(39, 39, '1', '2020-02-01 12:11:48', '2020-02-01 12:11:48'),
(40, 40, '', '2020-02-01 12:16:11', '2020-02-01 12:16:11'),
(41, 41, '2', '2020-04-24 09:17:13', '2020-04-24 09:17:13'),
(42, 42, '2', '2020-04-24 09:24:30', '2020-04-24 09:24:30'),
(43, 43, '4', '2020-04-24 09:34:17', '2020-04-24 09:34:17'),
(49, 49, '3', '2020-04-24 10:29:43', '2020-04-24 10:29:43');

-- --------------------------------------------------------

--
-- Table structure for table `question_options`
--

CREATE TABLE `question_options` (
  `id` int(10) UNSIGNED NOT NULL,
  `question_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `options` text COLLATE utf8_unicode_ci NOT NULL,
  `opt_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `question_options`
--

INSERT INTO `question_options` (`id`, `question_id`, `options`, `opt_image`, `created_at`, `updated_at`) VALUES
(1, '1', 'answer`', NULL, '2020-01-31 09:50:05', '2020-01-31 09:50:05'),
(2, '1', 'option 1', NULL, '2020-01-31 09:50:05', '2020-01-31 09:50:05'),
(3, '1', 'option 2', NULL, '2020-01-31 09:50:05', '2020-01-31 09:50:05'),
(4, '1', 'option 3', NULL, '2020-01-31 09:50:06', '2020-01-31 09:50:06'),
(5, '2', 'answer 2', NULL, '2020-01-31 09:50:55', '2020-01-31 09:50:55'),
(6, '2', 'option 1', NULL, '2020-01-31 09:50:55', '2020-01-31 09:50:55'),
(7, '2', 'option 2', NULL, '2020-01-31 09:50:56', '2020-01-31 09:50:56'),
(8, '2', 'option 3', NULL, '2020-01-31 09:50:56', '2020-01-31 09:50:56'),
(9, '3', 'Vander walls\' forces', NULL, '2020-02-01 06:30:35', '2020-02-01 06:30:35'),
(10, '3', 'Covalent bonds', NULL, '2020-02-01 06:30:35', '2020-02-01 06:30:35'),
(11, '3', 'Coordinate bonds', NULL, '2020-02-01 06:30:35', '2020-02-01 06:30:35'),
(12, '3', 'Hydrogen bonds', NULL, '2020-02-01 06:30:35', '2020-02-01 06:30:35'),
(13, '4', 'Isotropic', NULL, '2020-02-01 06:32:11', '2020-02-01 06:32:11'),
(14, '4', 'Anisotropic', NULL, '2020-02-01 06:32:11', '2020-02-01 06:32:11'),
(15, '4', 'Hard', NULL, '2020-02-01 06:32:11', '2020-02-01 06:32:11'),
(16, '4', 'Dense', NULL, '2020-02-01 06:32:12', '2020-02-01 06:32:12'),
(17, '5', 'They are anisotropic', NULL, '2020-02-01 06:34:38', '2020-02-01 06:34:38'),
(18, '5', 'They melt over a range  of temperature', NULL, '2020-02-01 06:34:38', '2020-02-01 06:34:38'),
(19, '5', 'There is no ordered  arrangement of particles', NULL, '2020-02-01 06:34:38', '2020-02-01 06:34:38'),
(20, '5', 'They are rigid and  incopressible', NULL, '2020-02-01 06:34:39', '2020-02-01 06:34:39'),
(21, '6', 'Metalic', NULL, '2020-02-01 06:35:29', '2020-02-01 06:35:29'),
(22, '6', 'Ionic', NULL, '2020-02-01 06:35:29', '2020-02-01 06:35:29'),
(23, '6', 'Molecular', NULL, '2020-02-01 06:35:29', '2020-02-01 06:35:29'),
(24, '6', 'Covalent', NULL, '2020-02-01 06:35:29', '2020-02-01 06:35:29'),
(25, '7', 'They exhibit directional properties of the bond', NULL, '2020-02-01 06:36:32', '2020-02-01 06:36:32'),
(26, '7', 'They posses high  melting points and  boiling point', NULL, '2020-02-01 06:36:32', '2020-02-01 06:36:32'),
(27, '7', '`All are electrolytes', NULL, '2020-02-01 06:36:32', '2020-02-01 06:36:32'),
(28, '7', 'They exhibit the property of isomorphism', NULL, '2020-02-01 06:36:32', '2020-02-01 06:36:32'),
(29, '8', 'Has carbon atom arranged in large plates of ring of strong bound carbon atoms with weak interplate bonds', NULL, '2020-02-01 06:37:51', '2020-02-01 06:37:51'),
(30, '8', 'Is an allotropic form  of diomond', NULL, '2020-02-01 06:37:51', '2020-02-01 06:37:51'),
(31, '8', 'Has molecules of  variable molecular masses', NULL, '2020-02-01 06:37:51', '2020-02-01 06:37:51'),
(32, '8', 'Is a non-crystaline  substance', NULL, '2020-02-01 06:37:51', '2020-02-01 06:37:51'),
(33, '9', 'Covalent solids', NULL, '2020-02-01 06:39:07', '2020-02-01 06:39:07'),
(34, '9', 'Psudo solids', NULL, '2020-02-01 06:39:07', '2020-02-01 06:39:07'),
(35, '9', 'Ionic solid', NULL, '2020-02-01 06:39:07', '2020-02-01 06:39:07'),
(36, '9', 'Molecular solid', NULL, '2020-02-01 06:39:07', '2020-02-01 06:39:07'),
(37, '10', 'KI is soluble in benzene', NULL, '2020-02-01 06:40:12', '2020-02-01 06:40:12'),
(38, '10', 'CaO has high melting point', NULL, '2020-02-01 06:40:12', '2020-02-01 06:40:12'),
(39, '10', ' Lattice energy of CaO is much larger than that of KI', NULL, '2020-02-01 06:40:12', '2020-02-01 06:40:12'),
(40, '10', 'Both are electricsal insulators', NULL, '2020-02-01 06:40:12', '2020-02-01 06:40:12'),
(41, '11', 'Methane', NULL, '2020-02-01 06:41:06', '2020-02-01 06:41:06'),
(42, '11', 'Calcium fluoride', NULL, '2020-02-01 06:41:06', '2020-02-01 06:41:06'),
(43, '11', 'Rock salt', NULL, '2020-02-01 06:41:06', '2020-02-01 06:41:06'),
(44, '11', 'Silicon carbide', NULL, '2020-02-01 06:41:06', '2020-02-01 06:41:06'),
(45, '12', '4 Unit', NULL, '2020-02-01 06:42:35', '2020-02-01 06:42:35'),
(46, '12', '6 Units', NULL, '2020-02-01 06:42:35', '2020-02-01 06:42:35'),
(47, '12', '8 Units', NULL, '2020-02-01 06:42:35', '2020-02-01 06:42:35'),
(48, '12', '10 Units', NULL, '2020-02-01 06:42:35', '2020-02-01 06:42:35'),
(49, '13', 'solid in liquid solution', NULL, '2020-02-01 06:53:18', '2020-02-01 06:53:18'),
(50, '13', 'solid in solid solution', NULL, '2020-02-01 06:53:18', '2020-02-01 06:53:18'),
(51, '13', 'solid in gas solution', NULL, '2020-02-01 06:53:18', '2020-02-01 06:53:18'),
(52, '13', 'gas in solid solution', NULL, '2020-02-01 06:53:18', '2020-02-01 06:53:18'),
(53, '14', 'Volume depends on temperature and molarity involves volume', NULL, '2020-02-01 06:56:37', '2020-02-01 06:56:37'),
(54, '14', 'Molarity involves non-volatile solute while all other terms involve volatile solute', NULL, '2020-02-01 06:56:37', '2020-02-01 06:56:37'),
(55, '14', 'number of moles of solute changes with change in temperature', NULL, '2020-02-01 06:56:37', '2020-02-01 06:56:37'),
(56, '14', 'molarity is used for polar solvent only', NULL, '2020-02-01 06:56:37', '2020-02-01 06:56:37'),
(57, '15', 'Powdered sugar in hot water', NULL, '2020-02-01 06:58:13', '2020-02-01 06:58:13'),
(58, '15', 'sugar crystals in hot water', NULL, '2020-02-01 06:58:13', '2020-02-01 06:58:13'),
(59, '15', 'powdered sugar in cold water', NULL, '2020-02-01 06:58:13', '2020-02-01 06:58:13'),
(60, '15', 'powdered sugar in hot water', NULL, '2020-02-01 06:58:13', '2020-02-01 06:58:13'),
(61, '16', 'Water + Nitric acid', NULL, '2020-02-01 06:59:38', '2020-02-01 06:59:38'),
(62, '16', 'Acetone + Aniline', NULL, '2020-02-01 06:59:38', '2020-02-01 06:59:38'),
(63, '16', 'Acetone + Ethanol', NULL, '2020-02-01 06:59:38', '2020-02-01 06:59:38'),
(64, '16', 'Chloroform + Benzene', NULL, '2020-02-01 06:59:38', '2020-02-01 06:59:38'),
(65, '17', 'A-B interactions are weaker than those between A-A or B-B.', NULL, '2020-02-01 07:00:32', '2020-02-01 07:00:32'),
(66, '17', 'A-B interactions are  stronger than those  between A-A or B-B', NULL, '2020-02-01 07:00:32', '2020-02-01 07:00:32'),
(67, '17', 'A-B interactions are equal to A-A and B-B interactions', NULL, '2020-02-01 07:00:32', '2020-02-01 07:00:32'),
(68, '17', 'vapour pressure of solution decreases because less number of molecules of only one of the liqyuids escape from the solution', NULL, '2020-02-01 07:00:32', '2020-02-01 07:00:32'),
(69, '18', 'hypertonic solution', NULL, '2020-02-01 07:01:50', '2020-02-01 07:01:50'),
(70, '18', 'hypotonic solution', NULL, '2020-02-01 07:01:50', '2020-02-01 07:01:50'),
(71, '18', 'isotonic solution', NULL, '2020-02-01 07:01:50', '2020-02-01 07:01:50'),
(72, '18', 'pure water', NULL, '2020-02-01 07:01:50', '2020-02-01 07:01:50'),
(73, '19', 'Osmotic pressure', NULL, '2020-02-01 07:02:54', '2020-02-01 07:02:54'),
(74, '19', 'Lowering of vapour pressure', NULL, '2020-02-01 07:02:54', '2020-02-01 07:02:54'),
(75, '19', 'boiling point', NULL, '2020-02-01 07:02:54', '2020-02-01 07:02:54'),
(76, '19', 'change entropy', NULL, '2020-02-01 07:02:54', '2020-02-01 07:02:54'),
(77, '20', 'Decreases', NULL, '2020-02-01 07:03:41', '2020-02-01 07:03:41'),
(78, '20', 'Increases', NULL, '2020-02-01 07:03:41', '2020-02-01 07:03:41'),
(79, '20', 'Remains unchanged', NULL, '2020-02-01 07:03:41', '2020-02-01 07:03:41'),
(80, '20', 'First decrease and then increase', NULL, '2020-02-01 07:03:42', '2020-02-01 07:03:42'),
(81, '21', 'The vapour will contain a  higher percentage of  benzene', NULL, '2020-02-01 07:04:52', '2020-02-01 07:04:52'),
(82, '21', 'The vapour will contain  equal amount of benzene  and toluene', NULL, '2020-02-01 07:04:52', '2020-02-01 07:04:52'),
(83, '21', 'Not enough information is given to make a prediction', NULL, '2020-02-01 07:04:53', '2020-02-01 07:04:53'),
(84, '21', 'The vapour will contain a higher percentage of toluence', NULL, '2020-02-01 07:04:53', '2020-02-01 07:04:53'),
(85, '22', '0.0177', NULL, '2020-02-01 07:07:02', '2020-02-01 07:07:02'),
(86, '22', '1.77', NULL, '2020-02-01 07:07:02', '2020-02-01 07:07:02'),
(87, '22', '0.0354', NULL, '2020-02-01 07:07:02', '2020-02-01 07:07:02'),
(88, '22', '0.177', NULL, '2020-02-01 07:07:02', '2020-02-01 07:07:02'),
(89, '23', 'the chemical energy liberated during the redox reaction is converted to electrical energy', NULL, '2020-02-01 11:13:04', '2020-02-01 11:13:04'),
(90, '23', 'the electrical energy of the  cell is converted to  chemical energy ', NULL, '2020-02-01 11:13:05', '2020-02-01 11:13:05'),
(91, '23', 'the energy of the cell is utilised in conduction of the redox reaction', NULL, '2020-02-01 11:13:05', '2020-02-01 11:13:05'),
(92, '23', 'the potential energy of the cell is converted into electrical energy', NULL, '2020-02-01 11:13:05', '2020-02-01 11:13:05'),
(93, '24', 'Pt', NULL, '2020-02-01 11:14:59', '2020-02-01 11:14:59'),
(94, '24', 'Pb', NULL, '2020-02-01 11:14:59', '2020-02-01 11:14:59'),
(95, '24', 'Pd', NULL, '2020-02-01 11:14:59', '2020-02-01 11:14:59'),
(96, '24', 'Ni', NULL, '2020-02-01 11:14:59', '2020-02-01 11:14:59'),
(97, '25', 'Standard hydrogen  electrode is used as a  secondary reference electrode', NULL, '2020-02-01 11:16:23', '2020-02-01 11:16:23'),
(98, '25', 'The electrode potential is  called standard electrode  potential if the electrode  is set up in ! M solution  at 298 K', NULL, '2020-02-01 11:16:23', '2020-02-01 11:16:23'),
(99, '25', 'The electrode potential of Pt, H2 | H+ is taken as zero', NULL, '2020-02-01 11:16:23', '2020-02-01 11:16:23'),
(100, '25', 'Greater the oxidation potential of a metal, more active is the metal', NULL, '2020-02-01 11:16:23', '2020-02-01 11:16:23'),
(101, '26', 'anode to cathode', NULL, '2020-02-01 11:17:11', '2020-02-01 11:17:11'),
(102, '26', 'cathod to anode', NULL, '2020-02-01 11:17:11', '2020-02-01 11:17:11'),
(103, '26', 'anode to solution', NULL, '2020-02-01 11:17:11', '2020-02-01 11:17:11'),
(104, '26', 'solution to cathode', NULL, '2020-02-01 11:17:11', '2020-02-01 11:17:11'),
(105, '27', 'For cell reaction to occur spontaneously, the EMF of the cell should be negative', NULL, '2020-02-01 11:18:25', '2020-02-01 11:18:25'),
(106, '27', 'The limiting equivalent conductance for weak electrolytes can be computed with the help of Kohlraushs\'s Law', NULL, '2020-02-01 11:18:25', '2020-02-01 11:18:25'),
(107, '27', 'EMF of a cell is the difference in the reduction potentials of cathode and anode', NULL, '2020-02-01 11:18:25', '2020-02-01 11:18:25'),
(108, '27', 'Fluorine is the strongest oxiding agent as its reducing potential is very high', NULL, '2020-02-01 11:18:26', '2020-02-01 11:18:26'),
(109, '28', 'From cathode to anode through internal supply', NULL, '2020-02-01 11:19:50', '2020-02-01 11:19:50'),
(110, '28', 'From cathode to anode in the solution', NULL, '2020-02-01 11:19:50', '2020-02-01 11:19:50'),
(111, '28', 'From cathode to anode through external supply', NULL, '2020-02-01 11:19:50', '2020-02-01 11:19:50'),
(112, '28', 'from anode to cathode through internal supply', NULL, '2020-02-01 11:19:50', '2020-02-01 11:19:50'),
(113, '29', '110 minutes', NULL, '2020-02-01 11:23:42', '2020-02-01 11:23:42'),
(114, '29', '55 minutes', NULL, '2020-02-01 11:23:42', '2020-02-01 11:23:42'),
(115, '29', '220 minutes', NULL, '2020-02-01 11:23:43', '2020-02-01 11:23:43'),
(116, '29', '330 minutes', NULL, '2020-02-01 11:23:43', '2020-02-01 11:23:43'),
(117, '30', 'Zinc has higher negative electrode potential than iron', NULL, '2020-02-01 11:24:56', '2020-02-01 11:24:56'),
(118, '30', 'Zinc is lighter than iron', NULL, '2020-02-01 11:24:56', '2020-02-01 11:24:56'),
(119, '30', 'Zinc has lower melting point than iron', NULL, '2020-02-01 11:24:56', '2020-02-01 11:24:56'),
(120, '30', 'Zinc has lower negative electrode potential than iron', NULL, '2020-02-01 11:24:56', '2020-02-01 11:24:56'),
(121, '31', 'Y is more accurate than x', NULL, '2020-02-01 11:28:23', '2020-02-01 11:28:23'),
(122, '31', 'Both are equally  accurate', NULL, '2020-02-01 11:28:23', '2020-02-01 11:28:23'),
(123, '31', 'X is more accurate than Y', NULL, '2020-02-01 11:28:24', '2020-02-01 11:28:24'),
(124, '31', 'Both are inaccurate scientifically.', NULL, '2020-02-01 11:28:24', '2020-02-01 11:28:24'),
(125, '32', '34.2,0.0460,10.4', NULL, '2020-02-01 11:31:33', '2020-02-01 11:31:33'),
(126, '32', '34.3,0.0461,10.4', NULL, '2020-02-01 11:31:33', '2020-02-01 11:31:33'),
(127, '32', '34.20,0.460,10.40', NULL, '2020-02-01 11:31:33', '2020-02-01 11:31:33'),
(128, '32', '34.21,4.597,1.04', NULL, '2020-02-01 11:31:33', '2020-02-01 11:31:33'),
(129, '33', 'Zero between two non-zero digits are not significant.', NULL, '2020-02-01 11:32:34', '2020-02-01 11:32:34'),
(130, '33', 'Zero preceding to first non-zero digit are not significant. ', NULL, '2020-02-01 11:32:34', '2020-02-01 11:32:34'),
(131, '33', 'Zero at the end or right of the number are significant if they are on the eright side of decimal point.', NULL, '2020-02-01 11:32:34', '2020-02-01 11:32:34'),
(132, '33', 'All non zero digit are significant', NULL, '2020-02-01 11:32:34', '2020-02-01 11:32:34'),
(133, '34', 'Law of multiple proportions', NULL, '2020-02-01 11:33:26', '2020-02-01 11:33:26'),
(134, '34', 'Law of conservation of mass', NULL, '2020-02-01 11:33:27', '2020-02-01 11:33:27'),
(135, '34', 'Law of definite proportions', NULL, '2020-02-01 11:33:27', '2020-02-01 11:33:27'),
(136, '34', 'Avogadro\'s Law', NULL, '2020-02-01 11:33:27', '2020-02-01 11:33:27'),
(137, '35', 'carbon forms two oxides namely CO2 and CO, Where masses of oxygen which combine with  fixed mass of carbon are in the simple ratio 2:1.', NULL, '2020-02-01 12:06:10', '2020-02-01 12:06:10'),
(138, '35', 'sample of carbon dioxide take from  any source will always have carbon  and oxygen in the 1:2', NULL, '2020-02-01 12:06:10', '2020-02-01 12:06:10'),
(139, '35', 'when magnesium burns in oxygen , the amount of magnesium take for  the amount of magnesium in  magnesium oxide formed', NULL, '2020-02-01 12:06:10', '2020-02-01 12:06:10'),
(140, '35', 'At constant temperature and pressure ,200 ML of hydrogen will  combine with 100 Ml of oxygen to produce 200 mL of water vapour', NULL, '2020-02-01 12:06:10', '2020-02-01 12:06:10'),
(141, '36', '20', NULL, '2020-02-01 12:07:55', '2020-02-01 12:07:55'),
(142, '36', '30', NULL, '2020-02-01 12:07:55', '2020-02-01 12:07:55'),
(143, '36', '40', NULL, '2020-02-01 12:07:55', '2020-02-01 12:07:55'),
(144, '36', '25', NULL, '2020-02-01 12:07:55', '2020-02-01 12:07:55'),
(145, '37', '15.999 u', NULL, '2020-02-01 12:09:46', '2020-02-01 12:09:46'),
(146, '37', '16.999u', NULL, '2020-02-01 12:09:46', '2020-02-01 12:09:46'),
(147, '37', '17.999u', NULL, '2020-02-01 12:09:46', '2020-02-01 12:09:46'),
(148, '37', '18.999u', NULL, '2020-02-01 12:09:46', '2020-02-01 12:09:46'),
(149, '38', '990 mL', NULL, '2020-02-01 12:10:37', '2020-02-01 12:10:37'),
(150, '38', '1000mL', NULL, '2020-02-01 12:10:37', '2020-02-01 12:10:37'),
(151, '38', '1010mL', NULL, '2020-02-01 12:10:38', '2020-02-01 12:10:38'),
(152, '38', '100mL', NULL, '2020-02-01 12:10:38', '2020-02-01 12:10:38'),
(153, '39', '0.1 M', NULL, '2020-02-01 12:11:48', '2020-02-01 12:11:48'),
(154, '39', '2 M', NULL, '2020-02-01 12:11:48', '2020-02-01 12:11:48'),
(155, '39', '0.2 M ', NULL, '2020-02-01 12:11:48', '2020-02-01 12:11:48'),
(156, '39', '0.1 M', NULL, '2020-02-01 12:11:48', '2020-02-01 12:11:48'),
(157, '40', '40,30', NULL, '2020-02-01 12:16:11', '2020-02-01 12:16:11'),
(158, '40', '60,40', NULL, '2020-02-01 12:16:11', '2020-02-01 12:16:11'),
(159, '40', '20,30', NULL, '2020-02-01 12:16:11', '2020-02-01 12:16:11'),
(160, '40', '30,20', NULL, '2020-02-01 12:16:11', '2020-02-01 12:16:11'),
(161, '41', '<p><span style=\"font-size:11pt\"><span style=\"font-family:Calibri,sans-serif\"><span style=\"font-size:11.0pt\"><span style=\"font-family:&quot;Calibri&quot;,sans-serif\"><img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACUAAAATCAIAAACY31PkAAAAAXNSR0IArs4c6QAAAAlwSFlzAAAOxAAADsQBlSsOGwAAARFJREFUSEvtVckRwyAMNCmL9KO0g5tRiiG9ODo4FvzwJOPxy7w0gNhLHodt25YL1+NCLIW68c41HP18v0J4rp9zAebXZD7LYtKzmHLf+qXSduKjjqVeyCnGpD1/Ax5B2XnFEzglN5GU3SI5M1mpl3LqtT3i10Bd75NT66zKC57g+A4CMhFnbSVZLDbbIdbdPjlqxgiflIWg7hQdTbvjCYVKYI4BLZ5qcN7SGIM3jVGQB58Nr6mbFbpVlQrWqGe81Z7H66hv5oYKtacSn2sYxoGwv60h917A21sBgCgD6gJtQfXQLW8Hk9KyswiZUgt68a9uv4z9OD0wgTZzLZwyjn3iyjQM+y4x6Ddx4br/D+ea/QUrY/JaCVrkHgAAAABJRU5ErkJggg==\" style=\"height:19px; width:37px\" /></span></span>H = +ve, <span style=\"font-size:11.0pt\"><span style=\"font-family:&quot;Calibri&quot;,sans-serif\"><img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACUAAAATCAIAAACY31PkAAAAAXNSR0IArs4c6QAAAAlwSFlzAAAOxAAADsQBlSsOGwAAARFJREFUSEvtVckRwyAMNCmL9KO0g5tRiiG9ODo4FvzwJOPxy7w0gNhLHodt25YL1+NCLIW68c41HP18v0J4rp9zAebXZD7LYtKzmHLf+qXSduKjjqVeyCnGpD1/Ax5B2XnFEzglN5GU3SI5M1mpl3LqtT3i10Bd75NT66zKC57g+A4CMhFnbSVZLDbbIdbdPjlqxgiflIWg7hQdTbvjCYVKYI4BLZ5qcN7SGIM3jVGQB58Nr6mbFbpVlQrWqGe81Z7H66hv5oYKtacSn2sYxoGwv60h917A21sBgCgD6gJtQfXQLW8Hk9KyswiZUgt68a9uv4z9OD0wgTZzLZwyjn3iyjQM+y4x6Ddx4br/D+ea/QUrY/JaCVrkHgAAAABJRU5ErkJggg==\" style=\"height:19px; width:37px\" /></span></span>V = 0, P<sub>Total</sub> =P<sup>0</sup><sub>A</sub>x<sub>A</sub> + P<sup>0</sup><sub>B</sub> x<sub>B</sub></span></span></p>\r\n\r\n<p><span style=\"font-size:11pt\"><span style=\"font-family:Calibri,sans-serif\"><span style=\"color:black\">Zn<sub>(s) </sub>| Zn<sup>2+ </sup><sub>(aq) </sub>|| Ag<sup>+</sup><sub>(aq) </sub>| Ag<sub>(s)</sub></span></span></span></p>\r\n', NULL, '2020-04-24 09:17:13', '2020-04-24 09:17:13'),
(162, '41', '<p><span style=\"font-size:11pt\"><span style=\"font-family:Calibri,sans-serif\"><span style=\"color:black\">Zn<sup>2+</sup> | Zn || Ag | Ag<sup>+</sup></span></span></span></p>\r\n', NULL, '2020-04-24 09:17:13', '2020-04-24 09:17:13'),
(163, '41', '<p><span style=\"font-size:11pt\"><span style=\"font-family:Calibri,sans-serif\"><span style=\"font-size:11.0pt\"><span style=\"font-family:&quot;Calibri&quot;,sans-serif\"><img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADQAAAATCAIAAABQs7kyAAAAAXNSR0IArs4c6QAAAAlwSFlzAAAOxAAADsQBlSsOGwAAAU9JREFUSEvtVssRgjAQDdaiHhwrgApsJDRiAXDEEjx5MXRgBZxMeon7SQgICiPOwIF3yrhm9+3btxkia61YKjZLJYa8VnKjp2PKNIkQSZobuAWeWwp0FseZBjZaSSGksh1y8A8KzAoggRTeySHnucnpTLI67YUo0/NBgXTVEyc+C0ye3vdFcaLijfE5MUE8Hv00QP9gkA5QE7IOFQF3+SNWqzWzCsUL5Bw1C79/IsdJe/HeDpTB2t7imNQbGetqTCWlzJQOoXb6JjlowcnlVmWabny7TtppGIsMzscpx3sQ8J91reXqcCPhBp8EWgiTn6vgMrzYvxEm5yeyBwk9mm2Ut0t82GH+ZyWO+22Imvv1waHvYHs2u0CPDio+YuhBLj6B+b0CI3dOuHUBj3qbUDPwCE5b2OZjTue4poYrMGKo1kbrJ9OQtz7E10+mH4UTL3TD08d3yqOEAAAAAElFTkSuQmCC\" style=\"height:19px; width:52px\" /></span></span></span></span></p>\r\n\r\n<p><span style=\"font-size:11.0pt\"><span style=\"font-family:&quot;Calibri&quot;,sans-serif\"><span style=\"color:black\">Zn<sub>(aq) </sub>| Zn<sup>2+</sup><sub>(s) </sub>|| Ag<sup>+</sup><sub>(s)</sub> || Aq<sub>(aq</sub></span></span></span></p>\r\n', NULL, '2020-04-24 09:17:13', '2020-04-24 09:17:13'),
(164, '41', '<p><span style=\"font-size:11pt\"><span style=\"font-family:Calibri,sans-serif\"><span style=\"color:black\">Zn<sub>(s) </sub>| Ag<sup>+</sup><sub>(aq) </sub>|| Zn<sup>2+</sup><sub>(aq) </sub>| Ag<sub>(s)</sub></span></span></span></p>\r\n', NULL, '2020-04-24 09:17:13', '2020-04-24 09:17:13'),
(165, '42', '<p><img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACUAAAATCAIAAACY31PkAAAAAXNSR0IArs4c6QAAAAlwSFlzAAAOxAAADsQBlSsOGwAAARFJREFUSEvtVckRwyAMNCmL9KO0g5tRiiG9ODo4FvzwJOPxy7w0gNhLHodt25YL1+NCLIW68c41HP18v0J4rp9zAebXZD7LYtKzmHLf+qXSduKjjqVeyCnGpD1/Ax5B2XnFEzglN5GU3SI5M1mpl3LqtT3i10Bd75NT66zKC57g+A4CMhFnbSVZLDbbIdbdPjlqxgiflIWg7hQdTbvjCYVKYI4BLZ5qcN7SGIM3jVGQB58Nr6mbFbpVlQrWqGe81Z7H66hv5oYKtacSn2sYxoGwv60h917A21sBgCgD6gJtQfXQLW8Hk9KyswiZUgt68a9uv4z9OD0wgTZzLZwyjn3iyjQM+y4x6Ddx4br/D+ea/QUrY/JaCVrkHgAAAABJRU5ErkJggg==\" width=\"37\" height=\"19\" />H = +ve, <img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACUAAAATCAIAAACY31PkAAAAAXNSR0IArs4c6QAAAAlwSFlzAAAOxAAADsQBlSsOGwAAARFJREFUSEvtVckRwyAMNCmL9KO0g5tRiiG9ODo4FvzwJOPxy7w0gNhLHodt25YL1+NCLIW68c41HP18v0J4rp9zAebXZD7LYtKzmHLf+qXSduKjjqVeyCnGpD1/Ax5B2XnFEzglN5GU3SI5M1mpl3LqtT3i10Bd75NT66zKC57g+A4CMhFnbSVZLDbbIdbdPjlqxgiflIWg7hQdTbvjCYVKYI4BLZ5qcN7SGIM3jVGQB58Nr6mbFbpVlQrWqGe81Z7H66hv5oYKtacSn2sYxoGwv60h917A21sBgCgD6gJtQfXQLW8Hk9KyswiZUgt68a9uv4z9OD0wgTZzLZwyjn3iyjQM+y4x6Ddx4br/D+ea/QUrY/JaCVrkHgAAAABJRU5ErkJggg==\" width=\"37\" height=\"19\" />V = 0, P<sub>Total</sub> =P<sup>0</sup><sub>A</sub>x<sub>A</sub> + P<sup>0</sup><sub>B</sub> x<sub>B</sub></p>\r\n<p>Zn<sub>(s) </sub>| Zn<sup>2+ </sup><sub>(aq) </sub>|| Ag<sup>+</sup><sub>(aq) </sub>| Ag<sub>(s)</sub></p>', NULL, '2020-04-24 09:24:30', '2020-04-24 09:24:30'),
(166, '42', '<p>Zn<sup>2+</sup> | Zn || Ag | Ag<sup>+</sup></p>', NULL, '2020-04-24 09:24:30', '2020-04-24 09:24:30'),
(167, '42', '<p><img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADQAAAATCAIAAABQs7kyAAAAAXNSR0IArs4c6QAAAAlwSFlzAAAOxAAADsQBlSsOGwAAAU9JREFUSEvtVssRgjAQDdaiHhwrgApsJDRiAXDEEjx5MXRgBZxMeon7SQgICiPOwIF3yrhm9+3btxkia61YKjZLJYa8VnKjp2PKNIkQSZobuAWeWwp0FseZBjZaSSGksh1y8A8KzAoggRTeySHnucnpTLI67YUo0/NBgXTVEyc+C0ye3vdFcaLijfE5MUE8Hv00QP9gkA5QE7IOFQF3+SNWqzWzCsUL5Bw1C79/IsdJe/HeDpTB2t7imNQbGetqTCWlzJQOoXb6JjlowcnlVmWabny7TtppGIsMzscpx3sQ8J91reXqcCPhBp8EWgiTn6vgMrzYvxEm5yeyBwk9mm2Ut0t82GH+ZyWO+22Imvv1waHvYHs2u0CPDio+YuhBLj6B+b0CI3dOuHUBj3qbUDPwCE5b2OZjTue4poYrMGKo1kbrJ9OQtz7E10+mH4UTL3TD08d3yqOEAAAAAElFTkSuQmCC\" width=\"52\" height=\"19\" /></p>\r\n<p>Zn<sub>(aq) </sub>| Zn<sup>2+</sup><sub>(s) </sub>|| Ag<sup>+</sup><sub>(s)</sub> || Aq<sub>(aq)</sub></p>', NULL, '2020-04-24 09:24:30', '2020-04-24 09:24:30'),
(168, '42', '<p>Zn<sub>(s) </sub>| Ag<sup>+</sup><sub>(aq) </sub>|| Zn<sup>2+</sup><sub>(aq) </sub>| Ag<sub>(s)</sub></p>', NULL, '2020-04-24 09:24:30', '2020-04-24 09:24:30'),
(169, '43', '<table style=\"width: 179pt;\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><colgroup><col style=\"width: 179pt;\" /></colgroup>\r\n<tbody>\r\n<tr style=\"height: 33.75pt;\">\r\n<td style=\"width: 179pt;\">Cu + Hg<span><sup>2+ </sup></span><span>-&gt; Cu</span><span><sup>2+ </sup></span><span>+ Hg</span></td>\r\n</tr>\r\n</tbody>\r\n</table>', NULL, '2020-04-24 09:34:17', '2020-04-24 09:34:17'),
(170, '43', '<table style=\"width: 168pt;\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><colgroup><col style=\"width: 168pt;\" /></colgroup>\r\n<tbody>\r\n<tr style=\"height: 33.75pt;\">\r\n<td style=\"width: 168pt;\">Hg + Cu<span><sup>2+</sup></span><span> -&gt; Hg</span><span><sup>2+</sup></span><span> + Cu</span></td>\r\n</tr>\r\n</tbody>\r\n</table>', NULL, '2020-04-24 09:34:17', '2020-04-24 09:34:17'),
(171, '43', '<table style=\"width: 174pt;\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><colgroup><col style=\"width: 174pt;\" /></colgroup>\r\n<tbody>\r\n<tr style=\"height: 33.75pt;\">\r\n<td style=\"width: 174pt;\">Hg + Cu<span><sup>2+ </sup></span><span>-&gt; Cu</span><span><sup>+ </sup></span><span>+ Hg</span><span><sup>+</sup></span></td>\r\n</tr>\r\n</tbody>\r\n</table>', NULL, '2020-04-24 09:34:17', '2020-04-24 09:34:17'),
(172, '43', '<table style=\"width: 195pt;\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><colgroup><col style=\"width: 195pt;\" /></colgroup>\r\n<tbody>\r\n<tr style=\"height: 33.75pt;\">\r\n<td style=\"width: 195pt;\">Cu + Hg -&gt; CuHg</td>\r\n</tr>\r\n</tbody>\r\n</table>', NULL, '2020-04-24 09:34:17', '2020-04-24 09:34:17'),
(193, '49', '<table style=\"border-collapse: collapse; width: 179pt;\" border=\"0\" width=\"239\" cellspacing=\"0\" cellpadding=\"0\">\r\n<tbody>\r\n<tr style=\"height: 33.75pt;\">\r\n<td class=\"xl65\" style=\"height: 33.75pt; width: 179pt;\" width=\"239\" height=\"45\">Cu + Hg<span class=\"font5\" style=\"\"><sup>2+ </sup></span><span class=\"font0\" style=\"\">-&gt; Cu</span><span class=\"font5\" style=\"\"><sup>2+ </sup></span><span class=\"font0\" style=\"\">+ Hg</span></td>\r\n</tr>\r\n</tbody>\r\n</table>', NULL, '2020-04-24 10:29:43', '2020-04-24 10:29:43'),
(194, '49', '<table style=\"border-collapse: collapse; width: 168pt;\" border=\"0\" width=\"224\" cellspacing=\"0\" cellpadding=\"0\">\r\n<tbody>\r\n<tr style=\"height: 33.75pt;\">\r\n<td style=\"height: 33.75pt; width: 168pt;\" width=\"224\" height=\"45\">Hg + Cu<span class=\"font5\" style=\"\"><sup>2+</sup></span><span class=\"font0\" style=\"\"> -&gt; Hg</span><span class=\"font5\" style=\"\"><sup>2+</sup></span><span class=\"font0\" style=\"\"> + Cu</span></td>\r\n</tr>\r\n</tbody>\r\n</table>', NULL, '2020-04-24 10:29:43', '2020-04-24 10:29:43'),
(195, '49', '<table style=\"border-collapse: collapse; width: 174pt;\" border=\"0\" width=\"232\" cellspacing=\"0\" cellpadding=\"0\">\r\n<tbody>\r\n<tr style=\"height: 33.75pt;\">\r\n<td class=\"xl65\" style=\"height: 33.75pt; width: 174pt;\" width=\"232\" height=\"45\">Hg + Cu<span class=\"font5\" style=\"\"><sup>2+ </sup></span><span class=\"font0\" style=\"\">-&gt; Cu</span><span class=\"font5\" style=\"\"><sup>+ </sup></span><span class=\"font0\" style=\"\">+ Hg</span><span class=\"font5\" style=\"\"><sup>+</sup></span></td>\r\n</tr>\r\n</tbody>\r\n</table>', NULL, '2020-04-24 10:29:43', '2020-04-24 10:29:43'),
(196, '49', '<table style=\"border-collapse: collapse; width: 195pt;\" border=\"0\" width=\"260\" cellspacing=\"0\" cellpadding=\"0\">\r\n<tbody>\r\n<tr style=\"height: 33.75pt;\">\r\n<td class=\"xl65\" style=\"height: 33.75pt; width: 195pt;\" width=\"260\" height=\"45\">Cu + Hg -&gt; CuHg</td>\r\n</tr>\r\n</tbody>\r\n</table>', NULL, '2020-04-24 10:29:43', '2020-04-24 10:29:43');

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE `students` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `class` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `place` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `dob` date NOT NULL,
  `yoa` int(11) NOT NULL,
  `center` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `school` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `parent_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `rws` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `parent_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `parent_phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) DEFAULT NULL,
  `added_by` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `pass_change_status` varchar(10) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`id`, `name`, `email`, `class`, `place`, `address`, `phone`, `dob`, `yoa`, `center`, `school`, `parent_name`, `rws`, `parent_email`, `parent_phone`, `password`, `status`, `added_by`, `remember_token`, `created_at`, `updated_at`, `pass_change_status`) VALUES
(1, 'Student', 'admin@saltu.in', 'Plus Two', '', 'Saltu Creative Suite, Nangiarkulangara, Alappuzha', '7012525741', '0000-00-00', 0, '1', '', '', '', '', '', '$2y$10$gnp2q6gW4gwkd.Rxn3NWIu/6ZwuXgKec4bT7JRIER1Kf6Q6eFvfBe', 0, '', 'Qo2JKExvDkVLf2seU63nQHIOelGCmpJhMloEzPiZUQUEq7Q4UGxup51wVrmJ', '2019-09-16 14:23:33', '2020-04-03 16:40:12', '1'),
(3, 'Akash Sanjeev', 'akashsanjeev@saltu.in', 'plus two', 'nangiarkulangara', 'Akkubhavan, Harripad, pallipad po kerala', '9446854520', '1995-10-12', 0, '5', 'Amritha', 'Sanjeev Shankar', 'son', 'SanjeevShankar@gmail.com', '9495305555', '$2y$10$GSf/PbycgQnvLh/e6Q25h.sJtjGzLyKQih8M2ea.F1JR08rxnGAdu', 0, 'center', NULL, '2019-11-02 07:43:45', '2019-11-02 07:43:45', ''),
(5, 'noufal', 'noufal@saltu.in', 'plus two', 'Haripad', 'Majeed house', '7012525743', '1996-01-15', 2019, '5', 'Holy trinity', 'Majeedh', 'son', 'majeed@gmail.com', '9495305555', '$2y$10$NLIhgtpdG4jjB4lE8AMQ9OBpweqaG2AqkDOyMK.37dYj4.qGP8mGW', 0, 'student', NULL, '2019-12-17 06:37:25', '2019-12-17 06:37:25', ''),
(6, 'Rajmohan', 'raj@saltu.in', 'plus one', 'Haripad', 'creative suite', '9446854523', '1996-12-14', 2020, '1', 'boys haripad', 'Mohan', 'son', 'mohan@saltu.in', '9495305554', '$2y$10$im1D/qlU7RDXtbOQNx7dq.reUrn8doqFET0VHn6P1Ka5IgnDTg9w2', 0, 'student', 'lGkYGBJwgi7un8ZOemgSZyqsbSseSSjldyLiscZRulNGkWHHew64948Jt9cJ', '2020-02-13 08:58:45', '2020-02-13 09:00:27', '0'),
(7, 'Prithviraj.K.R', 'prithviraj@saltu.in', 'plus one', 'muthukulam', 'Kochukalakkatu House, Muthukulam North', '7012525741', '1996-08-15', 2020, '1', 'KVSHSS', 'Rajendran', 'son', 'rajendran@gmail.com', '9446858804', '$2y$10$ZACFetRZ1TtjaZyCgjLnwu0aJCeKEkbAH2Le43OrsklqBPWZaPIzG', 1, 'admin', NULL, '2020-02-14 07:57:20', '2020-02-14 07:57:20', '0'),
(8, 'Sainath', 'sai@saltu.in', 'plus two', 'muthukulam', 'Koyikalethu house , Vettathumukku, Muthukulam, Alappuzha', '9995132132', '2020-04-14', 2020, '', 'SSVM', 'Vishvanadhan', 'son', 'vishvanadh@gmail.com', '9446858555', '$2y$10$7kNSkLIqVIEIwu5zzL19rOiZS/XLWYxli9mVPlGlWtfTuWLIvVlG.', 0, 'admin', 'V1JPD0yXFJamZwmnXoHWamzsiJjEsPhIVf1wlyzBL73uwGsTgWYMCt9iHnrG', '2020-04-01 15:23:07', '2020-04-02 18:32:28', '1');

-- --------------------------------------------------------

--
-- Table structure for table `students_approvals`
--

CREATE TABLE `students_approvals` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `added_from` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `school` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `center` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dob` date NOT NULL,
  `yoa` int(11) NOT NULL,
  `place` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `class` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `parent_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `rws` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `parent_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent_phone` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `student_exam_paids`
--

CREATE TABLE `student_exam_paids` (
  `id` int(10) UNSIGNED NOT NULL,
  `package_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `student_payments`
--

CREATE TABLE `student_payments` (
  `id` int(10) UNSIGNED NOT NULL,
  `amount` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `package_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `student_payments`
--

INSERT INTO `student_payments` (`id`, `amount`, `student_id`, `package_id`, `date`, `status`, `created_at`, `updated_at`) VALUES
(1, 500, 1, 1, '2020-01-27', 0, '2020-01-27 09:42:00', '2020-01-27 09:42:00'),
(2, 500, 8, 1, '2020-04-01', 0, '2020-04-01 16:09:07', '2020-04-01 16:09:07');

-- --------------------------------------------------------

--
-- Table structure for table `subjects`
--

CREATE TABLE `subjects` (
  `id` int(10) UNSIGNED NOT NULL,
  `class_id` int(11) NOT NULL,
  `examname_id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `added_by` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `subjects`
--

INSERT INTO `subjects` (`id`, `class_id`, `examname_id`, `name`, `description`, `added_by`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, '1,2', 'Chemistry', 'dummy', 1, 0, '2020-01-27 07:44:19', '2020-01-27 07:44:19'),
(2, 1, '1,2', 'Physics', '', 1, 0, '2020-01-28 07:19:39', '2020-01-28 07:19:39'),
(3, 1, '1', 'Maths', '', 1, 0, '2020-01-28 07:20:01', '2020-01-28 07:20:01'),
(4, 1, '2', 'Biology', '', 1, 0, '2020-01-28 07:20:24', '2020-01-28 07:20:24'),
(5, 2, '4', 'Biology', '', 1, 0, '2020-01-28 07:21:18', '2020-01-28 07:21:18'),
(6, 2, '3', 'Maths', '', 1, 0, '2020-01-28 07:21:32', '2020-01-28 07:21:32'),
(7, 2, '4,3', 'Chemistry', '', 1, 0, '2020-01-28 07:21:55', '2020-01-28 07:21:55'),
(8, 2, '4,3', 'Physics', '', 1, 0, '2020-01-28 07:22:22', '2020-01-28 07:22:22');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admins_email_unique` (`email`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `centers`
--
ALTER TABLE `centers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `centers_email_unique` (`email`);

--
-- Indexes for table `chapters`
--
ALTER TABLE `chapters`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `classes`
--
ALTER TABLE `classes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `exam_fees`
--
ALTER TABLE `exam_fees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `exam_names`
--
ALTER TABLE `exam_names`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `exam_results`
--
ALTER TABLE `exam_results`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `exam_type_packages`
--
ALTER TABLE `exam_type_packages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `question_details`
--
ALTER TABLE `question_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `question_options`
--
ALTER TABLE `question_options`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `students_email_unique` (`email`);

--
-- Indexes for table `students_approvals`
--
ALTER TABLE `students_approvals`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `students_approvals_email_unique` (`email`);

--
-- Indexes for table `student_exam_paids`
--
ALTER TABLE `student_exam_paids`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student_payments`
--
ALTER TABLE `student_payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subjects`
--
ALTER TABLE `subjects`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `centers`
--
ALTER TABLE `centers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `chapters`
--
ALTER TABLE `chapters`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `classes`
--
ALTER TABLE `classes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `exam_fees`
--
ALTER TABLE `exam_fees`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `exam_names`
--
ALTER TABLE `exam_names`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `exam_results`
--
ALTER TABLE `exam_results`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `exam_type_packages`
--
ALTER TABLE `exam_type_packages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT for table `question_details`
--
ALTER TABLE `question_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT for table `question_options`
--
ALTER TABLE `question_options`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=197;

--
-- AUTO_INCREMENT for table `students`
--
ALTER TABLE `students`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `students_approvals`
--
ALTER TABLE `students_approvals`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `student_exam_paids`
--
ALTER TABLE `student_exam_paids`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `student_payments`
--
ALTER TABLE `student_payments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `subjects`
--
ALTER TABLE `subjects`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
