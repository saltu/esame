<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExamResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exam_results', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('exam_id');
            $table->string('test_type');
            $table->string('chapter_id');
            $table->date('date');
            $table->integer('stud_id')->nullable();
            $table->string('q_id',1000)->nullable();
            $table->string('op_id',1000)->nullable();
            $table->string('result',1000)->nullable();
            $table->integer('p_id')->nullable();
            $table->integer('t_attend')->nullable();
            $table->integer('t_true')->nullable();
            $table->integer('t_question')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('exam_results');
    }
}
