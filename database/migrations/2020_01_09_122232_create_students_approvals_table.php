<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentsApprovalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students_approvals', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('added_from');
            $table->string('email')->unique();
            $table->string('school')->nullable();
            $table->string('phone',12);
            $table->string('center')->nullable();
            $table->date('dob');
            $table->integer('yoa');
            $table->string('place');
            $table->string('class');
            $table->string('parent_name');
            $table->string('rws');
            $table->string('parent_email')->nullable();
            $table->string('parent_phone',12);
            $table->string('address');
            $table->integer('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('students_approvals');
    }
}
