@extends('layouts.app')

@section('content')
<section class="banner">
    <div class="banner-img"><img src="{{ asset('resources/assets/images/banner/banner-img1.jpg')}}" alt=""></div>

                <div class="banner-text">
                <div class="container">
                    <h1>Prepare for exams  the smart way</h1>
                    <p>Join us today to know how  you can ace entrance exams  </p>
                    <div class="learning-btn">
                        <a href="#" class="btn">Explore</a>
                    </div>
                </div>
            </div>
</section>
<section class="our-course">
    <div class="container">
        <div class="section-title">
            <h2>Popular Courses</h2>
        </div>
        <div class="row">
        <div class="col-md-8">
            <div class="col-md-4">
                <div class="course-box">
                    <div class="img">
                        <img src="{{ asset('resources/assets/images/courses/courses-img1.jpg')}}" alt="">
                    </div>
                    <div class="boxz">
                        <h3 class="fnt">Enginerring By JEE</h3>
                        <p class="mrgn">
                                Lorem Ipsum is simply dummy text of the printing and typesetting industry
                        </p>
                    </div>  
        <!-- Star Rating HTML -->
                    <div class="starRating boxz">
                        <span><i class="fa fa-star-o fa-lg"></i><i class="fa fa-star fa-lg"></i></span>
                        <span><i class="fa fa-star-o fa-lg"></i><i class="fa fa-star fa-lg"></i></span>
                        <span><i class="fa fa-star-o fa-lg"></i><i class="fa fa-star fa-lg"></i></span>
                        <span><i class="fa fa-star-o fa-lg"></i><i class="fa fa-star fa-lg"></i></span>
                        <span><i class="fa fa-star-o fa-lg"></i><i class="fa fa-star fa-lg"></i></span>
                        <div class="box stu"><i class="fa fa-users"></i>  35 Student</div>
                    </div>
                    <a href="#">
                    <div class="comment-rows gradent look">
                        <div class="price-style">
                        <p class="fnts"> $100</p>
                        <p class="stuz">$80</p>
                        <i class="fa fa-arrow-circle-right fa-2x bdn" ></i>
                        </div>
                    </div>
                    </a>
                </div>
            </div>

           <div class="col-md-4">
                <div class="course-box">
                    <div class="img">
                        <img src="{{ asset('resources/assets/images/courses/courses-img2.jpg')}}" alt="">
                    </div>
                    <div class="boxz">
                        <h3 class="fnt">Medical By NEET</h3>
                        <p class="mrgn">
                                Lorem Ipsum is simply dummy text of the printing and typesetting industry
                        </p>
                    </div>  
        <!-- Star Rating HTML -->
                    <div class="starRating boxz">
                        <span><i class="fa fa-star-o fa-lg"></i><i class="fa fa-star fa-lg"></i></span>
                        <span><i class="fa fa-star-o fa-lg"></i><i class="fa fa-star fa-lg"></i></span>
                        <span><i class="fa fa-star-o fa-lg"></i><i class="fa fa-star fa-lg"></i></span>
                        <span><i class="fa fa-star-o fa-lg"></i><i class="fa fa-star fa-lg"></i></span>
                        <span><i class="fa fa-star-o fa-lg"></i><i class="fa fa-star fa-lg"></i></span>
                        <div class="box stu"><i class="fa fa-users"></i>  35 Student</div>
                    </div>
                    <a href="#">
                    <div class="comment-rows gradent look">
                        <div class="price-style">
                        <p class="fnts"> $100</p>
                        <p class="stuz">$80</p>
                        <i class="fa fa-arrow-circle-right fa-2x bdn" ></i>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-md-4">
                <div class="course-box">
                    <div class="img">
                        <img src="{{ asset('resources/assets/images/courses/courses-img3.jpg')}}" alt="">
                    </div>
                    <div class="boxz">
                        <h3 class="fnt">Commerce By CAT</h3>
                        <p class="mrgn">
                                Lorem Ipsum is simply dummy text of the printing and typesetting industry
                        </p>
                    </div>  
        <!-- Star Rating HTML -->
                    <div class="starRating boxz">
                        <span><i class="fa fa-star-o fa-lg"></i><i class="fa fa-star fa-lg"></i></span>
                        <span><i class="fa fa-star-o fa-lg"></i><i class="fa fa-star fa-lg"></i></span>
                        <span><i class="fa fa-star-o fa-lg"></i><i class="fa fa-star fa-lg"></i></span>
                        <span><i class="fa fa-star-o fa-lg"></i><i class="fa fa-star fa-lg"></i></span>
                        <span><i class="fa fa-star-o fa-lg"></i><i class="fa fa-star fa-lg"></i></span>
                        <div class="box stu"><i class="fa fa-users"></i>  35 Student</div>
                    </div>
                    <a href="#">
                    <div class="comment-rows gradent look">
                        <div class="price-style">
                        <p class="fnts"> $100</p>
                        <p class="stuz">$80</p>
                        <i class="fa fa-arrow-circle-right fa-2x bdn" ></i>
                        </div>
                    </div>
                    </a>
                </div>
            </div>

            
        </div>
        <div class="col-md-4">
            <div class="col-md-12">
                <div class="course-box">
                <div id="myCarousel" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                  <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                  <li data-target="#myCarousel" data-slide-to="1"></li>
                  <li data-target="#myCarousel" data-slide-to="2"></li>
                </ol>
                <!-- Wrapper for slides -->
                <div class="carousel-inner">
                  <div class="item active">
                    <img src="https://www.w3schools.com/bootstrap/la.jpg" alt="Los Angeles" style="width:100%; height: 356px;">
                  </div>

                  <div class="item">
                    <img src="https://www.w3schools.com/bootstrap/chicago.jpg" alt="Chicago" style="width:100%; height: 356px;">
                  </div>
                
                  <div class="item">
                    <img src="https://www.w3schools.com/bootstrap/ny.jpg" alt="New york" style="width:100%; height: 356px;">
                  </div>
                </div>

              </div>
            </div>
            </div>
        </div>
        </div>
    </div>
</section>

<section class="student-feedback preparation ">
    <div class="container">
        <div class="section-title">
            <h2 class="sldr2">Maximize your preparation</h2>
        </div>
        <div class="feedback-slider">
            <div class="item">
                <div class="student-name">Highest Quality Content by IIM Alimini</div>
                <p class="sldr2"><i class="fa fa-quote-left "></i> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum <i class="fa fa-quote-right"></i> </p>
            </div>
            <div class="item">
                <div class="student-name">Highest Quality Content by IIM Alimini</div>
                <p class="sldr2"><i class="fa fa-quote-left"></i> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum <i class="fa fa-quote-right"></i> </p>
            </div>
        </div>
    </div>
</section>

        <section class="testimonial-page">
            <div class="container">
                <div class="section-title">
                    <h2>Testimonial style1</h2>
                </div>
                <div class="testimonial-view">
                    <div class="item">
                        <div class="testimonial-box">
                            <div class="img"><img src="resources/assets/images/user-img/testimonial-img1.jpg" alt=""></div>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, </p>
                            <div class="name">when an unknown</div>
                            <div class="designation">by accident</div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="testimonial-box">
                            <div class="img"><img src="resources/assets/images/user-img/testimonial-img1.jpg" alt=""></div>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, </p>
                            <div class="name">when an unknown</div>
                            <div class="designation">by accident</div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="testimonial-box">
                            <div class="img"><img src="resources/assets/images/user-img/testimonial-img1.jpg" alt=""></div>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, </p>
                            <div class="name">when an unknown</div>
                            <div class="designation">by accident</div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="testimonial-box">
                            <div class="img"><img src="resources/assets/images/user-img/testimonial-img1.jpg" alt=""></div>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, </p>
                            <div class="name">when an unknown</div>
                            <div class="designation">by accident</div>
                        </div>
                    </div>
                </div>
                
            </div>
        </section>

<section class="start-learning">
    <div class="container">
        <p>I've read enough. Take me to Exam Formula</p>
        <a href="#" class="btn">Start learning now</a>
    </div>
</section>
@endsection