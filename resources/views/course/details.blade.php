@extends('layouts.app')
@section('content')
<!--  <section class="banner inner-page">
    <div class="banner-img"><img src="images/banner/courses-banner.jpg" alt=""></div>
    <div class="page-title">    
        <div class="container">
            <h1>courses details Free Courses</h1>
        </div>
    </div>
</section> -->


<?php
        if ($cls_id != '')
        {
            $clas_name = DB::table('classes')
                            ->where('id', $cls_id)
                            ->first()
                            ->class;
        }
	   if ($cat_id != '')
        {
            $cat_name = DB::table('categories')
                            ->where('id',$cat_id)
                            ->first()
                            ->name;
        } 
        if ($exam_id != '')
        {
            $exm_name = DB::table('exam_types')
                            ->where('id',$exam_id)
                            ->first()
                            ->name;
        }
?>


<section class="breadcrumb white-bg">
    <div class="container">
        <ul>
            <li><a href="{{ url('/')}}">Home</a></li>
            <li><a href="{{ route('course',$cls_id ) }}">Courses</a></li>
            <li><a href="{{route('course',$cls_id)}}">{{$clas_name}}</a></li>
            <li><a href="{{ route('course.categories',[$cls_id ,$cat_id])}}">{{$cat_name}}</a></li>
            <li><a href="{{ route('course.details',[$cls_id ,$cat_id,$exam_id,$subj_id])}}">{{$exm_name}}</a></li>
            <li><a href="#">Exam details</a></li>
        </ul>
    </div>
</section>

<?php 
    $examtype = DB::table('exam_types')
                ->select('*')
                ->where('id',$exam_id)
                ->get();
?>

<?php $i = 1; ?>
@if(count($examtype) > 0)
    @foreach($examtype as $exam)

<section class="courses-view">
<div class="course-details">
    <div class="container">
        <div class="row">
            <div class="col-sm-8 col-md-9">
                <h2>{{ $exam -> name}} </h2>
                <div class="course-details-main">
                    <div class="course-img">
                        <img src="{{ asset('resources/assets/images/courses/courses-img8.jpg') }}" alt="">
                    </div>
                </div>
                <div class="info">
                    <h4>Course Overview</h4>
                    <p>{{ $exam -> description}} </p>
                    <p> </p>
                </div>
                
                <div class="row">
                    <div class="col-md-3">
                        <div class="right-slide left">
                            <h3>Subjects</h3>
                                <ul class="catagorie-list">
                                <?php 
                                    $exams = DB::table('exams')
                                                ->select('*')
                                                ->where('type_id',$exam_id)
                                                ->get();
                                ?>
                                <?php $i = 1; ?>
                                @if(count($exams) > 0)
                                    @foreach($exams as $examss)
                                        <li><a href="{{ route('course.details',[$cls_id ,$cat_id,$exam_id,$examss->id ])}}">{{ $examss->name }}</a></li>    
                                    @endforeach
                                 @endif
                                </ul>
                            <h3>Price</h3>
                            <div class="filter-blcok">
                                <div class="check-slide">
                                    <label class="label_check" for="checkbox-01"><input id="checkbox-01" type="checkbox"> Free</label>
                                </div>
                                <div class="check-slide">
                                    <label class="label_check" for="checkbox-02"><input id="checkbox-02" type="checkbox"> Paid</label>
                                </div>
                            </div>
                        </div>
                    </div>

                    
                    <div class="col-md-9">                
                        <div class="row">
                            <?php 
                                    $exam_titles = DB::table('exam_titles')
                                                ->select('*')
                                                ->where('subject_id',$subj_id)
                                                ->get();
                                ?>
                                <?php $i = 1; ?>
                                @if(count($exam_titles) > 0)
                                    @foreach($exam_titles as $exam_title)
                                        <div class="col-sm-6 col-md-4" id="" >
                                            <div class="course-post">
                                                <div class="img">
                                                    <img src="{{ asset('resources/assets/images/courses/courses-img4.jpg') }}" alt="">   
                                                    <div class="icon">
                                                        <a href="#"><img src="{{ asset('resources/assets/images/book-icon.png') }}" alt=""></a>
                                                    </div>
                                                </div>
            <!--                                    <div class="info">-->
            <!--                                        <div class="name">name</div>-->
            <!--                                        <div class="expert"><span> dummy </span> dummy </div>-->
            <!--                                    </div>-->
                                                <div class="name" style="text-align:center"> {{ $exam_title->name }} </div>
                                                <div class="product-footer">
                                                    <div class="comment-box">   
                                                        <div class="box"><i class="fa fa-users"></i> Enrolled or Not</div>
                                                    </div>
                                                    <div class="view-btn">
                                                        <a href="{{ route('course.exams',[$cls_id ,$cat_id,$exam_id,$subj_id,$exam_title->id])}}" class="btn">view more</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>   
                                    @endforeach
                                 @endif
                        </div>
                        <div class="pagination">
                            <ul>
                                <li class="next"><a href="#"><i class="fa fa-angle-left"></i><span>Next</span></a></li>
                                <li class="active"><a href="#">1</a></li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                                <li><a href="#">4</a></li>
                                <li class="prev"><a href="#"><span>prev</span> <i class="fa fa-angle-right"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                
                <div class="syllabus">
                    <h4>Syllabus</h4>
                    <div class="syllabus-box">
                        <div class="syllabus-title">1st lesson</div>
                        <div class="syllabus-view first">
                            <div class="main-point active">Lorem Ipsum is simply dummy text of the printing and typesetting</div>
                            <div class="point-list">
                                <ul>
                                    <li><a href="course-lessons.html">vedio : Lorem ipsum dolor sit amet. <span class="hover-text">Watch Video<i class="fa fa-angle-right"></i></span></a></li>
                                    <li><a href="course-lessons.html">Document : semper dolor quis lectus facilisis laoreet. <span class="hover-text">Let's Go<i class="fa fa-angle-right"></i></span></a></li>
                                    <li><a href="quiz-intro.html">Quizzes : auctor quam quis commodo feugiat. <span class="hover-text">upgrade now<i class="fa fa-angle-right"></i></span></a></li>
                                    <li class="no-link">There are many variations of passages of Lorem Ipsum available, </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="syllabus-box">
                        <div class="syllabus-title">2st lesson</div>
                        <div class="syllabus-view">
                            <div class="main-point">It is a long established fact that a reader will be distracted by the</div>
                            <div class="point-list">
                                <ul>
                                    <li><a href="course-lessons.html">Lessons : auctor quam quis commodo feugiat. <span class="hover-text">understand now<i class="fa fa-angle-right"></i></span></a></li>                                  
                                    <li><a href="course-lessons.html">vedio : Lorem ipsum dolor sit amet. <span class="hover-text">Watch Video<i class="fa fa-angle-right"></i></span></a></li>
                                    <li><a href="course-lessons.html">Document : semper dolor quis lectus facilisis laoreet. <span class="hover-text">Let's Go<i class="fa fa-angle-right"></i></span></a></li>
                                    <li><a href="quiz-intro.html">Quizzes : auctor quam quis commodo feugiat. <span class="hover-text">upgrade now<i class="fa fa-angle-right"></i></span></a></li>

                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="syllabus-box">
                        <div class="syllabus-title">3st lesson</div>
                        <div class="syllabus-view">
                            <div class="main-point">readable content of a page when looking at its layout. The point of </div>
                            <div class="point-list">
                                <ul>
                                    <li><a href="course-lessons.html">Lessons : auctor quam quis commodo feugiat. <span class="hover-text">understand now<i class="fa fa-angle-right"></i></span></a></li>                                  
                                    <li><a href="course-lessons.html">vedio : Lorem ipsum dolor sit amet. <span class="hover-text">Watch Video<i class="fa fa-angle-right"></i></span></a></li>
                                    <li><a href="course-lessons.html">Document : semper dolor quis lectus facilisis laoreet. <span class="hover-text">Let's Go<i class="fa fa-angle-right"></i></span></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="syllabus-box">
                        <div class="syllabus-title">4st lesson</div>
                        <div class="syllabus-view">
                            <div class="main-point">using Lorem Ipsum is that it has a more-or-less normal distribution </div>
                            <div class="point-list">
                                <ul>
                                    <li><a href="course-lessons.html">Lessons : auctor quam quis commodo feugiat. <span class="hover-text">understand now<i class="fa fa-angle-right"></i></span></a></li>                                  
                                    <li><a href="course-lessons.html">vedio : Lorem ipsum dolor sit amet. <span class="hover-text">Watch Video<i class="fa fa-angle-right"></i></span></a></li>
                                    <li><a href="quiz-intro.html">Quizzes : auctor quam quis commodo feugiat. <span class="hover-text">upgrade now<i class="fa fa-angle-right"></i></span></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="syllabus-box">
                        <div class="syllabus-title">5st lesson</div>
                        <div class="syllabus-view">
                            <div class="main-point">of letters, as opposed to using 'Content here, content here', </div>
                            <div class="point-list">
                                <ul>
                                    <li><a href="course-lessons.html">Lessons : auctor quam quis commodo feugiat. <span class="hover-text">understand now<i class="fa fa-angle-right"></i></span></a></li>                                  
                                    <li><a href="course-lessons.html">Document : semper dolor quis lectus facilisis laoreet. <span class="hover-text">Let's Go<i class="fa fa-angle-right"></i></span></a></li>
                                    <li><a href="quiz-intro.html">Quizzes : auctor quam quis commodo feugiat. <span class="hover-text">upgrade now<i class="fa fa-angle-right"></i></span></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    
                </div>
                <div class="reviews">
                    <h4>Reviews</h4>
                    <div class="row">
                        <div class="col-sm-5">
                            <div class="rating-info">
                                <label>Detailed Rating</label>
                                <div class="rating-slide">
                                    <span>Stars 5</span>
                                    <div class="bar">
                                        <div class="fill" style="width:50%"></div>
                                    </div>
                                    <em>10</em>
                                </div>
                                <div class="rating-slide">
                                    <span>Stars 4</span>
                                    <div class="bar">
                                        <div class="fill" style="width:40%"></div>
                                    </div>
                                    <em>8</em>
                                </div>
                                <div class="rating-slide">
                                    <span>Stars 3</span>
                                    <div class="bar">
                                        <div class="fill" style="width:30%"></div>
                                    </div>
                                    <em>6</em>
                                </div>
                                <div class="rating-slide">
                                    <span>Stars 2</span>
                                    <div class="bar">
                                        <div class="fill" style="width:35%"></div>
                                    </div>
                                    <em>7</em>
                                </div>
                                <div class="rating-slide">
                                    <span>Stars 1</span>
                                    <div class="bar">
                                        <div class="fill" style="width:0"></div>
                                    </div>
                                    <em>0</em>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-5">
                            <label>Average Rating</label>
                            <div class="rating-box">
                                <div class="rating">4.3</div>
                                <div class="rating-star">
                                    <div class="fill" style="width:86%"></div>
                                </div>
                                <span>31 Ratings</span>
                            </div>
                        </div>
                    </div>
                    <div class="reviews-view">
                        <h4>Reviews List</h4>
                        <div class="reviews-slide">
                            <div class="img"><img src="images/user-img/student-img1.png" alt=""></div>
                            <div class="rating-star">
                                <div class="fill" style="width:82%"></div>
                            </div>
                            <span>5 Day ago</span>
                            <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you </p>
                        </div>
                        <div class="reviews-slide">
                            <div class="img"><img src="images/user-img/student-img1.png" alt=""></div>
                            <div class="rating-star">
                                <div class="fill" style="width:69%"></div>
                            </div>
                            <span>5 Month ago</span>
                            <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you </p>
                        </div>
                        <div class="reviews-slide">
                            <div class="img"><img src="images/user-img/student-img1.png" alt=""></div>
                            <div class="rating-star">
                                <div class="fill" style="width:70%"></div>
                            </div>
                            <span>2.5 Years ago</span>
                            <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 col-md-3">
                <div class="right-slide">
                    <h3>Course Details</h3>
                    <div class="course-details-list"> 
                        <ul>
                            <li><i class="fa fa-user-secret"></i>Rebecca Smith</li>
                            <li><i class="fa fa-file"></i>17 Lessons</li>
                            <li><i class="fa fa-exclamation"></i>7 Quizzes</li>
                            <li><i class="fa fa-file-text-o"></i>13 Documents</li>
                            <li><i class="fa fa-video-camera"></i>9 vedio</li>
                            <li><i class="fa fa-mortar-board"></i>1719 Students</li>
                            <li><i class="fa fa-calendar"></i>16-09-2016 - 15-08-2018 </li>
                        </ul>
                        <div class="price-info"><span>Price: </span>Free</div>
                        <?php 
                        if(Auth::guard('student')->user()->name != ""){
                        ?>
                        <div class="btn-block"> 
                            <a href="{{ route('student.exam',['cont','1']) }}" class="btn">ENROLL</a>
                        </div>
                        <?php
                        }
                        ?>
                    </div>
                    <h3>INSTRUCTORS</h3>
                    <div class="instructors-slider">
                        <div class="item">
                            <div class="teacher-box">
                                <div class="img">
                                    <img src="images/team-member/member-img8.jpg" alt="">
                                    <div class="sosiyal-mediya">
                                        <ul>
                                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="name">Lorem ipsum dolor sit amet, </div>
                                <div class="designation">consectetur </div>
                                <p>Vestibulum tincidunt nibh aliquam odio ultrices imperdiet.</p>
                            </div>
                        </div>
                        <div class="item">
                            <div class="teacher-box">
                                <div class="img">
                                    <img src="images/team-member/member-img7.jpg" alt="">
                                    <div class="sosiyal-mediya">
                                        <ul>
                                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="name">Lorem ipsum dolor sit amet, </div>
                                <div class="designation">consectetur </div>
                                <p>Vestibulum tincidunt nibh aliquam odio ultrices imperdiet.</p>
                            </div>
                        </div>
                    </div>
                    <h3>Working Hours</h3>
                    <ul class="working-list">
                        <li>Monday<span>10am - 7pm</span></li>
                        <li>Tuesday<span>10am - 7pm</span></li>
                        <li>Wednesday<span>10am - 7pm</span></li>
                        <li>Thursday<span>10am - 7pm</span></li>
                        <li>Friday<span>10am - 7pm</span></li>
                        <li>Saturday<span class="closed">Closed</span></li>
                        <li>Sunday<span class="closed ">Closed</span></li>
                    </ul>
                    <h3>Tags</h3>
                    <ul class="keyword-list">
                        <li><a href="#">Html</a></li>
                        <li><a href="#">Boostrap</a></li>
                        <li><a href="#">Css3</a></li>
                        <li><a href="#">Jquery</a></li>
                        <li><a href="#">Student</a></li>
                        <li><a href="#">Html</a></li>
                        <li><a href="#">Boostrap</a></li>
                        <li><a href="#">Css3</a></li>
                        <li><a href="#">Jquery</a></li>
                        <li><a href="#">Student</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
    @endforeach
 @endif 
@endsection