@extends('layouts.app')
@section('content')
 <section class="banner inner-page">
            <div class="banner-img">
                <img src="../resources/assets/images/banner/courses-banner.jpg" alt=""></div>
            <div class="page-title">    
                <div class="container">
                    <h1>Our Courses</h1>
                </div>
            </div>
        </section>
<?php
    $cate = array();
    if ($cls_id != ''){
        $clas_name = DB::table('classes')
						->select('class')
                        ->where('id',$cls_id)
                        ->first()
                        ->class;
        
    }
    if ($cat_id != ''){
        $cat_name = DB::table('categories')
						->select('name')
                        ->where('id',$cat_id)
                        ->first()
                        ->name;
    }
   else{
        $cat_name = "ALL";
    }
?>

<section class="breadcrumb white-bg">
    <div class="container">
        <ul>
            <li><a href="{{ url('/')}}">Home</a></li>
            <li><a href="{{ route('course',$cls_id ) }}">All courses</a></li>
            <li><a href="{{route('course',$cls_id)}}">{{ $clas_name}}</a></li>    
            <li><a href="{{ route('course.categories',[$cls_id ,$cat_id])}}"> {{$cat_name}}</a></li>    
        </ul>
    </div>
</section>
<!-- <section class="courses-view">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="right-slide left">
                    <h3>Classes</h3>
                    <ul class="catagorie-list">   
                    <?php $i = 1; ?>
                    @if(count($classes) > 0)
                        @foreach($classes as $class)
                            <li class="active"><a href="{{ route('course',$class->id) }}">{{ $class->class }}</a></li>    
                        @endforeach -->
                            <!-- <li><a href="{{ route('course','0') }}">All</a></li>  -->
<!--                      @endif 
                    </ul>
                    
                    <h3>Categories</h3>
                    <?php 
                        $categories = DB::table('categories')
                                    ->select('*')
                                    ->where('class_id',$cls_id)
                                    ->get();
                    ?>
                    <ul class="catagorie-list"> 
                    <?php $i = 1; ?>
                    @if(count($categories) > 0)
                        @foreach($categories as $categorie) 
                            <li><a href="{{ route('course.categories',[$cls_id ,$categorie->id]) }}">{{ $categorie->name }}</a></li> 
                            <?php
                                array_push($cate,$categorie->id);
                            ?>
                        @endforeach
                     @endif
                    </ul>
                </div>
            </div>
            <div class="col-md-9">                
                <div class="row">
                    <?php
                        if($cat_id == 0){
                            $examtypes = DB::table('exam_names')
                                    ->select('*')
                                    ->whereIn('id',$cate)
                                    ->get();
                           }
                        else{
                            $examtypes = DB::table('exam_names')
                                    ->select('*')
                                    ->where('category_id',$cat_id)
                                    ->get();
                        }

                    ?>
                    <?php $i = 1; ?>
                    @if(count($examtypes) > 0)
                        @foreach($examtypes as $examtype) 
                        <div class="col-sm-6 col-md-4" id="<?php echo $examtype->category_id; ?>" >
                            <div class="course-post">
                                <div class="img">
                                    <img src="{{ asset('resources/assets/images/courses/'.$examtype->image) }}" alt="">   
                                    <div class="icon">
                                        <a href="javascript:void(0)"><img src="{{ asset('resources/assets/images/book-icon.png') }}" alt=""></a>
                                    </div>
                                </div>
                                <div class="info">
                                    <div class="name">{{ $examtype->name }}</div>
                                    <div class="expert"></div>
                                </div>
                                <div class="product-footer">
                                    <div class="comment-box">   
                                        <div class="box">View More</div>
                                    </div>
                                    <div class="view-btn"> -->
<!--                                        <a href="{{ route('course.details',[$cls_id ,$cat_id ,$examtype->id, 2]) }}" class="btn">view more</a>-->
<!--                                         <a href="{{ route('student') }}" class="btn">view more</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    @else
                        <div class="col-sm-6 col-md-4" id="" >
                            <div class="section-title" style="width:800px; margin:0 auto;">
                                <h2>Select Categories</h2>
                            </div>
                        </div>
                     @endif                     
                </div> 
            </div>
        </div>
        
    </div>
</section>
<section>  -->
        <section>
            <div class="container">
                <div class="section-title con-magn">
                    <h2>Choose Examination</h2>
                </div> 
                <div class="row">
                    <div class="col-md-3">
                        <!-- <div class="bg-text">
                            
                            <div class="select-area">
                                <select id="select">
                                   <option value="default">Select Class</option>
                                   <option value="southern-terminus">+1</option>
                                   <option value="seth-warner-shelter">+2</option>
                                   <option value="seth-warner-shelter">+1 & +2</option>
                                 </select>
                                 <div class="select-arrow">
                                 </div>
                            </div>
                        </div> -->    
                        <div class="centerr">
                          <select name="sources" id="sources" class="custom-select sources" placeholder="Select Class">
                            <option value="profile">+1</option>
                            <option value="word">+2</option>
                            <option value="hashtag">+1 & +2</option>
                          </select>
                        </div>


                    </div>
                    <div class="col-md-3">
                        <!-- <div class="bg-text">
                            <div class="select-area">
                                <select id="select">
                                   <option value="default">Select Exam</option>
                                   <option value="southern-terminus">JEE</option>
                                   <option value="seth-warner-shelter">NEET</option>
                                   <option value="seth-warner-shelter">CAT</option>
                                 </select>
                                 <div class="select-arrow">
                                 </div>
                            </div>
                        </div> --> 

                        <div class="centerr">
                          <select name="sources" id="sources" class="custom-select sources" placeholder="Select Subject">
                            <option value="profile">JEE</option>
                            <option value="word">NEET</option>
                            <option value="hashtag">CAT</option>
                          </select>
                        </div>   
                    </div>
                </div>
            </div>
        </section>







<section class="our-course">
    <div class="container">
        <div class="row">
        <div class="col-md-12">
            <div class="col-md-4">
                <div class="course-box">
                    <div class="img">
                        <img src="{{ asset('resources/assets/images/courses/courses-img1.jpg')}}" alt="">
                    </div>
                    <div class="boxz">
                        <h3 class="fnt">Enginerring By JEE</h3>
                        <p class="mrgn">
                                Lorem Ipsum is simply dummy text of the printing and typesetting industry
                        </p>
                    </div>  
        <!-- Star Rating HTML -->
                    <div class="starRating boxz">
                        <span><i class="fa fa-star-o fa-lg"></i><i class="fa fa-star fa-lg"></i></span>
                        <span><i class="fa fa-star-o fa-lg"></i><i class="fa fa-star fa-lg"></i></span>
                        <span><i class="fa fa-star-o fa-lg"></i><i class="fa fa-star fa-lg"></i></span>
                        <span><i class="fa fa-star-o fa-lg"></i><i class="fa fa-star fa-lg"></i></span>
                        <span><i class="fa fa-star-o fa-lg"></i><i class="fa fa-star fa-lg"></i></span>
                        <div class="box stu"><i class="fa fa-users"></i>  35 Student</div>
                    </div>
                    <a href="#">
                    <div class="comment-rows gradent look">
                        <div class="price-style">
                        <p class="fnts"> $100</p>
                        <p class="stuz">$80</p>
                        <i class="fa fa-arrow-circle-right fa-2x bdn" ></i>
                        </div>
                    </div>
                    </a>
                </div>
            </div>

           <div class="col-md-4">
                <div class="course-box">
                    <div class="img">
                        <img src="{{ asset('resources/assets/images/courses/courses-img1.jpg')}}" alt="">
                    </div>
                    <div class="boxz">
                        <h3 class="fnt">Medical By NEET</h3>
                        <p class="mrgn">
                                Lorem Ipsum is simply dummy text of the printing and typesetting industry
                        </p>
                    </div>  
        <!-- Star Rating HTML -->
                    <div class="starRating boxz">
                        <span><i class="fa fa-star-o fa-lg"></i><i class="fa fa-star fa-lg"></i></span>
                        <span><i class="fa fa-star-o fa-lg"></i><i class="fa fa-star fa-lg"></i></span>
                        <span><i class="fa fa-star-o fa-lg"></i><i class="fa fa-star fa-lg"></i></span>
                        <span><i class="fa fa-star-o fa-lg"></i><i class="fa fa-star fa-lg"></i></span>
                        <span><i class="fa fa-star-o fa-lg"></i><i class="fa fa-star fa-lg"></i></span>
                        <div class="box stu"><i class="fa fa-users"></i>  35 Student</div>
                    </div>
                    <a href="#">
                    <div class="comment-rows gradent look">
                        <div class="price-style">
                        <p class="fnts"> $100</p>
                        <p class="stuz">$80</p>
                        <i class="fa fa-arrow-circle-right fa-2x bdn" ></i>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-md-4">
                <div class="course-box">
                    <div class="img">
                        <img src="{{ asset('resources/assets/images/courses/courses-img1.jpg')}}" alt="">
                    </div>
                    <div class="boxz">
                        <h3 class="fnt">Commerce By CAT</h3>
                        <p class="mrgn">
                                Lorem Ipsum is simply dummy text of the printing and typesetting industry
                        </p>
                    </div>  
        <!-- Star Rating HTML -->
                    <div class="starRating boxz">
                        <span><i class="fa fa-star-o fa-lg"></i><i class="fa fa-star fa-lg"></i></span>
                        <span><i class="fa fa-star-o fa-lg"></i><i class="fa fa-star fa-lg"></i></span>
                        <span><i class="fa fa-star-o fa-lg"></i><i class="fa fa-star fa-lg"></i></span>
                        <span><i class="fa fa-star-o fa-lg"></i><i class="fa fa-star fa-lg"></i></span>
                        <span><i class="fa fa-star-o fa-lg"></i><i class="fa fa-star fa-lg"></i></span>
                        <div class="box stu"><i class="fa fa-users"></i>  35 Student</div>
                    </div>
                    <a href="#">
                    <div class="comment-rows gradent look">
                        <div class="price-style">
                        <p class="fnts"> $100</p>
                        <p class="stuz">$80</p>
                        <i class="fa fa-arrow-circle-right fa-2x bdn" ></i>
                        </div>
                    </div>
                    </a>
                </div>
            </div>

            
        </div>
        </div>
    </div>
</section>
@endsection