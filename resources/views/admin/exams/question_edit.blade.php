@extends('admin.layouts.app')
@section('content')


<section class="b breadcrumb">
    <div class="container">
        <ul>
            <li><a href="#">Admin</a></li>
            <li><a href="#">Edit Question</a></li>
        </ul>
    </div>
</section>

@if(session()->has('message'))
    <div class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif
@if(session()->has('dmessage'))
    <div class="alert alert-danger">
        {{ session()->get('dmessage') }}
    </div>
@endif



<div class="sspage-content">
        <div class="ssform-v10-content">
            <div class="row">

            
            <form class="form-detail form-horizontal" method="POST" action="{{ url('admin/question_update') }}" >
            {{ csrf_field() }}
                <div class="form-left">
                <h2><center>Question</center></h2>
                    <div class="form-row{{ $errors->has('question') ? ' has-error' : '' }}">
                        <label class="mlb">Question</label>
                        <input type="hidden" name="id" class="street" id="id" value="{{ $questions[0]->id }}">
                        @if ($questions[0]->question != '')
                        <textarea name="question" class="street" id="question" placeholder="Question">{!! $questions[0]->question !!}</textarea>
                        @endif
                        @if ($questions[0]->q_image != null && $questions[0]->q_image != 'public/')
                        <img src="{{ asset($questions[0]->q_image) }}" />
                        @endif
                        @if ($errors->has('question'))
                            <span class="help-block">
                                <strong>{{ $errors->first('question') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-row{{ $errors->has('answer') ? ' has-error' : '' }}">
                        <label class="mlb">Answer</label>
                        @if ($q_options[0]->options != '')
                        <textarea name="answer" class="street" id="answer" placeholder="Answer">{!! $q_options[0]->options !!}</textarea>
                        @endif
                        <input type="hidden" name="danswer" class="street" readonly id="danswer" value="{{ $q_options[0]->id }}">
                        @if($q_options[0]->opt_image != null && $q_options[0]->opt_image != 'public/')
                        <img src="{{ asset($q_options[0]->opt_image) }}" />
                        @endif
                        @if ($errors->has('answer'))
                            <span class="help-block">
                                <strong>{{ $errors->first('answer') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-row{{ $errors->has('option1') ? ' has-error' : '' }}">
                        <label class="mlb">Option 1</label>
                        @if ($q_options[1]->options != '')
                        <textarea name="option1" class="additional" id="option1" placeholder="Option 1">{!! $q_options[1]->options !!}</textarea>
                        @endif
                        <input type="hidden" readonly name="doption1" class="additional" id="doption1" value="{{ $q_options[1]->id }}">
                        @if ($q_options[1]->opt_image != null && $q_options[1]->opt_image != 'public/')
                        <img src="{{ asset($q_options[1]->opt_image) }}" />
                        @endif
                        @if ($errors->has('option1'))
                            <span class="help-block">
                                <strong>{{ $errors->first('option1') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-row{{ $errors->has('option2') ? ' has-error' : '' }}">
                        <label class="mlb">Option 2</label>
                        @if ($q_options[2]->options != '')
                        <textarea name="option2" class="additional" id="option2" placeholder="Option 2">{!! $q_options[2]->options !!}</textarea>
                        @endif
                        <input type="hidden" name="doption2" readonly class="additional" id="doption2" value="{{ $q_options[2]->id }}">
                        @if ($q_options[2]->opt_image != null && $q_options[2]->opt_image != 'public/')
                        <img src="{{ asset($q_options[2]->opt_image) }}" />
                        @endif
                        @if ($errors->has('option2'))
                            <span class="help-block">
                                <strong>{{ $errors->first('option2') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-row{{ $errors->has('option3') ? ' has-error' : '' }}">
                        <label class="mlb">Option 3</label>
                        @if ($q_options[3]->options != '')
                        <textarea name="option3" class="additional" id="option3" placeholder="Option 3">{!! $q_options[3]->options !!}</textarea>
                        @endif
                        <input type="hidden" name="doption3" class="additional" readonly id="doption3" value="{{ $q_options[3]->id }}">
                        @if ($q_options[3]->opt_image != null && $q_options[3]->options != 'public/')
                        <img src="{{ asset($q_options[3]->opt_image) }}" />
                        @endif
                        @if ($errors->has('option3'))
                            <span class="help-block">
                                <strong>{{ $errors->first('option3') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-row{{ $errors->has('solution') ? ' has-error' : '' }}">
                        <label class="mlb">Description</label>
                        <textarea name="solution" class="street" id="solution" placeholder="Description (Optional)">{!! $questions[0]->description !!}</textarea>
                        @if ($errors->has('solution'))
                            <span class="help-block">
                                <strong>{{ $errors->first('solution') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-row-last">
                        <input type="submit" class="register" value="Update">
                    </div>   
                    
                </div>
               <!--  <div class="form-right">
                    <h2>Question</h2>
                    <div class="form-row{{ $errors->has('exam_category') ? ' has-error' : '' }}">
                        <input class="company" id="exam_category" type="text" name="exam_category" value="{{ old('exam_category') }}" placeholder="Exam Category">
                        @if ($errors->has('exam_category'))
                            <span class="help-block">
                                <strong>{{ $errors->first('exam_category') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-row{{ $errors->has('exam_type') ? ' has-error' : '' }}">
                        <input id="exam_type" type="text" name="exam_type" value="{{ old('exam_type') }}" class="company" placeholder="Exam Type">
                        @if ($errors->has('exam_type'))
                            <span class="help-block">
                                <strong>{{ $errors->first('exam_type') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-row{{ $errors->has('subject') ? ' has-error' : '' }}">
                        <input type="text" name="subject" class="company" id="subject" value="{{ old('subject') }}" placeholder="Subject">
                        @if ($errors->has('subject'))
                            <span class="help-block">
                                <strong>{{ $errors->first('subject') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-row{{ $errors->has('question') ? ' has-error' : '' }}">
                        <input type="text" name="question" class="company" id="question" value="{{ old('question') }}" placeholder="Question">
                        @if ($errors->has('question'))
                            <span class="help-block">
                                <strong>{{ $errors->first('question') }}</strong>
                            </span>
                        @endif
                    </div>

                </div> -->
            </form>
            </div>
    </div>
@endsection