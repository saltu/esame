@extends('admin.layouts.app')

@section('content')
<?php
            $classes = DB::table ('classes')
                        ->select('*')
                        ->orderBy('class','ASC')
                        ->get();
            $categories = DB::table ('categories')
                        ->select('*')
                        ->orderBy('name','ASC')
                        ->get();
            $titles = DB::table ('exam_names')
                        ->select('*')
                        ->orderBy('name','ASC')
                        ->get();
            $subjects = DB::table ('subjects')
                        ->select('*')
                        ->orderBy('name','ASC')
                        ->get();

            $categories1 = DB::table ('categories')
                        ->select('*')
                        ->orderBy('name','ASC')
                        ->groupby('name')
                        ->get();
            $titles1 = DB::table ('exam_names')
                        ->select('*')
                        ->orderBy('name','ASC')
                        ->groupby('name')
                        ->get();
            $subjects1 = DB::table ('subjects')
                        ->select('*')
                        ->orderBy('name','ASC')
                        ->groupby('name')
                        ->get();

            $level = DB::table ('chapters')
                        ->select('*')
                        ->orderBy('name','ASC')
                        ->get();
?>
<section class="b breadcrumb white-bg">
    <div class="container">
        <ul >
            <li><a href="#">Admin</a></li>
            <li><a href="#">Manage Exams</a></li>
        </ul>
    </div>
</section>
@if(session()->has('message'))
    <div class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif
@if(session()->has('dmessage'))
    <div class="alert alert-danger">
        {{ session()->get('dmessage') }}
    </div>
@endif

<div class="course-details">
    <div class="container">
        <div class="row">
            <div class="col-md-9 col-md-offset-2">
                <div class="syllabus">
                    <h4 class="cent text-center">Exam Management</h4>
                    <div class="syllabus-box">   
                        <div class="syllabus-view">
                            <div class="main-point">Add Class</div>
                            <div class="point-list">
                                <form class="form-horizontal" role="form" method="POST" action="{{ url('admin/exam_classes') }}">
                                {{ csrf_field() }}
                                    <div class="form-group{{ $errors->has('classes') ? ' has-error' : '' }}">
                                        <div class="col-md-7">
                                            <input type="text" autocomplete="off" name="classes" class="form-control" id="classes" value="{{ old('classes') }}" placeholder="Exam Class">
                                            @if ($errors->has('classes'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('classes') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-4 col-md-offset-5">
                                            <button type="submit" class="mbtn btn-primary">Add</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="syllabus-box">                        
                        <div class="syllabus-view">
                            <div class="main-point">Add Exam Category</div>
                            <div class="point-list">
                                <form class="form-horizontal" role="form" method="POST" action="{{ url('admin/exam_category') }}">
                                {{ csrf_field() }}
                                    <div class="form-group{{ $errors->has('cclasses') ? ' has-error' : '' }}">
                                        <div class="col-md-7">
                                            <select class="form-control" name="cclasses">
                                                <option value="">-- Select Class --</option>
                                                @if(count($classes) > 0 )
                                                    @foreach ($classes as $cclass)
                                                    <option value="{{ $cclass->id }}">{{ $cclass->class }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                            @if ($errors->has('cclasses'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('cclasses') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group{{ $errors->has('category') ? ' has-error' : '' }}">
                                        <div class="col-md-7">
                                            <input type="text" autocomplete="off" name="category" class="form-control" id="category" value="{{ old('category') }}" placeholder="Exam Category">
                                            @if ($errors->has('category'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('category') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-7">
                                            <textarea rows="4" cols="50" placeholder="Category description" name="c_description" class="form-control"id="c_description" value="{{ old('c_description') }}"></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-4 col-md-offset-5">
                                            <button type="submit" class="mbtn btn-primary">Add</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="syllabus-box">
                        <div class="syllabus-view">
                            <div class="main-point">Add Exam Name</div>
                            <div class="point-list">
                                <form class="form-horizontal" role="form" method="POST" action="{{ url('admin/exam_name') }}">
                                {{ csrf_field() }}
                                    <div class="form-group{{ $errors->has('classes') ? ' has-error' : '' }}">
                                        <div class="col-md-7">
                                            <select class="form-control" name="classes" id="tclasses">
                                                <option value="">-- Select Class --</option>
                                                @if(count($classes) > 0 )
                                                    @foreach ($classes as $cclass)
                                                    <option value="{{ $cclass->id }}">{{ $cclass->class }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                            @if ($errors->has('classes'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('classes') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group{{ $errors->has('category') ? ' has-error' : '' }}">
                                         <div class="col-md-7">
                                            <select class="form-control" name="category" id="ccategory">
                                                <option value="">-- Select Category --</option>
                                            </select>
                                            @if ($errors->has('category'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('category') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group{{ $errors->has('exam_name') ? ' has-error' : '' }}">
                                        <div class="col-md-7">
                                            <input type="text" autocomplete="off" name="exam_name" class="form-control" id="exam_name" value="{{ old('exam_name') }}" placeholder="Exam Name">
                                            @if ($errors->has('exam_name'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('exam_name') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-7">
                                            <textarea rows="4" cols="50" placeholder="Exam name description" name="e_description" class="form-control"id="e_description" value="{{ old('e_description') }}"></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-4 col-md-offset-5">
                                            <button type="submit" class="mbtn btn-primary">Add</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="syllabus-box">
                        <div class="syllabus-view">
                            <div class="main-point">Exam Subject</div>
                            <div class="point-list">
                                <form class="form-horizontal" role="form" method="POST" action="{{ url('admin/exam_subject') }}">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="{{ $errors->has('class') ? ' has-error' : '' }}">
                                        <select class="form-control classs" name="class" id="class">
                                            <option value="">-- Select Class --</option>
                                            @if(count($classes) > 0 )
                                                @foreach ($classes as $cclass)
                                                <option value="{{ $cclass->id }}">{{ $cclass->class }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                        @if ($errors->has('class'))
                                            <span class="help-block">
                                            <strong>{{ $errors->first('class') }}</strong>
                                            </span>
                                        @endif
                                        </div>
                                    </div>
                                    
                                        <div class="col-md-3"style="padding-right: 0px; padding-left: 0px;">   
                                                   <p class="rate">Select Category</p>
                                                    <div id="categorys"> </div>
                                        </div>
                                        
                                        <div class="col-md-3" style="padding-right: 0px; padding-left: 0px;" >
                                            <div class="{{ $errors->has('e_name') ? ' has-error' : '' }}">
                                                        <p class="rate">Select Exam Name</p>
                                                        <div id="exam_title"> </div>    
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group{{ $errors->has('subject') ? ' has-error' : '' }}">
                                        <div class="col-md-7">
                                            <input type="text" autocomplete="off" name="subject" class="form-control" id="subject" value="{{ old('subject') }}" placeholder="Exam Subject">
                                            @if ($errors->has('subject'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('subject') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-7">
                                            <textarea rows="4" cols="50" placeholder="Subject description" name="s_description" class="form-control"id="s_description" value="{{ old('s_description') }}"></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-4 col-md-offset-5">
                                            <button type="submit" class="mbtn btn-primary">Add</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="syllabus-box">
                        <div class="syllabus-view">
                            <div class="main-point">Chapter</div>
                            <div class="point-list">
                                <form class="form-horizontal" role="form" method="POST" action="{{ url('admin/chapter') }}">
                                {{ csrf_field() }}
                                <div class="form-group{{ $errors->has('categories') ? ' has-error' : '' }}">
                                        <div class="col-md-7">
                                            <select class="form-control chapter" name="subjects" id="lclass_id" data-dependent="lsubject">
                                                <option value="">-- Select Class --</option>
                                                @if(count($classes) > 0 )
                                                    @foreach ($classes as $cclass)
                                                    <option value="{{ $cclass->id }}">{{ $cclass->class }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                            @if ($errors->has('categories'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('categories') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group{{ $errors->has('subject') ? ' has-error' : '' }}">
                                        <div class="col-md-7">
                                            <select class="form-control" name="subject" id="lsubject">
                                                <option value="">-- Select Subject --</option>
                                            </select>
                                            @if ($errors->has('subject'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('subject') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group{{ $errors->has('chapter') ? ' has-error' : '' }}">
                                        <div class="col-md-7">
                                            <input type="text" name="chapter" autocomplete="off" class="form-control" id="chapter" value="{{ old('chapter') }}" placeholder="Chapter">
                                            @if ($errors->has('chapter'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('chapter') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-7">
                                            <textarea rows="4" cols="50" placeholder="Chapter description" name="c_description" class="form-control"id="ch_description" value="{{ old('c_description') }}"></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-4 col-md-offset-5">
                                            <button type="submit" class="mbtn btn-primary">Add</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                            
                </div>
            </div>
                <div class="col-md-4">
                    <div class="tablerow">
                        <table class="table table-striped table-mborder table-hover" id="sample_1">
                            <thead>
                                <tr>
                                    <th> Sl.No </th>
                                    <th> Class </th>
                                    <th> Edit </th>
                                    <!-- <th> Delete </th> -->
                                </tr>
                            </thead>
                            <tbody>
                            <?php $i = 1; ?>
                            @if(count($classes) > 0 )
                                @foreach ($classes as $class)
                                    <tr id="{{ $category1->id }}">
                                        <td class="text-center"> {{ $i++ }} </td>
                                        <td class="text-center"> {{ $class->class }} </td>
                                        <td class="text-center"><a class="editstock" href="{{ route('admin.exam.edit',[$class->id ,'1']) }}"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i> </a></td>
                                        <!-- <td class="text-center"><a onclick="javascript:check=confirm( 'Do You Want To Delete?'); if(check==false) return false;" class="deletestock" href="{{ url('admin.clas.delete', $class->id ) }}"> <i class="fa fa-trash" aria-hidden="true"></i> </a></td> -->
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="tablerow">
                        <table class="table table-striped table-mborder table-hover" id="sample_1">
                            <thead>
                                <tr>
                                    <th> Sl.No </th>
                                    <th> Exam Category </th>
                                    <th> Edit </th>
                                    <!-- <th> Delete </th> -->
                                </tr>
                            </thead>
                            <tbody>
                            <?php $i = 1; ?>
                            @if(count($categories1) > 0 )
                                @foreach ($categories1 as $category1)
                                    <tr id="{{ $category1->id }}">
                                        <td class="text-center"> {{ $i++ }} </td>
                                        <td class="text-center"> {{ $category1->name }} </td>
                                        <td class="text-center"><a class="editstock" href="{{ route('admin.exam.edit',[$category1->id ,'2']) }}"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i> </a></td>
                                        <!-- <td class="text-center"><a onclick="javascript:check=confirm( 'Do You Want To Delete?'); if(check==false) return false;" class="deletestock" href="{{ url('admin.cat.delete', $category1->id ) }}"> <i class="fa fa-trash" aria-hidden="true"></i> </a></td> -->
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="tablerow">
                        <table class="table table-striped table-mborder table-hover" id="sample_1">
                            <thead>
                                <tr>
                                    <th> Sl.No </th>
                                    <th> Exam type </th>
                                    <th> Edit </th>
                                    <!-- <th> Delete </th> -->
                                </tr>
                            </thead>
                            <tbody>
                            <?php $i = 1; ?>
                            @if(count($titles1) > 0 )
                                @foreach ($titles1 as $type1)
                                    <tr id="{{ $type1->id }}">
                                        <td class="text-center"> {{ $i++ }} </td>
                                        <td class="text-center"> {{ $type1->name }} </td>
                                        <td class="text-center"><a class="editstock" href="{{ route('admin.exam.edit',[$type1->id ,'3']) }}"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i> </a></td>
                                        <!-- <td class="text-center"><a onclick="javascript:check=confirm( 'Do You Want To Delete?'); if(check==false) return false;" class="deletestock" href="{{ url('admin.title.delete', $type1->id ) }}"> <i class="fa fa-trash" aria-hidden="true"></i> </a></td> -->
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="tablerow">
                        <table class="table table-striped table-mborder table-hover" id="sample_1">
                            <thead>
                                <tr>
                                    <th> Sl.No </th>
                                    <th> Exam Subject </th>
                                    <th> Edit </th>
                                    <!-- <th> Delete </th> -->
                                </tr>
                            </thead>
                            <tbody>
                            <?php $i = 1; ?>
                            @if(count($subjects1) > 0 )
                                @foreach ($subjects1 as $subject1)
                                    <tr id="{{ $subject1->id }}">
                                        <td class="text-center"> {{ $i++ }} </td>
                                        <td class="text-center"> {{ $subject1->name }} </td>
                                        <td class="text-center"><a class="editstock" href="{{ route('admin.exam.edit',[$subject1->id ,'4']) }}"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i> </a></td>
                                        <!-- <td class="text-center"><a onclick="javascript:check=confirm( 'Do You Want To Delete?'); if(check==false) return false;" class="deletestock" href="{{ url('admin.subject.delete', $subject1->id ) }}"> <i class="fa fa-trash" aria-hidden="true"></i> </a></td> -->
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>    
        </div>
        
        <div class="row">
           <div id="my-tree-view" class="tree-view">
             <div class="branch-node">
              <div class="branch-title">Esame</div>
              <ul class="branches">
                <?php $i = 1; ?>
                @if(count($classes) > 0 )
                  @foreach ($classes as $class) 
                    <div class="branch-node">
                        <div class="branch-title">{{$class->class}}</div>
                        <ul class="branches">
                            @if(count($categories) > 0 )
                            @foreach ($categories as $category)
                                @if(($class->id) == ( $category->class_id))
                                    <div class="branch-node">
                                        <div class="branch-title">{{$category->name}}</div>
                                        <ul class="branches">
                                        @if(count($titles) > 0 )
                                        @foreach ($titles as $type)
                                            @if(($category->id) == ( $type->category_id))
                                                <div class="branch-node">
                                                    <div class="branch-title">{{$type->name}}</div>
                                                    <ul class="branches">
                                                        @if(count($subjects) > 0 )
                                                        @foreach ($subjects as $subject)
                                                            @if(($type->id) == ( $subject->type_id))  
                                                                <div class="branch-node">
                                                                    <div class="branch-title">{{$subject->name}}</div>
                                                                    <ul class="branches">
                                                                        @if(count($level) > 0 )
                                                                        @foreach ($level as $lev)
                                                                            @if(($subject->id) == ( $lev->subject_id))
                                                                                <li> {{$lev->name}} </li>
                                                                            @endif
                                                                        @endforeach
                                                                        @endif
                                                                    </ul>     
                                                                </div>
                                                            @endif
                                                        @endforeach
                                                        @endif 
                                                    </ul>
                                                </div>
                                            @endif 
                                        @endforeach
                                        @endif     
                                        </ul>      
                                    </div>
                                 @endif   
                            @endforeach
                            @endif
                        </ul> 
                    </div>   
                   @endforeach
                @endif
              </ul>
             </div>
           </div>
        </div>
        
    </div>      
</div>
    
@endsection