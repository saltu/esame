@extends('admin.layouts.app')
@section('content')
<?php
    $exam_name = DB::table('exam_type_packages')
                    ->select('*')
                    ->where('id', $e_fee[0]->ex_package_id)
                    ->get();
    $class_id = explode(',', $exam_name[0]->class_ids);
    $cat_id = explode(',', $exam_name[0]->ex_type_ids);
?>
<section class="b breadcrumb white-bg">
    <div class="container">
        <ul >
            <li><a href="#">Admin</a></li>
            <li><a href="#">Exams Fee Manage</a></li>
        </ul>
    </div>
</section>
@if(session()->has('message'))
    <div class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif
@if(session()->has('dmessage'))
    <div class="alert alert-danger">
        {{ session()->get('dmessage') }}
    </div>
@endif
<div class="course-details">
    <div class="container">
        <div class="row">
            <div class="col-md-9 col-md-offset-2">
                <div class="syllabus">
                    <h4 class="cent text-center">Edit Exam Fee Details </h4>

                    <div class="syllabus-box">
                        <div class="syllabus-view first">
                            <div class="main-point active">Edit Exam Fee</div>
                            <div class="point-list">
                                <form class="form-horizontal" role="form" method="POST" action="{{ url('admin/fee_edit') }}">
                                {{ csrf_field() }}
                                    <div class="form-group{{ $errors->has('fee_for') ? ' has-error' : '' }}">
                                        <div class="col-md-7">
                                            <input type="hidden" name="id" value="{{ $e_fee[0]->id }}">
                                            <input type="text" name="title" class="form-control" readonly value="{{ $e_fee[0]->title }}">
                                        </div>
                                    </div>
                                    <div class="form-group{{ $errors->has('classes') ? ' has-error' : '' }}">
                                        <div class="col-md-7">
                                            <input type="text" name="classes" class="form-control" readonly value="<?php
                                            $n_class = count($class_id);
                                            $i = 0;
                                            do {
                                                $clas = DB::table('classes')
                                                            ->select('*')
                                                            ->where('id', $class_id[$i])
                                                            ->get();
                                                echo $clas[0]->class;
                                                $i++;
                                                if( $i < $n_class ){echo " + ";}
                                            }while ( $i < $n_class);
                                            ?>">
                                        </div>
                                    </div>
                                    <div class="form-group{{ $errors->has('category') ? ' has-error' : '' }}">
                                         <div class="col-md-7">
                                            <input type="text" name="category" class="form-control" readonly value="<?php
                                            $n_cat = count($cat_id);
                                            $j = 0;
                                            do {
                                                $cat = DB::table('exam_names')
                                                            ->select('*')
                                                            ->where('id', $cat_id[$j])
                                                            ->get();
                                                echo $cat[0]->name;
                                                $j++;
                                                if( $j < $n_cat ){echo ",";}
                                            }while ( $j < $n_cat);
                                            ?>">
                                        </div>
                                    </div>
                                    <div class="form-group{{ $errors->has('student_amount') ? ' has-error' : '' }}">
                                        <div class="col-md-8">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <label>Student Amount</label>
                                                    <input type="text" name="student_amount" class="form-control" id="student_amount" required value="{{ $e_fee[0]->student_amount }}" placeholder="Student Amount">
                                                </div>
                                                <div class="col-md-4">
                                                    <label>Center Amount</label>
                                                    <input type="text" name="center_amount" class="form-control" id="center_amount" required value="{{ $e_fee[0]->center_amount }}" placeholder="Center Amount">
                                                    @if ($errors->has('center_amount'))
                                                        <span class="help-block">
                                                        <strong>{{ $errors->first('center_amount') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-4 col-md-offset-4">
                                            <button type="submit" class="mbtn btn-primary">Update</button>
                                            <a href="{{ url('admin/manage_exam_package') }}"><button type="button" id="cancel" class="mbtn btn-warnning">Cancel</button></a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    </div>
            	</div>
                </div>
            </div>  
        </div>
    </div>      
</div>

@endsection