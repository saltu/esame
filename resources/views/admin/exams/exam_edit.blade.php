@extends('admin.layouts.app')
@section('content')
<section class="b breadcrumb white-bg">
    <div class="container">
        <ul >
            <li><a href="#">Admin</a></li>
            <li><a href="#">Exams Fee Manage</a></li>
        </ul>
    </div>
</section>
@if(session()->has('message'))
    <div class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif
@if(session()->has('dmessage'))
    <div class="alert alert-danger">
        {{ session()->get('dmessage') }}
    </div>
@endif
<div class="course-details">
    <div class="container">
        <div class="row">
            <div class="col-md-9 col-md-offset-2">
                <div class="syllabus">
                    <h4 class="cent text-center">Edit Exam Details </h4>
                        @if(count($disp_cls) > 0 )
                        <form class="form-horizontal" role="form" method="POST" action="{{ url('admin/exam_edit') }}">
                                {{ csrf_field() }}
                            <div class="form-group">
                                <label for="name" class="mtl control-label">Name</label>
                                <div class="col-md-8 input-box">
                                    <input id="cls_id" type="hidden" class="form-control" name="id" value="{{ $disp_cls[0]->id }}">
                                    <input id="name" type="text" class="form-control" name="name" value="{{ $disp_cls[0]->class }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <!-- <label for="place" class="mtl control-label">Place</label> -->
                                <div class="col-md-8 input-box">
                                    <input id="fil" type="hidden" class="form-control" name="fil" value="1">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-4 col-md-offset-5">
                                    <button type="submit" class="mbtn btn-primary">Update</button>
                                        <a href="{{ url('admin/manage_exams') }}"><button type="button" id="cancel" class="mbtn btn-warnning">Cancel</button></a>
                                </div>
                                </div>
                        </form>
                        @endif

                        @if(count($disp_cat) > 0 )
                        <form class="form-horizontal" role="form" method="POST" action="{{ url('admin/exam_edit') }}">
                                {{ csrf_field() }}
                            <div class="form-group">
                                <label for="name" class="mtl control-label">Name</label>
                                <div class="col-md-8 input-box">
                                    <input id="cat_id" type="hidden" class="form-control" name="id" value="{{ $disp_cat[0]->id }}">
                                    <input id="name" type="text" class="form-control" name="name" value="{{ $disp_cat[0]->name }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <!-- <label for="place" class="mtl control-label">Place</label> -->
                                <div class="col-md-8 input-box">
                                    <input id="fil" type="hidden" class="form-control" name="fil" value="2">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-4 col-md-offset-5">
                                    <button type="submit" class="mbtn btn-primary">Update</button>
                                            <a href="{{ url('admin/manage_exams') }}"><button type="button" id="cancel" class="mbtn btn-warnning">Cancel</button></a>
                                    </div>
                                </div>
                        </form>
                        @endif

                        @if(count($disp_ty) > 0 )
                        <form class="form-horizontal" role="form" method="POST" action="{{ url('admin/exam_edit') }}">
                                {{ csrf_field() }}
                            <div class="form-group">
                                <label for="name" class="mtl control-label">Name</label>
                                <div class="col-md-8 input-box">
                                    <input id="ty_id" type="hidden" class="form-control" name="id" value="{{ $disp_ty[0]->id }}">
                                    <input id="name" type="text" class="form-control" name="name" value="{{ $disp_ty[0]->name }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <!-- <label for="place" class="mtl control-label">Place</label> -->
                                <div class="col-md-8 input-box">
                                    <input id="fil" type="hidden" class="form-control" name="fil" value="3">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-4 col-md-offset-5">
                                    <button type="submit" class="mbtn btn-primary">Update</button>
                                            <a href="{{ url('admin/manage_exams') }}"><button type="button" id="cancel" class="mbtn btn-warnning">Cancel</button></a>
                                    </div>
                                </div>
                        </form>
                        @endif

                        @if(count($disp_ex) > 0 )
                        <form class="form-horizontal" role="form" method="POST" action="{{ url('admin/exam_edit') }}">
                                {{ csrf_field() }}
                            <div class="form-group">
                                <label for="name" class="mtl control-label">Name</label>
                                <div class="col-md-8 input-box">
                                    <input id="ex_id" type="hidden" class="form-control" name="id" value="{{ $disp_ex[0]->id }}">
                                    <input id="name" type="text" class="form-control" name="name" value="{{ $disp_ex[0]->name }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <!-- <label for="place" class="mtl control-label">Place</label> -->
                                <div class="col-md-8 input-box">
                                    <input id="fil" type="hidden" class="form-control" name="fil" value="4">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-4 col-md-offset-5">
                                    <button type="submit" class="mbtn btn-primary">Update</button>
                                    <a href="{{ url('admin/manage_exams') }}"><button type="button" id="cancel" class="mbtn btn-warnning">Cancel</button></a>
                                    </div>
                                </div>
                        </form>
                        @endif
                </div>
            </div>  
        </div>
    </div>      
</div>

@endsection