@extends('admin.layouts.app')
@section('content')


<section class="b breadcrumb">
    <div class="container">
        <ul>
            <li><a href="#">Admin</a></li>
            <li><a href="#">Add Question</a></li>
        </ul>
    </div>
</section>

@if(session()->has('message'))
    <div class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif
@if(session()->has('dmessage'))
    <div class="alert alert-danger">
        {{ session()->get('dmessage') }}
    </div>
@endif


<div class="sspage-content">
        <div class="ssform-v10-content">
            <div class="row" style="margin-right:0px !important;margin-left:0px !important;">
                <form id="form" name="form" action="" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                    <div class="col-md-12">
                        <div class="row">


                                    <div class="col-md-4 mt">
                                            <select class="form-control dynam chapter" name="subjects" id="lclass_id" data-dependent="lsubject">
                                                <option value="">-- Select Class --</option>
                                                @if(count($classes) > 0 )
                                                    @foreach ($classes as $cclass)
                                                    <option value="{{ $cclass->id }}">{{ $cclass->class }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                            @if ($errors->has('categories'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('categories') }}</strong>
                                                </span>
                                            @endif
                                    </div>

                                    <div class="col-md-4 mt">
                                            <select class="form-control dynam hidee" name="chapters" id="lsubject" data-dependent="chapter">
                                                <option value="">-- Select Subject --</option>
                                            </select>
                                            @if ($errors->has('subject'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('subject') }}</strong>
                                                </span>
                                            @endif
                                    </div>

                                        <div class="col-md-4 mt">
                                            <select class="form-control hidee" name="chapter" id="chapter">
                                                <option value="">-- Select Chapter --</option>
                                            </select>
                                        </div>
                        </div>
                    </div>
            
            <!-- <div class="section-title">
                    <h2>Add Question</h2>
            </div> -->
            <div class="form-detail form-horizontal" >
                <div class="form-left">
                <h2><center>Add Question</center></h2>
                    <div class="form-row{{ $errors->has('question') ? ' has-error' : '' }}">
                        <div class="row">
                            <input type="hidden" name="id_chap" id="id_chap" value="">
                            <div class="col-md-9">
                                <label class="mlb">Question</label>
                                <textarea name="question" class="street" id="question" placeholder="Question" value="{{ old('question') }}"></textarea>
                            </div>
                            <div class="col-md-3 pdt">
                                <input type="file" name="iquestion" id="iquestion" class="custom-file-input">
                            </div>
                        </div>
                        @if ($errors->has('question'))
                            <span class="help-block">
                                <strong>{{ $errors->first('question') }}</strong>
                            </span>
                        @endif
                        @if ($errors->has('iquestion'))
                            <span class="help-block">
                                <strong>{{ $errors->first('iquestion') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-row{{ $errors->has('answer') ? ' has-error' : '' }}">
                        <div class="row">
                            <div class="col-md-9">
                                <label class="mlb">Answer</label>
                                <textarea name="answer" class="street" id="answer" placeholder="Answer" value="{{ old('answer') }}"></textarea>
                            </div>
                            <div class="col-md-3 pdt">
                                <input type="file" name="i_answer" id="i_answer" class="custom-file-input">
                            </div>
                        </div>
                        @if ($errors->has('answer'))
                            <span class="help-block">
                                <strong>{{ $errors->first('answer') }}</strong>
                            </span>
                        @endif
                        @if ($errors->has('i_answer'))
                            <span class="help-block">
                                <strong>{{ $errors->first('i_answer') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-row{{ $errors->has('option1') ? ' has-error' : '' }}">
                        <div class="row">
                            <div class="col-md-9">
                                <label class="mlb">Option 1</label>
                                <textarea name="option1" class="additional" id="option1" placeholder="Option 1" value="{{ old('option1') }}"></textarea>
                            </div>
                            <div class="col-md-3 pdt">
                                <input type="file" name="i_option1" id="i_option1" class="custom-file-input">
                            </div>
                        </div>
                        @if ($errors->has('option1'))
                            <span class="help-block">
                                <strong>{{ $errors->first('option1') }}</strong>
                            </span>
                        @endif
                        @if ($errors->has('i_option1'))
                            <span class="help-block">
                                <strong>{{ $errors->first('option1') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-row{{ $errors->has('option2') ? ' has-error' : '' }}">
                        <div class="row">
                            <div class="col-md-9">
                                <label class="mlb">Option 2</label>
                                <textarea name="option2" class="additional" id="option2" placeholder="Option 2" value="{{ old('option2') }}"></textarea>
                            </div>
                            <div class="col-md-3 pdt">
                                <input type="file" name="i_option2" id="i_option2" class="custom-file-input">
                            </div>
                        </div>
                        @if ($errors->has('option2'))
                            <span class="help-block">
                                <strong>{{ $errors->first('option2') }}</strong>
                            </span>
                        @endif
                        @if ($errors->has('option2'))
                            <span class="help-block">
                                <strong>{{ $errors->first('i_option2') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-row{{ $errors->has('option3') ? ' has-error' : '' }}">
                        <div class="row">
                            <div class="col-md-9">
                                <label class="mlb">Option 3</label>
                                <textarea name="option3" class="additional" id="option3" placeholder="Option 3" value="{{ old('option3') }}"></textarea>
                            </div>
                            <div class="col-md-3 pdt">
                                <input type="file" name="i_option3" id="i_option3" class="custom-file-input">
                            </div>
                        </div>
                        @if ($errors->has('option3'))
                            <span class="help-block">
                                <strong>{{ $errors->first('option3') }}</strong>
                            </span>
                        @endif
                        @if ($errors->has('option3'))
                            <span class="help-block">
                                <strong>{{ $errors->first('i_option3') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-row{{ $errors->has('solution') ? ' has-error' : '' }}">
                        <div class="row">
                            <div class="col-md-9">
                                <label class="mlb">Description</label>
                                <textarea name="solution" class="street" id="solution" placeholder="Description (Optional)" value="{{ old('solution') }}"></textarea>
                                @if ($errors->has('solution'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('solution') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="form-row-last">
                        <input type="submit"  class="register" value="Add">
                    </div>   
                    
                </div>
               <!--  <div class="form-right">
                    <h2>Question</h2>
                    <div class="form-row{{ $errors->has('exam_category') ? ' has-error' : '' }}">
                        <input class="company" id="exam_category" type="text" name="exam_category" value="{{ old('exam_category') }}" placeholder="Exam Category">
                        @if ($errors->has('exam_category'))
                            <span class="help-block">
                                <strong>{{ $errors->first('exam_category') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-row{{ $errors->has('exam_type') ? ' has-error' : '' }}">
                        <input id="exam_type" type="text" name="exam_type" value="{{ old('exam_type') }}" class="company" placeholder="Exam Type">
                        @if ($errors->has('exam_type'))
                            <span class="help-block">
                                <strong>{{ $errors->first('exam_type') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-row{{ $errors->has('subject') ? ' has-error' : '' }}">
                        <input type="text" name="subject" class="company" id="subject" value="{{ old('subject') }}" placeholder="Subject">
                        @if ($errors->has('subject'))
                            <span class="help-block">
                                <strong>{{ $errors->first('subject') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-row{{ $errors->has('question') ? ' has-error' : '' }}">
                        <input type="text" name="question" class="company" id="question" value="{{ old('question') }}" placeholder="Question">
                        @if ($errors->has('question'))
                            <span class="help-block">
                                <strong>{{ $errors->first('question') }}</strong>
                            </span>
                        @endif
                    </div>

                </div> -->
            </div>
            </form>
            </div>
            <div class="container" id="del_top">
                @if(session()->has('dsmessage'))
                    <div class="alert alert-danger">
                        {{ session()->get('dsmessage') }}
                    </div>
                @endif
            </div>
            <div class="container">
            <div class="tablerow">
                <div id="question_list"></div>

                       
            </div>
            </div>
    </div>
@endsection