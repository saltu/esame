@extends('admin.layouts.app')
@section('content')
<section class="b breadcrumb white-bg">
    <div class="container">
        <ul >
            <li><a href="#">Admin</a></li>
            <li><a href="#">Questions</a></li>
        </ul>
    </div>
</section>

    <div class="alert alert-success hidee" id="msg">
        {{ session()->get('message') }}
    </div>

@if(session()->has('dmessage'))
    <div class="alert alert-danger">
        {{ session()->get('dmessage') }}
    </div>
@endif
<div class="course-details">
    <div class="container">
        <table class="table table-striped table-mborder table-hover" id="sample_1">
            <thead>
                <tr>
                    <th> Sl.No </th>
                    <!-- <th> Question id </th> -->
                    <th> Question </th>
                    <th> Answer </th>
                    <th> Description </th>
                    <th> View </th>
                     <th> Delete </th>
                </tr>
            </thead>
            <tbody>
            <?php $i = 1; ?>
            @if(count($questad) > 0 )
                @foreach ($questad as $quest)
                    <tr id="{{ $quest->id }}">
                        <td class="text-center"> {{ $i++ }} </td>
                        <!-- <td class="text-center"> {{ $quest->id }} </td> -->
                        <td class="text-center"> {!! $quest->question !!} </td>
                        <?php
                         $answer = DB::table('question_options')
                                    ->where('id',$quest->solution)
                                    ->get();
                        ?>
                        <td class="text-center"> {!! $answer[0]->options !!} </td>
                        <td class="text-center"> {!! $quest->description !!} </td>
                        <td class="text-center"><a class="editstock" href="{{ url('admin/question_edit', $quest->id ) }}"> <i class="fa fa-eye" aria-hidden="true"></i> </a></td>
                        <td class="text-center"><a class="deletestock"> <i class="fa fa-trash" aria-hidden="true"></i> </a></td>
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>
    </div>      
</div>

@endsection