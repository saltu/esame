<table class="table table-striped table-mborder table-hover" id="sample_1">
    <thead>
        <tr>
            <th> Sl.No </th>
            <!-- <th> Question id </th> -->
            <th> Question </th>
            <th> Answer </th>
            <th> Description </th>
            <th> View </th>
             <th> Delete </th>
        </tr>
    </thead>
    <tbody>
    <?php $i = 1; ?>
    @if(count($questad) > 0 )
        @foreach ($questad as $quest)
            <tr id="{{ $quest->id }}">
                <td class="text-center"> {{ $i++ }} </td>
                <!-- <td class="text-center"> {{ $quest->id }} </td> -->
                <td class="text-center"> {!! $quest->question !!} </td>
                <?php
                 $answer = DB::table('question_options')
                            ->where('id',$quest->solution)
                            ->get();
                ?>
                <td class="text-center"> {!! $answer[0]->options !!} </td>
                <td class="text-center"> {!! $quest->description !!} </td>
                <td class="text-center"><a class="editstock" href="{{ url('admin/question_edit', $quest->id ) }}"> <i class="fa fa-eye" aria-hidden="true"></i> </a></td>
                <td class="text-center"><a onclick="javascript:check=confirm( 'Do You Want To Delete?'); if(check==false) return false;" class="deletestock"> <i class="fa fa-trash" aria-hidden="true"></i> </a></td>
            </tr>
        @endforeach
    @endif
    </tbody>
</table>
<script type="text/javascript">
$('.deletestock').click( function(e){
  var id = $(this).closest('tr').attr('id');
  $.ajax({
      type : 'POST',
      url : 'question_delete',
      data : {id:id},
      success : function(result) {
        alert(result);
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type:'POST',
            url:'append_question',
            success:function(data) {
            $('#question_list').html(data);
            }
        });
      }
  });
});

</script>