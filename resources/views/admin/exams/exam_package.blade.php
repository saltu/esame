@extends('admin.layouts.app')
@section('content')
<?php
            $classes = DB::table ('classes')
                        ->select('*')
                        ->get();
?>
<section class="b breadcrumb white-bg">
    <div class="container">
        <ul >
            <li><a href="#">Admin</a></li>
            <li><a href="#">Exams Fee Manage</a></li>
        </ul>
    </div>
</section>
@if(session()->has('message'))
    <div class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif
@if(session()->has('dmessage'))
    <div class="alert alert-danger">
        {{ session()->get('dmessage') }}
    </div>
@endif
<div class="course-details">
    <div class="container">
        <div class="row">
            <div class="col-md-9 col-md-offset-2">
                <div class="syllabus">
                    <h4 class="cent text-center">Exam Fee Management</h4>

                    <div class="syllabus-box">
                        <div class="syllabus-view first">
                            <div class="main-point active">Add Exam Package</div>
                            <div class="point-list">
                                <form class="form-horizontal" role="form" method="POST" action="{{ url('admin/add_exam_package') }}">
                                {{ csrf_field() }}
                                    <div class="row">
                                        <div class="col-md-3"> 
                                         <p class="rate">Select class</p>
                                         <?php $i = 1; ?>
                                             @if(count($classes) > 0)
                                                 @foreach($classes as $class)
                                                 <div class="check-slide">
                                                     <label class="label_container">{{$class->class}}
                                                      <input type="checkbox" name="class[]" class="class checkbox_n" value="{{ $class->id }}">
                                                      <span class="label_checkmark"></span>
                                                    </label>                     
                                                </div>
                                                @endforeach
                                            @endif 
                                        </div> 

                                        <div class="col-md-3"style="padding-right: 0px; padding-left: 0px;">   
                                                   <p class="rate">Select Category</p>
                                                    <div id="category"> </div>
                                        </div>

                                        <div class="col-md-3" style="padding-right: 0px; padding-left: 0px;" >
                                                        <p class="rate">Select Exam Name</p>
                                                        <div id="exam_title"> </div>    
                                        </div>
                                    </div>
                                    <div class="form-group{{ $errors->has('package_name') ? ' has-error' : '' }}">
                                        <div class="col-md-7">
                                            <input type="text" name="package_name" class="form-control" id="package_name" value="{{ old('package_name') }}" placeholder="Package Name">
                                            @if ($errors->has('package_name'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('package_name') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-4 col-md-offset-5">
                                            <button type="submit" class="mbtn btn-primary">Add</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            </div>
                        </div>
                    <div class="syllabus-box">                        
                        <div class="syllabus-view">
                            <div class="main-point">Add Package Fee</div>
                            <div class="point-list">
                                <form class="form-horizontal" role="form" method="POST" action="{{ url('admin/add_fee') }}">
                                {{ csrf_field() }}
                                    <div class="form-group">
                                        <div class="col-md-7">
                                            <select class="form-control" name="package">
                                                <option value="">-- Select Package --</option>
                                                @if(count($packages) > 0 )
                                                    @foreach ($packages as $package)
                                                    <option value="{{ $package->id }}">{{ $package->title }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group{{ $errors->has('student_amount') ? ' has-error' : '' }}">
                                        <div class="col-md-7">
                                            <input type="text" autocomplete="off" name="student_amount" class="form-control" id="student_amount" value="{{ old('student_amount') }}" placeholder="Student Amount">
                                            @if ($errors->has('student_amount'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('student_amount') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group{{ $errors->has('center_amount') ? ' has-error' : '' }}">
                                        <div class="col-md-7">
                                            <input type="text" autocomplete="off" name="center_amount" class="form-control" id="center_amount" value="{{ old('center_amount') }}" placeholder="Center Amount">
                                            @if ($errors->has('center_amount'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('center_amount') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-4 col-md-offset-5">
                                            <button type="submit" class="mbtn btn-primary">Add</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="tablerow">
                    <h3>Package Fee Details</h3>
                        <table class="table table-striped table-mborder table-hover mt" id="sample_1">
                            <thead>
                                <tr>
                                    <th> Sl.No </th>
                                    <th> Title </th>
                                    <th> Class </th>
                                    <th> Exams </th>
                                    <th> Student Amount </th>
                                    <th> Center Amount </th>
                                    <th> Edit </th>
                                    <th> Delete </th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php $k = 1; ?>
                            @if(count($exam_fees) > 0 )
                                @foreach ($exam_fees as $exam_fee)
                                <?php
                                $exam_name = DB::table('exam_type_packages')
                                                ->select('*')
                                                ->where('id', $exam_fee->ex_package_id)
                                                ->get();
                                $class_id = explode(',', $exam_name[0]->class_ids);
                                $cat_id = explode(',', $exam_name[0]->ex_type_ids);
                                ?>
                                    <tr id="{{ $exam_fee->id }}">
                                        <td class="text-center"> {{ $k++ }} </td>
                                        <td class="text-center"> {{ $exam_fee->title }} </td>
                                        <td class="text-center"> <?php
                                            $n_class = count($class_id);
                                            $i = 0;
                                            do {
                                                $clas = DB::table('classes')
                                                            ->select('*')
                                                            ->where('id', $class_id[$i])
                                                            ->get();
                                                echo $clas[0]->class;
                                                $i++;
                                                if( $i < $n_class ){echo " + ";}
                                            }while ( $i < $n_class);
                                            ?> 
                                        </td>
                                        
                                        <td class="text-center"> <?php
                                            $n_cat = count($cat_id);
                                            $j = 0;
                                            do {
                                                $cat = DB::table('exam_names')
                                                            ->select('*')
                                                            ->where('id', $cat_id[$j])
                                                            ->get();
                                                echo $cat[0]->name;
                                                $j++;
                                                if( $j < $n_cat ){echo ",";}
                                            }while ( $j < $n_cat);
                                            ?> 
                                        </td>
                                        <td class="text-center"> {{ $exam_fee->student_amount }} </td>
                                        <td class="text-center"> {{ $exam_fee->center_amount }} </td>
                                        <td class="text-center"><a class="editstock" href="{{ url('admin/fee_edit', $exam_fee->id ) }}"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i> </a></td>
                                        <td class="text-center"><a onclick="javascript:check=confirm( 'Do You Want To Delete?'); if(check==false) return false;" class="deletestock" href="{{ url('admin/fee_delete', $exam_fee->id ) }}"> <i class="fa fa-trash" aria-hidden="true"></i> </a></td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="col-md-12">
                    <?php
                    $packs = DB::table('exam_type_packages')
                                ->select('*')
                                ->get();
                    ?>
                    <div class="tablerow">
                    <h3>Package Details</h3>
                        <table class="table table-striped table-mborder table-hover mt" id="sample_2">
                            <thead>
                                <tr>
                                    <th> Sl.No </th>
                                    <th> Title </th>
                                    <th> Class </th>
                                    <th> Exams </th>
                                    <th> Delete </th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php $k = 1; ?>
                            @if(count($packs) > 0 )
                                @foreach ($packs as $pack)
                                <?php
                                $class_id = explode(',', $pack->class_ids);
                                $cat_id = explode(',', $pack->ex_type_ids);
                                ?>
                                    <tr id="{{ $exam_fee->id }}">
                                        <td class="text-center"> {{ $k++ }} </td>
                                        <td class="text-center"> {{ $pack->title }} </td>
                                        <td class="text-center"> <?php
                                            $n_class = count($class_id);
                                            $i = 0;
                                            do {
                                                $clas = DB::table('classes')
                                                            ->select('*')
                                                            ->where('id', $class_id[$i])
                                                            ->get();
                                                echo $clas[0]->class;
                                                $i++;
                                                if( $i < $n_class ){echo " + ";}
                                            }while ( $i < $n_class);
                                            ?> 
                                        </td>
                                        
                                        <td class="text-center"> <?php
                                            $n_cat = count($cat_id);
                                            $j = 0;
                                            do {
                                                $cat = DB::table('exam_names')
                                                            ->select('*')
                                                            ->where('id', $cat_id[$j])
                                                            ->get();
                                                echo $cat[0]->name;
                                                $j++;
                                                if( $j < $n_cat ){echo ",";}
                                            }while ( $j < $n_cat);
                                            ?> 
                                        </td>
                                        <td class="text-center"><a onclick="javascript:check=confirm( 'Do You Want To Delete?'); if(check==false) return false;" class="deletestock" href="{{ url('admin/package_delete', $pack->id ) }}"> <i class="fa fa-trash" aria-hidden="true"></i> </a></td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>

                </div>
            </div>  
        </div>
    </div>      
</div>

@endsection