@extends('admin.layouts.app')
@section('title', 'register')

@section('content')

       <!--  <section class="breadcrumb white-bg">
            <div class="container">
                <ul>
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Student Login & Register</a></li>
                </ul>
            </div>
        </section>
        <section class="login-view">
            <div class="container">
                <div class="row">
                    <div class="section-title">
                        <h2>REGISTER</h2>
                        <p>New Student! Register Now</p>
                    </div>
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/student/register') }}">
                        {{ csrf_field() }}
                        <div class="col-sm-6">
                            <div class="section-title">
                                <p>Student Details</p>
                            </div>
                            <div class="sform-group{{ $errors->has('name') ? ' has-error' : '' }}">

                                <label for="name" class="control-label">Name Of Student</label>
                                <div class="input-box">
                                    <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}">

                                    @if ($errors->has('name'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                    @endif
                                </div>
                            </div>

                            <div class="sform-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email" class="control-label">E-Mail Address</label>
                                <div class="input-box">
                                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}">
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="sform-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                                <label for="phone" class="control-label">Phone Number</label>
                                <div class="input-box">
                                    <input id="phone" type="number" class="form-control" name="phone" value="{{ old('phone') }}">
                                    @if ($errors->has('phone'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('phone') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="sform-group{{ $errors->has('school') ? ' has-error' : '' }}">
                                <label for="school" class="control-label">Name Of School</label>
                                <div class="input-box">
                                    <input id="school" type="text" class="form-control" name="school" value="{{ old('school') }}">

                                    @if ($errors->has('school'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('school') }}</strong>
                                            </span>
                                        @endif
                                </div>
                            </div>

                            <div class="sform-group{{ $errors->has('class') ? ' has-error' : '' }}">
                                <label for="class" class="control-label">Select Class</label>
                                <div class="input-box">
                                    <select id="class" class="form-control" name="class" value="{{ old('class') }}">
                                        <option>6</option>
                                        <option>7</option>
                                        <option>8</option>
                                        <option>9</option>
                                        <option>10</option>
                                        <option>Plus One</option>
                                        <option>Plus Two</option>
                                    </select>
                                    @if ($errors->has('class'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('class') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="sform-group{{ $errors->has('address') ? ' has-error' : '' }}">
                                <label for="address" class="control-label">Place</label>
                                <div class="input-box">
                                    <input id="address" type="text" class="form-control" name="address" value="{{ old('address') }}">
                                    @if ($errors->has('address'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('address') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="section-title">
                            <p>Parent Details</p>
                            </div>

                            <div class="sform-group{{ $errors->has('parent_name') ? ' has-error' : '' }}">
                                <label for="parent_name" class="control-label">Name Of Parent</label>
                                <div class="input-box">
                                    <input id="parent_name" type="text" class="form-control" name="parent_name" value="{{ old('parent_name') }}">

                                    @if ($errors->has('parent_name'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('parent_name') }}</strong>
                                            </span>
                                        @endif
                                </div>
                            </div>


                            <div class="sform-group{{ $errors->has('parent_email') ? ' has-error' : '' }}">
                                <label for="parent_email" class="control-label">E-Mail Address</label>
                                <div class="input-box">
                                    <input id="parent_email" type="email" class="form-control" name="parent_email" value="{{ old('parent_email') }}">
                                    @if ($errors->has('parent_email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('parent_email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="sform-group{{ $errors->has('parent_phone') ? ' has-error' : '' }}">
                                <label for="parent_phone" class="control-label">Phone Number</label>
                                <div class="input-box">
                                    <input id="parent_phone" type="number" class="form-control" name="parent_phone" value="{{ old('parent_phone') }}">
                                    @if ($errors->has('parent_phone'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('parent_phone') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="sform-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label for="password" class="control-label">Password</label>
                                <div class="input-box">
                                    <input id="password" type="password" class="form-control" name="password">
                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="sform-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                <label for="password-confirm" class="control-label">Confirm Password</label>
                                <div class="input-box">
                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation">
                                     @if ($errors->has('password_confirmation'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-md-offset-5 submit-slide">
                            <input type="submit" value="SIGN UP" class="btn">
                        </div>
                    </form>
                </div>
                <div class="sosiyal-login">
                    <div class="row">
                        <div class="col-sm-3 col-md-3">
                            <a href="#" class="facebook"><i class="fa fa-facebook"></i>Facebook</a>
                        </div>
                        <div class="col-sm-3 col-md-3">
                            <a href="#" class="google-pluse"><i class="fa fa-google-plus"></i>Google</a>
                        </div>
                        <div class="col-sm-3 col-md-3">
                            <a href="#" class="twitter"><i class="fa fa-twitter"></i>Twitter</a>
                        </div>
                        <div class="col-sm-3 col-md-3">
                            <a href="#" class="linkedin"><i class="fa fa-linkedin"></i>Linkedin</a>
                        </div>
                    </div>
                </div>
            </div>
        </section> -->

        <section class="b breadcrumb">
        <div class="container">
            <ul>
                <li><a href="#">Admin</a></li>
                <li><a href="#">Add Student</a></li>
            </ul>
        </div>
        </section>
        @if(session()->has('message'))
        <div class="alert alert-success">
            {{ session()->get('message') }}
        </div>
        @endif
        @if(session()->has('dmessage'))
            <div class="alert alert-danger">
                {{ session()->get('dmessage') }}
            </div>
        @endif
        <div class="spage-content">
        <div class="sform-v10-content">
            <form class="form-detail form-horizontal" role="form" method="POST" action="{{ url('admin/student_add') }}">
            {{ csrf_field() }}
                <div class="form-left">
                    <h2>Student Details</h2>
                    <div class="form-row{{ $errors->has('name') ? ' has-error' : '' }}">
                        <input type="text" name="name" class="company" id="name" value="{{ old('name') }}" placeholder="Name of Student">
                        @if ($errors->has('name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-row{{ $errors->has('email') ? ' has-error' : '' }}">
                        <input type="email" name="email" class="company" id="email" value="{{ old('email') }}" placeholder="E-Mail Address">
                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-row{{ $errors->has('school') ? ' has-error' : '' }}">
                        <input type="text" name="school" class="company" id="school" value="{{ old('school') }}" placeholder="Name of School">
                        @if ($errors->has('school'))
                            <span class="help-block">
                                <strong>{{ $errors->first('school') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-row{{ $errors->has('phone') ? ' has-error' : '' }}">
                        <input type="text" name="phone" class="company" id="phone" value="{{ old('phone') }}" placeholder="Phone Number">
                        @if ($errors->has('phone'))
                        <span class="help-block">
                            <strong>{{ $errors->first('phone') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="sform-group">
                        <div class="form-row col-md-7{{ $errors->has('dob') ? ' has-error' : '' }}">
                        <input type="text" id="dob" name="dob" class="business" onfocus="(this.type='date')" onblur="(this.type='text')" placeholder="Date of Birth" value="{{ old('dob') }}">
                        @if ($errors->has('dob'))
                            <span class="help-block">
                                <strong>{{ $errors->first('dob') }}</strong>
                            </span>
                        @endif
                        </div>
                        <div class="form-row form-row-4{{ $errors->has('yoa') ? ' has-error' : '' }}">
                            <select name="yoa">
                                <option value="{{ old('yoa') }}">Year of Admission</option>
                                <option value="2019">2019</option>
                                <option value="2020">2020</option>
                                <option value="2021">2021</option>
                                <option value="2022">2022</option>
                            </select>
                            <span class="select-btn">
                                <i class="zmdi zmdi-chevron-down"></i>
                            </span>
                            @if ($errors->has('yoa'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('yoa') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="sform-group">
                        <div class="form-row col-md-7{{ $errors->has('center') ? ' has-error' : '' }}">
                            <input type="text" name="centername" class="company" id="centername" value="{{ old('centername') }}" placeholder="Center Name" autocomplete="off">
                            <input type="hidden" name="center" class="company" id="center">
                            @if ($errors->has('center'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('center') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-row form-row-4{{ $errors->has('class') ? ' has-error' : '' }}">
                            <select name="class">
                                <option value="{{ old('class') }}">Select Class</option>
                                <option value="9">9</option>
                                <option value="10">10</option>
                                <option value="plus one">Plus One</option>
                                <option value="plus two">Plus Two</option>
                            </select>
                            <span class="select-btn">
                                <i class="zmdi zmdi-chevron-down"></i>
                            </span>
                            @if ($errors->has('class'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('class') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    
                </div>
                <div class="form-right">
                    <h2>Parent Details</h2>
                    <div class="form-row{{ $errors->has('parent_name') ? 'has-error' : '' }}">
                        <input type="text" name="parent_name" class="street" id="parent_name" placeholder="Name of Parent" value="{{ old('parent_name') }}">
                        @if ($errors->has('parent_name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('parent_name') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-row{{ $errors->has('rws') ? 'has-error' : '' }}">
                        <input type="text" name="rws" class="street" id="rws" placeholder="Relation with Student" value="{{ old('rws') }}">
                        @if ($errors->has('rws'))
                            <span class="help-block">
                                <strong>{{ $errors->first('rws') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-row{{ $errors->has('parent_email') ? 'has-error' : '' }}">
                        <input type="email" name="parent_email" class="additional" id="parent_email" placeholder="E-Mail Address" value="{{ old('parent_email') }}">
                        @if ($errors->has('parent_email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('parent_email') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-row{{ $errors->has('parent_phone') ? 'has-error' : '' }}">
                        <input type="text" name="parent_phone" class="additional" id="parent_phone" placeholder="Phone Number" value="{{ old('parent_phone') }}">
                        @if ($errors->has('parent_phone'))
                            <span class="help-block">
                                <strong>{{ $errors->first('parent_phone') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-row{{ $errors->has('place') ? ' has-error' : '' }}">
                            <input type="text" name="place" class="business" id="place" value="{{ old('place') }}" placeholder="Place">
                            @if ($errors->has('place'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('place') }}</strong>
                                </span>
                            @endif
                    </div>
                    <div class="form-row{{ $errors->has('address') ? 'has-error' : '' }}">
                        <input type="text" name="address" class="additional" id="address" value="{{ old('address') }}" placeholder="Address">
                        @if ($errors->has('address'))
                            <span class="help-block">
                                <strong>{{ $errors->first('address') }}</strong>
                            </span>
                        @endif
                    </div>
                    <!-- <div class="sform-group">
                        <div class="form-row form-row-1{{ $errors->has('password') ? ' has-error' : '' }}">
                            <input type="password" name="password" id="password" class="input-text" placeholder="Password">
                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-row form-row-2{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <input type="password" name="password_confirmation" id="password-confirm" class="input-text" placeholder="Confirm Password">
                            @if ($errors->has('password_confirmation'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div> -->
                    <div class="sform-group">
                        <label for="added_from" class="control-label"></label>
                            <div class="input-box">
                                <input id="added_from" type="hidden" class="form-control" name="added_from" value="admin">
                            </div>
                    </div>

                    <div class="form-row-last">
                        <input type="submit" name="register" class="register" value="Add">
                    </div>

                </div>
            </form>
            </div>
            <br>
        </div>
    </div>
@endsection