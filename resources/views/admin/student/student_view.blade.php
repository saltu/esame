@extends('admin.layouts.app')
@section('content')

<section class="b breadcrumb">
    <div class="container">
        <ul>
            <li><a href="#">Admin</a></li>
            <li><a href="#">Student Approval</a></li>
        </ul>
    </div>
</section>

        <section class="login-view">
            <div class="container">
                <div class="row">
                    <div class="section-title">
                        <h2>New Student</h2>
                        <p>Approve</p>
                    </div>
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/admin/student_add') }}">
                        <div class="col-sm-6">
                            <div class="section-title">
                                <p>Student Details</p>
                            </div>
                            <div class="sform-group">

                                <label for="name" class="control-label">Name Of Student</label>
                                <div class="input-box">
                                    <input id="id" type="hidden" class="form-control" name="id" value="{{ $student[0]->id }}">
                                </div>
                                <div class="input-box">
                                    <input id="name" type="text" class="form-control" name="name" value="{{ $student[0]->name }}">
                                </div>
                            </div>

                            <div class="sform-group">
                                <label for="email" class="control-label">E-Mail Address</label>
                                <div class="input-box">
                                    <input id="email" type="email" class="form-control" name="email" value="{{ $student[0]->email }}">
                                </div>
                            </div>

                            <div class="sform-group">
                                <label for="phone" class="control-label">Phone Number</label>
                                <div class="input-box">
                                    <input id="phone" type="number" class="form-control" name="phone" value="{{ $student[0]->phone }}">
                                </div>
                            </div>

                            <div class="sform-group col-md-6">
                                <label for="school" class="control-label">Name Of School</label>
                                <div class="input-box">
                                    <input id="school" type="text" class="form-control" name="school" value="{{ $student[0]->school }}">
                                </div>
                            </div>

                            <div class="sform-group col-md-6">
                                <label for="center" class="control-label">Center</label>
                                <div class="input-box">
                                   <input id="center" type="text" class="form-control" name="center" value="{{ $student[0]->center }}">
                                </div>
                            </div>

                            <div class="sform-group col-md-6">
                                <label for="class" class="control-label">Select Class</label>
                                <div class="input-box">
                                   <input id="class" type="text" class="form-control" name="class" value="{{ $student[0]->class }}">
                                </div>
                            </div>

                            <div class="sform-group col-md-6">
                                <label for="place" class="control-label">Place</label>
                                <div class="input-box">
                                    <input id="place" type="text" class="form-control" name="place" value="{{ $student[0]->place }}">
                                </div>
                            </div>
                            <div class="sform-group col-md-6">
                                <label for="dob" class="control-label">Date of Birth</label>
                                <div class="input-box">
                                   <input id="dob" type="text" class="form-control" name="dob" value="{{ $student[0]->dob }}">
                                </div>
                            </div>

                            <div class="sform-group col-md-6">
                                <label for="yoa" class="control-label">Year of Addmission</label>
                                <div class="input-box">
                                    <input id="yoa" type="text" class="form-control" name="yoa" value="{{ $student[0]->yoa }}">
                                </div>
                            </div>

                        </div>
                        <div class="col-sm-6">
                            <div class="section-title">
                            <p>Parent Details</p>
                            </div>

                            <div class="sform-group">
                                <label for="parent_name" class="control-label">Name Of Parent</label>
                                <div class="input-box">
                                    <input id="parent_name" type="text" class="form-control" name="parent_name" value="{{ $student[0]->parent_name }}">
                                </div>
                            </div>

                            <div class="sform-group">
                                <label for="rws" class="control-label">Relation with Student</label>
                                <div class="input-box">
                                    <input id="rws" type="text" class="form-control" name="rws" value="{{ $student[0]->rws }}">
                                </div>
                            </div>

                            <div class="sform-group">
                                <label for="parent_email" class="control-label">E-Mail Address</label>
                                <div class="input-box">
                                    <input id="parent_email" type="email" class="form-control" name="parent_email" value="{{ $student[0]->parent_email }}">
                                </div>
                            </div>

                            <div class="sform-group">
                                <label for="parent_phone" class="control-label">Phone Number</label>
                                <div class="input-box">
                                    <input id="parent_phone" type="number" class="form-control" name="parent_phone" value="{{ $student[0]->parent_phone }}">
                                </div>
                            </div>

                            <div class="sform-group">
                                <label for="address" class="control-label">Address</label>
                                <div class="input-box">
                                    <input id="address" type="text" class="form-control" name="address" value="{{ $student[0]->address }}">
                                </div>
                            </div>
                            <div class="sform-group">
                                <label for="added_from" class="control-label"></label>
                                <div class="input-box">
                                    <input id="added_from" type="hidden" class="form-control" name="added_from" value="{{ $student[0]->added_from }}">
                                </div>
                            </div>

                            <!-- <div class="sform-group">
                                <label for="password" class="control-label">Password</label>
                                <div class="input-box">
                                    <input id="password" type="password" class="form-control" name="password">
                                </div>
                            </div>

                            <div class="sform-group">
                                <label for="password-confirm" class="control-label">Confirm Password</label>
                                <div class="input-box">
                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation">
                                </div>
                            </div> -->
                        </div>
                        <div class="col-md-8 col-md-offset-3 submit-slide">
                            <div class="col-md-6"><input style="color: #fff!important;" type="submit" value="Approve" class="btn"></div>
                            <div class="col-md-6"><a onclick="javascript:check=confirm( 'Do You Want To Reject?'); if(check==false) return false;" class="deletecust" href="{{ url('admin/student_delete', $student[0]->id ) }}"><input type="button" value="Reject" class="btn btn-warning"></a></div>
                        </div>
                    </form>
                </div>
                </div>
            </div>
        </section>

@endsection