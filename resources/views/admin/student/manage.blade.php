@extends('admin.layouts.app')
@section('content')

<section class="b breadcrumb">
    <div class="container">
        <ul>
            <li><a href="#">Admin</a></li>
            <li><a href="#">Manage Students</a></li>
        </ul>
    </div>
</section>

@if(session()->has('message'))
    <div class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif
@if(session()->has('dmessage'))
    <div class="alert alert-danger">
        {{ session()->get('dmessage') }}
    </div>
@endif
<section class="login-view">
    <div class="container">
        <!-- <div class="row" id="aapvl_stud">
            <div class="section-title">
            <h3>Students for Approval</h3>
            </div>
                            <table class="table table-striped table-bordered table-hover" id="sample_1">
                                 <thead>
                                    <tr>
                                        <th> Sl.No </th>
                                        <th> Name </th>
                                        <th> Class </th>
                                        <th> From </th>
                                        <th> View </th>
                                        <th> Approve </th>
                                        <th> Reject </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i = 1; ?>
                                    @if(count($students) > 0 )
                                        @foreach ($students as $student)
                                            <tr id="{{ $student->id }}">
                                                <td class="text-center"> {{ $i++ }} </td>
                                                <td class="text-center"> {{ $student->name }} </td>
                                                <td class="text-center"> {{ $student->class }} </td>
                                                <td class="text-center"> {{ $student->added_from }} </td>
                                                <td class="text-center"><a class="editcust" href="{{ url('admin/student_view', $student->id ) }}"> <i class="fa fa-eye" aria-hidden="true"></i> </a></td>
                                                <td class="text-center"><a class="editcust" href="{{ url('admin/student_approve', $student->id ) }}"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i> </a></td>
                                                <td class="text-center"><a onclick="javascript:check=confirm( 'Do You Want To Reject?'); if(check==false) return false;" class="deletecust" href="{{ url('admin/student_delete', $student->id ) }}"> <i class="fa fa-trash" aria-hidden="true"></i> </a></td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
        </div> -->
        <div class="row" id="appd_stud">
            <div class="section-title">
            <p>Registered Students</p>
            </div>
            <table class="table table-striped table-bordered table-hover" id="sample_1">
                                <thead>
                                    <tr>
                                        <th> Sl.No </th>
                                        <th> Status</th>
                                        <th> Name </th>
                                        <th> Class </th>
                                        <th> Place </th>
                                        <th> Email id </th>
                                        <th> Added by </th>
                                        <th> View /<br>Edit </th>
                                        <th> Block /<br>Unblock </th>
                                        <th> Delete </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i = 1; ?>
                                    @if(count($a_students) > 0 )
                                        @foreach ($a_students as $a_student)
                                            <tr id="{{ $student->id }}">
                                                <td class="text-center"> {{ $i++ }} </td>
                                                @if($a_student->status != 0)
                                                <td class="text-center block"> Blocked </td>
                                                @else
                                                <td class="aactive text-center"> Active </td>
                                                @endif
                                                <td class="text-center"> {{ $a_student->name }} </td>
                                                <td class="text-center"> {{ $a_student->class }} </td>
                                                <td class="text-center"> {{ $a_student->place }} </td>
                                                <td class="text-center"> {{ $a_student->email }} </td>
                                                <td class="text-center"> {{ $a_student->center }} </td>
                                                <td class="text-center"><a class="editcust" href="{{ url('admin/approved_student_details', $a_student->id ) }}"> <i class="fa fa-eye" aria-hidden="true"></i> </a></td>
                                                @if($a_student->status == 0)
                                                <td class="text-center"><a class="editcust" href="{{ url('admin/student_block', $a_student->id ) }}"> <i class="fa fa-ban" aria-hidden="true"></i> </a></td>
                                                @else
                                                <td class="text-center"><a class="editcust" href="{{ url('admin/student_unblock', $a_student->id ) }}"> <i class="fa fa-unlock" aria-hidden="true"></i> </a></td>
                                                @endif
                                                <td class="text-center"><a onclick="javascript:check=confirm( 'Do You Want To Delete?'); if(check==false) return false;" class="deletecust" href="{{ url('admin/approved_student_delete', $a_student->id ) }}"> <i class="fa fa-trash" aria-hidden="true"></i> </a></td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
            </table>
        </div>
    </div>
</section>

@endsection