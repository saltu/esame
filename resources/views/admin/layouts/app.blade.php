<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Admin Esame') }}</title>

     <!-- Favicon and touch icons  -->    
    <link href="{{ asset('resources/assets/images/Favicon.png') }}" rel="icon">

    <!-- CSS Stylesheet -->
    <!-- <link href="{{ asset('resources/assets/css/app.css') }}" rel="stylesheet"> -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.1/themes/base/minified/jquery-ui.min.css" type="text/css" />
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/assets/css/bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/assets/css/owl.carousel.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/assets/css/font-awesome.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/assets/css/loader.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/assets/css/docs.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/assets/css/custom.css') }}">
    <link href="https://fonts.googleapis.com/css?family=Arima+Madurai:100,200,300,400,500,700,800,900%7CPT+Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->

</head>
<body>

<div class="wapper">
        <header id="header" class="style2">
            <div class="container">
                <nav id="nav-main">
                    <div class="navbar navbar-inverse">
                        <div class="navbar-header">
                            <a href="{{ url('/admin')}}" class="navbar-brand"><img src="{{ asset('resources/assets/images/logo.png') }}" alt=""></a>
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>

                        <div class="navbar-collapse collapse">
                            <ul class="nav navbar-nav">
                                <li><a href="{{ url('/admin')}}">Home </a></li>
                                @if (Auth::guard('admin')->user()->name != "")
                                    <li class="sub-menu"><a href="javascript:void(0);">Exams</a>
                                        <ul>
                                            <li><a href="{{ url('/admin/manage_exams') }}">Manage Exams</a></li>
                                            <li><a href="{{ url('/admin/question_add') }}">Add Exams Questions</a></li>
                                            <li><a href="{{ url('/admin/manage_exam_package') }}">Manage Exam Package</a></li>
                                            <li><a href="{{ url('/admin/questions') }}">View Questions</a></li>
                                        </ul>
                                    </li>
                                    <li class="sub-menu"><a href="javascript:void(0);">Centers</a>
                                        <ul>
                                            <li><a href="{{ url('/admin/manage_center') }}">Manage Centers</a></li>
                                            <li><a href="{{ url('/admin/center_add') }}">Add Center</a></li>
                                        </ul>
                                    </li>
                                    <li class="sub-menu"><a href="javascript:void(0);">Students</a>
                                        <ul>
                                            <li><a href="{{ url('/admin/students_manage') }}">Manage Students</a></li>
                                            <li><a href="{{ url('/admin/student_add') }}">Add Student</a></li>
                                            <li><a href="{{ url('/admin/student_report') }}">Students Report</a></li>
                                        </ul>
                                    </li>
                                    <li class="sub-menu"><a href="javascript:void(0);">Reports</a>
                                        <ul>
                                            <li><a href="{{ url('/admin/student_report') }}">Students Report</a></li>
                                            <li><a href="{{ url('/admin/center_report') }}">Center Report</a></li>
                                            <li><a href="{{ url('/admin/exam_report') }}">Exam Report</a></li>
                                        </ul>
                                    </li>
                                    <!-- <li><a href="{{ url('/admin/about-us')}}">About Us</a></li>
                                    <li><a href="{{ url('/admin/contact')}}">Contact Us</a></li> -->
                                    <li class="sub-menu">
                                        <a href="javascript:void(0);">{{ Auth::guard('admin')->user()->name }}</a>
                                        <ul>
                                            <!-- <li><a href="typography.html">Typography</a></li> -->
                                            <li><a href="{{ url('/admin/password_reset') }}">Reset Password</a></li>
                                            <li><a href="{{ url('/admin/logout/') }}">Logout</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="{{ url('admin/notification')}}"><i class="fa fa-bell"></i></a></li>
                                @endif
                            </ul>
                        </div>

                    </div>
                </nav>
            </div>
        </header>
       
            @yield('content')

    <footer id="footer">
            
            <div class="container">
                <div class="row">
                    <div class="col-sm-8">
                        <div class="copy-right">
                            <p>Copyright © <span class="year">2019</span> | CornerStone.</p>
                        </div>
                    </div>
                    <div class="col-sm-4 "> 
                        <div class="social-media">
                            <ul>
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-skype"></i></a></li>
                                <li><a href="#"><i class="fa fa-youtube"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->

    <!-- Scripts -->
    <!-- <script src="{{ asset('resources/assets/js/app.js') }}" defer></script> -->
    <script src="{{ asset('resources/assets/js/jquery-1.12.4.min.js') }}" type="text/javascript"></script>
    <!-- <script src="{{ asset('resources/assets/tinymce/tinymce.min.js') }}" type="text/javascript"></script> -->
    <script type="text/javascript" src="http://code.jquery.com/ui/1.10.1/jquery-ui.min.js"></script>
    <script src="{{ asset('resources/assets/js/bootstrap.js') }}" type="text/javascript"></script>
    <script src="{{ asset('resources/assets/js/owl.carousel.js') }}" type="text/javascript"></script>
    <script src="{{ asset('resources/assets/js/jquery.form-validator.min.js') }}" type="text/javascript"></script>
    <!-- <script type='text/javascript' src='https://maps.google.com/maps/api/js?key=AIzaSyAciPo9R0k3pzmKu6DKhGk6kipPnsTk5NU'></script> -->
    <!-- <script src="{{ asset('resources/assets/js/map-styleMain.js') }}" type="text/javascript"></script> -->
   
    <script src="https://cdn.ckeditor.com/4.14.0/standard-all/ckeditor.js" type="text/javascript"></script>
    <script src="{{ asset('resources/assets/js/ckeditor.js') }}" type="text/javascript"></script>
    
    <!-- datatable -->
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <script src="{{ asset('resources/assets/js/placeholder.js') }}" type="text/javascript"></script>
    <script src="{{ asset('resources/assets/js/manual6.js') }}" type="text/javascript"></script>
    <script src="{{ asset('resources/assets/js/manual.js') }}" type="text/javascript"></script>
    <script src="{{ asset('resources/assets/js/coustem.js') }}" type="text/javascript"></script>
    <script src="{{ asset('resources/assets/js/extra.js') }}" type="text/javascript"></script>
    <script src="{{ asset('resources/assets/js/student.js') }}" type="text/javascript"></script>
    
</body>
</html>