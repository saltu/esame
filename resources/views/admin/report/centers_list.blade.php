@extends('admin.layouts.app')
@section('content')
<?php

    $s_name = DB::table('students')
                        ->select('*')
                        ->where('id',$find[0]->stud_id)
                        ->get();
    $i = 1;
?>
<section class="breadcrumb">
    <div class="container">
        <ul>
            <li><a href="{{ url('/center')}}">Admin</a></li>
            <li><a href="{{ url('/center/student_add') }}">Student Report</a></li>
        </ul>
    </div>
</section>
<div class="section-title">     
    <div class="container">
        <div class="row">
            <div class="container" style="max-width:10%; float:left;">
                <div class="row">
                    <div class="col-md-3">
                        <div class="right-slide left">
                            
                        </div>
                    </div>
                </div>
            </div>
            <!-- <form class="col-lg-6 col-lg-offset-3"  method="post" action=""> -->
                <div class="section-title" style="margin-right: 70%; margin-bottom: 3%">
                    <h2>Student Details</h2>
                </div>
        </div>
    </div>
</div>
<div class="container">
<table class="table table-striped table-bordered table-hover" border = 1 id='sample_1'>
    <thead>
        <tr class="tr">
            <th class="text-center">Sl. No</th>
            <th class="text-center">Center id</th>
            <th class="text-center">Center name</th>
            <th class="text-center">Total Students</th>
            <th class="text-center">View Report</th>
        </tr>
    </thead>
    <tbody>
        <?php $i = 1; ?>
        @foreach ($centers as $user)
            <tr>
               <td class="text-center">{{ $i++ }}</td>
               <td class="text-center" id="center_id" name="center_id">{{ $user->id }}</td>
               <td class="text-center">{{ $user->name }}</td>
               <?php
               $students_count = DB::table('students')
                                    ->where('center',$user->id)
                                    ->get();
               ?>
               <td class="text-center">{{ count($students_count) }}</td>
               <td class="text-center"><a class="editcust" href="{{ url('admin/center_detail', $user->id ) }}"><i class="fa fa-eye" aria-hidden="true"></i></a></td>
            </tr>
        @endforeach
    </tbody>
</table>
</div>
@endsection