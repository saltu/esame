@extends('admin.layouts.app')
@section('content')

        <section class="breadcrumb white-bg">
        	<div class="container">
            	<ul>
                	<li><a href="#">Admin</a></li>
                    <li><a href="#">Report</a></li>
                </ul>
            </div>
        </section>
        <section class="quiz-view">
        	<div class="container">
                <div class="quiz-title">
                    <h2>Exam Report</h2>
                    <h3>{{$users[0]->name}}</h3>
                </div>
                <div class="row">
                	<div class="col-sm-4 col-md-3">
<!--                    	<div id="countdown_stop"></div>-->
<!--
                            <div class="qustion-list">
                                <div class="qustion-slide fill">
                                    <div class="qustion-number">Question 1</div>
                                    <span>2</span>
                                </div>
                                <div class="qustion-slide active">
                                    <div class="qustion-number">Question 2</div>
                                    <span>2</span>
                                </div>
                                <div class="qustion-slide">
                                    <div class="qustion-number">Question 3</div>
                                    <span>2</span>
                                </div>
                                <div class="qustion-slide">
                                    <div class="qustion-number">Question 4</div>
                                    <span>2</span>
                                </div>
                                <div class="qustion-slide">
                                    <div class="qustion-number">Question 5</div>
                                    <span>2</span>
                                </div>
                                <div class="qustion-slide">
                                    <div class="qustion-number">Question 6</div>
                                    <span>2</span>
                                </div>
                                <div class="qustion-slide">
                                    <div class="qustion-number">Question 7</div>
                                    <span>2</span>
                                </div>
                                <div class="qustion-slide">
                                    <div class="qustion-number">Question 8</div>
                                    <span>2</span>
                                </div>
                                <div class="qustion-slide">
                                    <div class="qustion-number">Question 9</div>
                                    <span>2</span>
                                </div>
                                <div class="qustion-slide">
                                    <div class="qustion-number">Question 10</div>
                                    <span>2</span>
                                </div>
                            </div>
-->
                    </div>
                    <section class="cart-page">
                            <div class="container">
                                <div class="row">
                                    <div class="col-sm-12">
                                                @if(count($attented_exams) > 0 )
                                                <table class="table table-striped table-bordered table-hover" id="sample_1">
                                                    <thead>
                                                        <tr>
                                                            <th>SL.NO</th>
                                                            <th>CENTER</th>
                                                            <th>EXAM NAME</th>
                                                            <th class="col-sm-4">CHAPTERS</th>
                                                            <th>DATE</th>
                                                            <th>TOTAL QUESTION</th>
                                                            <th>MARKS</th>                          
                                                        </tr>
                                                    </thead>
                                                    <tbody>

                                                    <?php 
                                                     $i = 1;
                                                    ?>
                                                    
                                                        @foreach ($attented_exams as $exams)
                                                        <?php
                                                            $student = DB::table('students')
                                                                                ->select('*')
                                                                                ->where('id', $exams->stud_id)
                                                                                ->get();
                                                            $center = DB::table('centers')
                                                                                ->where('id', $student[0]->center)
                                                                                ->first()
                                                                                ->name;
                                                            $exam_names = $exams->exam_id;
                                                            if ($exam_names != ''){
                                                                $exam_name = DB::table('exam_names')
                                                                                ->select('name','category_id')
                                                                                ->where('id', $exam_names)
                                                                                ->get();
                                //                                $exam_time = DB::table('exam_titles')
                                //                                                ->where('id', $level)
                                //                                                ->first()
                                //                                                ->time;
                                                                // $exam_time = 30;
                                                            }
                                                            $chapters = explode(',', $exams->chapter_id);
                                                            if ($chapters != ''){
                                                                $chap_name = array();
                                                                $subj_names = array();
                                                                foreach ($chapters as $chapter) {
                                                                $subj = DB::table('chapters')
                                                                                ->select('subject_id','name')
                                                                                ->where('id',$chapter)
                                                                                ->get();
                                                                $chap_name[] = $subj[0]->name;    
                                                                $subj_name = DB::table('subjects')
                                                                                ->where('id',$subj[0]->subject_id)
                                                                                ->first()
                                                                                ->name;
                                                                    if (!in_array($subj_name, $subj_names)) {
                                                                        $subj_names[] = $subj_name;
                                                                    }

                                                                }
                                                            }
                                                    
                                                            $type = DB::table('subjects')
                                                                        ->where('id', $subject)
                                                                        ->first()
                                                                        ->type_id;
                                                            if ($type != ''){
                                                                $exm_name = DB::table('exam_names')
                                                                                ->where('id',$type)
                                                                                ->first()
                                                                                ->name;
                                                            }
                                                    
                                                            $category = DB::table('categories')
                                                                        ->select('name','class_id')
                                                                        ->where('id', $exam_name[0]->category_id)
                                                                        ->get();
                                                            // if ($category != ''){
                                                            //     $cat_name = DB::table('categories')
                                                            //                     ->where('id',$category)
                                                            //                     ->first()
                                                            //                     ->name;
                                                            // }
                                                    
                                                            $class = DB::table('classes')
                                                                        ->where('id', $category[0]->class_id)
                                                                        ->first()
                                                                        ->class;
                                                            // if ($class != ''){
                                                            //     $clas_name = DB::table('classes')
                                                            //                     ->where('id', $class)
                                                            //                     ->first()
                                                            //                     ->class;
                                                            // }

                                //                           $question_ids = explode(",",$exams->q_id);
                                //                           $question_options = explode(",",$exams->op_id);
                                                            
                                                            $question_ids = $exams->q_id;          
                                                            $question_options = $exams->op_id;
                                                            
                                                            $results = explode(",",$exams->result);

                                                            $counts = array_count_values($results);
                                                            $score = 0;
                                                            $negative = 0;
                                                            if ($counts['1'] != '')
                                                            {
                                                                $score = $counts['1'];
                                                            }
                                                            if ($counts['-1'] != '')
                                                            {
                                                                $negative = $counts['-1'];
                                                            }
                                                            $no_quest = count($results);
                                                        ?>
                                                        <tr id="{{ $exams->id }}">
                                                            <td> {{ $i++ }} </td>
                                                            <td> {{ $center }} </td>
                                                            <td>
                                                                <div>
                                                                    <div >{{$class}} >> {{$category[0]->name}} >> {{$exam_name[0]->name}} >> @foreach($subj_names as $subj_name) {{$subj_name}} @endforeach</div>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <ul style="list-style-position: inside;">
                                                                @foreach($chap_name as $chap)
                                                                    <li> {{$chap}} </li>
                                                                @endforeach
                                                                </ul>
                                                            </td>
                                                            <td>{{date("d-m-Y", strtotime($exams->date))}}</td>
                                                            <td>{{$exams->t_question}}</td>
                                                            <td>{{($score * 4) - $negative}} / {{4 * $exams->t_question}}</td>
                                                            
                                                        </tr>
                                                        @endforeach
                                                    @else
                                                        <h2 class="text-center"> No tests attented yet </h2>
                                                    @endif
                                                    </tbody>
                                                </table>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
        </section>
        @endsection