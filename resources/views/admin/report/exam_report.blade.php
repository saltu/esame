@extends('admin.layouts.app')
@section('content')
<?php
$i = 1;
?>
<section class="breadcrumb">
    <div class="container">
        <ul>
            <li><a href="{{ url('/admin')}}">Admin</a></li>
            <li><a href="{{ url('/admin/exam_report') }}">Exam Report</a></li>
        </ul>
    </div>
</section>

<div class="section-title">     
    <div class="container">
        <div class="row">
            <!-- <form class="col-lg-6 col-lg-offset-3"  method="post" action=""> -->
                <div class="section-title">
                    <h2>Exam Details</h2>
                </div>
                <div class="form-group{{ $errors->has('exam_types') ? ' has-error' : '' }}" >
                    <!-- <div class="col-md-6" style=" float:left">
                        <select class="form-control dynamicc"  name="exam_types" style="margin-bottom:10%; width:70%; margin-right: 70%;" id="rcategory_id" data-dependent="type_id">
                            <option value="">-- Select Category --</option>
                        </select>
                        @if ($errors->has('exam_types'))
                            <span class="help-block">
                            <strong>{{ $errors->first('exam_types') }}</strong>
                            </span>
                        @endif
                    </div> -->
                    <div class="col-md-12">
                        <p>{{$s_name[0]->name}}  Exam Report</p>
                        @if ( $first_view === null )
                        <section class="cart-page">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <table class="table table-striped table-bordered table-hover" id="sample_1">
                                            <thead>
                                                <tr>
                                                    <th>SL.NO</th>
                                                    <th>EXAM PACKAGE</th>
                                                    <th>EXAM NAME</th>
                                                    <th>TYPE</th>
                                                    <th>STUDENTS ATTENDED</th>                         
                                                </tr>
                                            </thead>
                                            <tbody>
                                            @if(count($pack_id) > 0 )
                                            @foreach ($pack_id as $pack)
                                            <?php
                                            $exam_names = explode(',', $pack->ex_type_ids);
                                            $ex_no = count($exam_names);
                                            $rowspans = 4 * $ex_no;
                                            
                                            ?>
                                                <tr id="{{ $pack->id }}">
                                                    <td rowspan="{{$rowspans}}"> {{ $i++ }} </td>
                                                    <td rowspan="{{$rowspans}}"> {{ $pack->title }} </td>
                                                    <?php
                                                    if (count($exam_names) > 0){
                                                    foreach ($exam_names as $key => $value){
                                                    $category = DB::table('categories')
                                                        ->select('name','class_id')
                                                        ->where('id', $value)
                                                        ->get();
                                        
                                                    $class = DB::table('classes')
                                                                ->where('id', $category[0]->class_id)
                                                                ->first()
                                                                ->class;
                                                    $ex_name = DB::table('exam_names')
                                                                ->select('*')
                                                                ->where('id', $value)
                                                                ->get();
                                                $cumulative = DB::table('exam_results')
                                                            ->select('*')
                                                            ->where('test_type','Cumulative_Test')
                                                            ->where('p_id',$pack->id)
                                                            ->where('exam_id',$value)
                                                            ->get();
                                                $unit = DB::table('exam_results')
                                                            ->select('*')
                                                            ->where('test_type','Unit_Test')
                                                            ->where('p_id',$pack->id)
                                                            ->where('exam_id',$value)
                                                            ->get();
                                                $mock = DB::table('exam_results')
                                                            ->select('*')
                                                            ->where('test_type','Mock_Test')
                                                            ->where('p_id',$pack->id)
                                                            ->where('exam_id',$value)
                                                            ->get();
                                                $model = DB::table('exam_results')
                                                            ->select('*')
                                                            ->where('test_type','Model_Test')
                                                            ->where('p_id',$pack->id)
                                                            ->where('exam_id',$value)
                                                            ->get();
                                                    ?>
                                                    <td rowspan="4"> {{ $class }} -> {{$ex_name[0]->name }} </td>
                                                    <td> Cumulative Test </td>
                                                    <td> {{ count($cumulative) }} </td>
                                                    <tr>
                                                    <td> Unit Test </td>
                                                    <td> {{ count($unit) }} </td>
                                                    </tr>
                                                    <tr>
                                                    <td> Model Test </td>
                                                    <td> {{ count($mock) }} </td>
                                                    </tr>
                                                    <tr>
                                                    <td> Mock Test </td>
                                                    <td> {{ count($model) }} </td>
                                                    </tr>
                                                    <?php
                                                        }
                                                    }
                                                    ?>      
                                                </tr>
                                                @endforeach
                                                @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                        </section>
                        @endif
                    </div>
                </div>
            <!-- </form> -->
        </div>
    </div>
</div>

@endsection