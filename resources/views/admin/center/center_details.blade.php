@extends('admin.layouts.app')

@section('content')
        <section class="breadcrumb white-bg">
            <div class="container">
                <ul>
                    <li><a href="#">Admin</a></li>
                    <li><a href="#">Center Details</a></li>
                </ul>
            </div>
        </section>
        <section class="login-view">
            <div class="container">
                <div class="row">
                    <!-- <div class="col-sm-6"> -->
                        <div class="section-title">
                            <h2>Edit Details</h2>
                            <!-- <p>Add Center</p> -->
                        </div>

                        <form class="col-lg-6 col-lg-offset-3" role="form" method="POST" action="{{ url('/admin/center_edit') }}">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="name" class="mtl control-label">Center Name</label>
                                <div class="col-md-8 input-box">
                                    <input id="id" type="hidden" class="form-control id" name="id" value="{{ $centers[0]->id }}">
                                    <input id="name" type="text" class="form-control" name="name" value="{{ $centers[0]->name }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="place" class="mtl control-label">Place</label>
                                <div class="col-md-8 input-box">
                                    <input id="place" type="text" class="form-control" name="place" value="{{ $centers[0]->place }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="registration_no" class="mtl control-label">Reg.Number</label>
                                <div class="col-md-8 input-box">
                                    <input id="registration_no" type="text" class="form-control" name="registration_no" value="{{ $centers[0]->registration_no }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="address" class="mtl control-label">Address</label>
                                <div class="col-md-8 input-box">
                                    <textarea id="address" rows="5" class="form-control" name="address">{{ $centers[0]->address }}</textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="email" class="mtl control-label">E-Mail Address</label>
                                <div class="col-md-8 input-box">
                                    <input id="email" type="email" class="form-control" name="email" value="{{ $centers[0]->email }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="phone" class="mtl control-label">Phone Number</label>
                                <div class="col-md-8 input-box">
                                    <input id="phone" type="number" class="form-control" name="phone" value="{{ $centers[0]->phone }}">
                                </div>
                            </div>
                            <!-- <div class="form-group">
                                <label for="password" class="mtl control-label">Password</label>
                                <div class="col-md-8 input-box">
                                    <input id="password" type="password" class="form-control" name="password">
                                    
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="password-confirm" class="mtl control-label">Confirm Password</label>
                                <div class="col-md-8 input-box">
                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation">
                                </div>
                            </div> -->
                            <div class="col-md-12 col-md-offset-2 submit-slide">
                            <div class="col-md-4"><input style="color: #fff!important;" type="submit" value="Submit" class="mbtn btn-primary"></div>
                            <div class="col-md-4"><a onclick="javascript:check=confirm( 'Do You Want To Delete?'); if(check==false) return false;" class="deletecust" href="{{ url('admin/center_delete', $centers[0]->id ) }}"><input type="button" value="Delete" class="mbtn btn-primary"></a></div>
                            <?php
                            if ($centers[0]->status != 0) {
                            ?>
                            <div class="col-md-4"><a onclick="javascript:check=confirm( 'Do You Want To Unblock?'); if(check==false) return false;" class="deletecust" href="{{ url('admin/center_unblock', $centers[0]->id ) }}"><input type="button" value="Unblock" class="mbtn btn-primary" style="margin-left: 50%!important;"></a></div>
                            <?php } ?>
                        </div>
                        </form>
                    <!-- </div> -->
                </div>
            </div>
        </section>
@endsection
