@extends('admin.layouts.app')
@section('content')

<section class="b breadcrumb">
    <div class="container">
        <ul>
            <li><a href="#">Admin</a></li>
            <li><a href="#">Manage Centers</a></li>
        </ul>
    </div>
</section>

@if(session()->has('message'))
    <div class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif
@if(session()->has('dmessage'))
    <div class="alert alert-danger">
        {{ session()->get('dmessage') }}
    </div>
@endif
<section class="login-view">
    <div class="container">
        <div class="row">
            <div class="section-title">
                <h3>Manage Centers</h3>
            </div>
            <table class="table table-striped table-bordered table-hover" id="sample_1">
                <thead>
                    <tr>
                        <th> Sl.No </th>
                        <th> Status</th>
                        <th> Name </th>
                        <th> Registration Number </th>
                        <th> Place </th>
                        <th> View/Edit </th>
                        <th> Block </th>
                        <th> Delete </th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i = 1; ?>
                    @if(count($centers) > 0 )
                        @foreach ($centers as $center)
                            <tr id="{{ $center->id }}">
                                <td class="text-center"> {{ $i++ }} </td>
                                @if($center->status != 0)
                                <td class="text-center block"> Blocked </td>
                                @else
                                <td class="text-center aactive"> Active </td>
                                @endif
                                <td class="text-center"> {{ $center->name }} </td>
                                <td class="text-center"> {{ $center->registration_no }} </td>
                                <td class="text-center"> {{ $center->place }} </td>
                                <td class="text-center"><a class="editcust" href="{{ url('admin/center_view', $center->id ) }}"> <i class="fa fa-eye" aria-hidden="true"></i> </a></td>
                                @if($center->status == 0)
                                <td class="text-center"><a class="editcust" href="{{ url('admin/center_block', $center->id ) }}"> <i class="fa fa-ban" aria-hidden="true"></i> </a></td>
                                @else
                                <td class="text-center"><a class="editcust" href="{{ url('admin/center_unblock', $center->id ) }}"> <i class="fa fa-unlock" aria-hidden="true"></i> </a></td>
                                @endif
                                <td class="text-center"><a onclick="javascript:check=confirm( 'Do You Want To Delete?'); if(check==false) return false;" class="deletecust" href="{{ url('admin/center_delete', $center->id ) }}"> <i class="fa fa-trash" aria-hidden="true"></i> </a></td>
                            </tr>
                        @endforeach
                    @endif
                </tbody>
            </table>
        </div>
    </div>
</section>

@endsection