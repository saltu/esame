@extends('admin.layouts.app')

@section('content')
        <section class="breadcrumb white-bg">
            <div class="container">
                <ul>
                    <li><a href="#">Admin</a></li>
                    <li><a href="#">Add Center</a></li>
                </ul>
            </div>
        </section>
        <section class="login-view">
            <div class="container">
                <div class="row">
                    <!-- <div class="col-sm-6"> -->
                        <div class="section-title">
                            <h2>Add Center</h2>
                            <!-- <p>Add Center</p> -->
                        </div>

                        <form class="col-lg-6 col-lg-offset-3" role="form" method="POST" action="{{ url('/admin/center_add') }}">
                            {{ csrf_field() }}
                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="name" class="mtl control-label">Center Name</label>
                                <div class="col-md-8 input-box">
                                    <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}">
                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('place') ? ' has-error' : '' }}">
                                <label for="place" class="mtl control-label">Place</label>
                                <div class="col-md-8 input-box">
                                    <input id="place" type="text" class="form-control" name="place" value="{{ old('place') }}">
                                    @if ($errors->has('place'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('place') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('registration_no') ? ' has-error' : '' }}">
                                <label for="registration_no" class="mtl control-label">Reg.Number</label>
                                <div class="col-md-8 input-box">
                                    <input id="registration_no" type="text" class="form-control" name="registration_no" value="{{ old('registration_no') }}">
                                    @if ($errors->has('registration_no'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('registration_no') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                                <label for="address" class="mtl control-label">Address</label>
                                <div class="col-md-8 input-box">
                                    <textarea id="address" rows="5" class="form-control" name="address" value="{{ old('address') }}"></textarea>
                                    @if ($errors->has('address'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('address') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email" class="mtl control-label">E-Mail Address</label>
                                <div class="col-md-8 input-box">
                                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}">
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                                <label for="phone" class="mtl control-label">Phone Number</label>
                                <div class="col-md-8 input-box">
                                    <input id="phone" type="number" class="form-control" name="phone" value="{{ old('phone') }}">
                                    @if ($errors->has('phone'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('phone') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <!-- <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label for="password" class="mtl control-label">Password</label>
                                <div class="col-md-8 input-box">
                                    <input id="password" type="password" class="form-control" name="password">
                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                <label for="password-confirm" class="mtl control-label">Confirm Password</label>
                                <div class="col-md-8 input-box">
                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation">

                                    @if ($errors->has('password_confirmation'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div> -->
                            <div class="col-md-6 col-md-offset-5 submit-slide">
                                <input type="submit" value="Register" class="mbtn btn-primary">
                            </div>
                        </form>
                    <!-- </div> -->
                </div>

                <!-- <div class="tablerow">

                        <table class="table table-striped table-bordered table-hover" id="sample_1">
                            <thead>
                                <tr>
                                    <th> Sl.No </th>
                                    <th> Center Name </th>
                                    <th> Place </th>
                                    <th> Edit </th>
                                    <th> Delete </th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php $i = 1; ?>
                            @if(count($centers) > 0 )
                                @foreach ($centers as $center)
                                    <tr id="{{ $center->id }}">
                                        <td class="text-center"> {{ $i++ }} </td>
                                        <td class="text-center"> {{ $center->name }} </td>
                                        <td class="text-center"> {{ $center->place }} </td>
                                        <td class="text-center"><a class="editstock" href="{{ route('stock.edit', $product->id ) }}"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i> </a></td>
                                        <td class="text-center"><a onclick="javascript:check=confirm( 'Do You Want To Delete?'); if(check==false) return false;" class="deletestock" href="{{ route('stock.delete', $product->id ) }}"> <i class="fa fa-trash" aria-hidden="true"></i> </a></td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div> -->
            </div>
        </section>
@endsection
