<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Esame') }}</title>

     <!-- Favicon and touch icons  -->    
    <link href="{{ asset('resources/assets/images/Favicon.png') }}" rel="icon">
    <!-- CSS Stylesheet -->
    <!-- <link href="{{ asset('resources/assets/css/app.css') }}" rel="stylesheet"> -->

    <link rel="stylesheet" type="text/css" href="{{ asset('resources/assets/css/bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/assets/css/owl.carousel.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/assets/css/font-awesome.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/assets/css/loader.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/assets/css/docs.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/assets/css/custom.css') }}">
    <link href="https://fonts.googleapis.com/css?family=Arima+Madurai:100,200,300,400,500,700,800,900%7CPT+Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->


</head>
<body>

<div class="wapper">
        <header id="header">
            <div class="container">
                <nav id="nav-main">
                    <div class="navbar navbar-inverse">
                        <div class="navbar-header">
                            <a href="{{ url('/')}}" class="navbar-brand"><img src="{{ asset('resources/assets/images/logo.png') }}" alt=""></a>
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>

                        <div class="navbar-collapse collapse" style="letter-spacing: 1px;">
                            <ul class="nav navbar-nav">
                                <li><a href="{{ url('/')}}">Home </a></li>
                                <li><a href="{{ route('course','1') }}">Courses</a></li>
                                 <li><a href="{{ url('about-us')}}">About Us</a></li>
                                <li><a href="{{ url('contact')}}">Contact Us</a></li>
                                @if (Auth::guard('student')->user()->name != "")
                                <li class="sub-menu">
                                    <a href="javascript:void(0);">{{ Auth::guard('student')->user()->name }}</a>
                                    <ul>
                                        <li><a href="{{ url('/student/logout/') }}">Logout</a></li>
                                    </ul>
                                </li>
                                @elseif (Auth::guard('admin')->user()->name != "")    
                                <li class="sub-menu">
                                    <a href="{{ url('/admin') }}">{{ Auth::guard('admin')->user()->name }}</a>
                                    <ul>
                                        <li><a href="{{ url('/admin/logout/') }}">Logout</a></li>
                                    </ul>
                                </li> 
                                @elseif (Auth::guard('center')->user()->name != "")    
                                <li class="sub-menu">
                                    <a href="javascript:void(0);">{{ Auth::guard('center')->user()->name }}</a>
                                    <ul>
                                        <li><a href="{{ url('/center/logout/') }}">Logout</a></li>
                                    </ul>
                                </li>                           
                                @else
                                <li><a href="{{ route('student') }}">Sign In</a></li>
                                @endif
                            </ul>
                        </div>

                    </div>
                </nav>
            </div>
        </header>

        <div class="bb" style="height: 458px;">
        <section class="signup">
            <!-- <img src="images/signup-bg.jpg" alt=""> -->
            <div class="slcontainer">
                <div class="slsignup-content">
                    <form class='login-form1'role="form" method="POST" action="{{ url('/admin/login') }}">
                            {{ csrf_field() }}
                        <h2 class="text-center hh form-title">Admin Login</h2>
                        <br>
                        @if ($errors->has())
                        <div class="alert alert-danger">
                            <button class="close" data-close="alert"></button>
                             @foreach ($errors->all() as $error)
                                {{ $error }}<br>        
                            @endforeach
                        </div>
                        @endif
                        <div class="slform-group">
                            <input type="email" id="email" class="slform-input" name="email" placeholder="User Name" value="{{ old('email') }}"/>
                        </div>
                        <div class="slform-group">
                            <input id="password" type="password" class="slform-input" name="password" placeholder="Password"/>
                            <span toggle="#password" class="fa fa-eye field-icon toggle-password"></span>
                        </div>
                        <div class="slform-group">
                            <input type="submit" value="Login" class="slform-submit"/>
                        </div>
                    </form>
                   <p class="pp slloginhere">
                        Forgot Password ? <a href="{{ url('#') }}" class="slloginhere-link">Click here</a>
                    </p>
                </div>
            </div>
        </section>
</div>
 <footer id="footer">
            <div class="container">
                <div class="row">
                    <div class="col-sm-8">
                        <div class="copy-right">
                            <p>Copyright © <span class="year">2019</span> | CornerStone.</p>
                        </div>
                    </div>
                    <div class="col-sm-4 "> 
                        <div class="social-media">
                            <ul>
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-skype"></i></a></li>
                                <li><a href="#"><i class="fa fa-youtube"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>
    
    <script src="{{ asset('resources/assets/js/jquery-1.12.4.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('resources/assets/js/bootstrap.js') }}" type="text/javascript"></script>
    <script src="{{ asset('resources/assets/js/owl.carousel.js') }}" type="text/javascript"></script>
    <script src="{{ asset('resources/assets/js/jquery.form-validator.min.js') }}" type="text/javascript"></script>
    <script type='text/javascript' src='https://maps.google.com/maps/api/js?key=AIzaSyAciPo9R0k3pzmKu6DKhGk6kipPnsTk5NU'></script>
    <script src="{{ asset('resources/assets/js/map-styleMain.js') }}" type="text/javascript"></script>
    <script src="{{ asset('resources/assets/js/placeholder.js') }}" type="text/javascript"></script>
    <script src="{{ asset('resources/assets/js/coustem.js') }}" type="text/javascript"></script>
</body>
</html>
