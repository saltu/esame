@extends('admin.layouts.app')

@section('content')
<?php
    if (count($users) == 0) {
    $users = DB::table('students')
                ->select('*')
                ->get();
    }
    $centers = DB::table('centers')
                ->select('*')
                ->get();
?>
<section class="b breadcrumb white-bg">
    <div class="container" >
        <ul >
            <li><a href="{{ url('/admin')}}">Admin</a></li>
            <li><a href="#">Notifications</a></li>
        </ul>
    </div>
</section>
<section class="courses-view">

@if(session()->has('message'))
    <div class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif
@if(session()->has('dmessage'))
    <div class="alert alert-danger">
        {{ session()->get('dmessage') }}
    </div>
@endif    

<div class="section-title">		
    <div class="container">
        <div class="row">
        	<div class="container" style="max-width:10%; float:left;">
		        <div class="row">
		            <div class="col-md-3">
		                <div class="right-slide left">
		                    <h3>Classes</h3>
		                    <ul class="catagorie-list"> 
		                    	<li><a href="{{route('admin.notify',1)}}">XI</a></li>
		                    	<li><a href="{{route('admin.notify',2)}}">XII</a></li>
		                    </ul>
		                </div>
		            </div>
		        </div>
		    </div>
            <div class="col-lg-5">
                <div class="section-title">
                	<h2>Send Notification To Student</h2>
                </div>
	            <div style=" margin-top:20px; float:left;">
		            <label class="radio">
				        <input type="checkbox" name="select_all"  id="select_all"
				        >
				        <span>  Select All</span>
				    </label>
				</div>
                <div style=" margin-top:20px; margin-bottom: 70px; ">
                <form method="post" action="{{ route('admin.notify.search')}}" >
                		<!-- <input type="text" placeholder="Student ID" name="id" id="id"> OR  -->
                        <input type="text" name="centername" class="company centername" id="centername" placeholder="Center Name" autocomplete="off">
                        <input type="hidden" name="center" value="" class="company" id="center">
                		<button type="submit" name="action1" value="search"><i class="fa fa-search"></i></button>	
                </form>
                </div>
                <form method="post" action="{{ route('admin.notify.send')}}" >
                    <div style="max-width: 100%; margin-left: 20px">
                    	<table class="display table-bordered" id="sample_2">
                    		<thead>
                                <tr>
                        			<th> </th>
                        			<th>id</th>
                        			<th>Student name</th>
                                </tr>
                            </thead>
                                <tbody>
                                    <?php $i = 1; ?>
                                    @if(count($users) > 0 )
                            		@foreach ($users as $user)
        		                        <tr id="{{ $user->id }}">
        		                           <td><input type="checkbox" name="selector[]" value="{{ $user->id }}" class = "new_check"></td>
        		                           <td  id="stud_id" name="stud_id" >{{ $user->id }}</td>
        		                           <td>{{ $user->name }}</td>
        		                        </tr>
        		                    @endforeach
                                    @endif
                                </tbody>
                    	</table>
                    </div>
                    <div class="form-row" style="margin-left: 35%;">
                    	<label>Enter Message</label><br>
                    	<textarea class="form-control" id="msg" name="msg"></textarea>
                        <input type="hidden" name="from" value="admin">
                    </div>
                    <div class="form-group" style="margin-left: 250px; margin-top:20px">
                    	<button type="submit" name="action" class="btn" id="action" value="send">Send </button>
                    </div>
                </form>
            </div>

            <div class="col-lg-5">
                <div class="section-title">
                    <h2>Send Notification To Center</h2>
                </div>
                <div style=" margin-top:20px; float:left;">
                    <label class="radio">
                        <input type="checkbox" name="select_all"  id="select_all"
                        >
                        <span>  Select All</span>
                    </label>
                </div>
                <form method="post" action="{{ route('admin.notify.send')}}" >
                    <div style="margin-top: 24.5%; max-width: 100%; margin-left: 20px">
                        <table class="display table-bordered" id="sample_1">
                            <thead>
                                <tr>
                                    <th> </th>
                                    <th>id</th>
                                    <th>Student name</th>
                                </tr>
                            </thead>
                                <tbody>
                                    <?php $i = 1; ?>
                                    @if(count($centers) > 0 )
                                    @foreach ($centers as $center)
                                        <tr id="{{ $center->id }}">
                                           <td><input type="checkbox" name="c_selector[]" value="{{ $center->id }}" class = "new_check"></td>
                                           <td  id="stud_id" name="stud_id" >{{ $center->id }}</td>
                                           <td>{{ $center->name }}</td>
                                        </tr>
                                    @endforeach
                                    @endif
                                </tbody>
                        </table>
                    </div>
                    <div class="form-row" style="margin-left: 35%;">
                        <label>Enter Message</label><br>
                        <textarea class="form-control" id="msg" name="msg"></textarea>
                    </div>
                    <div class="form-group" style="margin-left: 250px; margin-top:20px">
                        <button type="submit" name="action" class="btn" id="action" value="send">Send </button>
                    </div>
                </form>
            </div>

        </div>
	</div>
			
</div>

</section>
@endsection