@extends('student.layouts.app')
@section('title', '')
@section('description', 'Student Home')
@section('content')
@if(session()->has('message'))
    <div class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif
@if(session()->has('dmessage'))
    <div class="alert alert-danger">
        {{ session()->get('dmessage') }}
    </div>
@endif
<div  class="container">
    <div class="section-title">
        <h2>Mock Test</h2><br><br>
    </div>
    <?php
        $sdt_id = Auth::guard('student')->user()->id;
        $payments = DB::table('student_payments')
				        ->select('package_id')
                        ->where('student_id',$sdt_id)
                        ->get();
    
        $class_id = array();
        $exam_id = array();
        $pckg_id = array();
        $info = array(); 
    ?> 
    @if(count($payments) > 0)
        @foreach($payments as $payment)
          <?php
                $data = array();
                $packages = DB::table('exam_type_packages')
                                ->select('*')
                                ->where('id',$payment->package_id)
                                ->get();
            ?>
            @if(count($packages) > 0)
                @foreach($packages as $package)
                     <?php
    
                        $array = explode(",",$package->class_ids);
                        $array2 = explode(",",$package->ex_type_ids);
                        $array3 = explode(",",$package->id);
    
                        foreach($array2 as $val){
                             if(!in_array($val, $exam_id)){
                                 array_push($exam_id,$val);
                             }
                        }
    
                        foreach($array as $val){
                             if(!in_array($val, $class_id)){;
                                 array_push($class_id,$val);
                             } 
                        }
    
                        foreach($array3 as $val){
                             if(!in_array($val, $pckg_id)){;
                                 array_push($pckg_id,$val);
                             } 
                        }
    
                        
                        $data['package_id'] = $package->id;
                        $data['class_ids'] = $package->class_ids;
                     ?>
                @endforeach
            @endif
            <?php
                array_push($info,$data);
            ?>
        @endforeach
    @endif
    <?php
          $string_exam = implode(",",$exam_id);
          $string_cls = implode(",",$class_id);
          $string_pckg = implode(",",$pckg_id);
    ?>
    <input type="hidden" value="{{$string_cls}}" name="class_ids" id="class_ids">
    <input type="hidden" value="{{$string_exam}}" name="exam_ids" id="exam_ids">
    <input type="hidden" value="{{$string_pckg}}" name="pckg_ids" id="pckg_ids">
    
    <div class="row">
        <div class="col-md-6 drop">
            <select class="form-control mock_class" name="mock_class" style="width: 200%;">
                <option value="">-- Select class --</option>
                <?php 
                    foreach ($class_id as $i)
                    {
                        $cls_name = DB::table('classes')
                                        ->where('id',$i)
                                        ->first()
                                        ->class;
                        
                ?>
                        <option value="{{$i}}">{{$cls_name}}</option>
                <?php
                    }
                    $string_clsid = implode(",",$class_id);
                ?>
                <option value="{{$string_clsid}}">All</option>
            </select>
        </div>
<!--
        <div class="col-md-6 drop">
            <select class="form-control unit_package" name="unit_package" style="width: 200%;">
                <option value="">-- Select Package --</option>
                    @if(count($payments) > 0)
                        @foreach($payments as $payment)
                          <?php
                                $packages = DB::table('exam_type_packages')
                                            ->select('*')
                                            ->where('id',$payment->package_id)
                                            ->get();
                            ?>
                            @if(count($packages) > 0)
                                @foreach($packages as $package)
                                    <option value="{{$package->id}}">{{$package->title}}</option>
                                @endforeach
                            @endif
                        @endforeach
                    @endif
            </select>
        </div>
-->
        <br><br><br>
        <div class="col-md-6 drop">
            <select class="form-control mock_exam" name="mock_exam" style="width: 200%;">
                <option value="">-- Select Exam --</option>
            </select>
        </div>
        <br><br>
    </div>
    <div class="row">
<!--        <div class="submit-quiz batton"><a href="javascript:void(0);" class="mock_btn btn">SUBMIT</a></div>-->
    </div>
</div>

@endsection 