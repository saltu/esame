@extends('student.layouts.app')
@section('title', '')
@section('description', 'Student Home')
@section('content')
@if(session()->has('message'))
    <div class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif
@if(session()->has('dmessage'))
    <div class="alert alert-danger">
        {{ session()->get('dmessage') }}
    </div>
@endif
<div  class="container">
    <div class="section-title">
        <h2>Model Test</h2><br><br>
    </div>
    <?php
        $sdt_id = Auth::guard('student')->user()->id;
        $payments = DB::table('student_payments')
				->select('package_id')
                ->where('student_id',$sdt_id)
                ->get();
    ?> 
    
    <div class="row">
        <div class="col-md-6 drop">
            <select class="form-control unit_package" name="unit_package" style="width: 200%;">
                <option value="">-- Select Package --</option>
                    @if(count($payments) > 0)
                        @foreach($payments as $payment)
                          <?php
                                $packages = DB::table('exam_type_packages')
                                            ->select('*')
                                            ->where('id',$payment->package_id)
                                            ->get();
                            ?>
                            @if(count($packages) > 0)
                                @foreach($packages as $package)
                                    <option value="{{$package->id}}">{{$package->title}}</option>
                                @endforeach
                            @endif
                        @endforeach
                    @endif
            </select>
        </div>
        <br><br><br>
        <div class="col-md-6 drop">
            <select class="form-control unit_exam_name" name="unit_exam_name" style="width: 200%;">
                <option value="">-- Select Exam --</option>
            </select>
        </div>
        <br><br>
    </div>
    <div class="row">
        <div class="submit-quiz batton"><a href="javascript:void(0);" class="model_btn btn">SUBMIT</a></div>
    </div>
</div>

@endsection 