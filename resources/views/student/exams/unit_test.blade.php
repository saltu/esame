@extends('student.layouts.app')
@section('title', '')
@section('description', 'Student Home')
@section('content')
@if(session()->has('message'))
    <div class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif
@if(session()->has('dmessage'))
    <div class="alert alert-danger">
        {{ session()->get('dmessage') }}
    </div>
@endif
<div  class="container">
    <div class="section-title">
        <h2>Unit Test</h2><br><br>
    </div>
    <?php
        $sdt_id = Auth::guard('student')->user()->id;
        $payments = DB::table('student_payments')
				        ->select('package_id')
                        ->where('student_id',$sdt_id)
                        ->get();
    
        $class_id = array();
        $exam_id = array();
    ?> 
    @if(count($payments) > 0)
        @foreach($payments as $payment)
          <?php
                $packages = DB::table('exam_type_packages')
                                ->select('*')
                                ->where('id',$payment->package_id)
                                ->get();
            ?>
            @if(count($packages) > 0)
                @foreach($packages as $package)
                     <?php
                        $array = explode(",",$package->class_ids);
                        $array2 = explode(",",$package->ex_type_ids);
    
                        foreach($array2 as $val){
                             if(!in_array($val, $exam_id)){
//                                 array_push($exam_id,$val);
                                 $exam_id[] = $val;
                             }
                        }
    
                        foreach($array as $val){
                             if(!in_array($val, $class_id)){;
                                 array_push($class_id,$val);
                             } 
                        }
                     ?>
                @endforeach
            @endif
        @endforeach
    @endif
    <?php
          $string_exam = implode(",",$exam_id);
          $string_cls = implode(",",$class_id);
    ?>
    <input type="hidden" value="{{$string_cls}}" name="class_ids" id="class_ids">
    <input type="hidden" value="{{$string_exam}}" name="exam_ids" id="exam_ids">
    
    <div class="drop">
            <select class="form-control unit_package" name="unit_package" style="width: 200%;">
                <option value="">-- Select class --</option>
                <?php
                    foreach ($class_id as $i)
                    {
                        $cls_name = DB::table('classes')
                                        ->where('id',$i)
                                        ->first()
                                        ->class;
                ?>
                        <option value="{{$i}}">{{$cls_name}}</option>
                <?php
                    }
                ?>
            </select>

        
<!--
            <select class="form-control unit_package" name="unit_package" style="width: 200%;">
                <option value="">-- Select Package --</option>
                    @if(count($payments) > 0)
                        @foreach($payments as $payment)
                          <?php
                                $packages = DB::table('exam_type_packages')
                                            ->select('*')
                                            ->where('id',$payment->package_id)
                                            ->get();
                            ?>
                            @if(count($packages) > 0)
                                @foreach($packages as $package)
                                    <option value="{{$package->id}}">{{$package->title}}</option>
                                @endforeach
                            @endif
                        @endforeach
                    @endif
            </select>
-->
        <br><br>
            <select class="form-control unit_exam_name" name="unit_exam_name" style="width: 200%;">
                <option value="">-- Select Exam --</option>
            </select>
        <br><br><br><br>
        <p> Select the Chapter for the test!</p>
        <br><br>
    </div>
    <div class="row" id="chapters">
        
    </div>
    <div class="row" id="button">
        
    </div>
</div>


@endsection 