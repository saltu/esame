@extends('student.layouts.app')
@section('title', '')
@section('description', 'Student Home')
@section('content')
@if(session()->has('message'))
    <div class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif
@if(session()->has('dmessage'))
    <div class="alert alert-danger">
        {{ session()->get('dmessage') }}
    </div>
@endif
<div  class="container" id="main_class">
    <div class="section-title">
        <h2>Cumulative Test</h2><br><br>
    </div>
    <?php
        $sdt_id = Auth::guard('student')->user()->id;
        $payments = DB::table('student_payments')
				        ->select('package_id')
                        ->where('student_id',$sdt_id)
                        ->get();
    
        $class_id = array();
        $exam_id = array();
    ?> 
    @if(count($payments) > 0)
        @foreach($payments as $payment)
          <?php
                $packages = DB::table('exam_type_packages')
                                ->select('*')
                                ->where('id',$payment->package_id)
                                ->get();
            ?>
            @if(count($packages) > 0)
                @foreach($packages as $package)
                     <?php
                        $array = explode(",",$package->class_ids);
                        $array2 = explode(",",$package->ex_type_ids);
    
                        foreach($array2 as $val){
                             if(!in_array($val, $exam_id)){
//                                 array_push($exam_id,$val);
                                 $exam_id[] = $val;
                             }
                        }
    
                        foreach($array as $val){
                             if(!in_array($val, $class_id)){;
                                 array_push($class_id,$val);
                             } 
                        }
                     ?>
                @endforeach
            @endif
        @endforeach
    @endif
    <?php
          $string_exam = implode(",",$exam_id);
          $string_cls = implode(",",$class_id);
    ?>
    <input type="hidden" value="{{$string_cls}}" name="class_ids" id="class_ids">
    <input type="hidden" value="{{$string_exam}}" name="exam_ids" id="exam_ids">
    
    <div class="row">
        <div class="col-md-6 drop">
            <select class="form-control selected_class" name="selected_class" style="width: 200%;">
                <option value="">-- Select class --</option>
                <?php
                    foreach ($class_id as $i)
                    {
                        $cls_name = DB::table('classes')
                                        ->where('id',$i)
                                        ->first()
                                        ->class;
                ?>
                        <option value="{{$i}}">{{$cls_name}}</option>
                <?php
                    }
                ?>
            </select>
        </div>
        
        
<!--
        <div class="col-md-6 drop">
            <select class="form-control package" name="package" style="width: 200%;">
                <option value="">-- Select Package --</option>
                    @if(count($payments) > 0)
                        @foreach($payments as $payment)
                          <?php
                                $class_id = array();
                                $packages = DB::table('exam_type_packages')
                                            ->select('*')
                                            ->where('id',$payment->package_id)
                                            ->get();
                            ?>
                            @if(count($packages) > 0)
                                @foreach($packages as $package)
                                    <option value="{{$package->id}}">{{$package->title}}</option>
                                @endforeach
                            @endif
                        @endforeach
                    @endif
            </select>
        </div>
-->
        <br><br><br>
        <div class="col-md-6 drop">
            <select class="form-control exam_name" name="exam_name" style="width: 200%;">
                <option value="">-- Select Exam --</option>
            </select>
        </div>
        <br><br><br><br>
        <p> You can select Maximum of 4 Chapters for the test!</p>
        <br><br>
    </div>
    <div class="row" id="chapters">
        
    </div>
    <div class="row" id="button">
        
    </div>
    
</div>
@endsection