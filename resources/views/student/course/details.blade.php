@extends('student.layouts.app')
@section('content')


<?php
    if ($cls_id != ''){
        $clas_name = DB::table('classes')
                        ->where('id', $cls_id)
                        ->first()
                        ->class;
    }
   if ($cat_id != ''){
        $cat_name = DB::table('catagories')
                        ->where('id',$cat_id)
                        ->first()
                        ->name;
    } 
    if ($exam_id != '')
    {
        $exm_name = DB::table('exam_types')
                        ->where('id',$exam_id)
                        ->first()
                        ->name;
    }
?>


<section class="breadcrumb white-bg">
    <div class="container">
        <ul>
            <li><a href="{{ url('/')}}">Home</a></li>
            <li><a href="{{ route('course',$cls_id ) }}">Courses</a></li>
            <li><a href="{{route('course',$cls_id)}}">{{$clas_name}}</a></li>
            <li><a href="{{ route('course.categories',[$cls_id ,$cat_id])}}">{{$cat_name}}</a></li>
            <li><a href="{{ route('course.details',[$cls_id ,$cat_id,$exam_id,$subj_id])}}">{{$exm_name}}</a></li>
            <li><a href="#">Exam details</a></li>
        </ul>
    </div>
</section>

<?php 
    $examtype = DB::table('exam_types')
                ->select('*')
                ->where('id',$exam_id)
                ->get();
?>

<?php $i = 1; ?>
@if(count($examtype) > 0)
@foreach($examtype as $exam)
<section class="courses-view">
    <div class="course-details">
        <div class="container">
            <div class="row">
                <div class="col-sm-8 col-md-9">
                    <div class="info">
                        <h4>Course Overview</h4>
                        <p>{{ $exam -> description}} </p>
                        <p> </p>
                    </div>                
                    <div class="row">
                        <div class="col-md-3">
                            <div class="right-slide left">
                                <h3>Subjects</h3>
                                <ul class="catagorie-list">
                                <?php 
                                    $exams = DB::table('exams')
                                                ->select('*')
                                                ->where('type_id',$exam_id)
                                                ->get();
                                ?>
                                <?php $i = 1; ?>
                                @if(count($exams) > 0)
                                    @foreach($exams as $examss)
                                        <li><a href="{{ route('student.course.details',[$cls_id ,$cat_id,$exam_id,$examss->id ])}}">{{ $examss->name }}</a></li>    
                                    @endforeach
                                 @endif
                                </ul>
                            </div>
                        </div>                    
                        <div class="col-md-9">                
                            <div class="row">
                                <?php 
                                    $exam_titles = DB::table('exam_titles')
                                                ->select('*')
                                                ->where('subject_id',$subj_id)
                                                ->get();
                                ?>
                                <?php $i = 1; ?>
                                @if(count($exam_titles) > 0)
                                    @foreach($exam_titles as $exam_title)
                                        <div class="col-sm-6 col-md-4" id="" >
                                            <div class="course-post">
                                                <div class="img">
                                                    <img src="{{ asset('resources/assets/images/courses/courses-img4.jpg') }}" alt="">   
                                                    <div class="icon">
                                                        <a href="#"><img src="{{ asset('resources/assets/images/book-icon.png') }}" alt=""></a>
                                                    </div>
                                                </div>
            <!--                                    <div class="info">-->
            <!--                                        <div class="name">name</div>-->
            <!--                                        <div class="expert"><span> dummy </span> dummy </div>-->
            <!--                                    </div>-->
                                                <div class="name" style="text-align:center"> {{ $exam_title->name }} </div>
                                                <div class="product-footer">
                                                    <div class="comment-box">   
                                                        <div class="box">View More</div>
                                                    </div>
                                                    <div class="view-btn">
                                                        <a href="{{ route('student.exam',[$cls_id ,$cat_id,$exam_id,$subj_id,$exam_title->id])}}" class="btn">view more</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>   
                                    @endforeach
                                 @endif
                            </div>
                            <!-- <div class="pagination">
                                <ul>
                                    <li class="next"><a href="#"><i class="fa fa-angle-left"></i><span>Next</span></a></li>
                                    <li class="active"><a href="#">1</a></li>
                                    <li><a href="#">2</a></li>
                                    <li><a href="#">3</a></li>
                                    <li><a href="#">4</a></li>
                                    <li class="prev"><a href="#"><span>prev</span> <i class="fa fa-angle-right"></i></a></li>
                                </ul>
                            </div> -->
                             <div class="syllabus">
                                <h4>Syllabus</h4>
                                <div class="syllabus-box">
                                    <div class="syllabus-title">1st lesson</div>
                                    <div class="syllabus-view first">
                                        <div class="main-point active">Lorem Ipsum is simply dummy text of the printing and typesetting</div>
                                        <div class="point-list">
                                            <ul>
                                                <li><a href="course-lessons.html">vedio : Lorem ipsum dolor sit amet. <span class="hover-text">Watch Video<i class="fa fa-angle-right"></i></span></a></li>
                                                <li><a href="course-lessons.html">Document : semper dolor quis lectus facilisis laoreet. <span class="hover-text">Let's Go<i class="fa fa-angle-right"></i></span></a></li>
                                                <li><a href="quiz-intro.html">Quizzes : auctor quam quis commodo feugiat. <span class="hover-text">upgrade now<i class="fa fa-angle-right"></i></span></a></li>
                                                <li class="no-link">There are many variations of passages of Lorem Ipsum available, </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="syllabus-box">
                                    <div class="syllabus-title">2st lesson</div>
                                    <div class="syllabus-view">
                                        <div class="main-point">It is a long established fact that a reader will be distracted by the</div>
                                        <div class="point-list">
                                            <ul>
                                                <li><a href="course-lessons.html">Lessons : auctor quam quis commodo feugiat. <span class="hover-text">understand now<i class="fa fa-angle-right"></i></span></a></li>                                  
                                                <li><a href="course-lessons.html">vedio : Lorem ipsum dolor sit amet. <span class="hover-text">Watch Video<i class="fa fa-angle-right"></i></span></a></li>
                                                <li><a href="course-lessons.html">Document : semper dolor quis lectus facilisis laoreet. <span class="hover-text">Let's Go<i class="fa fa-angle-right"></i></span></a></li>
                                                <li><a href="quiz-intro.html">Quizzes : auctor quam quis commodo feugiat. <span class="hover-text">upgrade now<i class="fa fa-angle-right"></i></span></a></li>

                                            </ul>
                                        </div>
                                    </div>
                                </div>                    
                            </div>                        
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 col-md-3">
                    <div class="right-slide">
                        <h3>Course Details</h3>
                        <div class="course-details-list"> 
                            <ul>
                                <li><i class="fa fa-user-secret"></i>Rebecca Smith</li>
                                <li><i class="fa fa-file"></i>17 Lessons</li>
                                <li><i class="fa fa-exclamation"></i>7 Quizzes</li>
                                <li><i class="fa fa-file-text-o"></i>13 Documents</li>
                                <li><i class="fa fa-video-camera"></i>9 vedio</li>
                                <li><i class="fa fa-mortar-board"></i>1719 Students</li>
                                <li><i class="fa fa-calendar"></i>16-09-2016 - 15-08-2018 </li>
                            </ul>
                            <!-- <div class="price-info"><span>Price: </span>Free</div> -->
                            <?php 
                            if(Auth::guard('student')->user()->name != ""){
                            ?>
                            <!-- <div class="btn-block"> 
                                <a href="" class="btn">ENROLL</a>
                            </div> -->
                            <?php
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endforeach
@endif 
@endsection