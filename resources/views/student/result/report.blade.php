@extends('student.layouts.app')
@section('content')
<?php 
    $std_id = Auth::guard('student')->user()->id;
?>

        <section class="breadcrumb white-bg">
        	<div class="container">
            	<ul>
                	<li><a href="#">Home</a></li>
                    <li><a href="#">Report</a></li>
                </ul>
            </div>
        </section>
        <section class="quiz-view">
        	<div class="container">
                <div class="quiz-title">
                    <h2>General Quiz</h2>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                </div>
                <div class="row">
                	<div class="col-sm-4 col-md-3">
<!--                    	<div id="countdown_stop"></div>-->
<!--
                            <div class="qustion-list">
                                <div class="qustion-slide fill">
                                    <div class="qustion-number">Question 1</div>
                                    <span>2</span>
                                </div>
                                <div class="qustion-slide active">
                                    <div class="qustion-number">Question 2</div>
                                    <span>2</span>
                                </div>
                                <div class="qustion-slide">
                                    <div class="qustion-number">Question 3</div>
                                    <span>2</span>
                                </div>
                                <div class="qustion-slide">
                                    <div class="qustion-number">Question 4</div>
                                    <span>2</span>
                                </div>
                                <div class="qustion-slide">
                                    <div class="qustion-number">Question 5</div>
                                    <span>2</span>
                                </div>
                                <div class="qustion-slide">
                                    <div class="qustion-number">Question 6</div>
                                    <span>2</span>
                                </div>
                                <div class="qustion-slide">
                                    <div class="qustion-number">Question 7</div>
                                    <span>2</span>
                                </div>
                                <div class="qustion-slide">
                                    <div class="qustion-number">Question 8</div>
                                    <span>2</span>
                                </div>
                                <div class="qustion-slide">
                                    <div class="qustion-number">Question 9</div>
                                    <span>2</span>
                                </div>
                                <div class="qustion-slide">
                                    <div class="qustion-number">Question 10</div>
                                    <span>2</span>
                                </div>
                            </div>
-->
                    </div>
                    <section class="cart-page">
                            <div class="container">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="table-view">
                                            <div class="cart-table">
                                                <table>
                                                    <tr>
                                                        <th>EXAM NAME</th>
                                                        <th>CHAPTERS</th>
                                                        <th>DATE</th>
                                                        <th>TOTAL QUESTION</th>
                                                        <th>MARKS</th>
                                                        <th>RETEST</th>                                        
                                                    </tr>

<?php
            $std = Auth::guard('student')->user()->id;        
            $attented_exams = DB::table('exam_results')
                                ->where('stud_id', $std)
                                ->orderBy('id', 'DESC')
                                ->get();
?>
                    @if(count($attented_exams) > 0 )
                        @foreach ($attented_exams as $exams)
                        <?php
                            $exam_names = $exams->exam_id;
                            if ($exam_names != ''){
                                $exam_name = DB::table('exam_names')
                                                ->select('name','category_id')
                                                ->where('id', $exam_names)
                                                ->get();
//                                $exam_time = DB::table('exam_titles')
//                                                ->where('id', $level)
//                                                ->first()
//                                                ->time;
                                $exam_time = 30;
                            }
                    
                            $chapters = explode(',', $exams->chapter_id);
                            if ($chapters != ''){
                                $chap_name = array();
                                $subj_names = array();
                                foreach ($chapters as $chapter) {
                                $subj = DB::table('chapters')
                                                ->select('subject_id','name')
                                                ->where('id',$chapter)
                                                ->get();
                                $chap_name[] = $subj[0]->name;    
                                $subj_name = DB::table('subjects')
                                                ->where('id',$subj[0]->subject_id)
                                                ->first()
                                                ->name;
                                    if (!in_array($subj_name, $subj_names)) {
                                        $subj_names[] = $subj_name;
                                    }

                                }
                            }
                    
                            $type = DB::table('subjects')
                                        ->where('id', $subject)
                                        ->first()
                                        ->type_id;
                            if ($type != ''){
                                $exm_name = DB::table('exam_names')
                                                ->where('id',$type)
                                                ->first()
                                                ->name;
                            }
                    
                            $category = DB::table('categories')
                                        ->select('name','class_id')
                                        ->where('id', $exam_name[0]->category_id)
                                        ->get();
                            // if ($category != ''){
                            //     $cat_name = DB::table('categories')
                            //                     ->where('id',$category)
                            //                     ->first()
                            //                     ->name;
                            // }
                    
                            $class = DB::table('classes')
                                        ->where('id', $category[0]->class_id)
                                        ->first()
                                        ->class;
                            // if ($class != ''){
                            //     $clas_name = DB::table('classes')
                            //                     ->where('id', $class)
                            //                     ->first()
                            //                     ->class;
                            // }

//                            $question_ids = explode(",",$exams->q_id);
//                            $question_options = explode(",",$exams->op_id);
                            
                            $question_ids = $exams->q_id;          
                            $question_options = $exams->op_id;
                            
                            $results = explode(",",$exams->result);

                            $counts = array_count_values($results);
                            $score = 0;
                            $negative = 0;
                            if ($counts['1'] != '')
                            {
                                $score = $counts['1'];
                            }
                            if ($counts['-1'] != '')
                            {
                                $negative = $counts['-1'];
                            }
                            $no_quest = count($results);
                        ?>
                                                    <tr>
                                                        <td>
                                                            <div>
                                                                <div >{{$class}} >> {{$category[0]->name}} >> {{$exam_name[0]->name}} >> @foreach($subj_names as $subj_name) {{$subj_name}} @endforeach</div>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <ul class="text-left" style="list-style-position: inside;">
                                                            @foreach($chap_name as $chap)
                                                                <li> {{$chap}} </li>
                                                            @endforeach
                                                            </ul>
                                                        </td>
                                                        <td>{{$exams->date}}</td>
                                                        <td>{{$exams->t_question}}</td>
                                                        <td>{{($score * 4) - $negative}} / {{4 * $exams->t_question}}</td>
                                                        <td><a href="{{ route('retest',[$question_ids, $question_options, $exam_time])}}" onclick="retest_clear_storage()" class="btn" >Retest</a></td>
                                                    </tr>
                                                        @endforeach
                                                    @else
                                                    <tr>
                                                        <td colspan="6">No attented Test </td>
                                                    </tr>
                                                    @endif
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
        </section>
        @endsection