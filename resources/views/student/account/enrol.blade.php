@extends('student.layouts.app')
@section('content')
<?php
            // $class = DB::table('classes')
            //             ->select('*')
            //             ->where('id', $e_fee[0]->class)
            //             ->get();
            // $cat = DB::table('catagories')
            //             ->select('*')
            //             ->where('class_id', $e_fee[0]->class)
            //             ->where('id', $e_fee[0]->category)
            //             ->get();
?>
<!-- <section class="b breadcrumb white-bg">
    <div class="container">
        <ul >
            <li><a href="#">Admin</a></li>
            <li><a href="#">Manage Exams</a></li>
        </ul>
    </div>
</section> -->
@if(session()->has('message'))
    <div class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif
@if(session()->has('dmessage'))
    <div class="alert alert-danger">
        {{ session()->get('dmessage') }}
    </div>
@endif
<section class="our-course">
    <div class="container">
        <div class="section-title">
            <h2>Select Packages</h2>
        </div>
        <div class="row">
        <div class="col-md-8 col-md-offset-2">
         <h3 class="text-center padding20">Packages</h3>
        	@if(count($packages1) > 0)
                @foreach($packages1 as $package1)
                <?php
                    $exam_name = DB::table('exam_type_packages')
                                    ->select('*')
                                    ->where('id', $package1->ex_package_id)
                                    ->get();
                    $class_id = explode(',', $exam_name[0]->class_ids);
                    $cat_id = explode(',', $exam_name[0]->ex_type_ids);
                ?>
            <div class="col-md-4">
                <div class="course-box bg-skblue">
                    <div class="course-name text-center">
                        <?php
                        $n_class = count($class_id);
                        $i = 0;
                        do {
                            $clas = DB::table('classes')
                                        ->select('*')
                                        ->where('id', $class_id[$i])
                                        ->get();
                            echo $clas[0]->class;
                            $i++;
                            if( $i < $n_class ){echo " + ";}
                        }while ( $i < $n_class);
                        ?>
                        </div>
                    <div class="comment-">
                        <b>
                        <?php
                        $n_cat = count($cat_id);
                        $j = 0;
                        do {
                            $cat = DB::table('exam_names')
                                        ->select('*')
                                        ->where('id', $cat_id[$j])
                                        ->get();
                            echo $cat[0]->name;
                            $j++;
                            if( $j < $n_cat ){echo ",";}
                        }while ( $j < $n_cat);
                        ?>
                        </b>
                        <div class="rate"><i class="fa fa-inr" aria-hidden="true"></i>
                        	{{$package1->student_amount}}
                        </div>
                        <div class="eenroll">    
                            <a href="{{ route('student.payment.studentpayment',$package1->id) }}" class="btnn">Select</a>
                        </div>
                    </div>
                </div>
            </div>
            	@endforeach
            @endif
        </div>
        <!-- <div class="col-md-8 col-md-offset-2">
         <h3 class="text-center padding20">Plus Two</h3>
            @if(count($packages2) > 0)
                @foreach($packages2 as $package2)
            <div class="col-md-4">
                <div class="course-box bg-skblue">
                    <div class="course-name text-center">{{$package2->package}}</div>
                    <div class="comment-">
                        <div class="rate"><i class="fa fa-inr" aria-hidden="true"></i>
                        	{{$package2->amount}}
                        </div>
                        <div class="eenroll">    
                            <a href="url ('student/payment/', $package2->id)" class="btnn">Select</a>
                        </div>
                    </div>
                </div>
            </div>
            	@endforeach
            @endif
        </div>
        </div> -->
    </div>
</section>

@endsection