<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Student Esame') }}</title>

    <!-- Favicon and touch icons  -->    
    <link href="{{ asset('resources/assets/images/Favicon.png') }}" rel="icon">

    <!-- CSS Stylesheet -->
    <!-- <link href="{{ asset('resources/assets/css/app.css') }}" rel="stylesheet"> -->

    <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.1/themes/base/minified/jquery-ui.min.css" type="text/css" />
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/assets/css/bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/assets/css/owl.carousel.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/assets/css/font-awesome.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/assets/css/loader.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/assets/css/docs.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/assets/css/custom.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/assets/css/akash.css') }}">
    <link href="https://fonts.googleapis.com/css?family=Arima+Madurai:100,200,300,400,500,700,800,900%7CPT+Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->


</head>
<body>
<?php
$notifications = DB::table('notifications')
                            ->select('*')
                            ->where('student_id',Auth::guard('student')->user()->id)
                            ->where('view',0)
                            ->get();
$notification_count = count($notifications);
?>

<div class="wapper">
    <!-- <div id="loader-wrapper">
        <div id="loader"></div>
            <div class="loader-section section-left"></div>
        <div class="loader-section section-right"></div>
    </div> -->
        <div class="quck-nav style2">
            <div class="container">
                <div class="contact-no"><a href="mailto:support@esame.in"><i class="fa fa-envelope"></i>support@esame.in</a></div>
                <div class="contact-no"><a href="tel:"><i class="fa fa-phone"></i>+299 97 39 82</a></div>
                <div class="quck-right">
                    <div class="right-link"><a href="#"><i class="fa fa-handshake-o"></i>Help Center</a></div>
                    <div class="right-link"><a href="#"><i class="fa fa-headphones"></i>Online Support</a></div>
                    <div class="right-link language-select">
                        <a href="javascript:void(0);"><i class="fa fa-language"></i>English</a>
                        <!-- <ul class="language-list">
                            <li><a href="#">Guyana</a></li>
                            <li><a href="#">Haiti</a></li>
                            <li><a href="#">Honduras</a></li>
                            <li><a href="#">Andorra</a></li>
                            <li><a href="#">Armenia</a></li>
                            <li><a href="#">Bahrain</a></li>
                        </ul> -->
                    </div>
                    @if (Auth::guard('student')->user()->name == "")
                    <div class="right-link"><a href="{{ url('center')}}"><i class="fa  fa-user"></i>Center Login</a></div>
                    @endif
                    <!-- <div class="right-link"><a href="{{ url('student')}}"><i class="fa  fa-user"></i>Login \ Register</a></div> -->
                </div>
            </div>
        </div>
        <header id="header" class="style2">
            <div class="container">
                <nav id="nav-main">
                    <div class="navbar navbar-inverse">
                        <div class="navbar-header">
                            <a href="index.html" class="navbar-brand"><img src="{{ asset('resources/assets/images/logo.png') }}" alt=""></a>
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>

                        <div id="notification" class="navbar-collapse collapse">
                            <ul class="nav navbar-nav">
                                <li><a href="{{ url('/student')}}">Home </a></li>
                                @if (Auth::guard('student')->user()->name != "")
<!--                                <li><a href="{{ route('student.course','1') }}">Courses</a></li>-->
                                <li><a href="{{ url('student/about-us')}}">About Us</a></li>
                                <li><a href="{{ url('student/contact')}}">Contact Us</a></li>
                                <li class="sub-menu">
                                    <a href="javascript:void(0);">{{ Auth::guard('student')->user()->name }}</a>
                                    <ul>
                                        <li><a href="{{ route('enrol') }}">Enrol</a></li>
                                        <!-- <li><a href='student.payment.studentpayment'>Payment</a></li> -->
                                        <li><a href="{{ url('student/account')}}">Account</a></li>
                                        <li><a href="{{ url('student/result/report')}}">Report</a></li>
                                        <li><a href="{{ route('student.password_reset') }}">Reset Password</a></li>
                                        <li><a href="{{ url('/student/logout/') }}">Logout</a></li>
                                    </ul>
                                </li>

                                <li><a href="{{ route('notify.students')}}" id="notify">@if($notification_count > 0){{$notification_count}}@endif<i class="fa fa-bell"></i></a></li>
                                @endif
                            </ul>
                        </div>

                    </div>
                </nav>
            </div>
        </header>
        <?php
            $sdt_id = Auth::guard('student')->user()->id;
            $std_sts = DB::table('students')
                        ->select('status')
                        ->where('id',$sdt_id)
                        ->first()
                        ->status;
        ?>
        @if ($std_sts == 0)

            @yield('content')
        @else

        <div class="container">
            <div class="alert alert-danger">
                <h3><center>You are blocked by admin</center></h3>
            </div>
            
                <section class="contact-detail">
                    <div class="container">
                        <div class="section-title">
                            
                            <h2>Get in Touch</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla varius consequat magna, id molestie ipsum volutpat quis. Suspendisse consectetur fringilla suctus.</p>
                        </div>
                        <div class="contact-boxView">
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="contact-box yello">
                                        <div class="icon-box">
                                            <i class="fa fa-map-marker"></i>
                                        </div>
                                        <h4>location</h4>
                                        <p>249 Ung Van Khiem Street, Binh Thanh Dist, HCM City</p>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="contact-box green">
                                        <div class="icon-box">
                                            <i class="fa fa-phone"></i>
                                        </div>
                                        <h4>phone number</h4>
                                        <p>(02)121 321 322</p>
                                        <p>(02)121 321 322</p>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="contact-box red">
                                        <div class="icon-box">
                                            <i class="fa fa-envelope"></i>
                                        </div>
                                        <h4>email address</h4>
                                        <p><a href="mailTo:hello@gmail.com">hello@gmail.com</a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="contact-message">
                    <div class="container">
                        <div class="section-title">
                            <h2>SENT A MESSAGE</h2>
                        </div>
                        <div class="form-filde">
                            <form action="http://design.dev.drcsystems.ooo:8084/themeforest/academy/html/thank-you.html" method="post">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="input-box">
                                            <input type="text" placeholder="Name" data-validation="required" name="name">
                                        </div>
                                        <div class="input-box">
                                            <input type="text" placeholder="Email" data-validation="required" name="email">
                                        </div>
                                        <div class="input-box">
                                            <input type="text" placeholder="Subject" data-validation="required" name="subject" >
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="input-box">
                                            <textarea placeholder="Message" data-validation="required" name="message"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="submit-box">
                                            <input type="submit" value="SEND" class="btn">
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </section>
        </div>

        @endif

    <footer id="footer">
            <div class="footer-top">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6 col-md-3">
                            <h5>Popular Courses</h5>
                            <div class="course-slide">
                                <ul class="footer-link">
                                 <li> ENGINERRING</li>
                                 <li> MEDICAL</li>                                 
                             </ul>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="row">
                                 <div class="col-md-offset-1 col-sm-6 col-md-5 col-xs-6">
                                    <h5>Popular Exams</h5>
                                    <ul class="footer-link">
                                        <li>NEET</li>
                                        <li>JEE</li>
                                        <li>CAT</li>                                        
                                    </ul>   
                                </div>
                                <div class="col-md-offset-1 col-sm-6 col-md-5 col-xs-6">
                                    <h5>Quick Links</h5>
                                    <ul class="footer-link">
                                        <li><a href="{{ url('/')}}">HOME</a></li>
                                        <li><a href="{{ url('course','1','0')}}">COURSES</a></li>
                                        <li><a href="{{ url('about-us')}}">ABOUT US</a></li>

                                        <li><a href="{{ url('contact')}}">CONTACT</a></li>
                                    </ul>    
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-3">
                            <h5>Contact Us</h5>
                            <div class="contact-view">
                                <div class="contact-slide">
                                    <p><i class="fa fa-location-arrow"></i>76 Woodland Ave. Sherman Drive  <br>Fort Walton Beach,Harlingen</p>
                                </div>
                                <div class="contact-slide">
                                    <p><i class="fa fa-phone"></i>+299 97 39 82</p>
                                </div>
                                <div class="contact-slide">
                                    <p><i class="fa fa-fax"></i>(08) 8971 7450</p>
                                </div>
                                <div class="contact-slide">
                                    <p><i class="fa fa-envelope"></i><a href="mailTo:academy@info.com">academy@info.com</a></p>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-sm-8">
                        <div class="copy-right">
                            <p>Copyright © <span class="year">2019</span> | CornerStone.</p>
                        </div>
                    </div>
                    <div class="col-sm-4 "> 
                        <div class="social-media">
                            <ul>
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-skype"></i></a></li>
                                <li><a href="#"><i class="fa fa-youtube"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->

    <!-- Scripts -->
    <!-- <script src="{{ asset('resources/assets/js/app.js') }}" defer></script> -->
    <script src="{{ asset('resources/assets/js/jquery-1.12.4.min.js') }}" type="text/javascript"></script>
    <script type="text/javascript" src="http://code.jquery.com/ui/1.10.1/jquery-ui.min.js"></script>
    <script src="{{ asset('resources/assets/js/bootstrap.js') }}" type="text/javascript"></script>
    <script src="{{ asset('resources/assets/js/owl.carousel.js') }}" type="text/javascript"></script>
    <script src="{{ asset('resources/assets/js/jquery.form-validator.min.js') }}" type="text/javascript"></script>
    <!-- <script type='text/javascript' src='https://maps.google.com/maps/api/js?key=AIzaSyAciPo9R0k3pzmKu6DKhGk6kipPnsTk5NU'></script> -->
    <script src="{{ asset('resources/assets/js/map-styleMain.js') }}" type="text/javascript"></script>
    <!-- datatable -->
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <script src="{{ asset('resources/assets/js/placeholder.js') }}" type="text/javascript"></script>
    <script src="{{ asset('resources/assets/js/manual6.js') }}" type="text/javascript"></script>
    <script src="{{ asset('resources/assets/js/manual.js') }}" type="text/javascript"></script>
    <script src="{{ asset('resources/assets/js/coustem.js') }}" type="text/javascript"></script>
    <script src="{{ asset('resources/assets/js/extra.js') }}" type="text/javascript"></script>
    <script src="{{ asset('resources/assets/js/student.js') }}" type="text/javascript"></script>
    @yield('scripts')
</body>
</html>