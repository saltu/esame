@extends('student.layouts.app')
@section('title', '')
@section('description', 'Student Home')
@section('content')
@if(session()->has('message'))
    <div class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif
@if(session()->has('dmessage'))
    <div class="alert alert-danger">
        {{ session()->get('dmessage') }}
    </div>
@endif

<?php
    $sdt_id = Auth::guard('student')->user()->id;
    $payments = DB::table('student_payments')
				->select('package_id')
                ->where('student_id',$sdt_id)
                ->first()
                ->package_id;
    $std_pass = DB::table('students')
                ->select('pass_change_status')
                ->where('id',$sdt_id)
                ->first()
                ->pass_change_status;
?>
@if ($std_pass == 0) 
     <script>window.location='{{ route("student.password_change") }}'</script>
    die;
@endif

<section class="banner">
<div class="container">
        <div class="section-title">
            <h2>Our Exams</h2><br><br>
        </div>
    <?php $i = 1; ?>
@if(count($payments) > 0)
    <div class="row">
        <div class="col-md-3 col-sm-6 ">
            <div class="service-box">
                <div class="service-icon yellow">
                    <div class="front-content">
                        <i class="fa fa-trophy"></i>
                        <h3>Cumulative Test</h3>
                    </div>
                </div>
                <div class="service-content">
                    <h3>design</h3>
                    <p>No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure</p><br>
                    <div class="eenroll-btn">    
                        <a href="{{ route('student.exams.cumulative') }}"  class="btn">Enroll</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 ">
            <div class="service-box">
                <div class="service-icon orange">
                    <div class="front-content">
                        <i class="fa fa-anchor"></i>
                        <h3>Unit Test</h3>
                    </div>
                </div>
                <div class="service-content">
                    <h3>php developer</h3>
                    <p>No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure</p><br>
                    <div class="eenroll-btn">    
                        <a href="{{route('student.exams.unit')}}" class="btn">Enroll</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-6">
            <div class="service-box ">
                <div class="service-icon red">
                    <div class="front-content">
                        <i class="fa fa-trophy"></i>
                        <h3>Mock Test</h3>
                    </div>
                </div>
                <div class="service-content">
                    <h3>Developer</h3>
                    <p>No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure</p><br>
                    <div class="eenroll-btn">    
                        <a href="{{route('student.exams.mock')}}" class="btn">Enroll</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-6">
            <div class="service-box">
                <div class="service-icon grey">
                    <div class="front-content">
                        <i class="fa fa-paper-plane-o"></i>
                        <h3>Model Test</h3>
                    </div>
                </div>
                <div class="service-content">
                    <h3>java script</h3>
                    <p>No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure</p><br>
                    <div class="eenroll-btn">    
                    <a href="{{route('student.exams.model')}}" class="btn">Enroll</a>
                </div>
                </div>
            </div>
        </div>
    </div>
  @else
    <center><h3>Enroll to See Exams</h3></center>
  @endif 
</div>
</section>
<section class="our-course">
    <div class="container">
        <div class="section-title">
            <h2>Our Courses</h2>
        </div>
        <div class="row">
        <div class="col-md-8">
            <div class="col-md-4">
                <div class="course-box">
                    <div class="img">
                        <img src="resources/assets/images/courses/courses-img1.jpg" alt="">
                    </div>
                    <div class="course-name">Enginerring</div>
                    <div class="comment-row">
                        <div class="rating"> </div>
                        <div class="eenroll-btn">    
                            <a href="#" class="btn">Enroll</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="course-box">
                    <div class="img">
                        <img src="resources/assets/images/courses/courses-img2.jpg" alt="">
                    </div>
                    <div class="course-name">MBBS</div>
                    <div class="comment-row">
                        <div class="rating">
                        </div>
                        <div class="eenroll-btn">    
                            <a href="#" class="btn">Enroll</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="course-box">
                    <div class="img">
                        <img src="resources/assets/images/courses/courses-img3.jpg" alt="">
                    </div>
                    <div class="course-name">CAT</div>
                    <div class="comment-row">
                        <div class="rating">
                        </div>
                        <div class="eenroll-btn">    
                            <a href="#" class="btn">Enroll</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="col-md-13">
                <div class="course-box">
                <div id="myCarousel" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                  <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                  <li data-target="#myCarousel" data-slide-to="1"></li>
                  <li data-target="#myCarousel" data-slide-to="2"></li>
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner">
                  <div class="item active">
                    <img src="https://www.w3schools.com/bootstrap/la.jpg" alt="Los Angeles" style="width:100%; height: 300px;">
                  </div>

                  <div class="item">
                    <img src="https://www.w3schools.com/bootstrap/chicago.jpg" alt="Chicago" style="width:100%; height: 300px;">
                  </div>
                
                  <div class="item">
                    <img src="https://www.w3schools.com/bootstrap/ny.jpg" alt="New york" style="width:100%; height: 300px;">
                  </div>
                </div>

                <!-- Left and right controls -->
                <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                  <!-- <span class="fa fa-arrow-left glyphicon-chevron-left"></span> -->
                  <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#myCarousel" data-slide="next">
                  <!-- <span class="fa fa-arrow-right glyphicon-chevron-right"></span> -->
                  <span class="sr-only">Next</span>
                </a>
              </div>
            </div>
            </div>
        </div>
        </div>
    </div>
</section>
<section class="preparation">
    <div class="container">
        <div class="section-title white">
            <h2 >Maximize your preparation</h2>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry</p>
        </div>
        <div class="preparation-view">
            <div class="item">
                <div class="icon"><img src="resources/assets/images/preparation-icon1.png" alt=""></div>
                <div class="course-name">Highest Quality Content by IIM Alumni  <span>CONTENT</span></div>
                <p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries.</p>
            </div>
            <div class="item">
                <div class="icon"><img src="resources/assets/images/preparation-icon2.png" alt=""></div>
                <div class="course-name">Detailed Analysis with Performance Indicators<span>ANALYSIS</span></div>
                <p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries.</p>
            </div>
            <div class="item">
                <div class="icon"><img src="resources/assets/images/preparation-icon1.png" alt=""></div>
                <div class="course-name">Highest Quality Content by IIM Alumni  <span>CONTENT</span></div>
                <p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries.</p>
            </div>
            <div class="item">
                <div class="icon"><img src="resources/assets/images/preparation-icon2.png" alt=""></div>
                <div class="course-name">Detailed Analysis with Performance Indicators<span>ANALYSIS</span></div>
                <p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries.</p>
            </div>
        </div>
    </div>
</section>
<section class="student-feedback">
    <div class="container">
        <div class="section-title">
            <h2>What our students say</h2>
        </div>
        <div class="feedback-slider">
            <div class="item">
                <div class="student-img"><img src="resources/assets/images/user-img/student-img1.png" alt=""></div>
                <div class="student-name">Hardik Davaria</div>
                <div class="student-designation">software engineer</div>
                <p><i class="fa fa-quote-left"></i> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic. <i class="fa fa-quote-right"></i> </p>
            </div>
            <div class="item">
                <div class="student-img"><img src="resources/assets/images/user-img/student-img1.png" alt=""></div>
                <div class="student-name">Hardik Davaria</div>
                <div class="student-designation">software engineer</div>
                <p><i class="fa fa-quote-left"></i> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic. <i class="fa fa-quote-right"></i> </p>
            </div>
        </div>
    </div>
</section>
<section class="start-learning">
    <div class="container">
        <p>I've read enough. Take me to Exam Formula</p>
        <a href="#" class="btn">Start learning now</a>
    </div>
</section>

@endsection
