@extends('student.layouts.app')
@section('content')
<?php
$exam_fees = DB::table('exam_fees')
              ->select('*')
              ->where('id',$pack_id)
              ->get();
$exam_name = DB::table('exam_type_packages')
              ->select('*')
              ->where('id', $exam_fees[0]->ex_package_id)
              ->get();
$paid_pack = DB::table('student_payments')
              ->select('*')
              ->where('student_id',Auth::guard('student')->user()->id)
              ->where('package_id',$pack_id)
              ->get();
$class_id = explode(',', $exam_name[0]->class_ids);
$cat_id = explode(',', $exam_name[0]->ex_type_ids);
?>

        <section class="breadcrumb white-bg">
        	<div class="container">
            	<ul>
                	<li><a href="#">Home</a></li>
                    <li><a href="#">Payment</a></li>
                </ul>
            </div>
        </section>
        <section class="quiz-view">
        	<div class="container">
                <!-- <div class="quiz-title">
                    <h2>Payment</h2>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                </div> -->
                <!-- <div class="center"> -->
                    <div class="containerr">  
                      <form id="contact" action="{{ route('student.payment.paid') }}" method="post">
                        <h3>Payment</h3><br>
                        <!-- <h4>Contact us today, and get reply with in 24 hours!</h4> -->
                        <fieldset>
                          <input name="user_name" placeholder="Your name" type="text" tabindex="1" value="{{ Auth::guard('student')->user()->name }}" readonly required >

                          <input name="package_id" type="hidden" tabindex="1" value="{{ $exam_name[0]->id }}"required >

                        </fieldset>
                        <fieldset>
                          <input name="email" placeholder="Your Email Address" type="email" tabindex="2" value="{{ Auth::guard('student')->user()->email }}" required autofocus>
                        </fieldset>
                        <fieldset>
                          <input name="course" placeholder="Course" type="text" tabindex="3" value="<?php
                                $n_cat = count($cat_id);
                                $j = 0;
                                do {
                                    $cat = DB::table('exam_names')
                                        ->select('*')
                                        ->where('id', $cat_id[$j])
                                        ->get();
                                    echo $cat[0]->name;
                                    $j++;
                                    if( $j < $n_cat ){echo ",";}
                                }while ( $j < $n_cat);
                              ?>" readonly required>
                        </fieldset>
                        <fieldset>
                          <input name="class" placeholder="Class" type="text" tabindex="4" value="<?php
                                  $n_class = count($class_id);
                                  $i = 0;
                                  do {
                                      $clas = DB::table('classes')
                                        ->select('*')
                                        ->where('id', $class_id[$i])
                                        ->get();
                                      echo $clas[0]->class;
                                      $i++;
                                      if( $i < $n_class ){echo " + ";}
                                  }while ( $i < $n_class);
                              ?>" readonly required>
                        </fieldset>
                        <fieldset>
                          <input name="amount" placeholder="Amount" type="text" readonly tabindex="4" value="{{ $exam_fees[0]->student_amount }}" required>
                        </fieldset>    
                        <fieldset>
                          @if(count($paid_pack) == 0 )
                            <button name="submit" type="submit" id="contact-submit" data-submit="...Sending">Submit</button>
                          @else
                            <h4 class="block"><center>{{Auth::guard('student')->user()->name}} had already payed </center></h4>
                            <center><a href="{{ URL::previous() }}"><button type="button" id="cancel" class="mbtn ">Cancel</button></a></center>
                          @endif
                        </fieldset>
                      </form>
                    </div>
               <!--  </div> -->
            </div>
        </section>
        @endsection
