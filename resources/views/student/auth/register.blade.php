<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Esame') }}</title>

     <!-- Favicon and touch icons  -->    
    <link href="{{ asset('resources/assets/images/Favicon.png') }}" rel="icon">
    <!-- CSS Stylesheet -->
    <!-- <link href="{{ asset('resources/assets/css/app.css') }}" rel="stylesheet"> -->

    <link rel="stylesheet" type="text/css" href="{{ asset('resources/assets/css/bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/assets/css/owl.carousel.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/assets/css/font-awesome.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/assets/css/loader.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/assets/css/docs.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/assets/css/custom.css') }}">
    <link href="https://fonts.googleapis.com/css?family=Arima+Madurai:100,200,300,400,500,700,800,900%7CPT+Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->


</head>
<body>

<div class="wapper">
    <header id="header">
        <div class="container">
            <nav id="nav-main">
                <div class="navbar navbar-inverse">
                    <div class="navbar-header">
                        <a href="{{ url('/')}}" class="navbar-brand"><img src="{{ asset('resources/assets/images/logo.png') }}" alt=""></a>
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>

                    <div class="navbar-collapse collapse" style="letter-spacing: 1px;">
                        <ul class="nav navbar-nav">
                            <li><a href="{{ url('/')}}">Home </a></li>
                            <li><a href="{{ route('course','1') }}">Courses</a></li>
                             <li><a href="{{ url('about-us')}}">About Us</a></li>
                            <li><a href="{{ url('contact')}}">Contact Us</a></li>
                            @if (Auth::guard('student')->user()->name != "")
                            <li class="sub-menu">
                                <a href="javascript:void(0);">{{ Auth::guard('student')->user()->name }}</a>
                                <ul>
                                    <li><a href="{{ url('/student/logout/') }}">Logout</a></li>
                                </ul>
                            </li>
                            @elseif (Auth::guard('admin')->user()->name != "")    
                            <li class="sub-menu">
                                <a href="{{ url('/admin') }}">{{ Auth::guard('admin')->user()->name }}</a>
                                <ul>
                                    <li><a href="{{ url('/admin/logout/') }}">Logout</a></li>
                                </ul>
                            </li> 
                            @elseif (Auth::guard('center')->user()->name != "")    
                            <li class="sub-menu">
                                <a href="javascript:void(0);">{{ Auth::guard('center')->user()->name }}</a>
                                <ul>
                                    <li><a href="{{ url('/center/logout/') }}">Logout</a></li>
                                </ul>
                            </li>                           
                            @else
                            <li><a href="{{ route('student') }}">Sign In</a></li>
                            @endif
                        </ul>
                    </div>

                </div>
            </nav>
        </div>
    </header>
       
        <div class="spage-content">
        <div class="sform-v10-content">
            <form class="form-detail form-horizontal" role="form" method="POST" action="{{ url('/student/a_register') }}">
            {{ csrf_field() }}
                <div class="form-left">
                    <h2>Student Details</h2>
                    <div class="form-row{{ $errors->has('name') ? ' has-error' : '' }}">
                        <input type="text" name="name" class="company" id="name" value="{{ old('name') }}" placeholder="Name Of Student">
                        @if ($errors->has('name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-row{{ $errors->has('email') ? ' has-error' : '' }}">
                        <input type="email" name="email" class="company" id="email" value="{{ old('email') }}" placeholder="E-Mail Address">
                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-row{{ $errors->has('school') ? ' has-error' : '' }}">
                        <input type="text" name="school" class="company" id="school" value="{{ old('school') }}" placeholder="Name Of School">
                        @if ($errors->has('school'))
                            <span class="help-block">
                                <strong>{{ $errors->first('school') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-row{{ $errors->has('phone') ? ' has-error' : '' }}">
                        <input type="text" name="phone" class="company" id="phone" value="{{ old('phone') }}" placeholder="Phone Number">
                        @if ($errors->has('phone'))
                        <span class="help-block">
                            <strong>{{ $errors->first('phone') }}</strong>
                        </span>
                        @endif
                    </div>
                    <!-- <div class="sform-group"> -->
                        <div class="form-row{{ $errors->has('dob') ? ' has-error' : '' }}">
                        <input type="text" id="dob" name="dob" class="business" onfocus="(this.type='date')" onblur="(this.type='text')" placeholder="Date Of Birth" value="{{ old('dob') }}">
                        @if ($errors->has('dob'))
                            <span class="help-block">
                                <strong>{{ $errors->first('dob') }}</strong>
                            </span>
                        @endif
                        </div>
                        <!-- <div class="form-row form-row-4">
                            
                        </div> -->
                    <!-- </div> -->
                    <div class="sform-group">
                        <div class="form-row form-row-3{{ $errors->has('yoa') ? ' has-error' : '' }}">
                            <select name="yoa">
                                <option value="{{ old('yoa') }}">Year Of Addmission</option>
                                <option value="2019">2019</option>
                                <option value="2020">2020</option>
                                <option value="2021">2021</option>
                                <option value="2022">2022</option>
                            </select>
                            <span class="select-btn">
                                <i class="zmdi zmdi-chevron-down"></i>
                            </span>
                            @if ($errors->has('yoa'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('yoa') }}</strong>
                                </span>
                            @endif
                            <input type="hidden" name="center" class="company" id="center" value="student" placeholder="Center Name">
                        </div>
                        <div class="form-row form-row-4{{ $errors->has('class') ? ' has-error' : '' }}">
                            <select name="class">
                                <option value="{{ old('class') }}">Select Class</option>
                                <option value="9">9</option>
                                <option value="10">10</option>
                                <option value="plus one">Plus One</option>
                                <option value="plus two">Plus Two</option>
                            </select>
                            <span class="select-btn">
                                <i class="zmdi zmdi-chevron-down"></i>
                            </span>
                            @if ($errors->has('class'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('class') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    
                </div>
                <div class="form-right">
                    <h2>Parent Details</h2>
                    <div class="form-row{{ $errors->has('parent_name') ? 'has-error' : '' }}">
                        <input type="text" name="parent_name" class="street" id="parent_name" placeholder="Name Of Parent" value="{{ old('parent_name') }}">
                        @if ($errors->has('parent_name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('parent_name') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-row{{ $errors->has('rws') ? 'has-error' : '' }}">
                        <input type="text" name="rws" class="street" id="rws" placeholder="Relation With Student" value="{{ old('rws') }}">
                        @if ($errors->has('rws'))
                            <span class="help-block">
                                <strong>{{ $errors->first('rws') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-row{{ $errors->has('parent_email') ? 'has-error' : '' }}">
                        <input type="email" name="parent_email" class="additional" id="parent_email" placeholder="E-Mail Address" value="{{ old('parent_email') }}">
                        @if ($errors->has('parent_email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('parent_email') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-row{{ $errors->has('parent_phone') ? 'has-error' : '' }}">
                        <input type="text" name="parent_phone" class="additional" id="parent_phone" placeholder="Phone Number" value="{{ old('parent_phone') }}">
                        @if ($errors->has('parent_phone'))
                            <span class="help-block">
                                <strong>{{ $errors->first('parent_phone') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-row{{ $errors->has('place') ? ' has-error' : '' }}">
                            <input type="text" name="place" class="business" id="place" value="{{ old('place') }}" placeholder="Place">
                            @if ($errors->has('place'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('place') }}</strong>
                                </span>
                            @endif
                    </div>
                    <div class="form-row{{ $errors->has('address') ? 'has-error' : '' }}">
                        <input type="text" name="address" class="additional" id="address" value="{{ old('address') }}" placeholder="Address">
                        @if ($errors->has('address'))
                            <span class="help-block">
                                <strong>{{ $errors->first('address') }}</strong>
                            </span>
                        @endif
                    </div>
                    <!-- <div class="sform-group">
                        <div class="form-row form-row-1{{ $errors->has('password') ? ' has-error' : '' }}">
                            <input type="password" name="password" id="password" class="input-text" placeholder="Password">
                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-row form-row-2{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <input type="password" name="password_confirmation" id="password-confirm" class="input-text" placeholder="Confirm Password">
                            @if ($errors->has('password_confirmation'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div> -->

                    <div class="form-row-last">
                        <input type="submit" name="register" class="register" value="Register">
                    </div>
                    <div class="form-checkbox">
                        <label class="container"><p>Have an account ? <a href="{{ url('/student/login') }}" class="slloginhere-link">Click here</a></p>
                            <!-- <input type="checkbox" name="checkbox">
                            <span class="checkmark"></span> -->
                        </label>
                    </div>

                    <!-- <div class="login-view">
                        <div class="sosiyal-login">
                            <div class="row">
                                <div class="col-sm-3 col-md-3">
                                    <a href="#" class="facebook"><i class="fa fa-facebook"></i>Facebook</a>
                                </div>
                                <div class="col-sm-3 col-md-3">
                                    <a href="#" class="google-pluse"><i class="fa fa-google-plus"></i>Google</a>
                                </div>
                                <div class="col-sm-3 col-md-3">
                                    <a href="#" class="twitter"><i class="fa fa-twitter"></i>Twitter</a>
                                </div>
                                <div class="col-sm-3 col-md-3">
                                    <a href="#" class="linkedin"><i class="fa fa-linkedin"></i>Linkedin</a>
                                </div>
                            </div>
                        </div>
                    </div> -->
                </div>
            </form>
            </div>
        </div>
    </div>
    <footer id="footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-8">
                    <div class="copy-right">
                        <p>Copyright © <span class="year">2019</span> | CornerStone.</p>
                    </div>
                </div>
                <div class="col-sm-4 "> 
                    <div class="social-media">
                        <ul>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-skype"></i></a></li>
                            <li><a href="#"><i class="fa fa-youtube"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </footer>
</div>
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->

    <!-- Scripts -->
    <!-- <script src="{{ asset('resources/assets/js/app.js') }}" defer></script> -->
    
    <script src="{{ asset('resources/assets/js/jquery-1.12.4.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('resources/assets/js/bootstrap.js') }}" type="text/javascript"></script>
    <script src="{{ asset('resources/assets/js/owl.carousel.js') }}" type="text/javascript"></script>
    <script src="{{ asset('resources/assets/js/jquery.form-validator.min.js') }}" type="text/javascript"></script>
    <script type='text/javascript' src='https://maps.google.com/maps/api/js?key=AIzaSyAciPo9R0k3pzmKu6DKhGk6kipPnsTk5NU'></script>
    <script src="{{ asset('resources/assets/js/map-styleMain.js') }}" type="text/javascript"></script>
    <script src="{{ asset('resources/assets/js/placeholder.js') }}" type="text/javascript"></script>
    <script src="{{ asset('resources/assets/js/coustem.js') }}" type="text/javascript"></script>
</body>
</html>