@extends('exam.layouts.app')

@section('content')
<section class="breadcrumb white-bg">
    <div class="container">
        <ul>
            <li><a href="#">Home</a></li>
            <li><a href="#">Exam</a></li>
        </ul>
    </div>
</section>
<input type="hidden" value="<?php echo ($std = Auth::guard('student')->user()->id); ?>" id="userid">
<input type="hidden" value="{{$id}}" id="examid">


<section class="quiz-view">
    
    <div id="alert_Modal" class="pop_modal">
      <div class="modal-content">
        <span class="close">&times;</span>
        <p  class="alert alert-warning" >LAST 5 MINUTES</p>
      </div>
    </div>
    <div class="container">
        <div class="quiz-title">
            <h2>Exam {{ $eid }}</h2>
            <?php
                foreach($var as $av){
                    echo $av;
                    echo ',';
                }
            ?>
             <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                 <div class="timer">
                    <span class="timer_span" id="minutes"></span>Minutes
                    <span class="timer_span" id="seconds"></span>Seconds
                </div>
            <p>Instruction For Question</p>
        </div>
        <div class="row">
            <div class="col-sm-4 col-md-3">
                <div id="countdown"></div>
                <div class="qustion-list">
                    <?php
                    $new_arr = array(); 
                    
                    foreach($var as $varr){
//                        echo $varr;
                        array_push($new_arr,$varr);
                    }
                    
                    $i = 1;
//                    echo $new_arr;  
                    $nnew = implode(",",$new_arr);
//                    print_r($var);die;
                    
                    ?>
                    
                    @if(count($var) > 0)
                        <?php $i = 1; ?>
                        @foreach($var as $av)
                        <div id="{{$i}}" class="qustion-slide ">
                            <!--<div class="qustion-number"> Question {{$i}} </div>-->
                            <div class="qustion-number"> 
<!--                        {exam}/{exam_id}/{question_id}-->
                                <p><a href="javascript:void(0);" onclick="load_question('<?php echo $exam; ?>','<?php echo $exam_id; ?>','<?php echo $nnew; ?>','<?php echo $av; ?>');">Question {{$i}}</a></p>
                            </div>
                            <!-- <span>2</span> -->
                        </div>
                        <?php $i++; ?>
                        @endforeach
                    @endif
                </div>
            </div>
            <div class="col-sm-8 col-md-9">
                <div class="qustion-main">
                    <div class="qustion-box">
                        
                        <div id="questions">
                            <?php
                                $q1 = $var[0];
                                if ($q1 != '')
                                {
                                    $question = DB::table('questions')
                                                    ->where('id', $q1)
                                                    ->get();
                                    $options = DB::table('question_options')
                                                    ->where('question_id', $q1)
                                                    ->inRandomOrder()
                                                    ->get();
                                }
                            ?>
                            <p> <b>Question </b> <label id="id_label">1</label>. </p>
                            <div class="qustion">{!! $question[0]->question !!}</div>
                            @if ($question[0]->q_image != null && $question[0]->q_image != 'public/')
                             <div class="qustion"><img src="{{ asset($question[0]->q_image) }}" /></div>
                            @endif
                            <div class="ans">
                                <?php $i = 1; ?>
                                @if(count($options) > 0)
                                    @foreach($options as $option)

                                    <div class="ans-slide">

                                        

<!--                                            <input type="radio" class="option_radio" name="sample-radio[]" id="radio-{{$option->id}}" value="{{$option->id}}" >{{$option->options}}</label>-->
                                        <div style="float: left">
                                            <input type="radio" name="{{$option->question_id}}" class="option_radio radio_align" name="sample-radio[]" id="radio-{{$option->id}}" value="{{$option->id}}" >
                                        </div>
                                        <div>
                                            <label class="radio_container" for="radio-01" value='ddd'>{!!$option->options!!}</label>
                                            @if($option->opt_image != null && $option->opt_image != 'public/')
                                                <img src="{{ asset($option->opt_image) }}" />
                                            @endif
                                        </div>

<!--
                                        <label class="radio_container"> One 
                                            <input type="radio" checked="checked" name="radio">
                                            <span class="radio_checkmark"></span>
                                        </label>
-->
                                    </div>
                                    @endforeach
                                @endif
                            </div>
                            
                        </div>
                        
<!--
                        <div class="save-btn">
                            <a href="#" class="btn2">Save Ans</a>
                        </div>
-->
                        <div class="btn-slide">
                            <?php
                            $qid1 = $qid - 1;
                            $qid2 = $qid + 1;
                            if($qid1 <= 0){
                                $qid1 = 1;
                            }
                            if($qid2 <= 0){
                                $qid2 = 1;
                            }
                            ?>
                            
                            <a id="question_previous" style="visibility: hidden" href="javascript:void(0);" onclick="prev_question('<?php echo $nnew; ?>')" class="btn"><i class="fa fa-angle-left"></i></a>
                            <a href="javascript:void(0);" onclick="refresh_selection()" class="btn"><i class="fa fa-refresh"></i></a>
                            <a id="question_next" href="javascript:void(0);" onclick="next_question('<?php echo $nnew; ?>');" class="btn"><i class="fa fa-angle-right"></i></a>
                            
                        </div>
                    </div>
                    <div class="submit-quiz">
                        <a href="javascript:void(0);" onclick="submit_quiz('<?php echo $nnew; ?>');" class="btn" data-toggle="modal" data-backdrop="static" data-target="#Result_Modal" data-dismiss="modal">submit quiz</a>
                    </div>
                </div>
            </div>
<div class="modal fade" id="Result_Modal" tabindex="-1" role="dialog" aria-labelledby="favoritesModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="favoritesModalLabel"> RESULT </h4>
      </div>
      <div class="modal-body">
        <p>TOTAL QUESTION:: </p><p id="total_ques">  </p>
      </div>
      <div class="modal-body">
        <p>ATTENDED:: </p><p id="attented">  </p>
      </div>
      <div class="modal-body">
        <p>WRONG :: </p><p id="wrong">  </p>
      </div>
      <div class="modal-body">
        <p>CORRECT :: </p><p id="correct">  </p>
      </div>
      <div class="modal-body">
        <p>TOTAL SCORE :: </p><p id="total">  </p>
      </div>
      <div class="modal-footer">
        <button type="button" onclick="window.location='{{url("student/result/report")}}'" type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
            
            
        </div>
    </div>
</section>
@endsection
@section('scripts')
<script>
    function readyFn( jQuery ) {
        load_page('<?php echo $exam_time; ?>');
    }
 
    $( window ).on( "load", readyFn );
    
    // $(document).ready(function() {

    //   $(window).keydown(function(event){

    //     if(event.keyCode == 116) {

    //       event.preventDefault();

    //       return false;

    //     }

    //   });

    // });
</script>
@endsection