@extends('exam.layouts.app')

@section('content')
<!-- <section class="banner inner-page">
    <div class="banner-img"><img src="images/banner/quiz.jpg" alt=""></div>
    <div class="page-title">    
        <div class="container">
            <h1>Quiz Intro</h1>
        </div>
    </div>
</section> -->

<?php
    $question_ids = array();
    $chapter_ids = explode(",",$chapt_ids);
    if ($exam_id != '')
    {
        $exm_name = DB::table('exam_names')
                        ->where('id',$exam_id)
                        ->first()
                        ->name;
    }
    if ($test == 'Cumulative_Test')
    {
        $question_count = 90;
        $exam_time = 90;
        foreach ($chapter_ids as $id)
        {
            $q_ids = DB::table('question_details')
                                ->select('question_id')
                                ->where('chapter_id',$id)
                                ->inRandomOrder()
                                ->limit(10)
                                ->get();
            foreach ($q_ids as $ids)
            {
                array_push($question_ids,$ids->question_id);
            }
        }
    }
    if ($test == 'Unit_Test')
    {
        $question_count = 30;
        $exam_time = 30;
        foreach ($chapter_ids as $id)
        {
            $q_ids = DB::table('question_details')
                                ->select('question_id')
                                ->where('chapter_id',$id)
                                ->get();
            foreach ($q_ids as $id)
            {
                array_push($question_ids,$id->question_id);
            }
        }
    }
    if ($test == 'Mock_Test')
    {
        $question_count = 90;
        $exam_time = 90;
        
        $q_ids = DB::table('question_details')
                            ->select('question_id')
                            ->where('chapter_id',$id)
                            ->inRandomOrder()
                            ->limit(10)
                            ->get();
    }
    if ($test == 'Model_Test')
    {
        $question_count = 180;
        $exam_time = 180;
    }

    
//    echo "Exam ID :". $exam_id ."<br>"; 
//    echo "Chapter ID :".$chapt_ids."<br>";
    echo "Number of Question :".sizeof($question_ids);

//    foreach ($question_ids as $i)
//        {
//            echo $i;
//            echo ",";
//        }
?>

<section class="breadcrumb white-bg">
    <div class="container">
        <ul>
            <li><a href="{{ route('student.course',$cls_id) }}">Exam</a></li>
            <li><a href="{{ route('student.course.details',[$cls_id,$cat_id,$exam_id,$subj_id]) }}">{{$exm_name}}</a></li>
            <li><a href="{{ route('student.course.details',[$cls_id,$cat_id,$exam_id,$subj_id]) }}">{{$test}}</a></li>
        </ul>
    </div>
</section>
<section class="quiz-view">
    <div class="container">
        <div class="quiz-title">
            <h2>Exam</h2>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
        </div>
        <div class="row">
            <div class="col-sm-8 col-md-9">
                <div class="quiz-intro">
                    <h3>Introduction</h3>
                    <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their </p>
                    <h3>Instructions</h3>
                    <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their </p>
                    

                    <?php $i = 1;
                        $std = Auth::guard('student')->user()->id;
                        if(count($question_ids) >= $question_count)
                        {
                            shuffle($question_ids);
                            $array = array_slice($question_ids,0,$question_count);
                            $question_id = urlencode(serialize($array));
                            
                           // foreach ($array as $x)
                           // {
                           //     echo $x;
                           //     echo ',';
                           // }
                            $que =implode(",", $array);
                            $new_var = $exam_id."-".$std;
                    ?>
                            <div class="start-btn">
                                <a href="{{ route('student.exam.start',[$p_id, $test, $chapt_ids, $new_var, $exam_time, $question_id])}}" onclick="clear_storage(); question_save('<?php echo $que; ?>');" class="btn">Start Quizz</a>
                            </div>
                    <?php 
                        }
                        else{
                    ?>
                            <div class="start-btn">
                                <span>Not enough question in Database</span>
                            </div>
                    <?php  
                        }
                    ?>

                </div>
            </div>
        </div>
    </div>
</section>
@endsection