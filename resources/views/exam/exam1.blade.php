@extends('exam.layouts.app')

@section('content')
<section class="breadcrumb white-bg">
    <div class="container">
        <ul>
            <li><a href="#">Home</a></li>
            <li><a href="#">Exam</a></li>
        </ul>
    </div>
</section>


<section class="quiz-view">
    
    <div class="container">
        <div class="quiz-title">
            <h2>Exam </h2>
             <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                 <div class="timer">
                    <span class="timer_span" id="minutes"></span>Minutes
                    <span class="timer_span" id="seconds"></span>Seconds
                </div>
            <p>Instruction For Question</p>
        </div>
        <div class="row">
            <div class="col-sm-4 col-md-3">
                <div id="countdown"></div>
                <div class="qustion-list">

                </div>
            </div>
            <div class="col-sm-8 col-md-9">
                <div class="qustion-main">
                    <div class="qustion-box">
                        
                        <div id="questions">
                            <?php

                                    $question = DB::table('questions')
                                                    ->where('id', 217)
                                                    ->get();
                                    $options = DB::table('question_options')
                                                    ->where('question_id', 217)
                                                    ->inRandomOrder()
                                                    ->get();
                    
                            ?>
                            <p> <b>Question </b> <label>1</label>. </p>
                            <div class="qustion">{!! $question[0]->question !!}</div>
                            @if ($question[0]->q_image != null && $question[0]->q_image != 'public/')
                             <div class="qustion"><img src="{{ asset($question[0]->q_image) }}" /></div>
                            @endif
                            <div class="ans">
                                <?php $i = 1; ?>
                                @if(count($options) > 0)
                                    @foreach($options as $option)

                                    <div class="ans-slide">

                                        
                                            <div style="float: left">
<!--                                            <input type="radio" class="option_radio" name="sample-radio[]" id="radio-{{$option->id}}" value="{{$option->id}}" >{{$option->options}}</label>-->
                                            <input type="radio" name="{{$option->question_id}}" class="option_radio radio_align" name="sample-radio[]" id="radio-{{$option->id}}" value="{{$option->id}}" >
                                            </div>
                                            <div>
                                            <label class="radio_container" for="radio-01" value='ddd'>{!!$option->options!!}</label>
                                            @if($option->opt_image != null && $option->opt_image != 'public/')
                                                <img src="{{ asset($option->opt_image) }}" />
                                            @endif</label>
                                            </div>
                                            <hr>

<!--
                                        <label class="radio_container"> One 
                                            <input type="radio" checked="checked" name="radio">
                                            <span class="radio_checkmark"></span>
                                        </label>
-->
                                    </div>
                                    @endforeach
                                @endif
                            </div>
                            
                        </div>
                        
<!--
                        <div class="save-btn">
                            <a href="#" class="btn2">Save Ans</a>
                        </div>
-->
                        <div class="btn-slide">
                            
                            
                            
                        </div>
                    </div>
                    <div class="submit-quiz">
                        <a href="javascript:void(0);"  class="btn" data-toggle="modal" data-backdrop="static" data-target="#Result_Modal" data-dismiss="modal">submit quiz</a>
                    </div>
                </div>
            </div>
</section>
@endsection