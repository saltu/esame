@extends('center.layouts.main')
@section('title', 'My Account')
@section('description', 'My Account Settings')
@section('content')
				
<ul class="page-breadcrumb breadcrumb">
	<li class="">
		 <a href="{{ url('center') }}">Dashboard</a>
	</li>
	<i class="fa fa-circle"></i>
	<li class="active">
		 My Account
	</li>
</ul>

<div class="row">
	<div class="col-lg-6">
		<!-- BEGIN SAMPLE FORM PORTLET-->
	
		<div class="portlet light">
			<div class="portlet-title">
				<div class="caption">
			<span class="caption-subject font-green-sharp bold uppercase"><i class="fa fa-cogs"></i> My Account</span>
				</div>
			</div>
			<div class="portlet-body form">
				{!! Form::model($model, array('route' => array('accountpost'), 'method' => 'POST')) !!}
					<div class="form-body">
						<div class="form-group">
							<label>Email</label>									
							 {!! Form::text('email', null, array('class' => 'form-control','readonly' => 'readonly')) !!}
						</div>
						<div class="form-group">
							<label>Name</label>
							 {!! Form::text('name', null, array('class' => 'form-control')) !!}
						</div>
					</div>
					<div class="form-actions">
						<button type="submit" class="btn green">Save Changes</button>
					</div>	
				{!! Form::close() !!}
			</div>									
		</div>
	</div>
	<div class="col-lg-6">
		<!-- BEGIN SAMPLE FORM PORTLET-->	  
		<div class="portlet light">
			<div class="portlet-title">
				<div class="caption">
					<span class="caption-subject font-green-sharp bold uppercase"><i class="fa fa-cogs"></i>&nbsp;Change Password</span>
				</div>
			</div>
			<div class="portlet-body form">
			  {!! Form::open(array('route' => 'accountpass')) !!}			
				<div class="form-body">
					
					<div class="form-group">
						<label>New Password</label>
						<input class="form-control" value="{{ old('new_password') }}" type="password" autocomplete="off"  id="new_password" name="new_password"/>
					</div>
					<div class="form-group">
						<label>Retype Password</label>	
						<input class="form-control" type="password" value="{{ old('re_password') }}" autocomplete="off"  id="re_password" name="re_password"/>											
						<span id="error_ajax" style="color:red"><span>
					</div>
				</div>
				<div class="form-actions">
					<button type="submit" class="btn green">Set New Password</button>
				</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>	
@stop
@section('javascript')
<script>
$('#re_password').focusout(function() {
 
	var pass 		=	$('#new_password').val();
	var re_pass 	=	$('#re_password').val();
	
	if(pass != re_pass)
	{
		$('#error_ajax').html('Please Provide Same Password');
		return false;
	}
	else
	{
		$('#error_ajax').hide();
	}
	
});
</script>
@endsection
