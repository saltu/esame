@extends('center.layouts.app')
@section('title', '')
@section('description', 'Center Home')
@section('content')


@if(session()->has('message'))
    <div class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif
@if(session()->has('dmessage'))
    <div class="alert alert-danger">
        {{ session()->get('dmessage') }}
    </div>
@endif


        <section class="banner">
            <div class="banner-img"><img src="resources/assets/images/banner/banner-img1.jpg" alt=""></div>
            <div class="banner-text">
            </div>
        </section>
        <section class="our-course">
            <div class="container">
                <div class="section-title">
                    <h2>Our Courses</h2>
                </div>
                <div class="row">
                    <div class="col-md-4 col-sm-6">
                        <div class="course-box">
                            <div class="img">
                                <img src="resources/assets/images/courses/courses-img1.jpg" alt="">
                                <div class="course-info">
                                    <div class="favorite"><a href="#"><i class="fa fa-heart-o"></i></a></div>
                                </div>
                            </div>
                            <div class="course-name">ENGINEERING</div>
                            <div class="comment-row">
                                <div class="rating">
                                </div>
                                <div class="enroll-btn">    
                                    <a href="#" class="btn">Enroll</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6">
                        <div class="course-box">
                            <div class="img">
                                <img src="resources/assets/images/courses/courses-img2.jpg" alt="">
                            </div>
                            <div class="course-name">NEET</div>
                            <div class="comment-row">
                                <div class="rating">
                                </div>
                                <div class="enroll-btn">    
                                    <a href="#" class="btn">Enroll</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6">
                        <div class="course-box">
                            <div class="img">
                                <img src="resources/assets/images/courses/courses-img3.jpg" alt="">
                            </div>
                            <div class="course-name">CAT</div>
                            <div class="comment-row">
                                <div class="rating">
                                </div>
                                <div class="enroll-btn">    
                                    <a href="#" class="btn">Enroll</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="preparation">
            <div class="container">
                <div class="section-title white">
                    <h2>Maximize your preparation</h2>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry</p>
                </div>
                <div class="preparation-view">
                    <div class="item">
                        <div class="icon"><img src="resources/assets/images/preparation-icon1.png" alt=""></div>
                        <div class="course-name">Highest Quality Content by IIM Alumni  <span>CONTENT</span></div>
                        <p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries.</p>
                    </div>
                    <div class="item">
                        <div class="icon"><img src="resources/assets/images/preparation-icon2.png" alt=""></div>
                        <div class="course-name">Detailed Analysis with Performance Indicators<span>ANALYSIS</span></div>
                        <p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries.</p>
                    </div>
                    <div class="item">
                        <div class="icon"><img src="resources/assets/images/preparation-icon1.png" alt=""></div>
                        <div class="course-name">Highest Quality Content by IIM Alumni  <span>CONTENT</span></div>
                        <p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries.</p>
                    </div>
                    <div class="item">
                        <div class="icon"><img src="resources/assets/images/preparation-icon2.png" alt=""></div>
                        <div class="course-name">Detailed Analysis with Performance Indicators<span>ANALYSIS</span></div>
                        <p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries.</p>
                    </div>
                </div>
            </div>
        </section>
        <section class="start-learning">
            <div class="container">
                <p>I've read enough. Take me to Exam Formula</p>
                <a href="#" class="btn">Start learning now</a>
            </div>
        </section>


@endsection



