@extends('center.layouts.app')
@section('content')
<?php
$notifications = DB::table('notifications')
                                ->select('*')
                                ->where('center_id',Auth::guard('center')->user()->id)
                                ->get();
?>
<section class="b breadcrumb white-bg">
    <div class="container">
        <ul >
            <li><a href="{{ url('/student')}}">Home</a></li>
            <li><a href="#">Notifications</a></li>
        </ul>
    </div>
</section>
<div class="section-title">
			<section class="login-view">
			            <div class="container">
			                <div class="row">
			                    <div class="col-lg-6 col-lg-offset-3">
			                        <div class="section-title">
			                        	<h2>NOTIFICATION</h2>
			                        </div>
			                    </div>
			                    <div class="row col-lg-10 col-lg-offset-1">
			                        <table class="table table-striped table-bordered table-hover" id="sample_1">
			                        	<thead>
			                        		<tr>
			                        			<th class="col-lg-1">Sl.No</th>
			                        			<th class="col-lg-2">Date</th>
			                        			<th class="col-lg-2">Message From</th>
			                        			<th>Message</th>
			                        		</tr>
			                        	</thead>
			                        	<tbody>
			                        		<?php $i = 1; ?>
			                        		@if(count($notifications) > 0)
				                        		@foreach($notifications as $notify)
				                        		<tr id="">
				                        			<td>{{ $i++ }}</td>
				                        			<td class="text-left">{{ date("d-m-Y", strtotime($notify->date))}}</td>
				                        			<td>{{ $notify->from }}</td>
				                        			<td>{{ $notify->message }}</td>
				                        		</tr>
				                        		@endforeach
			                        		@endif
			                        	</tbody>
			                        </table>
			                    </div>
			                </div>
			            </div>
			</section>
		</div>
</div>
@endsection