<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Esame') }}</title>

     <!-- Favicon and touch icons  -->    
    <link href="{{ asset('resources/assets/images/Favicon.png') }}" rel="icon">
    <!-- CSS Stylesheet -->
    <!-- <link href="{{ asset('resources/assets/css/app.css') }}" rel="stylesheet"> -->

    <link rel="stylesheet" type="text/css" href="{{ asset('resources/assets/css/bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/assets/css/owl.carousel.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/assets/css/font-awesome.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/assets/css/loader.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/assets/css/docs.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/assets/css/custom.css') }}">
    <link href="https://fonts.googleapis.com/css?family=Arima+Madurai:100,200,300,400,500,700,800,900%7CPT+Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->


</head>
<body>

<div class="wapper">
        <header id="header">
            <div class="container">
                <nav id="nav-main">
                    <div class="navbar navbar-inverse">
                        <div class="navbar-header">
                            <a href="{{ url('/')}}" class="navbar-brand"><img src="{{ asset('resources/assets/images/logo.png') }}" alt=""></a>
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>

                        <div class="navbar-collapse collapse" style="letter-spacing: 1px;">
                            <ul class="nav navbar-nav">
                                <li><a href="{{ url('/')}}">Home </a></li>
                                <li><a href="{{ route('course','1') }}">Courses</a></li>
                                 <li><a href="{{ url('about-us')}}">About Us</a></li>
                                <li><a href="{{ url('contact')}}">Contact Us</a></li>
                                @if (Auth::guard('student')->user()->name != "")
                                <li class="sub-menu">
                                    <a href="javascript:void(0);">{{ Auth::guard('student')->user()->name }}</a>
                                    <ul>
                                        <li><a href="{{ url('/student/logout/') }}">Logout</a></li>
                                    </ul>
                                </li>
                                @elseif (Auth::guard('admin')->user()->name != "")    
                                <li class="sub-menu">
                                    <a href="{{ url('/admin') }}">{{ Auth::guard('admin')->user()->name }}</a>
                                    <ul>
                                        <li><a href="{{ url('/admin/logout/') }}">Logout</a></li>
                                    </ul>
                                </li> 
                                @elseif (Auth::guard('center')->user()->name != "")    
                                <li class="sub-menu">
                                    <a href="javascript:void(0);">{{ Auth::guard('center')->user()->name }}</a>
                                    <ul>
                                        <li><a href="{{ url('/center/logout/') }}">Logout</a></li>
                                    </ul>
                                </li>                           
                                @else
                                <li><a href="{{ route('student') }}">Sign In</a></li>
                                @endif
                            </ul>
                        </div>

                    </div>
                </nav>
            </div>
        </header>

        <div class="bb" style="padding-top: 20px !important;">
        <section class="login-view">
            <div class="container">
                <div class="row">
                    <!-- <div class="col-sm-6"> -->
                        <div class="section-title">
                            <b><h2 style="color: black;">REGISTER</h2></b>
                        </div>

                        <form class="col-lg-6 col-lg-offset-3" role="form" method="POST" action="{{ url('/center/register') }}">
                            {{ csrf_field() }}
                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="name" class="mtl control-label">Center Name</label>
                                <div class="col-md-8 input-box">
                                    <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}">
                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('place') ? ' has-error' : '' }}">
                                <label for="place" class="mtl control-label">Place</label>
                                <div class="col-md-8 input-box">
                                    <input id="place" type="text" class="form-control" name="place" value="{{ old('place') }}">
                                    @if ($errors->has('place'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('place') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                                <label for="address" class="mtl control-label">Address</label>
                                <div class="col-md-8 input-box">
                                    <textarea id="address" rows="5" class="form-control" name="address" value="{{ old('address') }}"></textarea>
                                    @if ($errors->has('address'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('address') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email" class="mtl control-label">E-Mail Address</label>
                                <div class="col-md-8 input-box">
                                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}">
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                                <label for="phone" class="mtl control-label">Phone Number</label>
                                <div class="col-md-8 input-box">
                                    <input id="phone" type="number" class="form-control" name="phone" value="{{ old('phone') }}">
                                    @if ($errors->has('phone'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('phone') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label for="password" class="mtl control-label">Password</label>
                                <div class="col-md-8 input-box">
                                    <input id="password" type="password" class="form-control" name="password">
                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                <label for="password-confirm" class="mtl control-label">Confirm Password</label>
                                <div class="col-md-8 input-box">
                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation">

                                    @if ($errors->has('password_confirmation'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6 col-md-offset-5 submit-slide">
                                <input type="submit" value="Register" class="btn">
                            </div>
                        </form>
                    <!-- </div> -->
                </div>
            </div>
        </section>
</div>
 <footer id="footer">
            <div class="container">
                <div class="row">
                    <div class="col-sm-8">
                        <div class="copy-right">
                            <p>Copyright © <span class="year">2019</span> | CornerStone.</p>
                        </div>
                    </div>
                    <div class="col-sm-4 "> 
                        <div class="social-media">
                            <ul>
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-skype"></i></a></li>
                                <li><a href="#"><i class="fa fa-youtube"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>
    
    <script src="{{ asset('resources/assets/js/jquery-1.12.4.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('resources/assets/js/bootstrap.js') }}" type="text/javascript"></script>
    <script src="{{ asset('resources/assets/js/owl.carousel.js') }}" type="text/javascript"></script>
    <script src="{{ asset('resources/assets/js/jquery.form-validator.min.js') }}" type="text/javascript"></script>
    <script type='text/javascript' src='https://maps.google.com/maps/api/js?key=AIzaSyAciPo9R0k3pzmKu6DKhGk6kipPnsTk5NU'></script>
    <script src="{{ asset('resources/assets/js/map-styleMain.js') }}" type="text/javascript"></script>
    <script src="{{ asset('resources/assets/js/placeholder.js') }}" type="text/javascript"></script>
    <script src="{{ asset('resources/assets/js/coustem.js') }}" type="text/javascript"></script>
</body>
</html>

