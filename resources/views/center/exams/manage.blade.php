@extends('center.layouts.app')

@section('content')
<section class="b breadcrumb white-bg">
    <div class="container">
        <ul>
            <li><a href="#">Center</a></li>
            <li><a href="#">Manage Exams</a></li>
        </ul>
    </div>
</section>
<div class="course-details">
    <div class="container">
        <div class="row">
            <div class="col-md-9 col-md-offset-2">
                <div class="syllabus">
                    <h4 class="cent text-center">Exam Management</h4>
                    <div class="syllabus-box">
                        
                        <div class="syllabus-view first">
                            <div class="main-point active">Add Exam Category</div>
                            <div class="point-list">
                                <form class="form-horizontal" role="form" method="POST" action="{{ url('center/exam_category') }}">
                                {{ csrf_field() }}
                                    <div class="form-group{{ $errors->has('category') ? ' has-error' : '' }}">
                                        <div class="col-md-7">
                                            <input type="text" name="category" class="form-control" id="category" value="{{ old('category') }}" placeholder="Exam Category">
                                            @if ($errors->has('category'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('category') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-4 col-md-offset-5">
                                            <button type="submit" class="mbtn btn-primary">Add</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="syllabus-box">
                        <div class="syllabus-view">
                            <div class="main-point">Add Exam Type</div>
                            <div class="point-list">
                                <form class="form-horizontal" role="form" method="POST" action="{{ url('admin/exam_type') }}">
                                {{ csrf_field() }}
                                    <div class="form-group">
                                        <div class="col-md-7">
                                            <select class="form-control" name="category">
                                                <option value="Engineering">Engineering</option>
                                                <option value="Medical">Medical</option>
                                                <option value="Others">Others</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
                                        <div class="col-md-7">
                                            <input type="text" name="type" class="form-control" id="type" value="{{ old('type') }}" placeholder="Exam Type">
                                            @if ($errors->has('type'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('type') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-4 col-md-offset-5">
                                            <button type="submit" class="mbtn btn-primary">Add</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="syllabus-box">
                        <div class="syllabus-view">
                            <div class="main-point">Exam Subject</div>
                            <div class="point-list">
                                <form class="form-horizontal" role="form" method="POST" action="{{ url('admin/exam_subject') }}">
                                {{ csrf_field() }}
                                    <div class="form-group">
                                        <div class="col-md-7">
                                            <select class="form-control" name="category">
                                                <option value="Engineering">Engineering</option>
                                                <option value="Medical">Medical</option>
                                                <option value="Others">Others</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-7">
                                            <select class="form-control" name="type">
                                                <option value="NEET">NEET</option>
                                                <option value="JEE">JEE</option>
                                                <option value="Kerala Entrance">Kerala Entrance</option>
                                                <option value="CAT">CAT</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group{{ $errors->has('subject') ? ' has-error' : '' }}">
                                        <div class="col-md-7">
                                            <input type="text" name="subject" class="form-control" id="subject" value="{{ old('subject') }}" placeholder="Exam Type">
                                            @if ($errors->has('subject'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('subject') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-4 col-md-offset-5">
                                            <button type="submit" class="mbtn btn-primary">Add</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                            
                </div>
            </div>
                <div class="col-md-4">
                    <div class="tablerow">
                        <table class="table table-striped table-mborder table-hover" id="sample_1">
                            <thead>
                                <tr>
                                    <th> Sl.No </th>
                                    <th> Exam Category </th>
                                    <th> Edit </th>
                                    <th> Delete </th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php $i = 1; ?>
                            @if(count($categories) > 0 )
                                @foreach ($categories as $category)
                                    <tr id="{{ $category->id }}">
                                        <td class="text-center"> {{ $i++ }} </td>
                                        <td class="text-center"> {{ $category->name }} </td>
                                        <td class="text-center"><a class="editstock" href="{{ route('stock.edit', $product->id ) }}"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i> </a></td>
                                        <td class="text-center"><a onclick="javascript:check=confirm( 'Do You Want To Delete?'); if(check==false) return false;" class="deletestock" href="{{ route('stock.delete', $product->id ) }}"> <i class="fa fa-trash" aria-hidden="true"></i> </a></td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="tablerow">
                        <table class="table table-striped table-mborder table-hover" id="sample_1">
                            <thead>
                                <tr>
                                    <th> Sl.No </th>
                                    <th> Exam type </th>
                                    <th> Edit </th>
                                    <th> Delete </th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php $i = 1; ?>
                            @if(count($exam_type) > 0 )
                                @foreach ($exam_type as $type)
                                    <tr id="{{ $category->id }}">
                                        <td class="text-center"> {{ $i++ }} </td>
                                        <td class="text-center"> {{ $type->name }} </td>
                                        <td class="text-center"><a class="editstock" href="{{ route('stock.edit', $product->id ) }}"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i> </a></td>
                                        <td class="text-center"><a onclick="javascript:check=confirm( 'Do You Want To Delete?'); if(check==false) return false;" class="deletestock" href="{{ route('stock.delete', $product->id ) }}"> <i class="fa fa-trash" aria-hidden="true"></i> </a></td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="tablerow">
                        <table class="table table-striped table-mborder table-hover" id="sample_1">
                            <thead>
                                <tr>
                                    <th> Sl.No </th>
                                    <th> Exam Subject </th>
                                    <th> Edit </th>
                                    <th> Delete </th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php $i = 1; ?>
                            @if(count($subjects) > 0 )
                                @foreach ($subjects as $subject)
                                    <tr id="{{ $category->id }}">
                                        <td class="text-center"> {{ $i++ }} </td>
                                        <td class="text-center"> {{ $subject->name }} </td>
                                        <td class="text-center"><a class="editstock" href="{{ route('stock.edit', $product->id ) }}"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i> </a></td>
                                        <td class="text-center"><a onclick="javascript:check=confirm( 'Do You Want To Delete?'); if(check==false) return false;" class="deletestock" href="{{ route('stock.delete', $product->id ) }}"> <i class="fa fa-trash" aria-hidden="true"></i> </a></td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
        </div>
    </div>
</div>
@endsection
