@extends('center.layouts.app')

@section('content')
<section class="b breadcrumb">
    <div class="container">
        <ul>
            <li><a href="#">Center</a></li>
            <li><a href="#">Add Question</a></li>
        </ul>
    </div>
</section>
<div class="spage-content">
        <div class="sform-v10-content">
            <div class="section-title">
                            <h2>Add Question</h2>
                            <!-- <p>Add Center</p> -->
                        </div>
            <form class="form-detail form-horizontal" role="form" method="POST" action="{{ url('/center/question_add') }}">
            {{ csrf_field() }}
                <div class="form-left">
                    <h2>Question</h2>
                    <div class="form-row{{ $errors->has('exam_category') ? ' has-error' : '' }}">
                        <input class="company" id="exam_category" type="text" name="exam_category" value="{{ old('exam_category') }}" placeholder="Exam Category">
                        @if ($errors->has('exam_category'))
                            <span class="help-block">
                                <strong>{{ $errors->first('exam_category') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-row{{ $errors->has('exam_type') ? ' has-error' : '' }}">
                        <input id="exam_type" type="text" name="exam_type" value="{{ old('exam_type') }}" class="company" placeholder="Exam Type">
                        @if ($errors->has('exam_type'))
                            <span class="help-block">
                                <strong>{{ $errors->first('exam_type') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-row{{ $errors->has('subject') ? ' has-error' : '' }}">
                        <input type="text" name="subject" class="company" id="subject" value="{{ old('subject') }}" placeholder="Subject">
                        @if ($errors->has('subject'))
                            <span class="help-block">
                                <strong>{{ $errors->first('subject') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-row{{ $errors->has('question') ? ' has-error' : '' }}">
                        <input type="text" name="question" class="company" id="question" value="{{ old('question') }}" placeholder="Question">
                        @if ($errors->has('question'))
                            <span class="help-block">
                                <strong>{{ $errors->first('question') }}</strong>
                            </span>
                        @endif
                    </div>
                    
                </div>
                <div class="form-right">
                    <h2>Options</h2>
                    <div class="form-row{{ $errors->has('answer') ? ' has-error' : '' }}">
                        <input type="text" name="answer" class="street" id="answer" placeholder="Answer" value="{{ old('answer') }}">
                        @if ($errors->has('answer'))
                            <span class="help-block">
                                <strong>{{ $errors->first('answer') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-row{{ $errors->has('option1') ? ' has-error' : '' }}">
                        <input type="text" name="option1" class="additional" id="option1" placeholder="Option 1" value="{{ old('option1') }}">
                        @if ($errors->has('option1'))
                            <span class="help-block">
                                <strong>{{ $errors->first('option1') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-row{{ $errors->has('option2') ? ' has-error' : '' }}">
                        <input type="text" name="option2" class="additional" id="option2" placeholder="Option 2" value="{{ old('option2') }}">
                        @if ($errors->has('option2'))
                            <span class="help-block">
                                <strong>{{ $errors->first('option2') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-row{{ $errors->has('option3') ? ' has-error' : '' }}">
                        <input type="text" name="option3" class="additional" id="option3" placeholder="Option 3" value="{{ old('option3') }}">
                        @if ($errors->has('option3'))
                            <span class="help-block">
                                <strong>{{ $errors->first('option3') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-row-last">
                        <input type="submit" name="register" class="register" value="Add">
                    </div>

                </div>
            </form>
            </div>
            <div class="container">
            <div class="tablerow">

                        <table class="table table-striped table-mborder table-hover" id="sample_1">
                            <thead>
                                <tr>
                                    <th> Sl.No </th>
                                    <th> Exam Category </th>
                                    <th> Exam Type </th>
                                    <th> Subject </th>
                                    <th> Question </th>
                                    <th> Answer </th>
                                    <th> Option1 </th>
                                    <th> Option2 </th>
                                    <th> Option3 </th>
                                    <th> Edit </th>
                                    <th> Delete </th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php $i = 1; ?>
                            @if(count($categories) > 0 )
                                @foreach ($categories as $category)
                                    <tr id="{{ $category->id }}">
                                        <td class="text-center"> {{ $i++ }} </td>
                                        <td class="text-center"> {{ $category->name }} </td>
                                        <td class="text-center"> {{ $exam_type->name }} </td>
                                        <td class="text-center"> {{ $subject->name }} </td>
                                        <td class="text-center"> {{ $questionss->question }} </td>
                                        <td class="text-center"> {{ $questionss->solution }} </td>
                                        <td class="text-center"> {{ $options->option1 }} </td>
                                        <td class="text-center"> {{ $options->option2 }} </td>
                                        <td class="text-center"> {{ $options->option3 }} </td>
                                        <td class="text-center"><a class="editstock" href="{{ route('stock.edit', $product->id ) }}"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i> </a></td>
                                        <td class="text-center"><a onclick="javascript:check=confirm( 'Do You Want To Delete?'); if(check==false) return false;" class="deletestock" href="{{ route('stock.delete', $product->id ) }}"> <i class="fa fa-trash" aria-hidden="true"></i> </a></td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
        </div>
    </div>
@endsection
