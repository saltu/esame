@extends('center.layouts.app')
@section('content')

<?php
    $cate = array();
    if ($cls_id != ''){
        $clas_name = DB::table('classes')
						->select('class')
                        ->where('id',$cls_id)
                        ->first()
                        ->class;
        
    }
    if ($cat_id != ''){
        $cat_name = DB::table('categories')
						->select('name')
                        ->where('id',$cat_id)
                        ->first()
                        ->name;
    }
   else{
        $cat_name = "ALL";
    }
?>
<section class="breadcrumb white-bg">
    <div class="container">
        <ul>
            <li><a href="{{ url('/')}}">Home</a></li>
            <li><a href="{{ route('course',$cls_id ) }}">All courses</a></li>
            <li><a href="{{route('course',$cls_id)}}">{{ $clas_name}}</a></li>    
            <li><a href="{{ route('course.categories',[$cls_id ,$cat_id])}}"> {{$cat_name}}</a></li>    
        </ul>
    </div>
</section>
<section class="courses-view">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="right-slide left">
                    <h3>Classes</h3>
                    <ul class="catagorie-list">   
                    <?php $i = 1; ?>
                    @if(count($classes) > 0)
                        @foreach($classes as $class)
                            <li><a href="{{ url('center/course',$class->id) }}">{{ $class->class }}</a></li>    
                        @endforeach
                            <li><a href="{{ url('center/course','0') }}">All</a></li> 
                     @endif 
                    </ul>
                    
                    <h3>Categories</h3>
                    <?php 
                        $categories = DB::table('categories')
                                    ->select('*')
                                    ->where('class_id',$cls_id)
                                    ->get();
                    ?>
                    <ul class="catagorie-list"> 
                    <?php $i = 1; ?>
                    @if(count($categories) > 0)
                        @foreach($categories as $categorie) 
                            <li><a href="{{ url('center/course',[$cls_id ,$categorie->id]) }}">{{ $categorie->name }}</a></li> 
                            <?php
                                array_push($cate,$categorie->id);
                            ?>
                        @endforeach
                     @endif
                    </ul>
                </div>
            </div>
            <div class="col-md-9">                
                <div class="row">
                    <?php 
                        if($cat_id == 0){
                            $examtypes = DB::table('exam_names')
                                    ->select('*')
                                    ->whereIn('id',$cate)
                                    ->get();
                           }
                        else{
                            $examtypes = DB::table('exam_names')
                                    ->select('*')
                                    ->where('category_id',$cat_id)
                                    ->get();
                        }

                    ?>
                    <?php $i = 1; ?>
                    @if(count($examtypes) > 0)
                        @foreach($examtypes as $examtype) 
                       
                        <div class="col-sm-6 col-md-4" id="<?php echo $examtype->category_id; ?>" >
                            <div class="course-post">
                                <div class="img">
                                    <img src="{{ asset('resources/assets/images/courses/'.$examtype->image) }}" alt="">    
                                    <div class="icon">
                                        <a href="#"><img src="{{ asset('resources/assets/images/book-icon.png') }}" alt=""></a>
                                    </div>
                                </div>
                                <div class="info">
                                    <div class="name">{{ $examtype->name }}</div>
                                </div>
                                <div class="product-footer">
                                    <div class="comment-box">   
                                        <div class="box"><i class="fa fa-users"></i> Enrolled or Not</div>
                                    </div>
                                    <div class="view-btn">
                                        <!-- <a href="{{ url('/center/course/details',[$cls_id ,$cat_id ,$examtype->id, 2]) }}" class="btn">view more</a> -->
                                        <a href="{{ url('/center/student_add') }}" class="btn">view more</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    @else
                        <div class="col-sm-6 col-md-4" id="" >
                            <div class="section-title" style="width:800px; margin:0 auto;">
                                <h2>Select Categories</h2>
                            </div>
                        </div>
                     @endif 
                    
                </div> 
            </div>
        </div>
        
    </div>
</section>
@endsection