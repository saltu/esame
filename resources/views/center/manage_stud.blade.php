@extends('center.layouts.app')
@section('content')
	<?php
	$center_id = Auth::guard('center')->user()->id;
	// echo $center_id;
	?>
<section class="b breadcrumb">
    <div class="container">
        <ul>
            <li><a href="{{ url('/center')}}">Center</a></li>
            <li><a href="{{ url('/center/manage_student')}}">Manage Students</a></li>
        </ul>
    </div>
</section>
<section class="login-view">
	
    <div class="container" >
        
	    <div class="row" style="margin-top: 10%; margin-bottom: 10%">
	        <div class="section-title">
	        <h3>Students Details</h3>
	        </div>
	        <table class="table table-striped table-bordered table-hover" id="sample_2">
                <thead>
                    <tr>
                    	<!-- <th>Id</th> -->
                        <th> Sl.No </th>
                        <th> Name </th>
                        <th> Class </th>
                        <th> Place </th>
                        <th> Email id </th>
                        <!-- <th> Center </th> -->
                        <th> View/Edit </th>
                        
                    </tr>
                </thead>
                <tbody>
                	<?php 
                        $a_stud = DB::table('students')
                                    ->select('*')
                                    ->where('center','=',$center_id)
                                    ->get();
                    ?>
                    <?php $i = 1; ?>
                    @if(count($a_stud) > 0 )
                        @foreach ($a_stud as $a_student)
                            <tr id="{{ $student->id }}">
                            	<!-- <td class="text-center"> {{ $a_student->id }} </td> -->
                                <td class="text-center"> {{ $i++ }} </td>
                                <td class="text-center"> {{ $a_student->name }} </td>
                                <td class="text-center"> {{ $a_student->class }} </td>
                                <td class="text-center"> {{ $a_student->place }} </td>
                                <td class="text-center"> {{ $a_student->email }} </td>
                                <!-- <td class="text-center"> {{ $a_student->center }} </td> -->
                                <td class="text-center"><a class="editcust" href="{{ url('center/approved_student_details', $a_student->id ) }}"> <i class="fa fa-edit" aria-hidden="true"></i> </a></td>
                                
                            </tr>
                        @endforeach
                    @endif
                </tbody>
            </table>
        </div>
    </div>
</section>




@endsection