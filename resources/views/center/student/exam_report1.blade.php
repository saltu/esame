@extends('center.layouts.app')
@section('content')
<?php
$classes = DB::table('classes')
                ->select('*')
                ->get();
?>
<section class="breadcrumb">
    <div class="container">
        <ul>
            <li><a href="{{ url('/center')}}">Center</a></li>
            <li><a href="{{ url('/center/exam_report') }}">Exam Report</a></li>
        </ul>
    </div>
</section>

<div class="section-title">     
    <div class="container">
        <div class="row">
            <div class="container" style="max-width:10%; float:left;">
                <div class="row">
                    <div class="col-md-3">
                        <div class="right-slide left">
                            
                        </div>
                    </div>
                </div>
            </div>
            
                <div class="section-title" style="margin-right: 40%; margin-bottom: 10%">
                    <h2>Exam Details</h2>
                </div>
                <form method="post" action="{{ url('/center/exam_result_submit') }}">
                <div class="col-md-7">
                    <select class="form-control exam_results" name="classreport_id" style=" margin-bottom: 10%;width: 120%; margin-right: 60%" id="classreport_id" data-dependent="categories_id">
                        <option value="">-- Select Class --</option>
                        @if(count($classes) > 0 )
                            @foreach ($classes as $cclass)
                            <option value="{{ $cclass->id }}<">{{ $cclass->class }}</option>
                            @endforeach
                        @endif
                    </select>
                    @if ($errors->has('classreport_id'))
                        <span class="help-block">
                        <strong>{{ $errors->first('classreport_id') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group{{ $errors->has('exam_name') ? ' has-error' : '' }}">
                    <div class="col-md-7">
                        <select class="form-control exam_name"  name="exam_name" style=" margin-bottom: 10%; width: 120%" id="categories_id" data-dependent="test_type">
                            <option value="">-- Select Exam --</option>
                        </select>
                    </div>
                </div>
                <div class="form-group{{ $errors->has('exam_types') ? ' has-error' : '' }}">
                    <div class="col-md-7">
                        <select class="form-control exam_result"  name="exam_types" style=" margin-bottom: 10%; width: 120%" id="test_type" data-dependent="chapter">
                            <option value="">-- Select Exam Type --</option>
                        </select>
                    </div>
                </div>
                <div class="form-group" style="margin-right: 60%;">
                    <div class="col-md-4 col-md-offset-5" style=" margin-bottom: 10%">
                        <button type="submit" name="e_search" id="e_search" class="mbtn btn-primary">Search</button>
                    </div>
                </div>
            </form>
            
        </div>
    </div>
</div>
@endsection