@extends('center.layouts.app')
@section('content')
<?php

    $s_name = DB::table('students')
                        ->select('*')
                        ->where('id',$stud_id)
                        ->get();
    $i = 1;
?>
<section class="breadcrumb">
    <div class="container">
        <ul>
            <li><a href="{{ url('/center')}}">Center</a></li>
            <li><a href="{{ url('/center/student_add') }}">Student Report</a></li>
            <?php $i = 1; ?>
                @if(count($clas_name) > 0)
                    @foreach($clas_name as $name)
                        <li><a href="{{url('/center/course',$cls_id)}}">{{ $name->class  }}</a></li>    
                    @endforeach
                @endif
        </ul>
    </div>
</section>
<div class="section-title">     
    <div class="container">
        <div class="row">
            <div class="container" style="max-width:10%; float:left;">
                <div class="row">
                    <div class="col-md-3">
                        <div class="right-slide left">
                            
                        </div>
                    </div>
                </div>
            </div>
            <!-- <form class="col-lg-6 col-lg-offset-3"  method="post" action=""> -->
                <div class="section-title" style="margin-right: 70%; margin-bottom: 3%">
                    <h2>Student Details</h2>
                </div>
                <div class="form-group{{ $errors->has('exam_types') ? ' has-error' : '' }}" >
                    <!-- <div class="col-md-6" style=" float:left">
                        <select class="form-control dynamicc"  name="exam_types" style="margin-bottom:10%; width:70%; margin-right: 70%;" id="rcategory_id" data-dependent="type_id">
                            <option value="">-- Select Category --</option>
                        </select>
                        @if ($errors->has('exam_types'))
                            <span class="help-block">
                            <strong>{{ $errors->first('exam_types') }}</strong>
                            </span>
                        @endif
                    </div> -->
                    <div class="col-md-12">
                        <p>{{$s_name[0]->name}}  Exam Report</p>
                        @if ( $first_view === null )
                        <section class="cart-page">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <table class="table table-striped table-bordered table-hover" id="sample_1">
                                            <thead>
                                                <tr>
                                                    <th>SL.NO</th>
                                                    <th>CENTER</th>
                                                    <th>STUDENT</th>
                                                    <th>EXAM NAME</th>
                                                    <th>CHAPTERS</th>
                                                    <th>DATE</th>
                                                    <th>TOTAL QUESTION</th>
                                                    <th>MARKS</th>                          
                                                </tr>
                                            </thead>
                                            <tbody>
                                            @if(count($find) > 0 )
                                            @foreach ($find as $exams)
                                            <?php
                                                $student = DB::table('students')
                                                                    ->select('*')
                                                                    ->where('id', $exams->stud_id)
                                                                    ->get();
                                                $center = DB::table('centers')
                                                                    ->where('id', $student[0]->center)
                                                                    ->first()
                                                                    ->name;
                                                $exam_names = $exams->exam_id;
                                                if ($exam_names != ''){
                                                    $exam_name = DB::table('exam_names')
                                                                    ->select('name','category_id')
                                                                    ->where('id', $exam_names)
                                                                    ->get();
                    //                                $exam_time = DB::table('exam_titles')
                    //                                                ->where('id', $level)
                    //                                                ->first()
                    //                                                ->time;
                                                    // $exam_time = 30;
                                                }
                                                $chapters = explode(',', $exams->chapter_id);
                                                if ($chapters != ''){
                                                    $chap_name = array();
                                                    $subj_names = array();
                                                    foreach ($chapters as $chapter) {
                                                    $subj = DB::table('chapters')
                                                                    ->select('subject_id','name')
                                                                    ->where('id',$chapter)
                                                                    ->get();
                                                    $chap_name[] = $subj[0]->name;    
                                                    $subj_name = DB::table('subjects')
                                                                    ->where('id',$subj[0]->subject_id)
                                                                    ->first()
                                                                    ->name;
                                                        if (!in_array($subj_name, $subj_names)) {
                                                            $subj_names[] = $subj_name;
                                                        }

                                                    }
                                                }
                                        
                                                $type = DB::table('subjects')
                                                            ->where('id', $subject)
                                                            ->first()
                                                            ->type_id;
                                                if ($type != ''){
                                                    $exm_name = DB::table('exam_names')
                                                                    ->where('id',$type)
                                                                    ->first()
                                                                    ->name;
                                                }
                                        
                                                $category = DB::table('categories')
                                                            ->select('name','class_id')
                                                            ->where('id', $exam_name[0]->category_id)
                                                            ->get();
                                                // if ($category != ''){
                                                //     $cat_name = DB::table('categories')
                                                //                     ->where('id',$category)
                                                //                     ->first()
                                                //                     ->name;
                                                // }
                                        
                                                $class = DB::table('classes')
                                                            ->where('id', $category[0]->class_id)
                                                            ->first()
                                                            ->class;
                                                // if ($class != ''){
                                                //     $clas_name = DB::table('classes')
                                                //                     ->where('id', $class)
                                                //                     ->first()
                                                //                     ->class;
                                                // }

                    //                            $question_ids = explode(",",$exams->q_id);
                    //                            $question_options = explode(",",$exams->op_id);
                                                
                                                $question_ids = $exams->q_id;          
                                                $question_options = $exams->op_id;
                                                
                                                $results = explode(",",$exams->result);

                                                $counts = array_count_values($results);
                                                $score = 0;
                                                $negative = 0;
                                                if ($counts['1'] != '')
                                                {
                                                    $score = $counts['1'];
                                                }
                                                if ($counts['-1'] != '')
                                                {
                                                    $negative = $counts['-1'];
                                                }
                                                $no_quest = count($results);
                                            ?>
                                                <tr id="{{ $exams->id }}">
                                                    <td> {{ $i++ }} </td>
                                                    <td> {{ $center }} </td>
                                                    <td> {{ $student[0]->name }} </td>
                                                    <td>
                                                        <div>
                                                            <div >{{$class}} >> {{$category[0]->name}} >> {{$exam_name[0]->name}} >> @foreach($subj_names as $subj_name) {{$subj_name}} @endforeach</div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <ul class="text-left" style="list-style-position: inside;">
                                                                @foreach($chap_name as $chap)
                                                            <li> {{$chap}} </li>
                                                                @endforeach
                                                        </ul>
                                                    </td>
                                                    <td>{{date("d-m-Y", strtotime($exams->date))}}</td>
                                                    <td>{{$exams->t_question}}</td>
                                                    <td>{{($score * 4) - $negative}} / {{4 * $exams->t_question}}</td>      
                                                </tr>
                                                @endforeach
                                                @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                        </section>
                        @endif
                    </div>
                </div>
                <div class="form-group{{ $errors->has('exam_types') ? ' has-error' : '' }}" >
                    <div class="col-md-7" style="float:left;margin-bottom: 5%; width: 50%;">
                        <label style="float:left;">Enter Student Id</label>
                        <input type="text" style="margin-right: 25%" name="student_name" class="company" id="student_id" value="{{ old('name') }}" >
                    </div>
                </div>
                <div class="form-group" style="margin-right: 50%; ">
                    <div class="col-md-4 col-md-offset-5" style=" margin-bottom: 10%">
                        <button type="submit"  id="s_submit" class="mbtn btn-primary">Search</button>
                    </div>
                </div>
            <!-- </form> -->
        </div>
    </div>
</div>
<div class="container">
<table class="table table-striped table-bordered table-hover" border = 1 id='sample_1'>
    <thead>
        <tr class="tr">
            <th class="text-center">Sl. No</th>
            <th class="text-center">Stud_id</th>
            <th class="text-center">Student name</th>
            <th class="text-center">Class</th>
            <th class="text-center">View Report</th>
        </tr>
    </thead>
    <tbody>
        <?php $i = 1; ?>
        @foreach ($users as $user)
            <tr>
               <td class="text-center">{{ $i++ }}</td>
               <td class="text-center" id="stud_id" name="stud_id">{{ $user->id }}</td>
               <td class="text-center">{{ $user->name }}</td>
               <td class="text-center">{{ $user->class}}</td>
               <td class="text-center"><a class="editcust" href="{{ url('center/stud_exam_detail', $user->id ) }}"><i class="fa fa-eye" aria-hidden="true"></i></a></td>
            </tr>
        @endforeach
    </tbody>
</table>
</div>
@endsection