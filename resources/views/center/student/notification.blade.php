@extends('center.layouts.app')
@section('content')
<?php
    if (count($users) == 0) {
    $users = DB::table('students')
                ->select('*')
                ->where('center',Auth::guard('center')->user()->id)
                ->get();
    }
?>
<section class="b breadcrumb white-bg">
    <div class="container" >
        <ul >
            <li><a href="{{ url('/center')}}">Center</a></li>
            <li><a href="#">Notifications</a></li>
        </ul>
    </div>
</section>
<section class="courses-view">

@if(session()->has('message'))
    <div class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif
@if(session()->has('dmessage'))
    <div class="alert alert-danger">
        {{ session()->get('dmessage') }}
    </div>
@endif    

<div class="section-title">		
    <div class="container">
        <div class="row">
        	<div class="container" style="max-width:10%; float:left;">
		        <div class="row">
		            <div class="col-md-3">
		                <div class="right-slide left">
		                    <h3>Classes</h3>
		                    <ul class="catagorie-list"> 
		                    	<li><a href="{{url('center/notification/XI')}}">XI</a></li>
		                    	<li><a href="{{url('center/notification/XII')}}">XII</a></li>
		                    </ul>
		                </div>
		            </div>
		        </div>
		    </div>
            <div class="col-lg-6 col-lg-offset-3">
                <div class="section-title">
                	<h2>Send a Notification</h2>
                </div>
	            <div style=" margin-top:20px; float:left;">
		            <label class="radio">
				        <input type="checkbox" name="select_all"  id="select_all"
				        >
				        <span>  Select All</span>
				    </label>
				</div>
                <div style=" margin-top:20px; margin-bottom: 70px; ">
                <form method="post" action="{{ route('notify.search')}}" >
                    <!-- <input type="text" placeholder="Student ID" name="id" id="id"> OR  -->
                    <input type="text" name="centername" class="company centername" id="centername" placeholder="Center Name" autocomplete="off">
                    <input type="hidden" name="center" value="" class="company" id="center">
                    <button type="submit" name="action1" value="search"><i class="fa fa-search"></i></button>   
                </form>
                </div>
                <form method="post" action="{{ route('notify.send')}}" >
                    <div style="max-width: 100%; margin-left: 20px">
                    	<table class="table table-striped table-bordered table-hover" id="sample_1">
                            <thead>
                                <tr>
                                    <th> </th>
                                    <th>id</th>
                                    <th>Student name</th>
                                </tr>
                            </thead>
                                <tbody>
                                    <?php $i = 1; ?>
                                    @if(count($users) > 0 )
                                    @foreach ($users as $user)
                                        <tr id="{{ $user->id }}">
                                           <td><input type="checkbox" name="selector[]" value="{{ $user->id }}" class = "new_check"></td>
                                           <td  id="stud_id" name="stud_id" >{{ $user->id }}</td>
                                           <td>{{ $user->name }}</td>
                                        </tr>
                                    @endforeach
                                    @endif
                                </tbody>
                        </table>
                    </div>
                    <div class="form-row" style="margin-left: 35%;">
                    	<label>Enter Message</label><br>
                    	<textarea class="form-control" id="msg" name="msg"></textarea>
                        <input type="hidden" name="from" value="center">
                    </div>
                    <div class="form-group" style="margin-left: 250px; margin-top:20px">
                    	<button type="submit" name="action" class="btn" id="action" value="send">Send </button>
                    </div>
                </form>
            </div>

        </div>
	</div>
			
</div>

</section>
@endsection