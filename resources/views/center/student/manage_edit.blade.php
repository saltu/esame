@extends('center.layouts.app')
@section('content')

<section class="b breadcrumb">
    <div class="container">
        <ul>
            <li><a href="{{ url('/center')}}">Center</a></li>
            <li><a href="{{ url('/center/manage_student')}}">Manage Student</a></li>
            <li><a href="#">Edit</a></li>
        </ul>
    </div>
</section>
<?php 
$users = DB::table('students')
                    ->select('*')
                    ->where('id','=',$stud_id)
                    ->get();

?>
    <section class="login-view">
@if(count($users) > 0 )
    @foreach ($users as $user)

        <div class="container">
            <div class="row">
                <div class="section-title">
                    <h2>Student Details</h2>
                    
                </div>
                <form class="form-horizontal" role="form" method="POST" action="{{ url('/center/student_edit',$user->id) }}">
                {{ csrf_field() }}
                
                    <div class="col-sm-6">
                        <div class="section-title">
                            <p>Student Details</p>
                        </div>

                        
                            <div class="sform-group">

                                <label for="name" class="control-label">Name Of Student</label>
                                <!-- <div class="input-box">
                                    <input id="id" type="hidden" class="form-control" name="id" value="">
                                </div> -->
                                <div class="input-box">
                                    <input id="name" type="text" readonly class="form-control" name="name" value="{{old('name',$user->name) }}">
                                </div>
                            </div>

                            <div class="sform-group">
                                <label for="email"  class="control-label">E-Mail Address</label>
                                <div class="input-box">
                                    <input id="email" readonly ="readoly" type="email" class="form-control" name="email" value="{{ old('name',$user->email) }}">
                                </div>
                            </div>

                            <div class="sform-group">
                                <label for="phone"  class="control-label">Phone Number</label>
                                <div class="input-box">
                                    <input id="phone"  readonly ="readoly" type="number" class="form-control" name="phone" value="{{ old('name',$user->phone) }}">
                                </div>
                            </div>

                            <div class="sform-group">
                                <label for="school" class="control-label">Name Of School</label>
                                <div class="input-box">
                                    <input id="school" type="text" readonly class="form-control" name="school" value="{{ old('name',$user->school) }}">
                                </div>
                            </div>

                            <!-- <div class="sform-group col-md-6">
                                <label for="center" class="control-label">Center</label>
                                <div class="input-box">
                                   <input id="center" readonly ="readoly" type="text" class="form-control" name="center" value="{{ old('name',$user->center)}}">
                                </div>
                            </div> -->

                            <div class="sform-group col-md-6">
                                <label for="class" class="control-label">Class</label>
                                <div class="input-box">
                                   <input id="class" type="text" readonly class="form-control" name="class" value="{{ old('name',$user->class )}}">
                                </div>
                            </div>

                            <div class="sform-group col-md-6">
                                <label for="place" class="control-label">Place</label>
                                <div class="input-box">
                                    <input id="place" type="text" readonly class="form-control" name="place" value="{{ old('name',$user->place)}}">
                                </div>
                            </div>
                            <div class="sform-group col-md-6">
                                <label for="dob" class="control-label">Date of Birth</label>
                                <div class="input-box">
                                   <input id="dob" type="text" readonly class="form-control" name="dob" value="{{ old('name',$user->dob) }}">
                                </div>
                            </div>

                            <div class="sform-group col-md-6">
                                <label for="yoa" class="control-label">Year of Addmission</label>
                                <div class="input-box">
                                    <input id="yoa" type="text" readonly class="form-control" name="yoa" value="{{ old('name',$user->yoa) }}">
                                </div>
                            </div>

                        </div>
                        <div class="col-sm-6">
                            <div class="section-title">
                            <p>Parent Details</p>
                            </div>

                            <div class="sform-group">
                                <label for="parent_name" class="control-label">Name Of Parent</label>
                                <div class="input-box">
                                    <input id="parent_name" type="text" readonly class="form-control" name="parent_name" value="{{ old('name',$user->parent_name) }}">
                                </div>
                            </div>

                            

                            <div class="sform-group">
                                <label for="parent_email" class="control-label">E-Mail Address</label>
                                <div class="input-box">
                                    <input id="parent_email" type="email" readonly class="form-control" name="parent_email" value="{{ old('name',$user->parent_email)}}">
                                </div>
                            </div>

                            <div class="sform-group">
                                <label for="parent_phone" class="control-label">Phone Number</label>
                                <div class="input-box">
                                    <input id="parent_phone" type="number" readonly class="form-control" name="parent_phone" value="{{ old('name',$user->parent_phone)}}">
                                </div>
                            </div>
                    </div>
                    <div class="col-md-8 col-md-offset-3 submit-slide" style="margin-left: 45%">
                        <div class="col-md-6"><a href="javascript:history.go(-1)"><input style="color: #fff!important; " type="button" value="Go Back" class="btn"></a></div>
                        
                    </div>
                </form>
            </div>
            </div>
        </div>

    </section>
    @endforeach
@endif

@endsection