@extends('center.layouts.app')

@section('content')


<section class="breadcrumb">
    <div class="container">
        <ul>
            <li><a href="{{ url('/center')}}">Center</a></li>
            <li><a href="#">Edit Profile</a></li>
            
        </ul>
    </div>
</section>
        <section class="login-view">
            <div class="container">
                <div class="row">
                    <!-- <div class="col-sm-6"> -->
                        <div class="section-title">
                            <h2>REGISTER</h2>
                            <p>Register Center</p>
                        </div>
                        <?php
                        $center_id = Auth::guard('center')->user()->id;
                        ?>

                        <form class="col-lg-6 col-lg-offset-3" role="form" method="POST" action="{{  url('/center/profile',$center_id) }}">
                            {{ csrf_field() }}
                            @foreach($classes as $class)
	                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
	                                <label for="name" class="mtl control-label">Center Name</label>
	                                <div class="col-md-8 input-box">
	                                    <input id="name" type="text"  class="form-control" name="name" value="{{ old('name',$class->name) }} ">
	                                    @if ($errors->has('name'))
	                                        <span class="help-block">
	                                            <strong>{{ $errors->first('name') }}</strong>
	                                        </span>
	                                    @endif
	                                </div>
	                            </div>
	                            
	                            <div class="form-group{{ $errors->has('place') ? ' has-error' : '' }}">
	                                <label for="place" class="mtl control-label">Place</label>
	                                <div class="col-md-8 input-box">
	                                    <input id="place" type="text" class="form-control" name="place" value="{{ old('place',$class->place )}}">
	                                    @if ($errors->has('place'))
	                                        <span class="help-block">
	                                            <strong>{{ $errors->first('place') }}</strong>
	                                        </span>
	                                    @endif
	                                </div>
	                            </div>
	                            <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
	                                <label for="address" class="mtl control-label">Address</label>
	                                <div class="col-md-8 input-box">
	                                    <textarea id="address" readonly ="readoly" rows="5" class="form-control" name="address" value="">{{ old('address',$class->address) }}</textarea>
	                                    @if ($errors->has('address'))
	                                        <span class="help-block">
	                                            <strong>{{ $errors->first('address') }}</strong>
	                                        </span>
	                                    @endif
	                                </div>
	                            </div>
	                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
	                                <label for="email" class="mtl control-label">E-Mail Address</label>
	                                <div class="col-md-8 input-box">
	                                    <input id="email" readonly ="readoly" type="email" class="form-control" name="email" value="{{ old('email', $class->email )}}">
	                                    @if ($errors->has('email'))
	                                        <span class="help-block">
	                                            <strong>{{ $errors->first('email') }}</strong>
	                                        </span>
	                                    @endif
	                                </div>
	                            </div>
	                            <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
	                                <label for="phone" class="mtl control-label">Phone Number</label>
	                                <div class="col-md-8 input-box">
	                                    <input id="phone" readonly ="readoly" type="number" class="form-control" name="phone" value="{{ old('phone',$class->phone) }}">
	                                    @if ($errors->has('phone'))
	                                        <span class="help-block">
	                                            <strong>{{ $errors->first('phone') }}</strong>
	                                        </span>
	                                    @endif
	                                </div>
	                            </div>
                            @endforeach
                           <!--  <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label for="password" class="mtl control-label">Password</label>
                                <div class="col-md-8 input-box">
                                    <input id="password" type="password" class="form-control" name="password">
                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div> -->
                            <!-- <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                <label for="password-confirm" class="mtl control-label">Confirm Password</label>
                                <div class="col-md-8 input-box">
                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation">

                                    @if ($errors->has('password_confirmation'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div> -->
                            <div class="col-md-6 col-md-offset-5 submit-slide">
                                <input type="submit" value="Update" class="btn">
                            </div>
                        </form>
                    <!-- </div> -->
                </div>
                
            </div>
        </section>
@endsection
