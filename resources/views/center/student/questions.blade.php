@extends('center.layouts.app')
@section('content')
<section class="breadcrumb">
    <div class="container">
        <ul>
            <li><a href="{{ url('/center')}}">Center</a></li>
            <li><a href="{{ url('/center/question') }}">Question</a></li>
        </ul>
    </div>
</section>
	<div>
        <div class="sform-v10-content">
            <form class="form-detail form-horizontal" role="form" method="POST" action="{{ url('center/question/view') }}">
            {{ csrf_field() }}
                <div class="form-left">
                    <h2>Questions and Answers</h2>
                    <div class="form-row{{ $errors->has('id') ? ' has-error' : '' }}">
                        <input type="text" name="Question_id" class="company" id="Question_id" value="{{ old('Question_id') }}" placeholder="Enter Question ID">
                        <!-- @if ($errors->has('Question_id'))
                            <span class="help-block">
                                <strong>{{ $errors->first('Question_id') }}</strong>
                            </span>
                        @endif -->
                    </div>
                    
                	<!-- <button class="view">view</button> -->
                	<div style="float:right; margin-bottom: 100px">
                    	<button type="submit" class="btn" id="view_button">view </button>
                    </div>
                   
                    
                </div>
            </form>
            <div>
                <table class="table table-striped table-bordered table-hover" border = 1>
                	<tr class="tr">
	            		<th>id</th>
	            		<th>question</th>
	            		<th>exam type</th>
	            		<th>description</th>
	            		<th>solution</th>
	            	
	            	</tr>
	           
	            	@foreach ($users as $user)
				        <tr>
				           <td>{{ $user->id }}</td>
				           <td>{{ $user->question }}</td>
				           <td>{{ $user->exam_type }}</td>
				           <td>{{ $user->description }}</td>
				           <td>{{ $user->solution }}</td>
				        
				        </tr>
			        @endforeach
                </table>
            </div>
        </div>
    </div>

@endsection

<!-- @section('script')
	<script type="text/javascript">
		$('#view_button').on('click',function(){
			alert('test code')
		})
	</script>
@endsection -->

