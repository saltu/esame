@extends('center.layouts.app')
@section('content')

<div class="spage-content" style="border:solid black">
        <div class="sform-v10-content">
            <form class="form-detail form-horizontal" role="form" method="POST" action="{{ route('update.table',que_id)}}">
            {{ csrf_field() }}
                <div class="form-left">
                    <h2>Edit Details</h2>
                    <div class="form-row{{ $errors->has('Exam_id') ? ' has-error' : '' }}">
                    	@foreach ($users as $user)
	                    	<input type="text" name="que_id" class="company" id="que_id" value="{{ old('que_id',$user->id) }}" placeholder="Enter Id" style="margin-bottom: 30px">
	                        
	                        <input type="text" name="Question" class="company" id="Question" value="{{ old('Question',$user->question) }}" placeholder="Enter Questions" style="margin-bottom: 30px">
	                        
	                        <input type="text" name="Que_type" class="company" id="Que_type" value="{{ old('Que_typ',$user->exam_type) }}" placeholder="Enter Questions Type" style="margin-bottom: 30px">
	                        
	                        <input type="text" name="description" class="company" id="description" value="{{ old('description',$user->description) }}" placeholder="Enter a description" style="margin-bottom: 30px">
	                        <input type="text" name="Solution" class="company" id="Solution" value="{{ old('Solution',$user->solution) }}" placeholder="Enter the solution" style="margin-bottom: 30px">
                        @endforeach
                    </div>
                    
                	<!-- <button class="view">view</button> -->
                	<div style="float:right; margin-bottom: 100px; margin-top:100px; ">
                    	<button class="btn">Submit </button>
                    </div>
                    
                </div>
            </form>
        </div>
    </div>


@endsection

<!-- url('center/exam/view/update') -->