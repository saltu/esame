@extends('layouts.app')

@section('content')
        <section class="banner inner-page">
        	<div class="banner-img"><img src="resources/assets/images/banner/aboutUs-banner.jpg" alt=""></div>
            <div class="page-title">	
	            <div class="container">
                    <h1>About Us</h1>
                </div>
            </div>
        </section>
        <section class="breadcrumb">
        	<div class="container">
            	<ul>
                	<li><a href="index.html">Home</a></li>
                    <li><a href="about-us.html">About Us</a></li>
                </ul>
            </div>
        </section>
        <section class="about-page">
        	<section class="about-ourInfo">
            	<div class="container">
                	<div class="section-title">
                    	<h2>Vision and Mission</h2>
                    </div>
                    <div class="row">
                        <div class="col-md-4 bx">
                            <div class="svg">
                                <img src="{{ asset('resources/assets/images/icons/target.svg')}}" alt="">
                            </div>
                            <p class="centr">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                            </p>
                        </div>
                        <div class="col-md-4 bx">
                            <div class="img svg">
                                <img src="{{ asset('resources/assets/images/icons/vision.svg')}}" alt="">
                            </div>
                            <p class="centr">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                            </p>
                        </div>
                        <div class="col-md-4 bx">
                            <div class="img svg">
                                <img src="{{ asset('resources/assets/images/icons/observation.svg')}}" alt="">
                            </div>
                            <p class="centr">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                            </p>
                        </div>
                    </div>
                </div>
            </section>
            <section class="con">
                    <div class="img">
                        <img src="{{ asset('resources/assets/images/about-us/about-us-img.png')}}" alt="">
                    </div>
                    <!-- <div class="row"> -->
                        <div class="col-md-4">
                        </div>
                        <div class="col-md-8">
                    <div class="text-block">
                        <h4 class="con-magn">Why Choose Us</h4>
                        <p class="sldr2">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
                    </div>
                    </div>
                    <!-- </div> -->
            </section>
            <section>
                <div class="container">
                    <div class="col-md-8">   
                        <h2 class="abt-head">Our Target Audiance</h2>
                        <p>
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
                            <p> It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old.
                        </p>
                    </div>
                
                <div class="col-md-4">
                    <div class="img">
                        <img src="{{ asset('resources/assets/images/about-us/abt-us-img-1.png')}}" alt="">
                    </div>
                </div>
            </div>
            </section>
            <section>
                <div class="container">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="img">
                              <img src="{{ asset('resources/assets/images/about-us/abt-us-img-2.png')}}" alt="">
                            </div>
                        </div>
                        <div class="col-md-8">   
                         <h2 class="abt-head" style="float: right;">Goals and Venture of academy venture</h2>
                     </div>
                         <p>
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
                            <p> It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old.
                        </p>
                    </div>
                </div>
                </div>
            </section>
        </section>
@endsection

