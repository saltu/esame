<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Esame') }}</title>

     <!-- Favicon and touch icons  -->    
    <link href="{{ asset('resources/assets/images/Favicon.png') }}" rel="icon">
    <!-- CSS Stylesheet -->
    <!-- <link href="{{ asset('resources/assets/css/app.css') }}" rel="stylesheet"> -->

    <link rel="stylesheet" type="text/css" href="{{ asset('resources/assets/css/bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/assets/css/owl.carousel.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/assets/css/font-awesome.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/assets/css/loader.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/assets/css/docs.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/assets/css/custom.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/assets/css/akash.css') }}">
    <link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@600;700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Nunito+Sans&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Arima+Madurai:100,200,300,400,500,700,800,900%7CPT+Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->


</head>
<body>

<div class="wapper">
    <!-- <div id="loader-wrapper">
        <div id="loader"></div>
            <div class="loader-section section-left"></div>
        <div class="loader-section section-right"></div>
    </div> -->
        <div class="quck-nav">
            <div class="container">
                <div class="contact-no"><a href="mailto:support@esame.in"><i class="fa fa-envelope"></i>support@esame.in</a></div>
                <div class="contact-no"><a href="tel:"><i class="fa fa-phone"></i>+299 97 39 82</a></div>
                <div class="quck-right">
                    <div class="right-link"><a href="#"><i class="fa fa-handshake-o"></i>Help Center</a></div>
                    <div class="right-link"><a href="#"><i class="fa fa-headphones"></i>Online Support</a></div>
                    <div class="right-link language-select">
                        <a href="javascript:void(0);"><i class="fa fa-language"></i>English</a>
                        <!-- <ul class="language-list">
                            <li><a href="#">Guyana</a></li>
                            <li><a href="#">Haiti</a></li>
                            <li><a href="#">Honduras</a></li>
                            <li><a href="#">Andorra</a></li>
                            <li><a href="#">Armenia</a></li>
                            <li><a href="#">Bahrain</a></li>
                        </ul> -->
                    </div>
                    @if (Auth::guard('student')->user()->name != "")
                    <div class="right-link"></div>
                    @elseif(Auth::guard('center')->user()->name != "")
                    <div class="right-link"></div>
                    @else
                    <div class="right-link"><a href="{{ url('center')}}"><i class="fa  fa-user"></i>Center Login</a></div>
                    @endif
                    <!-- <div class="right-link"><a href="{{ url('student')}}"><i class="fa  fa-user"></i>Login \ Register</a></div> -->
                </div>
            </div>
        </div>
        <header id="header">
            <div class="container">
                <nav id="nav-main">
                    <div class="navbar navbar-inverse">
                        <div class="navbar-header">
                            <a href="{{ url('/')}}" class="navbar-brand"><img src="{{ asset('resources/assets/images/logo.png') }}" alt=""></a>
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>

                        <div class="navbar-collapse collapse" style="letter-spacing: 1px;">
                            <ul class="nav navbar-nav">
                                <li><a href="{{ url('/')}}">Home </a></li>
                                <li><a href="{{ route('course','1') }}">Courses</a></li>
                                 <li><a href="{{ url('about-us')}}">About Us</a></li>
                                <li><a href="{{ url('contact')}}">Contact Us</a></li>
                                @if (Auth::guard('student')->user()->name != "")
                                <li class="sub-menu">
                                    <a href="{{ url('/student') }}">{{ Auth::guard('student')->user()->name }}</a>
                                    <ul>
                                        <li><a href="{{ url('/student/logout/') }}">Logout</a></li>
                                    </ul>
                                </li>
                                @elseif (Auth::guard('admin')->user()->name != "")    
                                <li class="sub-menu">
                                    <a href="{{ url('/admin') }}">{{ Auth::guard('admin')->user()->name }}</a>
                                    <ul>
                                        <li><a href="{{ url('/admin/logout/') }}">Logout</a></li>
                                    </ul>
                                </li> 
                                @elseif (Auth::guard('center')->user()->name != "")    
                                <li class="sub-menu">
                                    <a href="{{ url('/center') }}">{{ Auth::guard('center')->user()->name }}</a>
                                    <ul>
                                        <li><a href="{{ url('/center/logout/') }}">Logout</a></li>
                                    </ul>
                                </li>                           
                                @else
                                <li><a href="{{ route('student') }}">Sign In</a></li>
                                @endif
                            </ul>
                        </div>

                    </div>
                </nav>
            </div>
        </header>
       
            @yield('content')

    <footer id="footer">
            <div class="footer-top">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6 col-md-3">
                            <h5>Popular Courses</h5>
                            <div class="course-slide">
                             <ul class="footer-link">
                                 <li> ENGINEERING </li>
                                 <li> MEDICAL </li>
                                 
                             </ul>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="row">
                                 <div class="col-md-offset-1 col-sm-6 col-md-5 col-xs-6">
                                    <h5>Popular Exams</h5>
                                    <ul class="footer-link">
                                        <li> NEET </li>
                                        <li> JEE </li>
                                        <li> CAT </li>
                                        
                                    </ul>   
                                </div>
                                <div class="col-md-offset-1 col-sm-6 col-md-5 col-xs-6">
                                    <h5>Quick Links</h5>
                                    <ul class="footer-link">
                                        <li><a href="{{ url('/')}}">HOME</a></li>
                                        <li><a href="{{ url('course','1')}}">COURSES</a></li>
                                        <li><a href="{{ url('about-us')}}">ABOUT US</a></li>

                                        <li><a href="{{ url('contact')}}">CONTACT</a></li>
                                    </ul>   
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-3">
                            <h5>Contact Us</h5>
                            <div class="contact-view">
                                <div class="contact-slide">
                                    <p><i class="fa fa-location-arrow"></i>76 Woodland Ave. Sherman Drive  <br>Fort Walton Beach,Harlingen</p>
                                </div>
                                <div class="contact-slide">
                                    <p><i class="fa fa-phone"></i>+299 97 39 82</p>
                                </div>
                                <div class="contact-slide">
                                    <p><i class="fa fa-fax"></i>(08) 8971 7450</p>
                                </div>
                                <div class="contact-slide">
                                    <p><i class="fa fa-envelope"></i><a href="mailTo:academy@info.com">academy@info.com</a></p>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-sm-8">
                        <div class="copy-right">
                            <p>Copyright © <span class="year">2019</span> | CornerStone.</p>
                        </div>
                    </div>
                    <div class="col-sm-4 "> 
                        <div class="social-media">
                            <ul>
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-skype"></i></a></li>
                                <li><a href="#"><i class="fa fa-youtube"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>
    
    <script src="{{ asset('resources/assets/js/jquery-1.12.4.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('resources/assets/js/bootstrap.js') }}" type="text/javascript"></script>
    <script src="{{ asset('resources/assets/js/owl.carousel.js') }}" type="text/javascript"></script>
    <script src="{{ asset('resources/assets/js/jquery.form-validator.min.js') }}" type="text/javascript"></script>
    <script type='text/javascript' src='https://maps.google.com/maps/api/js?key=AIzaSyAciPo9R0k3pzmKu6DKhGk6kipPnsTk5NU'></script>
    <script src="{{ asset('resources/assets/js/map-styleMain.js') }}" type="text/javascript"></script>
    <script src="{{ asset('resources/assets/js/placeholder.js') }}" type="text/javascript"></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
    <script src="{{ asset('resources/assets/js/coustem.js') }}" type="text/javascript"></script>
    <script src="{{ asset('resources/assets/js/extra.js') }}" type="text/javascript"></script>
<script type="text/javascript">
    $(".custom-select").each(function() {
  var classes = $(this).attr("class"),
      id      = $(this).attr("id"),
      name    = $(this).attr("name");
  var template =  '<div class="' + classes + '">';
      template += '<span class="custom-select-trigger">' + $(this).attr("placeholder") + '</span>';
      template += '<div class="custom-options">';
      $(this).find("option").each(function() {
        template += '<span class="custom-option ' + $(this).attr("class") + '" data-value="' + $(this).attr("value") + '">' + $(this).html() + '</span>';
      });
  template += '</div></div>';
  
  $(this).wrap('<div class="custom-select-wrapper"></div>');
  $(this).hide();
  $(this).after(template);
});
    $(".custom-option:first-of-type").hover(function() {
  $(this).parents(".custom-options").addClass("option-hover");
}, function() {
  $(this).parents(".custom-options").removeClass("option-hover");
});
$(".custom-select-trigger").on("click", function() {
  $('html').one('click',function() {
    $(".custom-select").removeClass("opened");
  });
  $(this).parents(".custom-select").toggleClass("opened");
  event.stopPropagation();
});
$(".custom-option").on("click", function() {
  $(this).parents(".custom-select-wrapper").find("select").val($(this).data("value"));
  $(this).parents(".custom-options").find(".custom-option").removeClass("selection");
  $(this).addClass("selection");
  $(this).parents(".custom-select").removeClass("opened");
  $(this).parents(".custom-select").find(".custom-select-trigger").text($(this).text());
});
</script>
</body>
</html>