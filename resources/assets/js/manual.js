$("#tclasses").on('change',function(){
    var select = $(this).attr("id");
    var value = $(this).val();
	if($(this).val() != '') {
    $.ajax({
        type:'POST',
        url:'exam/category',
        data:{select:select, value:value},
        success:function(result)
        {
            $('#ccategory').html(result);
            // alert(json['id']);
        }
    })
	}
});
$('.dynamic').on('change',function(){
    var select = $(this).attr("id");
    var db = $(this).attr("name");
    var value = $(this).val();
    var dependent = $(this).data('dependent');
    if( dependent == 'stitle' ){
        var dependentt = 'Exam';
    }else{
        var dependentt = 'Category';
    }
    if($(this).val() != '') {
    $.ajax({
        type:'POST',
        url:'exam/title',
        data:{select:select, value:value, db:db, dependentt:dependentt},
        success:function(result)
        {
            $('#'+dependent).html(result);
        }
    })
    }
});
$('#class_id').change(function(){
  $('#stitle').val('');
});


$('.dynamicc').on('change',function(){
    var select = $(this).attr("id");
    if (select == 'lclass_id') {
        var select = 'class_id';
    }
    if (select == 'lcategory_id') {
        var select = 'category_id';
    }
    var db = $(this).attr("name");
    var value = $(this).val();
    var dependent = $(this).data('dependent');
    if( dependent == 'lcategory_id' ){
        var dependentt = 'Category';
    }else if( dependent == 'examname_id' ){
        var dependentt = 'Exam';
    }else if( dependent == 'lsubject' ){
        var dependentt = 'Subject';
    }else{
        var dependentt = 'Category';
    }
    if($(this).val() != '') {
    $.ajax({
        type:'POST',
        url:'exam/subject',
        data:{select:select, value:value, db:db, dependentt:dependentt},
        success:function(result)
        {
            $('#'+dependent).html(result);
        }
    })
    }
});
$('#lclass_id').change(function(){
  $('#type_id').val('');
  $('#lsubject').val('');
  $('#subject_id').val('');
  $('#chapter').val('');

 });
$('#lcategory_id').change(function(){
  $('#lsubject').val('');
  $('#subject_id').val('');
  $('#chapter').val('');
 });
$('#type_id').change(function(){
  $('#chapter').val('');
 });

$('.chapter').on('change',function(){
    var select = $(this).attr("id");
    if (select == 'lclass_id') {
        var select = 'class_id';
    }
    var db = $(this).attr("name");
    var value = $(this).val();
    var dependent = $(this).data('dependent');
    if( dependent == 'lsubject' ){
        var dependentt = 'Subject';
    }
    if($(this).val() != '') {
    $.ajax({
        type:'POST',
        url:'exam/subject',
        data:{select:select, value:value, db:db, dependentt:dependentt},
        success:function(result)
        {
            $('#'+dependent).html(result);
        }
    })
    }
});


$('.dynam').on('change',function(){
    var select = $(this).attr("id");
    if (select == 'lclass_id') {
        var select = 'class_id';
    }
    if (select == 'lcategory_id') {
        var select = 'category_id';
    }
    if (select == 'lsubject') {
        var select = 'subject_id';
    }
    var db = $(this).attr("name");
    var value = $(this).val();
    var dependent = $(this).data('dependent');
    if( dependent == 'lcategory_id' ){
        var dependentt = 'Category';
        $('#lcategory_id').css({"display":"block"});
    }else if( dependent == 'examname_id' ){
        var dependentt = 'Exam';
        $('#examname_id').css({"display":"block"});
    }else if( dependent == 'lsubject' ){
        var dependentt = 'Subject';
        $('#lsubject').css({"display":"block"});
    }else if( dependent == 'chapter' ){
        var dependentt = 'Chapter';
        $('#chapter').css({"display":"block"});
    }else{
        var dependentt = 'Category';
        $('#lcategory_id').css({"display":"block"});
    }
    if($(this).val() != '') {
    $.ajax({
        type:'POST',
        url:'exam/chapter',
        data:{select:select, value:value, db:db, dependentt:dependentt},
        success:function(result)
        {
            $('#'+dependent).html(result);
        }
    })
    }
});

$("#chapter").change(function(){
 var chapterid = $(this).val();
 // console.log(chapterid);
 $("#id_chap").val(chapterid);
});

$("#form").submit(function(e){
    e.preventDefault();
    var quest = $("#question").val();
    var ans = $("#answer").val();
    var chap = $("#id_chap").val();
    var opt1 = $("#option1").val();
    var opt2 = $("#option2").val();
    var opt3 = $("#option3").val();
    // image
    var iquest = $("#iquestion").val();
    var ians = $("#i_answer").val();
    var iopt1 = $("#i_option1").val();
    var iopt2 = $("#i_option2").val();
    var iopt3 = $("#i_option3").val();

    if (chap == '') {
        alert("Chapter is not selected!! Select one chapter to continue");
        return false;
    }
    else if (quest == '' && iquest == '') {
        alert("Question field is EMPTY!.Check text or image field");
        return false;
    }
    else if (ans == '' && ians == '') {
        alert("Answer field is EMPTY!.Check text or image field");
        return false;
    }
    else if (opt1 == '' && iopt1 == '') {
        alert("Option 1 field is EMPTY!.Check text or image field");
        return false;
    }
    else if (opt2 == '' && iopt2 == '') {
        alert("Option 2 field is EMPTY!.Check text or image field");
        return false;
    }
    else if (opt3 == '' && iopt3 == '') {
        alert("Option 3 field is EMPTY!.Check text or image field");
        return false;
    }
    else{
    var fd = new FormData(this);
        $.ajax({
            type:'POST',
            url:'question_add',
            data:fd,
            contentType: false,
            cache: false,
            processData:false,
            success:function(result)
            {   
                var json = $.parseJSON(result);
                var status = json['status'];
                var q_id = json['q_id'];
                if (q_id != 0){
                    CKEDITOR.instances.question.setData('');
                    CKEDITOR.instances.answer.setData('');
                    CKEDITOR.instances.solution.setData('');
                    CKEDITOR.instances.option1.setData('');
                    CKEDITOR.instances.option2.setData('');
                    CKEDITOR.instances.option3.setData('');
                    $("#iquestion").val(null);
                    $("#i_answer").val(null);
                    // $("#solution").val("");
                    $("#i_option1").val(null);
                    $("#i_option2").val(null);
                    $("#i_option3").val(null);
                    alert(status);
                    $("#question").val("");
                    $("#answer").val("");
                    $("#solution").val("");
                    $("#option1").val("");
                    $("#option2").val("");
                    $("#option3").val("");
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type:'POST',
                        url:'append_question',
                        success:function(data) {
                        $('#question_list').html(data);
                        }
                    });
                }else{
                    alert(status);
                    // console.log(result);
                }
            }
        });
    }
});

$('.classs').on('change ', function () {
        var class_id = $(this).val();
        if($(this).val() != '') {
            $("#categorys").empty();
            $("#exam_title").empty();
            $.ajax({
                    url:"exam/cate/"+class_id,
                    type:'POST',
                    success:function(response) {
                        var className = "#".concat(class_id);
                        $('#categorys').append("<div class='"+class_id+"' id='"+class_id+"' > </div>");
                        var myObj = JSON.parse(response);
                        for (i = 0; i < myObj.length; i++) {
                          $(className).append("<label class='label_container'> " +myObj[i].name+ " <input type='checkbox' class='category' name='category[]' id='cat"+myObj[i].id+"' onclick='category("+myObj[i].id+")' value='"+myObj[i].id+"'> <span class='label_checkmark'> </span>");
                        }
                        $(className).append("<hr>");
                    }});
        }
});

function category(category_id){
    var id = "cat".concat(category_id) 
    var c = document.getElementById(id);
    console.log(category_id);
    if (c.checked)
    {
        $.ajax({
            url:"exam/types/"+category_id,
            type:'POST',
            success:function(response) {
                var class_title = "#category".concat(category_id);
                $('#exam_title').append("<div class='category"+category_id+"' id='category"+category_id+"' ></div>");
                var myObj = JSON.parse(response);
                for (i = 0; i < myObj.length; i++){
                  $(class_title).append("<label class='label_container'> " +myObj[i].name+ " <input type='checkbox' class='category checkbox_n' name='e_name[]' id='title"+myObj[i].id+"' onclick='exam_title("+myObj[i].id+")' value='"+myObj[i].id+"'> <span class='label_checkmark'> </span>");
                }
                $(class_title).append("<hr>");
            }});
    }
    else
    {
        var class_title = "#category".concat(category_id);
        $(class_title).empty();
    }
}

function exam_title(title_id){
    var id = "title".concat(title_id) 
    var c = document.getElementById(id);
    console.log(title_id);
    if (c.checked)
    {
        $.ajax({
            url:"exam/subjects/"+title_id,
            type:'POST',
            success:function(response) {
                var class_type = "#types".concat(title_id);
                $('#exam_subject').append("<div class='type"+title_id+"' id='types"+title_id+"' ></div>");
                var myObj = JSON.parse(response);
                for (i = 0; i < myObj.length; i++){
                  $(class_type).append("<label class='label_container'> " +myObj[i].name+ " <input type='checkbox' class='category checkbox_n' id='type"+myObj[i].id+"' name='e_titles' onclick='exam("+myObj[i].id+")' value='"+myObj[i].id+"'> <span class='label_checkmark'> </span>");
                }
                $(class_type).append("<hr>");
            }});
    }
    else
    {
        var class_type = "#types".concat(title_id);
        $(class_type).empty();
    }
}

function exam(subject_id){
    var id = "type".concat(subject_id) 
    var c = document.getElementById(id);
    if (c.checked)
    {
         var array = []; 
         $("input:checkbox[name=e_titles]:checked").each(function() { 
                array.push($(this).val()); 
            });
        console.log('value in array');
        for (x in array)
            {
                console.log(x);
            }

        $.ajax({
            url:"exam/exams/"+subject_id,
            type:'POST',
            success:function(response) {
                
                var subject = "#subj".concat(subject_id);
                $('#exam').append("<div class='subj"+subject_id+"' id='subj"+subject_id+"' ></div>");
                var myObj = JSON.parse(response);
                for (i = 0; i < myObj.length; i++){
                  $(subject).append("<label class='label_container'> " +myObj[i].name+ " <input type='checkbox' class='category checkbox_n' id='exam"+myObj[i].id+"' value='"+myObj[i].id+"'> <span class='label_checkmark'> </span>");
                }
                $(subject).append("<hr>");
            }});
    }
    else
    {
        var subject = "#subj".concat(subject_id);
        $(subject).empty();
    }
}