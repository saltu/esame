var ans_array = [];
var ques_id = 0;

$(document).ready(function() {
	"use strict";
	/* form validate */
	$.validate({
		'scrollToTopOnError' : false
	});
	// select box 
	if ($(".order-select").length == 1) {
		$(".order-select").selectbox();
	}
	if ($(".area-select").length == 1) {
		$(".area-select").selectbox();
	}
	if ($(".degree-select").length == 1) {
		$(".degree-select").selectbox();
	}
	if ($(".specialization-select").length == 1) {
		$(".specialization-select").selectbox();
	}
	// home page loader
	setTimeout(function(){
		$('body').addClass('loaded');
	}, 1000);
	
	// preparation slider call
	if ($(".preparation-view").length == 1) {
		var owl1 = $('.preparation-view');
		  owl1.owlCarousel({
			margin: 30,
			loop: true,
			items : 2,
			dots: true,
			nav: false,
			responsive: {
			  0: {
				items: 1,
				margin: 0,
			  },
			  768: {
				items: 2
			  }
			}
		});
	}
	
	// banner img in backgound 
	if ($(".banner.inner-page").length == 1)
	{
		var img_src = $(".banner.inner-page .banner-img").children("img").attr("src");
		$(".banner.inner-page .banner-img").css("background-image", 'url(' + img_src + ')');
		$(".banner.inner-page .banner-img").children("img").hide();
	}
	
	// feedback slider call
	if ($(".feedback-slider").length == 1) {
		var owl1 = $('.feedback-slider');
		  owl1.owlCarousel({
			margin: 0,
			loop: true,
			items : 1,
			dots: false,
			nav: true,
			navText: [ '<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>' ],
		});
	}
	
	// member slider Call
	if ($(".member-slider").length == 1) {
		var owl1 = $('.member-slider');
		  owl1.owlCarousel({
			margin: 0,
			loop: true,
			items : 3,
			dots: true,
			nav: false,
			responsive: {
			  0: {
				items: 1,
			  },
			  768: {
				items: 2
			  },
			  1200: {
				items: 3
			  }
			}
		});
	}
	
	// radio and check box js call
//	$('.label_check, .label_radio').click(function(){
//		setupLabel();
//	});
//	setupLabel(); 
	
	/* faq accordion */
	$(".faq-page .faq-slide").each(function() {
        $(this).children(".title").on('click',function(){
			if($(this).next(".faq-content").is(":visible"))
			{
				$(this).next(".faq-content").slideUp();
				$(this).removeClass("active");
				$(this).children(".fa").removeClass("fa-angle-down").addClass("fa-angle-right");
			}
			else
			{
				$(".faq-page .faq-slide .title").removeClass("active");
				$(".faq-page .faq-slide .title .fa").removeClass("fa-angle-down").addClass("fa-angle-right");
				$(".faq-page .faq-slide .faq-content").slideUp();
				$(this).addClass("active");
				$(this).children(".fa").removeClass("fa-angle-right").addClass("fa-angle-down");
				$(this).next(".faq-content").slideDown();
				
			}
		});
    });
	
	// check Out address change
	$(".step2 .check-slide .label_check").on('click',function(){
		if($(this).hasClass("c_on"))
		{
			$(".step2 .billing-add").show();
		}
		else
		{
			$(".step2 .billing-add").hide();
		}
	});
	
	// Galler Light box
	if ($(".gallery-grid").length == 1)
	{
		$(".fancybox").fancybox({ // Fancybox
			padding:5,
			helpers: {
				overlay: {
					locked: false
				}
			}
		});
	}
	
	// images galler filter
	var workFilter = $('.gallery-filter li');
	workFilter.on("click", function(){
		workFilter.removeClass('active');
		$(this).addClass('active');
	});
    if ($(".gallery-grid").length == 1)
	{
		$('.gallery-grid').filterizr();
	}
	
	// syllabus accident 
	$(".syllabus-view .main-point").click(function(){
		if($(this).next(".point-list").is(":visible"))	
		{
			$(this).next(".point-list").slideUp();
			$(this).removeClass("active");
		}
		else
		{
			$(this).next(".point-list").slideDown();
			$(this).addClass("active");
		}
	});
	
	// Testimonial slider Call
	if ($(".testimonial-view").length == 1) {
		var owl1 = $('.testimonial-view');
		  owl1.owlCarousel({
			margin: 30,
			loop: true,
			items : 3,
			dots: true,
			nav: false,
			responsive: {
			  0: {
				items: 1,
			  },
			  768: {
				items: 2
			  },
			  1200: {
				items: 3
			  }
			}
		});
	}
	
	if ($(".testimonial-view2").length == 1) {
		var owl1 = $('.testimonial-view2');
		  owl1.owlCarousel({
			margin: 30,
			loop: true,
			items : 2,
			dots: true,
			nav: false,
			responsive: {
			  0: {
				items: 1,
			  },
			  768: {
				items: 2
			  }
			}
		});
	}
	
	if ($(".testimonial-view3").length == 1) {
		var owl1 = $('.testimonial-view3');
		  owl1.owlCarousel({
			margin: 20,
			loop: true,
			items : 3,
			dots: true,
			nav: false,
			responsive: {
			  0: {
				items: 1,
			  },
			  768: {
				items: 2
			  },
			  1200: {
				items: 3
			  }
			}
		});
	}
	
	if ($(".testimonial-view4").length == 1) {
		var owl1 = $('.testimonial-view4');
		  owl1.owlCarousel({
			margin: 0,
			loop: true,
			items : 1,
			dots: true,
			nav: false,
		});
	}
	
	// datepicker call
	if ($("#datepicker1").length) {
		$('#datepicker1').datepicker();
	}
	
	// instructors slider call
	if ($(".instructors-slider").length == 1) {
		var owl1 = $('.instructors-slider');
		  owl1.owlCarousel({
			margin: 0,
			loop: true,
			items : 1,
			dots: false,
			nav: false,
			autoplay: true,
		});
	}
	
	// nav arrow set in moblie view
	$(".nav > li.sub-menu").each(function() {
        $(this).children("a").after("<span class='arrow'><i class='fa fa-plus'></i></span>");
    });
	$(".nav > li.sub-menu .arrow").click(function(){
		if($(this).next().is(":visible"))
		{
			$(this).children(".fa").removeClass("fa-minus");
			$(this).children(".fa").addClass("fa-plus");
			$(this).next().slideUp();
		}
		else
		{
			$(".nav > li.sub-menu .arrow .fa").removeClass("fa-minus");
			$(".nav > li.sub-menu .arrow .fa").addClass("fa-plus");
			$(".nav > li.sub-menu .arrow").next().slideUp();
			$(this).children(".fa").removeClass("fa-plus");
			$(this).children(".fa").addClass("fa-minus");
			$(this).next().slideDown();
		}
	});
	
	// comming soon counter
	if ($("#countdown_commigsoon").length == 1)
	{
		$("#countdown_commigsoon").countdown({ // Counter timer
				date: "01 May 2017 12:00:00", // Enter new date here
				format: "on"
		});
	}
	
	// comming soon page height
	if ($(".comming-soon").length == 1)
	{
		$(".comming-soon").css("min-height", $(window).height() +"px");
		$(window).resize(function(){
			$(".comming-soon").css("min-height", $(window).height() +"px");
		});
	}
	
	// index2 banner Slider
	if ($(".left-slider").length == 1) {
		var owl1 = $('.left-slider');
		  owl1.owlCarousel({
			margin: 0,
			loop: true,
			items : 1,
			dots: false,
			nav: true,
			autoplay: true,
		});
		$(".left-slider .item").each(function() {
            var img_src = $(this).children("img").attr("src");
			$(this).css("background-image","url('" + img_src + "')");
			$(this).children("img").hide();
        });
	}
	
	//reviews slider Call 
	if ($(".reviews-slider").length == 1) {
		var owl1 = $('.reviews-slider');
		  owl1.owlCarousel({
			margin: 30,
			loop: true,
			items : 2,
			dots: false,
			nav: true,
			autoplay: true,
			navText: [ '<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>' ],
			responsive: {
			  0: {
				items: 1,
			  },
			  768: {
				items: 1
			  },
			  991: {
				items: 2
			  }
			}
		});
	}
	// search open
	$(".search-box").click(function(){
		$(".search-blcok").fadeIn();
	});
	$(".close-icon").click(function(){
		$(".search-blcok").fadeOut();
	});
	
	// body scroll Top
	$("#goTop").on("click",function(e) {
		$("html, body").animate({ scrollTop: 0 }, 500);
	});
	
	// language Dropdown
	$(".language-select a").on("click",function(e) {
		if($(this).next(".language-list").is(":visible"))
		{
			$(this).next(".language-list").slideUp();
		}
		else
		{
			$(".right-link .accout-link").slideUp();
			$(this).next(".language-list").slideDown();
		}
	});
	
	// userProfile Dropdown
	$(".user-profileLink a").on("click",function(e) {
		if($(this).next(".accout-link").is(":visible"))
		{
			$(this).next(".accout-link").slideUp();
		}
		else
		{
			$(".right-link .language-list").slideUp();
			$(this).next(".accout-link").slideDown();
		}
	});
	
	//user account tab 
	$(".account-tab ul li a").each(function() {
		$(this).click(function(){
			var open_id = $(this).attr("id");
			if($("."+ open_id + "-con").is(":visible"))
			{
				
			}
			else
			{
				$(".account-tab ul li").removeClass("active");
				$(this).parents("li").addClass("active");
				$(".my-account .tab-content").slideUp();
				$("."+ open_id + "-con").slideDown();	
			}
		});
	});
	
	//Copyright Year
	var currentYear = (new Date).getFullYear();
	$("#footer .copy-right .year").text(currentYear);
    
    
	var myTreeView = document.getElementById("my-tree-view");
	treeView(myTreeView);

	function hasClass(element, className)
	{
	    return element.classList.contains(className);
	}

	function treeView(element)
	{
	    element.addEventListener("click", (e) => {
	        let target = e.target;
	        if(hasClass(target, "branch-node"))
                {
	            target.classList.toggle("open");
                }
	        else if(hasClass(target, "branch-title"))
                {
	            target.parentNode.classList.toggle("open");
                }
	    });
	}
    
});

// fiexd menu 
$(window).on('scroll', function() {
	if ($(window).scrollTop() > 50)
	{
		$(".top-arrow").fadeIn();
		$("#header").addClass("fiexd");
	}
	else
	{
		$(".top-arrow").fadeOut();
		$("#header").removeClass("fiexd");
	}
});

/* check and radio js start */
//function setupLabel() {
//	if ($('.label_check input').length) {
//		$('.label_check').each(function(){ 
//				$(this).removeClass('c_on');
//			});
//			$('.label_check input:checked').each(function(){ 
//				$(this).parent('label').addClass('c_on');
//			});                
//		};
//		if ($('.label_radio input').length) {
//			$('.label_radio').each(function(){ 
//				$(this).removeClass('r_on');
//			});
//			$('.label_radio input:checked').each(function(){ 
//				$(this).parent('label').addClass('r_on');
//			});
//		};
//};
	
$(".toggle-password").click(function() {

        $(this).toggleClass("fa-eye fa-eye-slash");
        var input = $($(this).attr("toggle"));
        if (input.attr("type") == "password") {
          input.attr("type", "text");
        } else {
          input.attr("type", "password");
        }
});

$('.fun12').on('change', function (){
        console.log('new');
        var class_id = $(this).val();
         $('#cat').append(" <input id='new' type='checkbox'> New <label for='fun'>Make my profile visible</label>" );
});


$('.class').on('change ', function () {
       if ($(this).is(':checked') == true)
   	{
           
           var class_id = $(this).val();
           $.ajax({
                   url:"exam/cate/"+class_id,
                   type:'POST',
                   success:function(response) {
                       var className = "#".concat(class_id);
                       $('#category').append("<div class='"+class_id+"' id='"+class_id+"' > </div>");
                       var myObj = JSON.parse(response);
                       for (i = 0; i < myObj.length; i++) {
                         $(className).append("<label class='label_container'> " +myObj[i].name+ " <input type='checkbox' class='category name= checkbox_n' id='cat"+myObj[i].id+"' onclick='category("+myObj[i].id+")' value='"+myObj[i].id+"'> <span class='label_checkmark'> </span>");
                       }
                       $(className).append("<hr>");
                   }});
   	}
   	else
   	{
           var class_id = $(this).val();
           var className = "#".concat(class_id);
           console.log(className);
           $(className).empty();
   	}
});

function category(category_id){
    var id = "cat".concat(category_id) 
    var c = document.getElementById(id);
    console.log(category_id);
    if (c.checked)
    {
        $.ajax({
            url:"exam/types/"+category_id,
            type:'POST',
            success:function(response) {
                var class_title = "#category".concat(category_id);
                $('#exam_title').append("<div class='category"+category_id+"' id='category"+category_id+"' ></div>");
                var myObj = JSON.parse(response);
                for (i = 0; i < myObj.length; i++){
                  $(class_title).append("<label class='label_container'> " +myObj[i].name+ " <input type='checkbox' class='category checkbox_n' name='e_name[]' id='title"+myObj[i].id+"' onclick='exam_title("+myObj[i].id+")' value='"+myObj[i].id+"'> <span class='label_checkmark'> </span>");
                }
                $(class_title).append("<hr>");
            }});
    }
    else
    {
        var class_title = "#category".concat(category_id);
        $(class_title).empty();
    }
}

function exam_title(title_id){
    var id = "title".concat(title_id) 
    var c = document.getElementById(id);
    console.log(title_id);
    if (c.checked)
    {
        $.ajax({
            url:"exam/subjects/"+title_id,
            type:'POST',
            success:function(response) {
                var class_type = "#types".concat(title_id);
                $('#exam_subject').append("<div class='type"+title_id+"' id='types"+title_id+"' ></div>");
                var myObj = JSON.parse(response);
                for (i = 0; i < myObj.length; i++){
                  $(class_type).append("<label class='label_container'> " +myObj[i].name+ " <input type='checkbox' class='category checkbox_n' id='type"+myObj[i].id+"' name='e_titles' onclick='exam("+myObj[i].id+")' value='"+myObj[i].id+"'> <span class='label_checkmark'> </span>");
                }
                $(class_type).append("<hr>");
            }});
    }
    else
    {
        var class_type = "#types".concat(title_id);
        $(class_type).empty();
    }
}

function exam(subject_id){
    var id = "type".concat(subject_id) 
    var c = document.getElementById(id);
    if (c.checked)
    {
         var array = []; 
         $("input:checkbox[name=e_titles]:checked").each(function() { 
                array.push($(this).val()); 
            });
        console.log('value in array');
        for (x in array)
            {
                console.log(x);
            }

        $.ajax({
            url:"exam/exams/"+subject_id,
            type:'POST',
            success:function(response) {
                var subject = "#subj".concat(subject_id);
                $('#exam').append("<div class='subj"+subject_id+"' id='subj"+subject_id+"' ></div>");
                var myObj = JSON.parse(response);
                for (i = 0; i < myObj.length; i++){
                  $(subject).append("<label class='label_container'> " +myObj[i].name+ " <input type='checkbox' class='category checkbox_n' id='exam"+myObj[i].id+"' value='"+myObj[i].id+"'> <span class='label_checkmark'> </span>");
                }
                $(subject).append("<hr>");
            }});
    }
    else
    {
        var subject = "#subj".concat(subject_id);
        $(subject).empty();
    }
}

$(function(){
      $('#action').click(function(){
      	var msg = $('#msg').val();
        var val = [];
        $(':checkbox:checked').each(function(i){
          val = $(this).val();
          // alert(val[i]);
          // alert(msg);
          
          $.ajax({
          	url: 'notification/send',
          	type: "POST",
          	data: { val ,  msg},
          	success: function(response){ 
          		console.log(response);
		    }
          	
          });
        });
      });
    });

$('#select_all').click(function() {
	if($('.new_check').is(':checked')){
		$('.new_check').prop('checked', false);
	}
	else{
		$('.new_check').prop('checked', true);
	}
	
});
    $("input[type='checkbox']").not('#select_all').change( function() {
    $('#select_all').prop('checked', false);
});

/**************** Exam  *********************/
function clear_storage(){
    console.log('clear storage');
    removeCookies();
    localStorage.clear();
}
function question_save(que){
	if(localStorage.getItem("qes") == null){
		localStorage.setItem("qes", que);
		// alert(JSON.stringify(localStorage.getItem("qes")));
	}
}

function load_question(exam, exam_id, arr, q_id){
//    console.log('function');
    ques_id = q_id;
    var question_arr = arr.split(",");
    var index = question_arr.indexOf(q_id);
//    console.log(index);
    
    for (var i = 1; i <= question_arr.length; i++){
        hide_property(i);
    }
    
    hide_buttons(index, question_arr);
    var index = Number(index) + Number(1);
    $.ajax({
        type:'POST',
        url:'../../../../../../../exam/single',
        data:{q_id:q_id},
        success:function(response) {
            var myObj = JSON.parse(response);
            fun_loadquestion(index,myObj,arr);
        }
    });
}

function hide_buttons(index, question_arr){
        /**** Hiding Next button on second last Qustion *****/   
        var next_index = Number(index) + Number(1) ;
        var next_q_id = question_arr[next_index];
//        console.log(next_q_id);
        if (typeof next_q_id == "undefined"){
            document.getElementById("question_next").style.visibility = "hidden";
        }
        else{
            document.getElementById("question_next").style.visibility = "visible";
        }
        /***************** END ******************************/
    
        /**** Hiding Prev on second last Qustion ************/ 

        if (index == '0'){
            document.getElementById("question_previous").style.visibility = "hidden";
        }
        else{
            document.getElementById("question_previous").style.visibility = "visible";
        }
        /***************** END ******************************/
}

function fun_loadquestion(index,myObj,arr){
        var element = document.getElementById(index);
        element.classList.remove("fill");
        var element = document.getElementById(index);
        element.classList.add("active");
    
        $('#questions').empty();
        $('#questions').append("<p> <b>Question</b> <label id='id_label'>"+index+"</label>.</p>");
        $('#questions').append("<div class='qustion'> " +myObj[0]+ "</div>");
        $('#questions').append("<div class='ans' id='ans'> </div>");

        if (localStorage.getItem("arr") === null) {
            for (i = 0; i < myObj[1].length; i++){
                $('#ans').append("  <div class='ans-slide'><label class='radio_container' for='radio-01'><input type='radio' name='sample-radio[]' class='option_radio' id='radio-"+myObj[1][i].id+"' value="+myObj[1][i].id+" >" +myObj[1][i].options+ "</label></div>");
            }
        }
        else{
            var getarray = localStorage.getItem('arr');
            var ans = getarray.split(",");
            index = index-1;
            if(ans[index] == '0' ){
                for (i = 0; i < myObj[1].length; i++){
                    $('#ans').append("  <div class='ans-slide'><label class='radio_container' for='radio-01'><input type='radio' name='sample-radio[]' class='option_radio' id='radio-"+myObj[1][i].id+"' value="+myObj[1][i].id+" >" +myObj[1][i].options+ "</label></div>");
                }
            }
            else{
                for (i = 0; i < myObj[1].length; i++){
                    if(ans[index] ==myObj[1][i].id ) { 
                        $('#ans').append("  <div class='ans-slide'><label class='radio_container' for='radio-01'><input type='radio' name='sample-radio[]' class='option_radio' id='radio-"+myObj[1][i].id+"' value="+myObj[1][i].id+" checked >" +myObj[1][i].options+ "</label></div>");
                    }
                    else{
                        $('#ans').append("  <div class='ans-slide'><label class='radio_container' for='radio-01'><input type='radio' name='sample-radio[]' class='option_radio' id='radio-"+myObj[1][i].id+"' value="+myObj[1][i].id+">" +myObj[1][i].options+ "</label></div>");
                    }
                }
            }
        }
}

function next_question(arr){
//        console.log('next_question');
        var index = $("#id_label").text();
        var question_arr = arr.split(",");
    
        hide_property(index);
        hide_buttons(index, question_arr);
    
        var q_id = question_arr[index];
    
        if (ques_id == 0){
            ques_id = question_arr[0];
        }
        var indx = Number(index) - Number(1);
        save_answer(indx,arr);

        $.ajax({
                type:'POST',
                url:'../../../../../../../exam/single',
                data:{q_id:q_id},
                success:function(response) {
                    index = Number(index) + Number(1);
                    var myObj = JSON.parse(response);
                    fun_loadquestion(index,myObj,arr);
        }});
}

function hide_property(index){
        var element = document.getElementById(index);
        element.classList.remove("active");
}

function save_answer(index,arr){
        var Value = $('.option_radio:checked').val();
        var inx = Number(index)+ Number(1);
        if(typeof Value == "undefined"){
            Value = 0;
            var element = document.getElementById(inx);
            element.classList.remove("fill");
        }
        else{
            var element = document.getElementById(inx);
            element.classList.add("fill");
        }
        var question_arr = arr.split(",");
        var ques_length = question_arr.length;

        var ans_length = ans_array.length;

        if(ans_length == 0){
            for(var i = 0; i < ques_length ; i++) {
                ans_array.push('0');
            }
            var getarray = localStorage.getItem('arr');            
            if (localStorage.getItem("arr") === null) {
                ans_array[index] = Value;
                localStorage.setItem("arr", ans_array);
            }
            else{
                ans_array = getarray.split(",");
                ans_array[index] = Value;
                localStorage.setItem("arr", ans_array);
            }
        }
        else{
            var getarray = localStorage.getItem('arr');
            ans_array = getarray.split(",");
            ans_array[index] = Value; 
            localStorage.setItem("arr", ans_array);
        }
    
    var std_id = $('#userid').val();
    var exm_id = $('#examid').val();
    var arrl = localStorage.getItem("arr");
    
    $.ajax({
        type:'POST',
        url:'../../../../../../../exam/save/answer',
        data:{std_id:std_id, ans:arrl, exm_id:exm_id},
        success:function(response) {
        }
    });
}

function prev_question(arr){
        var index = $("#id_label").text();
        var question_arr = arr.split(",");
        
        hide_property(index);
    
        var indx = Number(index) - Number(1); 
        save_answer(indx,arr);

        var index = Number(index) - Number(2);
        var q_id = question_arr[index];
        hide_buttons(index, question_arr);
        $.ajax({
                type:'POST',
                url:'../../../../../../../exam/single',
                data:{q_id:q_id},
                success:function(response) {
                    index = Number(index) + Number(1);
                    var myObj = JSON.parse(response);
                    fun_loadquestion(index,myObj,arr);
        }});
}

function submit_quiz(arr){
	// alert(JSON.stringify(arr));
        var index = $("#id_label").text();
        var question_arr = arr.split(",");
        var q_id = question_arr[index];
    
        if (ques_id == 0){
            ques_id = question_arr[0];
        }
        var indx = Number(index) - Number(1);
        save_answer(indx,arr);
    
        var getarray = localStorage.getItem('arr');
        var ans = getarray.split(",");
        var result_array= [];
         $.ajax({
                type:'POST',
                url:'../../../../../../../exam/result',
                data:{question:question_arr},
                success:function(response) {
                	// alert(response);
                    var myObj = JSON.parse(response);
                    for (i = 0; i < myObj.length; i++){
                        if( myObj[i] == ans[i] ){
                            result_array.push("1");
                        }
                        else{
                            if( ans[i] == '0' ){
                                result_array.push("0");
                            }
                            else{
                                result_array.push("-1");
                            }
                        }
                    }
                    
                    var std_id = $('#userid').val();
                    var exm_id = $('#examid').val();
                    $.ajax({
                        type:'POST',
                        url:'../../../../../../../exam/save/result',
                        data:{std_id:std_id, exm_id:exm_id, result_array:result_array},
                        success:function(response) {
                            check_result(arr);
                        }
                    });
            }});

}

function refresh_selection(){
    $( ".option_radio" ).prop( "checked", false );
    var arrl = localStorage.getItem("arr");
}

function load_page(exam_time){
    console.log('Load page');
    var getarray = localStorage.getItem('arr');
	if (localStorage.getItem("arr") === null) {
        console.log('no question anwered');
        removeCookies();
    }
    else{
        ans_array = getarray.split(",");
        for (var i = 0; i < ans_array.length; i++){
            if(ans_array[i] != '0' ){
                    var inx = Number(i)+Number(1);
                    var element = document.getElementById(inx);
                    element.classList.add("fill");
                }
        }
    }
    var element1 = document.getElementById("1");
    element1.classList.add("active");
    countdown(exam_time,true);
}

var timeoutHandle;
function countdown(minutes,stat) {
    var seconds = 60;
    var mins = minutes;
    var flag = 1;
	if(getCookie("exam_minutes")&&getCookie("exam_seconds")&&stat)
	{
		 var seconds = getCookie("exam_seconds");
    	 var mins = getCookie("exam_minutes");
	}
    function tick() {
		setCookie("exam_minutes",mins)
		setCookie("exam_seconds",seconds)
        var current_minutes = mins-1
        seconds--;
        document.getElementById('minutes').innerText = +current_minutes;
        document.getElementById('seconds').innerText = +seconds;
        
		//save the time in cookie
        if( seconds > 0 ) {
            timeoutHandle=setTimeout(tick, 1000);
        } else {
            if(mins > 1){
               // countdown(mins-1);   never reach “00″ issue solved:Contributed by Victor Streithorst    
               setTimeout(function () { countdown(parseInt(mins)-1,false); }, 1000);
            }
        } 
        if(current_minutes == 4 && seconds == 59  ){
                var modal = document.getElementById("alert_Modal");
                modal.style.display = "block";

                var element = document.getElementById("count_timer");
                element.classList.remove("timer");
                element.classList.add("alert_timerr");
        }
        
        if(current_minutes==0 && seconds ==0 ){
            alert ("Your time is Over. All the best! ");
            console.log('Time over');
            var arrr = localStorage.getItem("qes");
            $("#Result_Modal").modal("show");
            // var f_result = document.getElementById("Result_Modal");
            // f_result.model.("modal");
            // alert(JSON.stringify(arrr));
            submit_quiz(arrr);
            // clear_storage();
            // window.location='/';
        }
    }
    tick();
}

$('#drop').change(function() {
	var val= $('#drop').val();
		$.ajax({
          	url: 'manage_student/view/'+val,
          	type: "POST",
          	success: function(response){ 
          		var x= JSON.parse(response);
          		console.log(response);
          		
          		for(var i=0; i<x.length;i++) {
          			// console.log(x[i].name);
			        $('#mytable tbody').append('<tr>'+
	          			'<td>'+x[i].id+'</td>'+
	          			'<td>'+x[i].name+'</td>'+
	          			'<td>'+x[i].email+'</td>'+
	          			'<td>'+x[i].status+'</td>'+
	          			'<td>'+'<i class="fa fa-edit">'+'</i>'+'</td>'+
	          			// '<td><a href="{{ route('manage.edit',$user->id)}}"><i class="fa fa-edit"></i></a>'+'</td>'+
	          			'<td>'+'<i class="fa fa-trash">'+'</i>'+'</td>'+
	          			'</tr>');
			      }
          		// @endforeach
		    }
          });
});

function setCookie(cname,cvalue) {
//    var d = new Date();
//    d.setTime(d.getTime() + (exdays*24*60*60*1000));
//    var expires = "expires=" + d.toGMTString();
//    document.cookie = num+" = "+num;
    document.cookie = cname+"="+cvalue+"; ";
    
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function removeCookies() {
  console.log('clear cookie');
  var res = document.cookie;
  var multiple = res.split(";");
 for(var i = 0; i < multiple.length; i++) {
  var key = multiple[i].split("=");
  document.cookie = key[0]+" =; expires = Thu, 01 Jan 1970 00:00:00 UTC";
  }
}

/************************* Pop up ******************************/

var span = document.getElementsByClassName("close")[0];
var modal = document.getElementById("alert_Modal");
span.onclick = function() {
  modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}

/**************************************************************/
function check_result(arr) {
	// alert(JSON.stringify(arr));
    var index = $("#id_label").text();
    var question_arr = arr.split(",");
    var q_id = question_arr[index];

    if (ques_id == 0){
        ques_id = question_arr[0];
    }
    var indx = Number(index) - Number(1);
    save_answer(indx,arr);

    var getarray = localStorage.getItem('arr');
    var ans = getarray.split(",");
    var result_array= [];
     $.ajax({
            type:'POST',
            url:'../../../../../../../exam/result',
            data:{question:question_arr},
            success:function(response) {
                var myObj = JSON.parse(response);
                
                var attented = 0;
                var wrong = 0;
                var correct = 0;
                var mark = 0;
                var length = myObj.length;
                for (i = 0; i < myObj.length; i++){
                    if( myObj[i] == ans[i] ){
                        result_array.push("1");
                        attented++;
                        correct ++;
                        mark += 4;
                    }
                    else{
                        if( ans[i] == '0' ){
                            result_array.push("0");
                        }
                        else{
                            result_array.push("-1");
                            wrong++;
                            attented++;
                        }
                    }
                }
            
//            var x = result_array.join();
//            console.log('newww');
//            console.log(x);
			var total = mark - wrong;
            document.getElementById("total_ques").innerHTML = length;
            document.getElementById("attented").innerHTML = attented;
            document.getElementById("wrong").innerHTML = wrong;
            document.getElementById("correct").innerHTML = correct;
            document.getElementById("total").innerHTML = total;
            var exm_id = $('#examid').val();
            $.ajax({
		        type:'POST',
		        url:'../../../../../../../exam/result/final',
		        data:{exm_id:exm_id, length:length, attented:attented, correct:correct},
		        success:function(result)
		        {
		            // alert(result);
		        }
		    });

        }});
//        alert(response);
        console.log('submitttt');
        clear_storage();
    }
/************************* Admin ******************************/

var MorrisBarReturnObject;
var MorrisDonutReturnObject;

$(function() {

  Morris.Area({
    element: 'morris-area-chart',
    data: [{
      period: '2010 Q1',
      iphone: 2666,
      ipad: null,
      itouch: 2647
    }, {
      period: '2010 Q2',
      iphone: 2778,
      ipad: 2294,
      itouch: 2441
    }, {
      period: '2010 Q3',
      iphone: 4912,
      ipad: 1969,
      itouch: 2501
    }, {
      period: '2010 Q4',
      iphone: 3767,
      ipad: 3597,
      itouch: 5689
    }, {
      period: '2011 Q1',
      iphone: 6810,
      ipad: 1914,
      itouch: 2293
    }, {
      period: '2011 Q2',
      iphone: 5670,
      ipad: 4293,
      itouch: 1881
    }, {
      period: '2011 Q3',
      iphone: 4820,
      ipad: 3795,
      itouch: 1588
    }, {
      period: '2011 Q4',
      iphone: 15073,
      ipad: 5967,
      itouch: 5175
    }, {
      period: '2012 Q1',
      iphone: 10687,
      ipad: 4460,
      itouch: 2028
    }, {
      period: '2012 Q2',
      iphone: 8432,
      ipad: 5713,
      itouch: 1791
    }],
    xkey: 'period',
    ykeys: ['iphone', 'ipad', 'itouch'],
    labels: ['iPhone', 'iPad', 'iPod Touch'],
    pointSize: 2,
    hideHover: 'auto',
    resize: true
  });

  MorrisDonutReturnObject = Morris.Donut({
    element: 'morris-donut-chart',
    data: [{
      label: "Download Sales",
      value: 12
    }, {
      label: "In-Store Sales",
      value: 30
    }, {
      label: "Mail-Order Sales",
      value: 20
    }],
    resize: true
  });

  MorrisBarReturnObject = Morris.Bar({
    element: 'morris-bar-chart',
    data: [{
      y: 'ADEP',
      a: 16361.8,
    }, {
      y: 'EAD',
      a: 8205.05,
    }, {
      y: 'EID',
      a: 137.5,
    }, {
      y: 'EMD',
      a: 1296.05,
    }, {
      y: 'ESMD',
      a: 514,
    }, {
      y: 'EWD',
      a: 323.5,
    }, {
      y: 'ITMD',
      a: 454.5,
    }],
    xkey: 'y',
    ykeys: ['a'],
    labels: ['Actual Work'],
    hideHover: 'auto',
    resize: true
  });
  
  $('#morris-donut-chart cvg path').each(function(index) {
    addEventListener('mouseout', showDefaultDonutData());
  });

  $("#morris-bar-chart svg rect").on('click', function() {
    setFilterForBottomMorrisTable($(this));
  });

  $(window).resize(function() {
    $("#morris-bar-chart svg rect").on('click', function() {
      setFilterForBottomMorrisTable($(this));
    });
  });
  
});

var tableDataJson = {
  "headers": ["Directorate or Division", "Actual Work", "FTEs", "Resource Count"],
  "data": [{
    "Directorate or Division": "ADEP",
    "Actual Work": 16361.8,
    "FTEs": 32,
    "Resource Count": 64
  }, {
    "Directorate or Division": "EAD",
    "Actual Work": 8205.05,
    "FTEs": 16,
    "Resource Count": 25
  }, {
    "Directorate or Division": "EID",
    "Actual Work": 137.5,
    "FTEs": 0.27,
    "Resource Count": 1
  }, {
    "Directorate or Division": "EMD",
    "Actual Work": 1296.05,
    "FTEs": 2.53,
    "Resource Count": 8
  }, {
    "Directorate or Division": "ESMD",
    "Actual Work": 514,
    "FTEs": 1,
    "Resource Count": 4
  }, {
    "Directorate or Division": "EWD",
    "Actual Work": 323.5,
    "FTEs": 0.63,
    "Resource Count": 4
  }, {
    "Directorate or Division": "ITMD",
    "Actual Work": 454.5,
    "FTEs": 0.89,
    "Resource Count": 2
  }]
};

var allMorrisTableData = {
  "headers": ["Enterprise Projects", "Directorate or Division", "Actual Work", "FTEs", "Resource Count"],
  "data": [{
    "Enterprise Projects": "2020 Census Program Management",
    "Directorate or Division": "ITMD",
    "Actual Work": 418.50,
    "FTEs": 0.82,
    "Resource Count": 1
  }, {
    "Enterprise Projects": "2020 Nonresponse Followup",
    "Directorate or Division": "ESMD",
    "Actual Work": 477.00,
    "FTEs": 0.93,
    "Resource Count": 1
  }, {
    "Enterprise Projects": "2020 Response Processing",
    "Directorate or Division": "ADEP",
    "Actual Work": 277.50,
    "FTEs": 0.54,
    "Resource Count": 1
  }, {
    "Enterprise Projects": "AHS 2015 Revised v1",
    "Directorate or Division": "ITMD",
    "Actual Work": 36.00,
    "FTEs": 0.07,
    "Resource Count": 1
  }, {
    "Enterprise Projects": "CE Diary Survey 2016",
    "Directorate or Division": "ADEP",
    "Actual Work": 3.00,
    "FTEs": 0.01,
    "Resource Count": 1
  }, {
    "Enterprise Projects": "CE Quarterly Survey 2016",
    "Directorate or Division": "ADEP",
    "Actual Work": 4.00,
    "FTEs": 0.01,
    "Resource Count": 1
  }, {
    "Enterprise Projects": "CEDCaP-Electronic Correspondence Portal",
    "Directorate or Division": "EAD",
    "Actual Work": 1321.05,
    "FTEs": 2.58,
    "Resource Count": 9
  }, {
    "Enterprise Projects": "CEDCaP-Electronic Correspondence Portal",
    "Directorate or Division": "EMD",
    "Actual Work": 1294.05,
    "FTEs": 2.53,
    "Resource Count": 7
  }, {
    "Enterprise Projects": "CEDCaP-Questionnaire Design and Metadata-COMET",
    "Directorate or Division": "ADEP",
    "Actual Work": 3916.50,
    "FTEs": 7.65,
    "Resource Count": 11
  }, {
    "Enterprise Projects": "CEDCaP-Questionnaire Design and Metadata-COMET",
    "Directorate or Division": "EAD",
    "Actual Work": 3670.50,
    "FTEs": 7.17,
    "Resource Count": 8
  }, {
    "Enterprise Projects": "CEDCaP-Scanning Data Capture from Paper-ICADE",
    "Directorate or Division": "ADEP",
    "Actual Work": 7061.30,
    "FTEs": 13.79,
    "Resource Count": 27
  }, {
    "Enterprise Projects": "CEDCaP-Service Oriented Architecture-API",
    "Directorate or Division": "EAD",
    "Actual Work": 168.00,
    "FTEs": 0.33,
    "Resource Count": 1
  }, {
    "Enterprise Projects": "CEDCaP-Survey and Listing Interview Operational Control-MOJO",
    "Directorate or Division": "ADEP",
    "Actual Work": 3768.25,
    "FTEs": 7.36,
    "Resource Count": 13
  }, {
    "Enterprise Projects": "CEDCaP-Survey and Listing Interview Operational Control-MOJO",
    "Directorate or Division": "EAD",
    "Actual Work": 3045.50,
    "FTEs": 5.95,
    "Resource Count": 7
  }, {
    "Enterprise Projects": "CEDCaP-Survey Response Processing-CARDS",
    "Directorate or Division": "ADEP",
    "Actual Work": 381.00,
    "FTEs": 0.74,
    "Resource Count": 1
  }, {
    "Enterprise Projects": "CLMSO FY16 Education and Training",
    "Directorate or Division": "EWD",
    "Actual Work": 32.00,
    "FTEs": 0.06,
    "Resource Count": 1
  }, {
    "Enterprise Projects": "FY2015 CPS 2014-2016 DSMD",
    "Directorate or Division": "ESMD",
    "Actual Work": 9.00,
    "FTEs": 0.02,
    "Resource Count": 1
  }, {
    "Enterprise Projects": "HSDC2016",
    "Directorate or Division": "ADEP",
    "Actual Work": 13.50,
    "FTEs": 0.03,
    "Resource Count": 1
  }, {
    "Enterprise Projects": "National Crime Victimization Survey SY 2015",
    "Directorate or Division": "ADEP",
    "Actual Work": 228.00,
    "FTEs": 0.45,
    "Resource Count": 1
  }, {
    "Enterprise Projects": "National Crime Victimization Survey SY 2016",
    "Directorate or Division": "ADEP",
    "Actual Work": 420.25,
    "FTEs": 0.82,
    "Resource Count": 2
  }, {
    "Enterprise Projects": "National Crime Victimization Survey SY 2016",
    "Directorate or Division": "EID",
    "Actual Work": 137.50,
    "FTEs": 0.27,
    "Resource Count": 1
  }, {
    "Enterprise Projects": "NHIS 2016",
    "Directorate or Division": "ESMD",
    "Actual Work": 18.00,
    "FTEs": 0.04,
    "Resource Count": 1
  }, {
    "Enterprise Projects": "NHIS Administrative Schedule",
    "Directorate or Division": "ESMD",
    "Actual Work": 10.00,
    "FTEs": 0.02,
    "Resource Count": 1
  }, {
    "Enterprise Projects": "NSCH 2016 Production",
    "Directorate or Division": "ADEP",
    "Actual Work": 241.00,
    "FTEs": 0.47,
    "Resource Count": 3
  }, {
    "Enterprise Projects": "NTPS 2015-2016",
    "Directorate or Division": "ADEP",
    "Actual Work": 47.50,
    "FTEs": 0.09,
    "Resource Count": 2
  }, {
    "Enterprise Projects": "NTPS 2015-2016",
    "Directorate or Division": "EMD",
    "Actual Work": 2.00,
    "FTEs": 0,
    "Resource Count": 1
  }, {
    "Enterprise Projects": "z6385B10 CATI Response Data",
    "Directorate or Division": "EWD",
    "Actual Work": 153.00,
    "FTEs": 0.3,
    "Resource Count": 1
  }, {
    "Enterprise Projects": "z6385B20 CAPI Response Data",
    "Directorate or Division": "EWD",
    "Actual Work": 114.50,
    "FTEs": 0.22,
    "Resource Count": 1
  }, {
    "Enterprise Projects": "z6385B30 PR Data Collection Data",
    "Directorate or Division": "EWD",
    "Actual Work": 24.00,
    "FTEs": 0.05,
    "Resource Count": 1
  }]
};

// This updates the data table beside the Morris.Bar 
function buildTable(localTableDataJson, tagId) {
  console.log('building table for ' + tagId);
  var content = "";
  var header = "<thead><tr>";
  console.log(localTableDataJson.headers)
  for (var index in localTableDataJson.headers) {
    console.log('added header');
    header += "<th>" + localTableDataJson.headers[index] + "</th>";
  }
  header += "</tr></thead>";
  content += header + "<tbody>";

  for (var index in localTableDataJson.data) {

    content += "<tr>";
    jQuery.each(localTableDataJson.data[index], function(index2, dataColumn) {
      content += "<td>" + dataColumn + "</td>";
    });
    content += "</tr>";

  }

  content += "</tbody>"

  $('#' + tagId).empty();
  $('#' + tagId).append(content);
}

buildTable(tableDataJson, 'first-morris-table');
buildTable(allMorrisTableData, 'second-morris-table');

var previousTagSelected;

function filterBottomMorrisTable(filterString, filterColumn) {
  console.log('filtering bottom Morris table for ' + filterString + ' in ' + filterColumn);
  console.log('previous tag: ' + previousTagSelected);
  if (filterString != previousTagSelected) {
    $('#second-morris-table > tbody > tr').each(function(index) {
      console.log('showing row');
      $(this).show();
    });
  }

  $('#second-morris-table > tbody > tr').each(
    function(index) {
      //console.log('table index ' + index);
      
      $(this).find('td').each(
        function(index2) {
          //console.log('column index ' + index2);
          var columnValue = $(this).text();
          //console.log('columnValue: ' + columnValue);
          var columnName = allMorrisTableData.headers[index2];
          //console.log('column name : ' + columnName);

          hideMorrisTable2Row($(this), columnName, columnValue, filterColumn, filterString);

        });

    }
  );
}

function hideMorrisTable2Row(selection, columnName, columnValue, filterColumn, filterString) {
  console.log('checking for ' + columnName + ' matching ' + filterColumn);
  if (columnName != filterColumn) {
    console.log(columnName + ' does not match' + filterColumn);
    return;
  }
  console.log(columnName + ' matches' + filterColumn);
  if ((columnValue != filterString) || (columnValue == '')) {
    console.log('toggle hiding table row for ' + columnValue);
    if ($(selection).parent().css('display') == 'none') {
      $(selection).parent().show();
      previousTagSelected = filterString;
    } else {
      $(selection).parent().hide();
    }
  } else {
    $(selection).parent().show();
    previousTagSelected = filterString;
  }
}

function updateMorrisBar(columnNumber) {
  console.log("updating morris bar");
  var newJSON = {
    element: 'morris-bar-chart',
    data: [{
      y: 'ADEP',
      a: 16361.8,
    }, {
      y: 'EAD',
      a: 8205.05,
    }, {
      y: 'EID',
      a: 137.5,
    }, {
      y: 'EMD',
      a: 1296.05,
    }, {
      y: 'ESMD',
      a: 514,
    }, {
      y: 'EWD',
      a: 323.5,
    }, {
      y: 'ITMD',
      a: 454.5,
    }],
    xkey: 'y',
    ykeys: ['a'],
    labels: ['Actual Work'],
    hideHover: 'auto',
    resize: true
  };
  switch (columnNumber) {
    case 1:
      for (var index in newJSON.data) {
        newJSON.data[index].a = tableDataJson.data[index]["Actual Work"];
      }
      newJSON.labels = ['Actual Work'];
      break;
    case 2:
      for (var index in newJSON.data) {
        newJSON.data[index].a = tableDataJson.data[index]["FTEs"];
      }
      newJSON.labels = ['FTEs'];
      break;
    case 3:
      for (var index in newJSON.data) {
        newJSON.data[index].a = tableDataJson.data[index]["Resource Count"];
      }
      newJSON.labels = ['Resource Count'];
      break;
  }

  $('#morris-bar-chart').empty();
  Morris.Bar(newJSON);
  //MorrisBarReturnObject.setData(newJSON);
  MorrisBarReturnObject.setData(newJSON.data, true);

  $("#morris-bar-chart svg rect").on('click', function() {
    //alert( "Handler for .on() called." );
    setFilterForBottomMorrisTable($(this));
  });
}

function setFilterForBottomMorrisTable(selectedTag) {
  var tagIndex = $('#morris-bar-chart svg rect').index(selectedTag);
  console.log('tag index: ' + tagIndex);
  var name = tableDataJson.data[tagIndex]['Directorate or Division'];
  console.log('Directorate or Division name: ' + name);
  filterBottomMorrisTable(name, 'Directorate or Division');
  showTotalFTEs();
}

// shows the total FTEs in the morris donut
function showTotalFTEs() {
  console.log('updating donut data');
  var totalFTEs = 0;
  var totalAmountWorked = 0;
  var totalResourceCount = 0;
  $('#second-morris-table > tbody > tr').each(
    function(index) {
      //console.log('table index ' + index);
      $(this).find('td').each(
        function(index2) {
          if ($(this).parent().css('display') != 'none') {
            if (allMorrisTableData.headers[index2] == 'FTEs') {
              totalFTEs += parseFloat($(this).text());
            }
            if (allMorrisTableData.headers[index2] == 'Actual Work') {
              totalAmountWorked += parseFloat($(this).text());
            }
            if (allMorrisTableData.headers[index2] == 'Resource Count') {
              totalResourceCount += parseFloat($(this).text());
            }
          }
        });
    });
  
  var donutJSON = {
    element: 'morris-donut-chart',
    data: [{
      label: "Total FTEs",
      value: totalFTEs.toFixed(2)
    }/*, {
      label: "Total Amount Worked",
      value: totalAmountWorked.toFixed(2)
    }, {
      label: "Total Resource Count",
      value: totalResourceCount.toFixed(2)
    }*/],
    resize: true
  };
  
  $('#morris-donut-chart').empty();
  Morris.Donut(donutJSON);
  //MorrisBarReturnObject.setData(newJSON);
  MorrisDonutReturnObject.setData(donutJSON.data, true);
}


function showDefaultDonutData() {
  console.log('leaving svg donut part');
}


//*************************** cumulative test ********************//


/* ***** Display appropriate div based on selection, altered from https://codepen.io/atelierbram/pen/gusxF and https://stackoverflow.com/questions/26831696/how-to-display-an-image-from-a-dropdown-menu-selection***** */

$(function() {
  $('select').change(function(){
    $('.box').hide();
    $('.' + $(this).val()).show();
  });
});

/* document.getElementById("submit").addEventListener("click", /function() {
  $("select").change(function() {
    $("select option:selected").each(function() {
      if ($(this).attr("value") == "nt") {
        $(".box").hide();
        $(".northern-terminus").show();
      }
      if ($(this).attr("value") == "cs") {
        $(".box").hide();
        $(".congdon-shelter").show();
      }
      if ($(this).attr("value") == "sws") {
        $(".box").hide();
        $(".seth-warner-shelter").show();
      }
      if ($(this).attr("value") == "st") {
        $(".box").hide();
        $(".southern-terminus").show();
      }
    });
  }).change();
}); */



$(".custom-select").each(function() {
  var classes = $(this).attr("class"),
      id      = $(this).attr("id"),
      name    = $(this).attr("name");
  var template =  '<div class="' + classes + '">';
      template += '<span class="custom-select-trigger">' + $(this).attr("placeholder") + '</span>';
      template += '<div class="custom-options">';
      $(this).find("option").each(function() {
        template += '<span class="custom-option ' + $(this).attr("class") + '" data-value="' + $(this).attr("value") + '">' + $(this).html() + '</span>';
      });
  template += '</div></div>';
  
  $(this).wrap('<div class="custom-select-wrapper"></div>');
  $(this).hide();
  $(this).after(template);
});
$(".custom-option:first-of-type").hover(function() {
  $(this).parents(".custom-options").addClass("option-hover");
}, function() {
  $(this).parents(".custom-options").removeClass("option-hover");
});
$(".custom-select-trigger").on("click", function() {
  $('html').one('click',function() {
    $(".custom-select").removeClass("opened");
  });
  $(this).parents(".custom-select").toggleClass("opened");
  event.stopPropagation();
});
$(".custom-option").on("click", function() {
  $(this).parents(".custom-select-wrapper").find("select").val($(this).data("value"));
  $(this).parents(".custom-options").find(".custom-option").removeClass("selection");
  $(this).addClass("selection");
  $(this).parents(".custom-select").removeClass("opened");
  $(this).parents(".custom-select").find(".custom-select-trigger").text($(this).text());
});