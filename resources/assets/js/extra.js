
var ans_array = [];
var ques_id = 0;

/**************** Exam  *********************/

function retest_clear_storage(){
    console.log('clear storage');
    retest_removeCookies();
    localStorage.clear();
}

function retest_load_question(exam, exam_id, arr, q_id){
//    console.log('function');
    ques_id = q_id;
    var question_arr = arr.split(",");
    var index = question_arr.indexOf(q_id);
//    console.log(index);
    
    for (var i = 1; i <= question_arr.length; i++){
        retest_hide_property(i);
    }
    
    retest_hide_buttons(index, question_arr);
    var index = Number(index) + Number(1);
    $.ajax({
        type:'POST',
        url:'../../../../exam/single',
        data:{q_id:q_id},
        success:function(response) {
            var myObj = JSON.parse(response);
            retest_fun_loadquestion(index,myObj,arr);
        }
    });
}

function retest_hide_buttons(index, question_arr){
        /**** Hiding Next button on second last Qustion *****/   
        var next_index = Number(index) + Number(1) ;
        var next_q_id = question_arr[next_index];
//        console.log(next_q_id);
        if (typeof next_q_id == "undefined"){
            document.getElementById("question_next").style.visibility = "hidden";
        }
        else{
            document.getElementById("question_next").style.visibility = "visible";
        }
        /***************** END ******************************/
    
        /**** Hiding Prev on second last Qustion ************/ 

        if (index == '0'){
            document.getElementById("question_previous").style.visibility = "hidden";
        }
        else{
            document.getElementById("question_previous").style.visibility = "visible";
        }
        /***************** END ******************************/
}

function retest_fun_loadquestion(index,myObj,arr){
        var element = document.getElementById(index);
        element.classList.remove("fill");
        var element = document.getElementById(index);
        element.classList.add("active");
    
        $('#questions').empty();
        $('#questions').append("<p> <b>Question</b> <label id='id_label'>"+index+"</label>.</p>");
        $('#questions').append("<div class='qustion'> " +myObj[0]+ "</div>");
        $('#questions').append("<div class='ans' id='ans'> </div>");

        if (localStorage.getItem("arr") === null) {
            for (i = 0; i < myObj[1].length; i++){
                $('#ans').append("  <div class='ans-slide'><label class='radio_container' for='radio-01'><input type='radio' name='sample-radio[]' class='option_radio' id='radio-"+myObj[1][i].id+"' value="+myObj[1][i].id+" >" +myObj[1][i].options+ "</label></div>");
            }
        }
        else{
            var getarray = localStorage.getItem('arr');
            var ans = getarray.split(",");
            index = index-1;
            if(ans[index] == '0' ){
                for (i = 0; i < myObj[1].length; i++){
                    $('#ans').append("  <div class='ans-slide'><label class='radio_container' for='radio-01'><input type='radio' name='sample-radio[]' class='option_radio' id='radio-"+myObj[1][i].id+"' value="+myObj[1][i].id+" >" +myObj[1][i].options+ "</label></div>");
                }
            }
            else{
                for (i = 0; i < myObj[1].length; i++){
                    if(ans[index] ==myObj[1][i].id ) { 
                        $('#ans').append("  <div class='ans-slide'><label class='radio_container' for='radio-01'><input type='radio' name='sample-radio[]' class='option_radio' id='radio-"+myObj[1][i].id+"' value="+myObj[1][i].id+" checked >" +myObj[1][i].options+ "</label></div>");
                    }
                    else{
                        $('#ans').append("  <div class='ans-slide'><label class='radio_container' for='radio-01'><input type='radio' name='sample-radio[]' class='option_radio' id='radio-"+myObj[1][i].id+"' value="+myObj[1][i].id+">" +myObj[1][i].options+ "</label></div>");
                    }
                }
            }
        }
}


function retest_next_question(arr){
        console.log('next_question');
        var index = $("#id_label").text();
        var question_arr = arr.split(",");
    
        retest_hide_property(index);
        retest_hide_buttons(index, question_arr);
    
        var q_id = question_arr[index];
    
        if (ques_id == 0){
            ques_id = question_arr[0];
        }
        var indx = Number(index) - Number(1);
        retest_save_answer(indx,arr);

        $.ajax({
                type:'POST',
                url:'../../../../exam/single',
                data:{q_id:q_id},
                success:function(response) {
                    index = Number(index) + Number(1);
                    var myObj = JSON.parse(response);
                    retest_fun_loadquestion(index,myObj,arr);
        }});
}

function retest_hide_property(index){
        var element = document.getElementById(index);
        element.classList.remove("active");
}

function retest_save_answer(index,arr){
        var Value = $('.option_radio:checked').val();
        var inx = Number(index)+ Number(1);
        if(typeof Value == "undefined"){
            Value = 0;
            var element = document.getElementById(inx);
            element.classList.remove("fill");
        }
        else{
            var element = document.getElementById(inx);
            element.classList.add("fill");
        }
        var question_arr = arr.split(",");
        var ques_length = question_arr.length;

        var ans_length = ans_array.length;

        if(ans_length == 0){
            for(var i = 0; i < ques_length ; i++) {
                ans_array.push('0');
            }
            var getarray = localStorage.getItem('arr');            
            if (localStorage.getItem("arr") === null) {
                ans_array[index] = Value;
                localStorage.setItem("arr", ans_array);
            }
            else{
                ans_array = getarray.split(",");
                ans_array[index] = Value;
                localStorage.setItem("arr", ans_array);
            }
        }
        else{
            var getarray = localStorage.getItem('arr');
            ans_array = getarray.split(",");
            ans_array[index] = Value; 
            localStorage.setItem("arr", ans_array);
        }
    
    var std_id = $('#userid').val();
    var exm_id = $('#examid').val();
    var arrl = localStorage.getItem("arr");
}

function retest_prev_question(arr){
        var index = $("#id_label").text();
        var question_arr = arr.split(",");
        
        retest_hide_property(index);
    
        var indx = Number(index) - Number(1); 
        retest_save_answer(indx,arr);

        var index = Number(index) - Number(2);
        var q_id = question_arr[index];
        retest_hide_buttons(index, question_arr);
        $.ajax({
                type:'POST',
                url:'../../../../exam/single',
                data:{q_id:q_id},
                success:function(response) {
                    index = Number(index) + Number(1);
                    var myObj = JSON.parse(response);
                    retest_fun_loadquestion(index,myObj,arr);
        }});
}

function retest_submit_quiz(arr){
        var index = $("#id_label").text();
        var question_arr = arr.split(",");
        var q_id = question_arr[index];
    
        if (ques_id == 0){
            ques_id = question_arr[0];
        }
        var indx = Number(index) - Number(1);
        retest_save_answer(indx,arr);
    
        var getarray = localStorage.getItem('arr');
        var ans = getarray.split(",");
        var result_array= [];
         $.ajax({
                type:'POST',
                url:'../../../../exam/result',
                data:{question:question_arr},
                success:function(response) {
                    var myObj = JSON.parse(response);
                    for (i = 0; i < myObj.length; i++){
                        if( myObj[i] == ans[i] ){
                            result_array.push("1");
                        }
                        else{
                            if( ans[i] == '0' ){
                                result_array.push("0");
                            }
                            else{
                                result_array.push("-1");
                            }
                        }
                    }
                    
                    var std_id = $('#userid').val();
                    var exm_id = $('#examid').val();
                    $.ajax({
                        type:'POST',
                        url:'../../../../exam/save/result',
                        data:{std_id:std_id, exm_id:exm_id, result_array:result_array},
                        success:function(response) {
                            alert(response);
                            retest_clear_storage();
                            window.location='/student';
                        }
                    });
            }});

}

function retest_refresh_selection(){
    $( ".option_radio" ).prop( "checked", false );
    var arrl = localStorage.getItem("arr");
}

function retest_load_page(exam_time){
    console.log('Load page');
    
    var getarray = localStorage.getItem('arr'); 
    if (localStorage.getItem("arr") === null) {
        console.log('no question anwered');
        retest_removeCookies();
    }
    else{
        ans_array = getarray.split(",");
        for (var i = 0; i < ans_array.length; i++){
            if(ans_array[i] != '0' ){
                    var inx = Number(i)+Number(1);
                    var element = document.getElementById(inx);
                    element.classList.add("fill");
                }
        }
    }
    var element1 = document.getElementById("1");
    element1.classList.add("active");
    retest_countdown(exam_time,true);
}

var timeoutHandle;
function retest_countdown(minutes,stat) {
    var seconds = 60;
    var mins = minutes;
    var flag = 1;
	if(getCookie("minutes")&&getCookie("seconds")&&stat)
	{
		 var seconds = getCookie("seconds");
    	 var mins = getCookie("minutes");
	}

    function tick() {
        
		setCookie("minutes",mins)
		setCookie("seconds",seconds)
        var current_minutes = mins-1
        seconds--; 
        document.getElementById('minutes').innerText = +current_minutes;
        document.getElementById('seconds').innerText = +seconds;
        
		//save the time in cookie
        if( seconds > 0 ) {
            timeoutHandle=setTimeout(tick, 1000);
        } else {
            if(mins > 1){
               // countdown(mins-1);   never reach “00? issue solved:Contributed by Victor Streithorst    
               setTimeout(function () { countdown(parseInt(mins)-1,false); }, 1000);
            }
        } 
        
        if(current_minutes == 4 && seconds == 59  ){
                var modal = document.getElementById("alert_Modal");
                modal.style.display = "block";

                var element = document.getElementById("count_timer");
                element.classList.remove("timer");
                element.classList.add("alert_timerr");
        }
        
        if(current_minutes==0 && seconds ==0 ){
            alert ("Your time is Over. All the best! ");
            console.log('Time over');
            clear_storage();
            window.location='/student';
        }
    }
    tick();
}


function setCookie(cname,cvalue) {
//    var d = new Date();
//    d.setTime(d.getTime() + (exdays*24*60*60*1000));
//    var expires = "expires=" + d.toGMTString();
//    document.cookie = num+" = "+num;
    document.cookie = cname+"="+cvalue+"; ";
    
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function retest_removeCookies() {
  console.log('clear cookie');
  var res = document.cookie;
  var multiple = res.split(";");
 for(var i = 0; i < multiple.length; i++) {
  var key = multiple[i].split("=");
  document.cookie = key[0]+" =; expires = Thu, 01 Jan 1970 00:00:00 UTC";
  }
}

/************************* Pop up ******************************/

var span = document.getElementsByClassName("close")[0];
var modal = document.getElementById("alert_Modal");
span.onclick = function() {
  modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}


/**************************************************************/
function fun23(arr) {
    var index = $("#id_label").text();
    var question_arr = arr.split(",");
    var q_id = question_arr[index];

    if (ques_id == 0){
        ques_id = question_arr[0];
    }
    var indx = Number(index) - Number(1);
    retest_save_answer(indx,arr);

    var getarray = localStorage.getItem('arr');
    var ans = getarray.split(",");
    var result_array= [];
     $.ajax({
            type:'POST',
            url:'../../../../exam/result',
            data:{question:question_arr},
            success:function(response) {
                var myObj = JSON.parse(response);
                
                var attented = 0;
                var wrong = 0;
                var correct = 0;
                var mark = 0;
                var length = myObj.length;
                for (i = 0; i < myObj.length; i++){
                    if( myObj[i] == ans[i] ){
                        result_array.push("1");
                        attented++;
                        correct ++;
                        mark += 4;
                    }
                    else{
                        if( ans[i] == '0' ){
                            result_array.push("0");
                        }
                        else{
                            result_array.push("-1");
                            wrong++;
                            attented++;
                        }
                    }
                }
            
//            var x = result_array.join();
//            console.log('newww');
//            console.log(x);
            var total = mark - wrong;
            document.getElementById("total_ques").innerHTML = length;
            document.getElementById("attented").innerHTML = attented;
            document.getElementById("wrong").innerHTML = wrong;
            document.getElementById("correct").innerHTML = correct;
            document.getElementById("total").innerHTML = total;  
        }});

$('.starRating span').click(function(){
  $(this).siblings().removeClass('active');
  $(this).addClass('active');
  $(this).parent().addClass('starRated');
  
  // Added for Demo
  let rating = $(this).index() + 1;
  $('#currentRating').html( "<small>Rating: <b>" + rating + "</b></small>" );
});


     
    }