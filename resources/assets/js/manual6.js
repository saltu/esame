$(document).ready( function () {
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });
  $.ajax({
      type:'POST',
      url:'append_question',
      success:function(data) {
      $('#question_list').html(data);
      }
  });
});
$(document).ready( function () {
  $('#sample_1').DataTable();
  $('#sample_2').DataTable();
});
$("#notify").click(function(){
    var view = 0;
    $.ajax({
            type:'get',
            url:'notify_view',
            data:{view:view},
            success:function(data)
            {
                var json = $.parseJSON(data);
                var count = json['count'];
                // alert(json['count']);
                if (count == '1') {
                    location.reload();
                    // load(document.'student/notification'+ ' #notification');
                }else{
                    break;
                }
            }
    })
});

$("#centername").autocomplete({
    source: 'centername',
    minLength: 1,
    select: function(event, ui)
    {
      value = ui.item.value;
        // alert(ui.item.value);
        $.ajax({
            type:'get',
            url:'fetchname',
            data:{value:value},
            success:function(data)
            {
                var json = $.parseJSON(data);
                // alert(json['id']);
                $('#center').val(json['id']);
            }
        })
    }
});

$('.exam_results').on('change',function(){
    var value = $(this).val();
    var dependent = $(this).data('dependent');
    if($(this).val() != '') {
    $.ajax({
        type:'POST',
        url:'exam_result/exam',
        data:{value:value},
        success:function(result)
        {
            $('#'+dependent).html(result);
        }
    })
    }
});
$('.exam_name').on('change',function(){
    $result = '<option value="">-- Select Exam Type --</option>'+
    '<option value="Cumulative_Test">Cumulative_Test</option>'+
    '<option value="Unit Test">Unit Test</option>'+
    '<option value="Mock Test">Mock Test</option>'+
    '<option value="Model Test">Model Test</option>';
    $('#test_type').html($result);
});

CKEDITOR.replace('question', {
      // Define the toolbar: https://ckeditor.com/docs/ckeditor4/latest/features/toolbar
      // The full preset from CDN which we used as a base provides more features than we need.
      // Also by default it comes with a 3-line toolbar. Here we put all buttons in a single row.
      toolbar: [{
          name: 'clipboard',
          items: ['PasteFromWord', '-', 'Undo', 'Redo']
        },
        {
          name: 'basicstyles',
          items: ['Bold', 'Italic', 'Underline', 'Strike', 'RemoveFormat', 'Subscript', 'Superscript']
        },
        {
          name: 'links',
          items: ['Link', 'Unlink']
        },
        {
          name: 'paragraph',
          items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote']
        },
        {
          name: 'insert',
          items: ['Image', 'Table']
        },
        {
          name: 'editing',
          items: ['Scayt']
        },
        '/',

        {
          name: 'styles',
          items: ['Format', 'Font', 'FontSize']
        },
        {
          name: 'colors',
          items: ['TextColor', 'BGColor', 'CopyFormatting']
        },
        {
          name: 'align',
          items: ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock']
        },
        {
          name: 'document',
          items: ['Print', 'Source']
        }
      ],

      // Enabling extra plugins, available in the full-all preset: https://ckeditor.com/cke4/presets
      extraPlugins: 'colorbutton,font,justify,print,tableresize,pastefromword,liststyle',
      removeButtons: '',
    });

CKEDITOR.replace('answer', {
      // Define the toolbar: https://ckeditor.com/docs/ckeditor4/latest/features/toolbar
      // The full preset from CDN which we used as a base provides more features than we need.
      // Also by default it comes with a 3-line toolbar. Here we put all buttons in a single row.
      toolbar: [{
          name: 'clipboard',
          items: ['PasteFromWord', '-', 'Undo', 'Redo']
        },
        {
          name: 'basicstyles',
          items: ['Bold', 'Italic', 'Underline', 'Strike', 'RemoveFormat', 'Subscript', 'Superscript']
        },
        {
          name: 'links',
          items: ['Link', 'Unlink']
        },
        {
          name: 'paragraph',
          items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote']
        },
        {
          name: 'insert',
          items: ['Image', 'Table']
        },
        {
          name: 'editing',
          items: ['Scayt']
        },
        '/',

        {
          name: 'styles',
          items: ['Format', 'Font', 'FontSize']
        },
        {
          name: 'colors',
          items: ['TextColor', 'BGColor', 'CopyFormatting']
        },
        {
          name: 'align',
          items: ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock']
        },
        {
          name: 'document',
          items: ['Print', 'Source']
        }
      ],

      // Enabling extra plugins, available in the full-all preset: https://ckeditor.com/cke4/presets
      extraPlugins: 'colorbutton,font,justify,print,tableresize,pastefromword,liststyle',
      removeButtons: '',
    });
  
CKEDITOR.replace('solution', {
      // Define the toolbar: https://ckeditor.com/docs/ckeditor4/latest/features/toolbar
      // The full preset from CDN which we used as a base provides more features than we need.
      // Also by default it comes with a 3-line toolbar. Here we put all buttons in a single row.
      toolbar: [{
          name: 'clipboard',
          items: ['PasteFromWord', '-', 'Undo', 'Redo']
        },
        {
          name: 'basicstyles',
          items: ['Bold', 'Italic', 'Underline', 'Strike', 'RemoveFormat', 'Subscript', 'Superscript']
        },
        {
          name: 'links',
          items: ['Link', 'Unlink']
        },
        {
          name: 'paragraph',
          items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote']
        },
        {
          name: 'insert',
          items: ['Image', 'Table']
        },
        {
          name: 'editing',
          items: ['Scayt']
        },
        '/',

        {
          name: 'styles',
          items: ['Format', 'Font', 'FontSize']
        },
        {
          name: 'colors',
          items: ['TextColor', 'BGColor', 'CopyFormatting']
        },
        {
          name: 'align',
          items: ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock']
        },
        {
          name: 'document',
          items: ['Print', 'Source']
        }
      ],

      // Enabling extra plugins, available in the full-all preset: https://ckeditor.com/cke4/presets
      extraPlugins: 'colorbutton,font,justify,print,tableresize,pastefromword,liststyle',
      removeButtons: '',
    });

CKEDITOR.replace('option1', {
      // Define the toolbar: https://ckeditor.com/docs/ckeditor4/latest/features/toolbar
      // The full preset from CDN which we used as a base provides more features than we need.
      // Also by default it comes with a 3-line toolbar. Here we put all buttons in a single row.
      toolbar: [{
          name: 'clipboard',
          items: ['PasteFromWord', '-', 'Undo', 'Redo']
        },
        {
          name: 'basicstyles',
          items: ['Bold', 'Italic', 'Underline', 'Strike', 'RemoveFormat', 'Subscript', 'Superscript']
        },
        {
          name: 'links',
          items: ['Link', 'Unlink']
        },
        {
          name: 'paragraph',
          items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote']
        },
        {
          name: 'insert',
          items: ['Image', 'Table']
        },
        {
          name: 'editing',
          items: ['Scayt']
        },
        '/',

        {
          name: 'styles',
          items: ['Format', 'Font', 'FontSize']
        },
        {
          name: 'colors',
          items: ['TextColor', 'BGColor', 'CopyFormatting']
        },
        {
          name: 'align',
          items: ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock']
        },
        {
          name: 'document',
          items: ['Print', 'Source']
        }
      ],

      // Enabling extra plugins, available in the full-all preset: https://ckeditor.com/cke4/presets
      extraPlugins: 'colorbutton,font,justify,print,tableresize,pastefromword,liststyle',
      removeButtons: '',
    });

CKEDITOR.replace('option2', {
      // Define the toolbar: https://ckeditor.com/docs/ckeditor4/latest/features/toolbar
      // The full preset from CDN which we used as a base provides more features than we need.
      // Also by default it comes with a 3-line toolbar. Here we put all buttons in a single row.
      toolbar: [{
          name: 'clipboard',
          items: ['PasteFromWord', '-', 'Undo', 'Redo']
        },
        {
          name: 'basicstyles',
          items: ['Bold', 'Italic', 'Underline', 'Strike', 'RemoveFormat', 'Subscript', 'Superscript']
        },
        {
          name: 'links',
          items: ['Link', 'Unlink']
        },
        {
          name: 'paragraph',
          items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote']
        },
        {
          name: 'insert',
          items: ['Image', 'Table']
        },
        {
          name: 'editing',
          items: ['Scayt']
        },
        '/',

        {
          name: 'styles',
          items: ['Format', 'Font', 'FontSize']
        },
        {
          name: 'colors',
          items: ['TextColor', 'BGColor', 'CopyFormatting']
        },
        {
          name: 'align',
          items: ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock']
        },
        {
          name: 'document',
          items: ['Print', 'Source']
        }
      ],

      // Enabling extra plugins, available in the full-all preset: https://ckeditor.com/cke4/presets
      extraPlugins: 'colorbutton,font,justify,print,tableresize,pastefromword,liststyle',
      removeButtons: '',
    });

CKEDITOR.replace('option3', {
      // Define the toolbar: https://ckeditor.com/docs/ckeditor4/latest/features/toolbar
      // The full preset from CDN which we used as a base provides more features than we need.
      // Also by default it comes with a 3-line toolbar. Here we put all buttons in a single row.
      toolbar: [{
          name: 'clipboard',
          items: ['PasteFromWord', '-', 'Undo', 'Redo']
        },
        {
          name: 'basicstyles',
          items: ['Bold', 'Italic', 'Underline', 'Strike', 'RemoveFormat', 'Subscript', 'Superscript']
        },
        {
          name: 'links',
          items: ['Link', 'Unlink']
        },
        {
          name: 'paragraph',
          items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote']
        },
        {
          name: 'insert',
          items: ['Image', 'Table']
        },
        {
          name: 'editing',
          items: ['Scayt']
        },
        '/',

        {
          name: 'styles',
          items: ['Format', 'Font', 'FontSize']
        },
        {
          name: 'colors',
          items: ['TextColor', 'BGColor', 'CopyFormatting']
        },
        {
          name: 'align',
          items: ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock']
        },
        {
          name: 'document',
          items: ['Print', 'Source']
        }
      ],

      // Enabling extra plugins, available in the full-all preset: https://ckeditor.com/cke4/presets
      extraPlugins: 'colorbutton,font,justify,print,tableresize,pastefromword,liststyle',
      removeButtons: '',
    });

$('.deletestock').click( function(e){
  if (confirm('Do You Want To Delete?')) {
    var id = $(this).closest('tr').attr('id');
    $.ajax({
        type : 'POST',
        url : 'question_delete',
        data : {id:id},
        success : function(result) {
          alert(result);
          location.reload();
          $('#msg').html(result);
        }
    });
  }
});