///*************************************Cumulative Test ****************************************************

$(".selected_class").on('change',function(){
    $("#chapters").empty();
    $("#button").empty();
    $(".exam_name").empty();
    $('.exam_name').append("<option value=''>-- Select Exam --</option>");
    var all_clas = $("#class_ids").val();
    var all_exam = $("#exam_ids").val();
    var class_id = $(this).val();
    if($(this).val() != '') {
        $.ajax({
            url:"packages",
            type:'POST',
            data:{class_id:class_id, all_clas:all_clas, all_exam:all_exam},
            success:function(response) {
                var myObj = JSON.parse(response);
                for (j = 0; j < myObj.length; j++){
                    $.ajax({
                        url:"exam_name",
                        data: {exam_id:myObj[j]},
                        type:'POST',
                        success:function(response) {
                            var Obj = JSON.parse(response);
                            $('.exam_name').append("<option value='"+Obj.id+"'>"+Obj.name+"</option>");
                        }});
                }
            }
        });
    }
    
});



$(".package").on('change',function(){
//    console.log("Selected Packagess:");
    $("#chapters").empty();
    $("#button").empty();
    $(".exam_name").empty();
    $('.exam_name').append("<option value=''>-- Select Exam --</option>");
    var package_id = $(this).val();
    if($(this).val() != '') {
        $.ajax({
            url:"packages",
            type:'POST',
            data:{package_id:package_id},
            success:function(response) {
                var myObj = JSON.parse(response);
                for (i = 0; i < myObj.length; i++){
                    var exam_ids = myObj[i].ex_type_ids;
                    var ids = exam_ids.split(",");
                    for (j = 0; j < ids.length; j++){
                        $.ajax({
                            url:"exam_name",
                            data: {exam_id:ids[j]},
                            type:'POST',
                            success:function(response) {
                                var Obj = JSON.parse(response);
                                var exam = Obj.class_name + "--" + Obj.name;
                                $('.exam_name').append("<option value='"+Obj.id+"'>"+exam+"</option>");
                                
//                                for (k = 0; k < Obj.length; k++){
//                                    $('.exam_name').append("<option value='"+Obj[k].id+"'>"+Obj[k].name+"</option>");
//                                }
                            }});
                    }
                }
            }});
    }
});

$(".exam_name").on('change',function(){
    console.log("Selected:");
    var exam_id = $(this).val();
    console.log(exam_id);
    if($(this).val() != '') {
        var i =0 ;
        $("#chapters").empty();
        $("#button").empty();
        $.ajax({
            url:"get_subject",
            type:'POST',
            data:{exam_id:exam_id},
            success:function(response) {
                var myObj = JSON.parse(response);
                for (i = 0; i < myObj.length; i++){
                    var ids =  (myObj[i].subject_ids).split(",");
                    for (j = 0; j < ids.length; j++){
                        if(ids[j] != '') {
                            $.ajax({
                                url:"cumulative",
                                type:'POST',
                                data:{ids:ids[j]},
                                success:function(response) {
                                    var myObj2 = JSON.parse(response);
                                    for (k = 0; k < myObj2.length; k++){
                                        var sbj_name = myObj2[k].name;
                                        var sbj_id = i ;
                                        $('#chapters').append("<div class='col-sm-4 style' id='"+sbj_name+"'><div class='section-title'><h2>"+sbj_name+"</h2><br><br></div></div>");
                                        $.ajax({
                                            url:"chapters",
                                            type:'POST',
                                            data:{subj_id:myObj2[k].id},
                                            success:function(response) {
                                                var myObj3 = JSON.parse(response);
                                                var div_id = "#".concat(sbj_name);
                                                for (l = 0; l < myObj3.length; l++){
                                                    $(div_id).append("<div class='check-slide'><label class='label_container'><input type='checkbox' name='class' id='"+sbj_id+"' class='class checkbox_n con "+sbj_id+"' value='"+myObj3[l].id+"'><span class='label_checkmark'></span>"+myObj3[l].name+sbj_id+"</label></div>");
                                                }
                                            }});
                                        i++;
                                    }
                                }});
                        }
                    }
                }
                $('#button').append("<div class='row'><div class='submit-quiz batton'><a href='javascript:void(0);' class='cumaltive_btn btn'>SUBMIT</a></div></div>");
            }});
    }
});

//Limit Check Box 
$(document).on('change', '.1', function () {
    
    if($('.1:checked').length > 3) {
        this.checked = false;
        alert("Maximum of 3 chapters of each subject")
    }
});
$(document).on('change', '.2', function () {
    
    if($('.2:checked').length > 3) {
        this.checked = false;
        alert("Maximum of 3 chapters of each subject")
    }
});

$(document).on('change', '.3', function () {
    
    if($('.3:checked').length > 3) {
        this.checked = false;
        alert("Maximum of 3 chapters of each subject")
    }
});


//CUMULATIVE EXAM BUTTON ACTION 
$(document).on('click', '.cumaltive_btn', function () {
    var chapters = [];
    
    $.each($(".con:checked"), function(){
        chapters.push($(this).val());
    });
    
    if (chapters.length == 9){
        var qst_count = '90' ;
        var p_id = $('.selected_class').val();
        var exam_id = $('.exam_name').val();
        var test = "Cumulative_Test";
        location.href = "exam/index/"+p_id+"/"+exam_id+"/"+test+"/"+chapters;
    }
    else{
        alert ("Less than 9 Chapters ");
    }
});

///*********************************Unit test **************************************************************
$(".unit_package").on('change',function(){
    $("#chapters").empty();
    $(".unit_exam_name").empty();
    $('.unit_exam_name').append("<option value=''>-- Select Exam --</option>");
    var all_clas = $("#class_ids").val();
    var all_exam = $("#exam_ids").val();
    var class_id = $(this).val();
    if($(this).val() != '') {
        $.ajax({
            url:"packages",
            type:'POST',
            data:{class_id:class_id, all_clas:all_clas, all_exam:all_exam},
            success:function(response) {
                var myObj = JSON.parse(response);
                for (j = 0; j < myObj.length; j++){
                    $.ajax({
                        url:"exam_name",
                        data: {exam_id:myObj[j]},
                        type:'POST',
                        success:function(response) {
                            var Obj = JSON.parse(response);
                            $('.unit_exam_name').append("<option value='"+Obj.id+"'>"+Obj.name+"</option>");
                        }});
                }
            }
        });
    }
    
});


//$(".unit_package").on('change',function(){
////    console.log("Selected Packagess:");
//    var package_id = $(this).val();
//    if($(this).val() != '') {
//        $.ajax({
//            url:"packages",
//            type:'POST',
//            data:{package_id:package_id},
//            success:function(response) {
//                var myObj = JSON.parse(response);
//                for (i = 0; i < myObj.length; i++){
//                    var exam_ids = myObj[i].ex_type_ids;
//                    var ids = exam_ids.split(",");
//                    for (j = 0; j < ids.length; j++){
//                        $.ajax({
//                            url:"exam_name",
//                            type:'POST',
//                            data:{exam_id:ids[j]},
//                            success:function(response) {
//                                var Obj = JSON.parse(response);
//                                var exam = Obj.class_name + "--" + Obj.name;
//                                $('.unit_exam_name').append("<option value='"+Obj.id+"'>"+exam+"</option>");
////                                for (k = 0; k < Obj.length; k++){
////                                    $('.unit_exam_name').append("<option value='"+Obj[k].id+"'>"+Obj[k].name+"</option>");
////                                }
//                            }});
//                    }
//                }
//            }});
//    }
//});

$(".unit_exam_name").on('change',function(){
    console.log("Selected:");
    var exam_id = $(this).val();
    var test = "Unit_Test";
    var p_id = $('.unit_package').val();
    console.log(exam_id);
    if($(this).val() != '') {
        $("#chapters").empty();
        $.ajax({
            url:"get_subject",
            type:'POST',
            data:{exam_id:exam_id},
            success:function(response) {
                var myObj = JSON.parse(response);
                for (i = 0; i < myObj.length; i++){
                    var ids =  (myObj[i].subject_ids).split(",");
                    for (j = 0; j < ids.length; j++){
                        if(ids[j] != '') {
                            $("#chapters").empty();
                            $.ajax({
                                url:"cumulative",
                                type:'POST',
                                data:{ids:ids[j]},
                                success:function(response) {
                                    var myObj2 = JSON.parse(response);
                                    for (k = 0; k < myObj2.length; k++){
                                        var sbj_name = myObj2[k].name;
                                        $('#chapters').append("<div class='col-sm-4 '><div class='section-title'><h2>"+sbj_name+"</h2><br><br></div> <div class='right-slide left'><ul class='catagorie-list' id='"+sbj_name+"' style='text-align: left'></div>");
                                        $.ajax({
                                            url:"chapters",
                                            type:'POST',
                                            data:{subj_id:myObj2[k].id},
                                            success:function(response) {
                                                var myObj3 = JSON.parse(response);
                                                var div_id = "#".concat(sbj_name);
                                                for (l = 0; l < myObj3.length; l++){
                                                    var chapter = myObj3[l].id;
//                                                    console.log()
                                                    $(div_id).append("<div><li><a id='hreid' href='exam/index/"+p_id+"/"+exam_id+"/"+test+"/"+chapter+"'>"+myObj3[l].name+"</a></li></div>");
                                                }
                                            }});
                                    }
                                }});
                        }
                    }
                }
            }});
    }
});


///*********************************mock test **************************************************************


$(".mock_class").on('change',function(){
    $("#chapters").empty();
    $("#button").empty();
    $(".mock_exam").empty();
    $('.mock_exam').append("<option value=''>-- Select Exam --</option>");
    var all_clas = $("#class_ids").val();
    var all_exam = $("#exam_ids").val();
    var all_pckg = $("#pckg_ids").val();
    var class_id = $(this).val();

    var variable = class_id.split(",");

    if($(this).val() != '') {
        $.ajax({
            url:"packages",
            type:'POST',
            data:{class_id:class_id, all_clas:all_clas, all_pckg:all_pckg, all_exam:all_exam},
            success:function(response) {
                var myObj = JSON.parse(response);
                alert (response);
                alert (myObj.length);
                
                
//                for (j = 0; j < myObj.length; j++){
//                    $.ajax({
//                        url:"exam_name",
//                        data: {exam_id:myObj[j]},
//                        type:'POST',
//                        success:function(response) {
//                            var Obj = JSON.parse(response);
//                            alert(response);
//                            $('.mock_exam').append("<option value='"+Obj.id+"'>"+Obj.name+"</option>");
//                        }});
//                }
            }
        });
    }
    
});


$(document).on('click', '.mock_btn', function () {
    var p_id = $('.mock_class').val();
    var exam_id = $('.mock_exam').val();
    
    if(p_id == '' || exam_id == ''){
        alert ('select valid' );
    }
    else{
        var qst_count = '90' ;
        var test = "Mock_Test";
        var chapters = [];
        location.href = "exam/index/"+p_id+"/"+exam_id+"/"+test+"/"+chapters;
    }

//    $.each($(".con:checked"), function(){
//        chapters.push($(this).val());
//    });
//    if(chapters != '') {
//        location.href = "exam/index/"+p_id+"/"+exam_id+"/"+test+"/"+chapters;
////         "exam/index/"+exam_id+"/"+test+"/"++"/"+qst_count;
//    }
});

