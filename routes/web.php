<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
*/

if (version_compare(PHP_VERSION, '7.1.0', '>=')) {
    // Ignores notices and reports all other kinds... and warnings
    error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
    // error_reporting(E_ALL ^ E_WARNING); // Maybe this is enough
}

Route::get('clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    // return what you want
});

Route::auth();  

Route::get('login', function () {    
    return redirect('/');
});
Route::get('register', function () {    
    return redirect('/');
});

Route::get('/', function () {    
    return view('home');
});

Route::get('about-us', function () {
    return view('about-us');
});

Route::get('contact', function () {
    return view('contact');
});
Route::get('admin/about-us', function () {
    return view('admin.about-us');
});

Route::get('admin/contact', function () {
    return view('admin.contact');
});
Route::get('student/about-us', function () {
    return view('student.about-us');
});

Route::get('student/contact', function () {
    return view('student.contact');
});

Route::get('student/result/report', function () {
    return view('student.result.report');
});


Route::group(['middlewareGroups' => ['web']], function () {    
    //Login Routes...
    Route::get('admin/login','Adminauth\AuthController@showLoginForm');
    Route::post('admin/login','Adminauth\AuthController@login');
    Route::get('admin/logout','Adminauth\AuthController@logout');
    // Registration Routes...
    // Route::get('admin/register', 'Adminauth\AuthController@showRegistrationForm');
    // Route::post('admin/register', 'Adminauth\AuthController@register');
    //Login Routes...
	Route::get('student/login','Studentauth\AuthController@showLoginForm');
    Route::post('student/login','Studentauth\AuthController@login');
    Route::get('student/logout','Studentauth\AuthController@logout');
    // Registration Routes...
	Route::get('student/register', 'Studentauth\AuthController@showRegistrationForm');
    Route::post('student/register', 'Studentauth\AuthController@register');
    Route::post('student/a_register', 'Studentauth\AuthController@a_register');	
    //Login Routes...
	Route::get('center/login','Centerauth\AuthController@showLoginForm');
    Route::post('center/login','Centerauth\AuthController@login');
    Route::get('center/logout','Centerauth\AuthController@logout');
    // Registration Routes...
	// Route::get('center/register', 'Centerauth\AuthController@showRegistrationForm');
    // Route::post('center/register', 'Centerauth\AuthController@register');
});

//Admin
Route::group(['middleware' => ['admin'], 'prefix' => 'admin', 'namespace' => 'Admin'], function () {
	Route::get('/','AccountController@index')->name('admin');
    Route::get('password_reset', 'AccountController@reset_password');
    Route::post('password_reset', 'AccountController@reset_p');
    Route::get('notification', 'AccountController@notify');
    Route::get('notification/{class}','AccountController@notify_class') ->name('admin.notify');
    Route::post('notification','AccountController@notify_try')->name('admin.notify.search');
    Route::post('notification/send','AccountController@send')->name('admin.notify.send');
    // center
    Route::get('center_add', 'CenteraddController@create');
    Route::post('center_add', 'CenteraddController@store');
    Route::get('manage_center', 'CenteraddController@manage');
    Route::get('center_view/{id}', 'CenteraddController@view');
    Route::post('center_edit', 'CenteraddController@update');
    Route::get('center_delete/{id}', 'CenteraddController@destroy');
    Route::get('center_block/{id}', 'CenteraddController@block');
    Route::get('center_unblock/{id}', 'CenteraddController@unblock');
    Route::get('center_report', 'CenteraddController@center_report');
    Route::get('center_detail/{id}', 'CenteraddController@center_view');
    // student Management
    Route::get('student_add', 'StudentaddController@create');
    Route::post('student_add', 'StudentaddController@store');
    Route::get('students_manage', 'StudentaddController@manage');
    Route::get('student_view/{id}', 'StudentaddController@view');
    Route::get('student_delete/{id}', 'StudentaddController@delete');
    Route::get('approved_student_delete/{id}', 'StudentaddController@a_s_delete');
    Route::get('student_approve/{id}', 'StudentaddController@approve');
    Route::post('student_edit', 'StudentaddController@update');
    Route::get('approved_student_details/{id}', 'StudentaddController@a_s_view');
    Route::get('student_block/{id}', 'StudentaddController@block');
    Route::get('student_unblock/{id}', 'StudentaddController@unblock');
    Route::get('student_report', 'StudentaddController@s_report');
    Route::get('stud_exam_detail/{id}', 'StudentaddController@exam_view');
    // Exam Management
    Route::get('question_add', 'ExamController@index');
    Route::get('questions', 'ExamController@questions')->name('admin.questions');
    Route::post('question_add', 'ExamController@store');
    Route::get('question_edit/{id}', 'ExamController@question_edit');
    Route::post('question_update', 'ExamController@update');
    Route::post('question_delete', 'ExamController@delete');//delete
    Route::get('manage_exams', 'ExamController@manage');
    Route::post('exam_category', 'ExamController@category');
    Route::post('exam_classes', 'ExamController@add_class');
    Route::post('exam_name', 'ExamController@add_name');
    Route::post('exam_subject', 'ExamController@add_subject');
    Route::post('chapter', 'ExamController@add_chapter');
    Route::get('manage_exam_package', 'ExamController@package');
    Route::post('add_fee', 'ExamController@add_fee');
    Route::post('add_exam_package', 'ExamController@exam_package');
    Route::get('fee_edit/{id}', 'ExamController@edit_exam_fee');
    Route::post('fee_edit', 'ExamController@fee_edit');
    Route::get('exam_edit/{id}/{name_id}', 'ExamController@edit_exam')->name('admin.exam.edit');
    Route::post('exam_edit', 'ExamController@exam_edit');
    Route::get('fee_delete/{id}', 'ExamController@destroy');
    Route::get('package_delete/{id}', 'ExamController@packdestroy');
    Route::get('category/delete/{id}', 'ExamController@exam_delete')->name('admin.cat.delete');
    Route::get('title/delete/{id}', 'ExamController@title_delete')->name('admin.title.delete');
    Route::get('subject/delete/{id}', 'ExamController@subject_delete')->name('admin.subject.delete');
    Route::get('exam_report', 'ExamController@exam');

    // Autocomplete
    Route::get('centername', 'StudentaddController@centername');
    Route::get('fetchname', 'StudentaddController@fetchname');
    Route::get('exam/classes', 'ExamController@classes');
    Route::post('exam/category', 'ExamController@categories');
    Route::post('exam/title', 'ExamController@title');
    Route::post('exam/subject', 'ExamController@subject');
    Route::post('exam/chapter', 'ExamController@chapter');
    //fetch ajax
    Route::post('exam/cate/{c_id}', 'ExamController@exam_category');
    Route::post('exam/types/{cat_id}', 'ExamController@exam_type');
    Route::post('exam/subjects/{title_id}', 'ExamController@exam_subject');
    Route::post('exam/exams/{subject_id}', 'ExamController@exams');

    Route::post('append_question', 'ExamController@append_question');
});

// student
Route::group(['middleware' => ['student'], 'prefix' => 'student', 'namespace' => 'Student'], function () {
	// Account details
    Route::get('/','AccountController@index')->name('student');
    Route::get('/account','AccountController@account')->name('student.account');
    Route::post('/account/update', 'AccountController@update')->name('student.account.update');
    Route::get('enrol','AccountController@enrol')->name('enrol');

    Route::get('/password_reset', 'AccountController@reset_password')->name('student.password_reset');
    Route::get('/password_change', 'AccountController@reset_password_f')->name('student.password_change');
    Route::post('/password_reset', 'AccountController@reset_p')->name('student.password_reset');
    // course details
    Route::get('course/{cls_id}', 'AccountController@classes')->name('student.course');
    Route::get('course/details/{exam_id}', 'AccountController@details')->name('student.course.details');    
    Route::get('course/{cls_id}/{cat_id}', 'AccountController@exam_title')->name('student.course.categories');    
    Route::get('course/details/{cls_id}/{cat_id}/{exam_id}/{subj_id}', 'AccountController@details')->name('student.course.details');
   
    
    // exam details
    
    // Route::get('exam/{cls_id}/{cat_id}/{exam_id}/{subj_id}/{exam}', 'Exam\ExamController@index')->name('student.exam');
    Route::get('exam/start/{p_id}/{test}/{chapt_ids}/{new_var}/{exam_time}/{question_id}', 'Exam\ExamController@exam')->name('student.exam.start');
    Route::post('exam/single', 'Exam\ExamController@exam_question')->name('student.exam.single');
    Route::post('exam/save/answer', 'Exam\ExamController@save_answer')->name('student.exam.save.answer'); 
    Route::post('exam/save/result', 'Exam\ExamController@save_result')->name('student.exam.save.result'); 
    Route::post('exam/result', 'Exam\ExamController@check_answer')->name('student.exam.result');
    Route::post('exam/result/final', 'Exam\ExamController@final_result')->name('student.exam.result.final');
 
    Route::get('/result','AccountController@result')->name('result');
    Route::get('/report','AccountController@report')->name('report');


    Route::get('/payment/{pack_id}','AccountController@payment')->name('student.payment.studentpayment');
    Route::post('/payment/paid','AccountController@paymentpaid')->name('student.payment.paid');

    // Cumulative Test
    Route::get('/cumulative','AccountController@cumulative')->name('student.exams.cumulative');
    Route::post('/packages', 'AccountController@load_Packages')->name('student.exams.cumulative.packages');
    Route::post('/exam_name', 'AccountController@load_Exams')->name('student.exams.cumulative.exam_name');
    Route::post('/get_subject', 'AccountController@get_subjectid')->name('student.exams.cumulative.get_subject');
    Route::post('/cumulative', 'AccountController@load_subjects')->name('student.exams.cumulative.subject');
    Route::post('/chapters', 'AccountController@load_chapters')->name('student.exams.cumulative.chapter');
    
    Route::get('/exam/index/{p_id}/{exam_id}/{test}/{chapters}','AccountController@exam_index')->name('student.exams.index');
    
    //Unit Test
    Route::get('/unit','AccountController@unit')->name('student.exams.unit');
    
    //Mock Test
    Route::get('/mock','AccountController@mock')->name('student.exams.mock');
    
    //Model Test
    Route::get('/model','AccountController@model')->name('student.exams.model');
    
    
    Route::get('/report/retest/{question_ids}/{question_options}/{exam_time}','AccountController@retest')->name('retest');
    
    
//    student/result/report/retest/{$question_id}/{$question_options}/{$exam_time}
    //notify
    Route::get('notification','AccountController@notify')-> name('notify.students');
    Route::get('notify_view','AccountController@ntf_view');
    Route::get('exam1', 'Exam\ExamController@exam1');

});

// center
Route::group(['middleware' => ['center'], 'prefix' => 'center', 'namespace' => 'Center'], function () {
	Route::get('/','AccountController@index')->name('center');
    Route::get('/password_reset', 'AccountController@reset_password')->name('center.password_reset');
    Route::post('/password_reset', 'AccountController@reset_p')->name('center.password_reset');
    // Exam Management
    Route::get('question_add', 'ExamController@index');
    Route::post('question_add', 'ExamController@store');
    Route::get('manage_exams', 'ExamController@manager');

    Route::get('course/{cls_id}', 'AccountController@classes');
    Route::get('course/{cls_id}/{cat_id}', 'AccountController@exam_title');
    Route::get('course/details/{cls_id}/{cat_id}/{exam_id}/{subj_id}','AccountController@details');
    // Report
    Route::get('report', 'ReportController@general');
    // Route::get('student_report/{cls_id}', 'ReportController@students');
    Route::get('student_report', 'ReportController@students');
    Route::get('exam_report', 'ReportController@exam');
    Route::get('question', 'ReportController@question');

    Route::post('question/view','ReportController@q_answer');
    Route::post('student_add/view','ReportController@student_view');
    Route::post('exam/view','ReportController@exam_view');
    Route::post('exam_result_submit', 'ReportController@exam_result_submit');


    
    Route::post('question/view/update/{id}','ReportController@update') -> name('update.table');
    // Route::get('profile/{center_id}','AccountController@profile');
    // Route::post('profile/{center_id}','AccountController@update');

    Route::get('/notification_center','AccountController@notify_center')-> name('notify.center');
    
    Route::post('/notification','AccountController@notify_try')->name('notify.search');
    Route::post('/notification/send','AccountController@send')->name('notify.send');

    Route::get('notification','AccountController@notify')-> name('notify.student');
    Route::get('/notification/XI','AccountController@notify_XI') ->name('notify.XI');
    Route::get('/notification/XII','AccountController@notify_XII') ->name('notify.XII');
    Route::get('notify_view','AccountController@ntf_view');

    Route::get('/manage_student','AccountController@manage')->name('manage.student');

    // Route::get('/manage/edit','AccountController@manage_edit')->name('manage.edit');

    Route::post('student_edit/{id}','ReportController@edit') -> name('manage.edit');
    Route::get('manage/delete/{id}','ReportController@delete') -> name('manage.record');

    Route::get('approved_student_details/{id}', 'AccountController@edit');
    Route::get('approved_student_delete/{id}', 'ReportController@delete');

    Route::get('unapproved_delete/{id}','ReportController@delete_unapproved');
    Route::get('unapproved_details/{id}', 'AccountController@edit_unapproved');
    Route::post('approve/{id}','AccountController@approve');

    // Autocomplete
    // Route::get('center/classes', 'ExamController@classes');
    Route::post('center/category', 'ReportController@category');
    Route::post('center/exam_type','ReportController@etype');
    Route::post('center/esubject','ReportController@esubject');
    Route::post('center/exam_submit','ReportController@esubmit');


    // Route::post('center/subject', 'ReportController@subject');

    Route::post('center/stud_detail','ReportController@cate');
    Route::post('center/stud_d','ReportController@table');

    Route::post('center/ssubmit','ReportController@sSubmit');

    // Route::post('center/categories', 'ReportController@category');
    Route::post('exam_report/detail','ReportController@stud_view');
    Route::get('stud_exam_detail/{id}', 'ReportController@exam_view');

    //fetch ajax
    Route::post('exam_result/exam', 'ExamController@exam_names');

});

// Courses
Route::group(['prefix' => 'course', 'namespace' => 'Course'], function() {
//    Route::get('/', 'CoursesController@index')->name('course');
    Route::get('{cls_id}', 'CoursesController@classes')->name('course');
    Route::get('{cls_id}/{cat_id}', 'CoursesController@exam_title')->name('course.categories');
    Route::get('details/{cls_id}/{cat_id}/{exam_id}/{subj_id}', 'CoursesController@details')->name('course.details');
    
//    Route::get('exam/{cls_id}/{cat_id}/{exam_id}/{subj_id}/{exam}', 'CoursesController@exam')->name('course.exams');
//    Route::get('/details/{cls_id}/{cat_id}/{exam_id}/{subj_id}', 'CoursesController@exam_title')->name('course.details.title');
    
});

