<?php

namespace App\Http\Controllers\Centerauth;

use App\Models\Center;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Auth;
use HtmlFacade;
use Illuminate\Http\Request;
class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/center';
	protected $guard= 'center';
	

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware($this->guestMiddleware(), ['except' => 'logout']);
    }

	//Controller function for show login form
	public function showLoginForm()
   {
      // if (view()->exists('auth.authenticate')) {
      //   return view('auth.authenticate');
      // }
      if (Auth::guard('center')->check()){
        return redirect('center');
      }
      return view('center.auth.login');
   }
	   
  public function resetPassword()
	{
		return view('center.auth.passwords.email');
	}
	
	public function logout()
	{
		Auth::guard('center')->logout();
		return redirect('/center/login');
	}
	   //Controller function for show registration form
    public function showRegistrationForm()
		{
			return view('center.auth.register');
		}  
    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:centers',
            'address' => 'required|max:255',
            'phone' => 'required|min:10|max:12|unique:centers',
            'password' => 'required|min:5|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return Center::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'address' => $data['address'],
            'phone' => $data['phone'],
            'password' => bcrypt($data['password']),
        ]);
    }
	public function login(Request $request)
   {
      // echo "<pre>";print_r($_POST);die;
       $this->validateLogin($request);

       // If the class is using the ThrottlesLogins trait, we can automatically throttle
       // the login attempts for this application. We'll key this by the username and
       // the IP address of the client making these requests into this application.
       $throttles = $this->isUsingThrottlesLoginsTrait();

       if ($throttles && $lockedOut = $this->hasTooManyLoginAttempts($request)) {
           $this->fireLockoutEvent($request);

           return $this->sendLockoutResponse($request);
       }

       $credentials = $this->getCredentials($request);
	  // echo"<pre>";print_r($credentials);die;
        if (Auth::guard($this->getGuard())->attempt($credentials, $request->has('remember'))) {
      	 //  $loginhistory = new LoginHistoryModel();
      		// $loginhistory->teach_id =Auth::guard('center')->user()->id;
      		// $loginhistory->teach_uname =Auth::guard('center')->user()->email;
      		// $loginhistory->school_id = Auth::guard('center')->user()->school_id;
      		// $loginhistory->login     = date('Y-m-d H:i:s');
      		// $loginhistory->save();
           return $this->handleUserWasAuthenticated($request, $throttles);
        }

       // If the login attempt was unsuccessful we will increment the number of attempts
       // to login and redirect the user back to the login form. Of course, when this
       // user surpasses their maximum number of attempts they will get locked out.
       if ($throttles && ! $lockedOut) {
           $this->incrementLoginAttempts($request);
       }

       return $this->sendFailedLoginResponse($request);
   }
}
