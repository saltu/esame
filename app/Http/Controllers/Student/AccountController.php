<?php
namespace App\Http\Controllers\Student;
use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Auth;
use App\Models\Student;
use App\Models\Admin;
use App\Models\Student_payment;
use App\Models\Notification;
use Session;
use Carbon\Carbon;
use Hash;
use DB;
//use App\Models\UserPermission;
//use Illuminate\Html\HtmlServiceProvider;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;

class AccountController extends Controller
{
     
	public function index()
    {			
		// ,compact('circulars','logins','circulasars'))
        return view('student.home');
    }

    public function account()
    {   
        $student = DB::table('students')
                        ->select('*')
                        ->where('id',Auth::guard('student')->user()->id)
                        ->get();    
        return view('student.account.account',compact('student'));
    }

    public function update(Request $request)
    {
        DB::update("UPDATE students SET name = '$request->name', email = '$request->email', school = '$request->school', phone = '$request->phone', center = '$request->center', dob = '$request->dob', yoa = '$request->yoa', place = '$request->place', class = '$request->class', parent_name = '$request->parent_name', rws = '$request->rws', parent_email = '$request->parent_email', parent_phone = '$request->parent_phone', address = '$request->address', status = '0', added_by = '$request->added_by' WHERE id = ? ",[$request->id]);
        return redirect('/student')->with('message', 'Updated Student Details Successfully');
    }

    public function enrol()
    {           
        $packages1 = DB::table('exam_fees')
                            ->select('*')
                            ->get();
        return view('student.account.enrol', compact('packages1'));
    }
    public function reset_password()
    {
        return view('student.auth.passwords.reset');
    }
    public function reset_password_f()
    {
        return redirect('student/password_reset')->with('dmessage', 'Change your default password given with email');
    }
    public function reset_p( Request $request )
    {
        $this->validate(request(),[
            'c_password' => 'required|min:5',
            'password' => 'required|min:5|confirmed',
        ]);
        $admin = DB::table('students')
                        ->select('password')
                        ->where('id',$request->user_id)
                        ->get();
        if (Hash::check($request->c_password, $admin[0]->password)) {
        DB::table('students')
            ->where('id',$request->user_id)
            ->update(['password' => Hash::make($request->password),
                    'pass_change_status' => 1]);
        return redirect('/student')->with('message', 'Password Changed Sucessfully');
        }else{
            return Redirect::back()->withInput(Input::all())->with('dmessage', 'Current password is wrong');
        }
    }
    
    /*
	* Admin account view
	*/
    public function showAccount( Request $request )
    { 
		$adminUser = Auth::guard('student')->user();
		return view('student.account.account',['model'=>$adminUser]);
    }
    

   /*
    * Current user account edit
    */
    public function showAccountpost( Request $request )
    {
		die(route('paccount'));
		$adminUser = Auth::guard('student')->user();
		$request->merge( array_map('trim', $request->all()) );
		$rules = array(
            'email'      => 'required|email',
            'princ_name'  => 'required|max:50',
        );
        
        $validator = Validator::make($request->all(), $rules);       
        //process the login
        if ($validator->fails()) 
        {
            return redirect()->route('paccount')->withErrors($validator)->withInput();
        } 
        else 
        {    
			//checking username address exists
			$condition = 'princ_id != ? and email = ?';
			$duplication_check = Student::whereRaw($condition, array( $adminUser->princ_id, $request->input('email') ))->get();
			
			if(count($duplication_check) > 0 )
			{
			    $validator->getMessageBag()->add('email','This Email already exists');
                return redirect()->route('paccount')->withErrors($validator)->withInput();
            }
            else
            {
				//echo $var=$request->input('email');die;
			   //updating the user data
               $adminUser->email = $request->input('email'); 
               $adminUser->princ_name = $request->input('princ_name');
               
               $adminUser->save();
               Session::flash('success', 'Settings updated successfully');
               return redirect()->route('paccount');	
			}
		}
    }
    
    
    
   /*
    * Password reset
    */
    public function showAccountpass( Request $request )
    {
		
		$adminUser = Auth::guard('student')->user();
		$request->merge(array_map('trim', $request->all()));
		$rules = array(
            'old_password'  => 'required',
            'new_password'  => 'required|min:5',
            're_password'   => 'required|same:new_password',
        );
        $validator = Validator::make($request->all(), $rules);       
        //process the login
        if ($validator->fails()) 
        {	
            return redirect()->route('paccount')->withErrors($validator)->withInput();
        } 
        else 
        {    
            //checking old password matches with new
            if( Hash::check( $request->input('old_password') , $adminUser->password ) )
            {
				$adminUser->password   = Hash::make($request->input('new_password')); 
				$adminUser->save();
				Session::flash('success', 'Password updated successfully');
				return redirect()->route('paccount');
			}
			else
			{
				$validator->getMessageBag()->add('old_password', 'Old password doesn t match with system data');
			    return redirect()->route('paccount')->withErrors($validator)->withInput();
			}	
		}
		
    }

//    public function details($exam_id){
//        return view('student.course.details', compact('exam_id'));
//    }
    
    
    public function catagories($cat_id){
            // echo $billno;die;

        $catagories = DB::table('catagories')
                            ->select('*')
                            ->get();
        return view('student.course.index', compact('catagories','cat_id'));
    }
    
    public function details($cls_id,$cat_id,$exam_id, $subj_id){
    	return view('student.course.details', compact('cls_id','cat_id','exam_id' ,'subj_id'));
    }
    
    
    
	public function classes($cls_id){
			// echo $billno;die;
		$classes = DB::table('classes')
							->select('*')
							->get();
		return view('student.course.index', compact('classes','cls_id'));
	}
    
    public function exam_title($cls_id,$cat_id){
        $classes = DB::table('classes')
							->select('*')
							->get();
    	return view('student.course.index', compact('classes' , 'cls_id' ,'cat_id'));
    }
    
    public function result()
    {
        // ,compact('circulars','logins','circulasars'))
        return view('student.result.result');
    }


    public function report()
    {           
        // ,compact('circulars','logins','circulasars'))
        return view('student.result.report');
    }
    
    public function retest($question_ids, $question_options, $exam_time)
    {
        $var = explode(",",$question_ids);
        $question_options =  explode(",",$question_options);
        return view('student.result.retest',compact('question_options', 'exam_time', 'var'));
    }
    
    public function notify()
    {           
        // ,compact('circulars','logins','circulasars'))
        return view('student.notification.notify');
    }

    public function ntf_view(Request $request){
        
        $json=array();

        $status = Notification::where('student_id',Auth::guard('student')->user()->id)
                            ->where('view',$request->view)
                            ->get();
        foreach ($status as $key => $value) {
            DB::table('notifications')
                ->where('id',$value->id)
                ->update(['view' => 1]);
        }
        if (count($status) > 0) {
            $json['count'] = '1';
        }else{
            $json['count'] = '0';
        }
        
        echo json_encode($json);                        
    }
    
    public function payment($pack_id)
    {
        return view('student.payment.studentpayment',compact('pack_id'));
    }
    public function paymentpaid( Request $request )
    {
        $payment = new Student_payment();
        $payment->amount = $request->amount;
        $payment->student_id = Auth::guard('student')->user()->id;
        $payment->package_id = $request->package_id;
        $payment->date = date('Y-m-d');
        $payment->status = '0';
        $payment->save();
        return redirect('/student')->with('message', 'Package payment paid Sucessfully');
    }

    public function cumulative()
    {
        // ,compact('circulars','logins','circulasars'))
        return view('student.exams.cumulative_list');
    }
    public function unit()
    {
        // ,compact('circulars','logins','circulasars'))
        return view('student.exams.unit_test');
    }
    
    public function mock()
    {
        return view('student.exams.mock_test');
    }
    
    public function model()
    {
        return view('student.exams.model_test');
    }
    
    public function load_Packages(Request $request)
    {   
        $class_id = $request->class_id;
        $variable = preg_split ("/\,/", $class_id);
        
        if(sizeof($variable) > 1){
            $all_pckg = preg_split ("/\,/", $request->all_pckg);
            $exam_array = array();
            foreach ($all_pckg as $pckg){
                $packages = DB::table('exam_type_packages')
                                ->select('*')
                                ->where('id',$pckg)
                                ->get();
                $data = array();
                $data['package_id'] = $packages[0]->id;
                $data['class_ids'] = $packages[0]->class_ids;
                $data['title'] = $packages[0]->title;
                array_push($exam_array,$data);
            }
            $result = $exam_array;
        }
        else{
            $all_clas = preg_split ("/\,/",  $request->all_clas);
            $all_exam = preg_split ("/\,/",  $request->all_exam);

            $examid_array = array();
            $cat_ids = DB::table('categories')
                                ->where('class_id',$request->class_id)
                                ->pluck('id');

            foreach($cat_ids as $id){
                $exam_ids = DB::table('exam_names')
                                ->where('category_id',$id)
                                ->pluck('id');

                foreach($exam_ids as $val){
                     if(!in_array($val, $examid_array)){
                         array_push($examid_array,$val);
                     }
                }
            }
            $result=array_intersect($all_exam,$examid_array);
            $result = implode("",$result);
        }
        echo json_encode($result);
    }
    
    public function load_Exams(Request $request)
    {
//get exam name and details 
        
        $exam_name = DB::table('exam_names')
							->select('*')
                            ->where('id',$request->exam_id)
							->get();
//get Class
        $cat_id = DB::table('exam_names')
							->select('category_id')
                            ->where('id',$request->exam_id)
							->get();
        
        $class_id = DB::table('categories')
                            ->select('class_id')
                            ->where('id',$cat_id[0]->category_id)
                            ->get();
        
        $class_name = DB::table('classes')
                            ->select('class')
                            ->where('id',$class_id[0]->class_id)
                            ->get();
        
//append class details to exam name
        
        $data = array(); 
        $data['id'] = $exam_name[0]->id;
        $data['name'] = $exam_name[0]->name;
        $data['class_id'] = $class_id[0]->class_id;
        $data['class_name'] = $class_name[0]->class;
                            
        echo json_encode($data);
    }
    
    public function get_subjectid(Request $request)
    {
//        echo($exam_id);
        $exam_details = DB::table('exam_names')
							->select('*')
                            ->where('id',$request->exam_id)
							->get();
        echo json_encode($exam_details);
    }
    
    public function load_subjects(Request $request)
    {
        $subjects = DB::table('subjects')
							->select('*')
                            ->where('id',$request->ids)
							->get();
        echo json_encode($subjects);
    }

    public function load_chapters(Request $request)
    {
        $chapters = DB::table('chapters')
							->select('*')
                            ->where('subject_id',$request->subj_id)
							->get();
        echo json_encode($chapters);
	}
    
    
    public function exam_index($p_id,$exam_id,$test,$chapt_ids)
    {
        return view('exam.index',compact('p_id','exam_id','test','chapt_ids'));
    }
}