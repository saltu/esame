<?php

namespace App\Http\Controllers\Student\Exam;
use App\Models\Exam_result;
use Illuminate\Http\Request;
use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;


class ExamController extends Controller
{
    public function index($cls_id, $cat_id, $exam_id, $subj_id, $exam){
//        echo $cls_id.$cat_id.$exam_id.$subj_id.$exam ;die;
        
    	return view('exam.index', compact('cls_id','cat_id','exam_id' ,'subj_id', 'exam'));
    }

    public function exam($p_id, $test, $chapt_ids, $new_var, $exam_time, $question_id){

        $new_array = explode('-',$new_var,2);
        $exam = $new_array[0];
        $std_id = $new_array[1];
        $var = unserialize(urldecode($question_id));
        $new_array = implode(',',$var);
        $datee = date('Y-m-d');

        $results = Exam_result::select('*')
                            ->where('date', $datee)
                            ->where('stud_id', $std_id)
                            ->where('q_id',$new_array)
                            ->orderBy('id', 'desc')->take(1)
                            ->get();
        if (count($results) > 0) {
            $id = $results[0]->id;
            return view('exam.exam',compact('exam', 'exam_time', 'var' , 'id'));
        }else{
        
            $results = new Exam_result();
            $results->exam_id = $exam;
            $results->test_type = $test;
            $results->chapter_id = $chapt_ids;
            $results->date = $datee;
            $results->stud_id = $std_id;
            $results->q_id = $new_array;
            $results->op_id = '0';
            $results->result = '0';
            $results->p_id = $p_id;
            $results->save();
            
            $id = $results->id;
    		if($var <= 0){
    			return redirect()->route('student.exam.start',[$p_id, $test, $chapt_ids, $exam, $exam_time, $var]);
            }
        	return view('exam.exam',compact('exam', 'exam_time', 'var' , 'id'));
        }
    }
      
    public function exam_question(Request $request){
        $result = array();
        $question = DB::table('questions')
                        ->where('id', $request->q_id)
                        ->first()
                        ->question;
        $options = DB::table('question_options')
                        ->where('question_id', $request->q_id)
                        ->inRandomOrder()
                        ->get();
        
        array_push($result,$question);
        array_push($result,$options);
        
        echo json_encode($result);
    }
    
    public function save_answer(Request $request){
        DB::table('exam_results')
            ->where('id', $request->exm_id)
            ->update(['op_id' => $request->ans]);
        echo json_encode('sucess');
    }
    
    public function save_result(Request $request){
        DB::table('exam_results')
            ->where('id', $request->exm_id)
            ->update(['result' => implode(",",$request->result_array)]);
        echo json_encode('sucess');
    }
    
    public function check_answer(Request $request){
        $question_arr = $request->question;
        $answe_array = array();
        foreach ($question_arr as $q_id) {
            $crct_answer = DB::table('questions')
                        ->where('id', $q_id)
                        ->first()
                        ->solution;
            array_push($answe_array,$crct_answer);
        }
        echo json_encode($answe_array);
    }

    public function final_result(Request $request){
        // echo $exm_id . $length . $attented . $correct;
        DB::update("UPDATE exam_results SET t_attend = '$request->attented', t_true = '$request->correct', t_question = '$request->length' WHERE id = ? ",[$request->exm_id]);
    }

    public function exam1(){

            return view('exam.exam1');
        }
}