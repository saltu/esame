<?php
/**********************************************
 * LoginController - backend user account management
 * @author  : helena.pg@shtishtionline.com *
 * @date    : 10/03/2017                      *
 **********************************************/
namespace App\Http\Controllers\Center;
use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Auth;
use DB;
use App\Models\Center;
use App\Models\Notification;
use App\Homeworks;
use App\Circular;
use App\CircularTo;
use Session;
use Hash;
//use App\Models\UserPermission;
//use Illuminate\Html\HtmlServiceProvider;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;

class AccountController extends Controller
{
     
	public function index()
    {
		// $id = \DB::table('centers')
		// 				->select('school_status')
		// 				->where('school_id',Auth::guard('center')->user()->school_id)
		// 				->get();
		// echo"<pre>";print_r($schoolid[0]->school_status);die;
		// if($schoolid[0]->school_status == '0' ){
			// Session::flash('danger', 'Your school is not active. Please contact Providers');
			// echo "<script>alert('Your school is not active. Please contact Providers');</script>";
            // return redirect('/center/logout');
		// }
		
		// $toDate=date('Y-m-d',strtotime("+1 week"));
  //       $fromDate=date("Y-m-d", strtotime("-1 week"));
		
		// $jd = cal_to_jd(CAL_GREGORIAN,date("m"),date("d"),date("Y"));
		
		// $result		=	\DB::table('timetables')
		// 									->select('timetables.*')
		// 									->where('timetables.day',jddayofweek($jd,1))
		// 									->where('timetables.school_id','=',Auth::guard('center')->user()->school_id)
		// 									->get();
											
		
		// $center_id = Auth::guard('center')->user()->id;
		// $resA = array();
		// foreach($result as $key => $value){
		// 	$timetable = unserialize($value->timetables);
		// 	foreach($timetable as $key1 => $value1){
		// 		if($center_id==$value1['center_id'])
		// 		$resA[] = array('details'=>$value1,'standard'=>$value->std_id,'division'=>$value->div_id);
		// 	}
		// }
		//echo "<pre>";print_r(Auth::guard('center')->user()->school_id);die;
		// $homeworks	=	Homeworks::select('homeworks.*','subjects1.sub_name','standards.std_name','divisions.div_name')
		// 								->join('subjects1','subjects1.id','=','homeworks.subject_id')
		// 								->join('standards','standards.id','=','homeworks.std_id')
		// 								->join('divisions','divisions.id','=','homeworks.div_id')
		// 								->where('homeworks.done_by',Auth::guard('center')->user()->id)										
		// 								->whereBetween('homeworks.created_at', [$fromDate, $toDate])
		// 								->orderBy('homeworks.created_at', 'DESC')
		// 								->get();
						
		//     $leave_requests  =   \DB::table('leave_request')
		// 									->select('leave_request.*')											
		// 									->where('leave_request.school_id','=',Auth::guard('center')->user()->school_id)											
		// 									->where('leave_request.std_id','=',Auth::guard('center')->user()->std_id)
		// 									->where('leave_request.div_id','=',Auth::guard('center')->user()->div_id)
		// 									->whereBetween('leave_request.created_at', [$fromDate, $toDate])
		// 									->orderBy('leave_request.created_at', 'DESC')
		// 									->paginate(5);
						
		//     $circular = \DB::table('circular_to')
		// 						->select('circulars.id','circulars.subject','circulars.content','circulars.file','circular_to.created_at as tcre')
		// 						->join('circulars','circulars.id','=','circular_to.circ_id')
		// 						->join('centers', 'centers.school_id', '=', 'circular_to.school_id')
		// 						->where('centers.id',Auth::guard('center')->user()->id)
		// 						->where('circular_to.to_centers','=',1)
		// 						->orderBy('circular_to.created_at', 'DESC')
		// 						->whereBetween('circular_to.created_at', [$fromDate, $toDate])
		// 						->get();	
											
		
		// $inbox = \DB::table('comments')  
		// 					->select('*','comments.id as cid','comments.created_at as cre','students.stud_fname as f_name','students.stud_surname as l_name')
		// 					->join('centers', 'comments.to_who', '=', 'centers.id')
		// 					->leftjoin('reply','reply.comment_id','=', 'comments.id')
		// 					->join('students', 'comments.from_who', '=', 'students.id')
		// 					->where('centers.id',Auth::guard('center')->user()->id)
		// 					->where('comments.status',1)
		// 					->orderBy('comments.id','DESC')
		// 					->whereBetween('comments.created_at', [$fromDate, $toDate])
		// 					->paginate(5);
 	// 		//echo"<pre>";print_r($inbox);die;
		// 	$examss = 	\DB::table('exams')
		// 					->select('*','exams.id as exaid')
		// 					->join('centers', 'exams.done_by', '=', 'centers.id')
		// 					->where('exams.done_by',Auth::guard('center')->user()->id)		
		// 					->orderBy('exams.created_at','DESC')
		// 					->whereBetween('exams.created_at', [$fromDate, $toDate])
		// 					->get();
        // return view('center.home',compact('circular','resA','leave_requests','homeworks','examss','inbox'));
        return view('center.home');
    }
    public function reset_password()
    {
        return view('center.auth.passwords.reset');
    }
    public function reset_p( Request $request )
    {
        $this->validate(request(),[
            'c_password' => 'required|min:5',
            'password' => 'required|min:5|confirmed',
        ]);
        $admin = DB::table('centers')
                        ->select('password')
                        ->where('id',$request->user_id)
                        ->get();
        if (Hash::check($request->c_password, $admin[0]->password)) {
        DB::table('centers')
            ->where('id',$request->user_id)
            ->update(['password' => Hash::make($request->password)]);
        return redirect('/center')->with('message', 'Password Changed Sucessfully');
        }else{
            return Redirect::back()->withInput(Input::all())->with('dmessage', 'Current password is wrong');
        }
    }
	 /*
	* Admin account view
	*/
	public function ChangeStatus(Request $request)
	{
	//echo $request->status;die;
		
			
				 if($request->status==2)
				{
					\DB::table('leave_request')
						->where('id', $request->id)
						->where('student_id',$request->stud_id)
						->update(['status' => $request->status]);
				}
				else if($request->status==3)
				{
					\DB::table('leave_request')
						->where('id', $request->id)
						->where('student_id',$request->stud_id)
						->update(['status' => $request->status]);
				}
			
			return $request->status;
}
    public function showAccount( Request $request )
    { 
		$adminUser = Auth::guard('center')->user();
		return view('center.account.account',['model'=>$adminUser]);
    }
    

   /*
    * Current user account edit
    */
    public function showAccountpost( Request $request )
    {
		
		$adminUser = Auth::guard('center')->user();
		$request->merge( array_map('trim', $request->all()) );
		
		$rules = array(
            'email'      => 'required|email',
            'name'  => 'required|max:10',
        );
        
        $validator = Validator::make($request->all(), $rules);       
        //process the login
        if ($validator->fails()) 
        {
            return redirect()->route('account')->withErrors($validator)->withInput();
        } 
        else 
        {    
			//checking username address exists
			$condition = 'id != ? and email = ?';
			$duplication_check = Center::whereRaw($condition, array( $adminUser->mid , $request->input('email') ))->get();
			
			if(count($duplication_check) > 0 )
			{
			    $validator->getMessageBag()->add('email','This Email already exists');
                return redirect()->route('account')->withErrors($validator)->withInput();
            }
            else
            {
			   //updating the user data
               $adminUser->email = $request->input('email'); 
               $adminUser->name = $request->input('name');
               
               $adminUser->save();
               Session::flash('success', 'Account details updated successfully');
               return redirect()->route('account');	
			}
		}
    }
    
    
    
   /*
    * Password reset
    */
    public function showAccountpass( Request $request )
    {
		
		$adminUser = Auth::guard('center')->user();
		$request->merge(array_map('trim', $request->all()));
		$rules = array(
            'new_password'  => 'required|min:5',
            're_password'   => 'required|same:new_password',
        );
        $validator = Validator::make($request->all(), $rules);       
        //process the login
        if ($validator->fails()) 
        {	
            return redirect()->route('account')->withErrors($validator)->withInput();
        } 
        else 
        {    
           
				$adminUser->password   = Hash::make($request->input('new_password')); 
				$adminUser->save();
				Session::flash('success', 'Password updated successfully');
				return redirect()->route('account');
		}
		
    }
	
	/* Function for work schedule */
	
	public function schedule()
	{
		$result		=	\DB::table('timetables')
										->select('timetables.*')
										->where('timetables.school_id','=',Auth::guard('center')->user()->school_id)
										->get();
		
		$center_id = Auth::guard('center')->user()->id;
		$resA = array();
		foreach($result as $key => $value){
			$timetable = unserialize($value->timetables);
			foreach($timetable as $key1 => $value1){
				if($center_id==$value1['center_id'])
				$resA[$value->day][] = array('details'=>$value1,'standard'=>$value->std_id,'division'=>$value->div_id);
			}
		}
		//echo "<pre>";print_r($resA);die;
		
		return view('center.schedule',compact('resA'));  
	}
	
	/* EOF Function for work schedule */
	
	public function leaveRequestView($id)
	{
		
		$leave_requests  = DB::table('leave_request')
											->select('leave_request.*')											
											->where('leave_request.school_id','=',Auth::guard('center')->user()->school_id)											
											->where('leave_request.std_id','=',Auth::guard('center')->user()->std_id)
											->where('leave_request.div_id','=',Auth::guard('center')->user()->div_id)
											->where('leave_request.id',$id)
											->get();
		return view('center.leave_requestView',compact('leave_requests'));									
	}

	public function classes($cls_id){
			// echo $billno;die;
		$classes = DB::table('classes')
							->select('*')
							->get();
		return view('center.course.course', compact('classes','cls_id'));
	}

	public function exam_title($cls_id,$cat_id){
        $classes = DB::table('classes')
							->select('*')
							->get();
    	return view('center.course.course', compact('classes' , 'cls_id' ,'cat_id'));
    }

    public function details($cls_id,$cat_id,$exam_id, $subj_id){
    	return view('center.course.course_detail', compact('cls_id','cat_id','exam_id' ,'subj_id'));
    }

    

  //   public function profile($center_id){
  //   	// echo $center_id;
  //   	$classes = DB::table('centers')
		// 					->select('*')
		// 					->where('centers.id','=',$center_id)
		// 					->get();
		// return view('center.student.profile', compact('classes'));
  //   }

    // public function update(Request $request, $center_id){
    // 	$name = $request -> input('name');
    // 	$place = $request -> input('place');
    // 	$address = $request -> input('address');
    // 	$email = $request -> input('email');
    // 	$phone = $request -> input('phone');
    // 	DB::update('update centers set name = ?,place = ?,address = ?,email = ?,phone = ? where id = ?',[$name,$place,$address,$email,$phone,$center_id]);
    // 	return view('center.student.profile_updated');
    // }
    public function notify_center()
    {           
        return view('center.notification.notify');
    }

    public function notify(){
    	return view('center.student.notification');
    }

  //   public function notify_search(Request $request){
  //   	$id = $request->id;
  //   	// die($id);
  //   	$users = DB::table('students')
  //   						->select(array('id', 'name'))
  //   						->where('students.id','=',$id)
  //   						->get();
    	
  //   	return view('center.student.notification',['users'=>$users]);
  //   }

    public function notify_try(Request $request){
    			if ($request->id != NULL) {
                $id = $request->id;
                $users = DB::table('students')
                            ->select('*')
                            ->where('id',$id)
                            ->get();
                return view('admin.notification',['users'=>$users]);
            }else{
                $id = $request->center;
                $users = DB::table('students')
                            ->select('*')
                            ->where('center',$id)
                            ->get();
                return view('admin.notification',['users'=>$users]);
            }
    }

    public function send(Request $request){
    			$ids = $request->selector;
                // echo $ids;
    			// echo $msg;
    			if(count($ids) == 0){
                    return redirect('/center/notification')->with('dmessage', 'Select students');
    			}
    			else{
                    // echo "other";
                    foreach ($ids as $id) {
                        $notification = new Notification();
                        $notification->student_id = $id;
                        $notification->date = date('Y-m-d');
                        $notification->from = $request->from;
                        $notification->message = $request->msg;
                        $notification->view = 0;
                        $notification->save();
                    }
    			}
    			
    			// alert("message send");
                return redirect('/center/notification')->with('message', 'Message Send Sucessfully');
    }

    public function notify_XI(){
      $users = DB::table('students')
                    ->select(array('id', 'name'))
                    ->where('students.class','=','plus one')
                    ->get();
        return view('center.student.notification',['users'=>$users]);
    }

    public function notify_XII(){
        $users = DB::table('students')
                    ->select(array('id', 'name'))
                    ->where('students.class','=','plus two')
                    ->get();
        return view('center.student.notification',['users'=>$users]);
    }

    public function ntf_view(Request $request){
        
        $json=array();

        $status = Notification::where('center_id',Auth::guard('center')->user()->id)
                            ->where('view',$request->view)
                            ->get();
        foreach ($status as $key => $value) {
            DB::table('notifications')
                ->where('id',$value->id)
                ->update(['view' => 1]);
        }
        if (count($status) > 0) {
            $json['count'] = '1';
        }else{
            $json['count'] = '0';
        }
        
        echo json_encode($json);                        
    }

    public function manage(){
    	return view('center.manage_stud');
    }

    public function edit($stud_id){
    	return view('center.student.manage_edit', compact('stud_id'));
    }

	public function edit_unapproved($stud_id){
    	return view('center.student.manage_unapproved', compact('stud_id'));
    }    

    

    // public function approve(Request $request, $id){
    // 	$name = $request -> input('name');
    //     $email =  $request -> input('email');
    //     $phone = $request -> input('phone');
    //     $school = $request -> input('school');
    //     $center = $request -> input('center');
    //     $class = $request -> input('class');
    //     $place = $request -> input('place');
    //     $dob = $request -> input('dob');
    //     $yoa =  $request -> input('yoa');
    //     $p_name = $request -> input('parent_name');
    //     $p_email = $request -> input('parent_email');
    //     $p_no = $request -> input('parent_phone');

    //     $users = DB::insert('insert into students values(?,?,?,?,?,?,?,?,?,?,?,?)',[$id,$name,$email,$class,$place,'',$phone,$dob,$yoa,$center,$school,$p_name,'',$p_email,$p_no,'','','','','']);

        
    //     return view('center.manage_stud');
    // }
    public function approve($stud_id){
    	$users = DB::table('students_approvals')
    						->select('*')
    						->where('id','=',$stud_id)
    						->get();
    	$insert = DB::insert('insert into students(id,name,email,class,place,address,phone,dob,yoa,center,school,parent_name,rws,parent_email,parent_phone,password,status,remember_token,created_at,updated_at) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',[$users->id,$users->name,$users->email,$users->class,$users->place,'',$users->phone,$users->dob,$users->yoa,$users->center,$users->school,$users->parent_name,$users->rws,$users->parent_email,$users->parent_phone,$users->password,$users->status,'',$users->created_at,$users->updated_at]);
    						

    	// return view('center.student.manage_edit', compact('stud_id'));
    }

    // public function m_view($rt){
    // 			// $val =  $request->val;
    // 			// echo $val;
    // 			if($rt=='approved'){
    // 				// echo "approved";
    // 				$users = DB::table('students')
    // 						->select(array('id','name','email','status'))
    // 						->get();
    // 				echo json_encode($users);
    // 				// return view('center.manage',['users'=>$users]);
    // 			}
    // 			elseif($rt=='unapproved'){
    // 				// echo "unapproved";
    // 				$users = DB::table('students_approvals')
    // 						->select(array('id','name','email','status'))
    // 						->get();
    // 				echo json_encode($users);
    // 			}
    // 			else{
    // 				echo "select case";
    // 			}
    // }

    // public function about(){
    // 	return view('center.about_us');
    // }

    // public function contact(){
    // 	return view('center.contact-us');
    // }

}