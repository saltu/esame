<?php

namespace App\Http\Controllers\Center;

use Illuminate\Http\Request;
use Auth;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class ExamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('center.exams.add');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function manage()
    {
        return view('center.exams.manage');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function exam_names(Request $request){
        $values = $request->get('value');
        $category_id = DB::table ('categories')
                    ->select('id')
                    ->where('class_id',$values)
                    ->get();
        $output = '<option value="">-- Select Exam --</option>';
        foreach ($category_id as $key => $value) {
            $exam_name = DB::table ('exam_names')
                    ->select('*')
                    ->where('category_id',$value->id)
                    ->get();
            foreach ($exam_name as $key => $value) {
                    $output .= '<option value="'.$value->id.'">'.$value->name.'</option>';
                }
        }
        echo $output;
        
    }
}
