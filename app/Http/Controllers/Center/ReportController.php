<?php
/**********************************************
 * LoginController - backend user account management
 * @author  : helena.pg@shtishtionline.com *
 * @date    : 10/03/2017                      *
 **********************************************/
namespace App\Http\Controllers\Center;
use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Auth;
use DB;
use App\Models\Center;
use App\Homeworks;
use App\Circular;
use App\CircularTo;
use Session;
use Hash;
//use App\Models\UserPermission;
//use Illuminate\Html\HtmlServiceProvider;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;

class ReportController extends Controller
{
    public function category(Request $request){

        $value = $request->get('value');
        $category = DB::table ('catagories')
                    ->select('*')
                    ->where('class_id',$value)
                    ->orderBy('name','ASC')
                    ->get();

        $output = '<option value="">-- Select Category --</option>';
        foreach ($category as $key => $value) {
                $output .= '<option value="'.$value->id.'">'.$value->name.'</option>';
            }
            echo $output;
        
    }

    public function etype(Request $request){

        $value = $request->get('value');
        $etype = DB::table ('exam_types')
                    ->select('*')
                    ->where('category_id',$value)
                    ->orderBy('name','ASC')
                    ->get();

        $output = '<option value="">-- Select Exam Type --</option>';
        foreach ($etype as $key => $value) {
                $output .= '<option value="'.$value->id.'">'.$value->name.'</option>';
            }
            echo $output;
        
    }

    public function esubject(Request $request){

        $value = $request->get('value');
        $etype = DB::table ('exams')
                    ->select('*')
                    ->where('type_id',$value)
                    ->orderBy('name','ASC')
                    ->get();

        $output = '<option value="">-- Select Subject --</option>';
        foreach ($etype as $key => $value) {
                $output .= '<option value="'.$value->id.'">'.$value->name.'</option>';
            }
            echo $output;
        
    }

    public function esubmit(Request $request){
        
        $sub = $request->get('sub');
        // $users = DB::table('exam_titles')
        //             ->select('name')
        //             ->where('subject_id','=',$sub)
        //             ->get();
        $users = DB::table('exam_detail')                
                        ->selectRaw('exam_name, count(exam_name)')
                        ->where('subject_id','=',$sub)
                        ->get();
        echo json_encode($users);

        
    }

    public function cate(Request $request){

        $value = $request->get('value');
        $category = DB::table ('catagories')
                    ->select('*')
                    ->where('class_id',$value)
                    ->orderBy('name','ASC')
                    ->get();

        $output = '<option value="">-- Select Category --</option>';
        foreach ($category as $key => $value) {
                $output .= '<option value="'.$value->id.'">'.$value->name.'</option>';
            }
        echo $output;

        
        
    }

    public function table(Request $request){
        $value = $request->get('value');
        if($value == '1'){
            // echo "plus one";
            $table = DB::table('students')
                    ->select('id','name','class')
                    ->where('class','=','plus one')
                    ->get();
        }
        else{
            // echo "plus two";
            $table = DB::table('students')
                    ->select('id','name','class')
                    // ->where('class','=','plus two')
                    ->where('class','=','Plus Two')
                    ->get();
        }    

        echo json_encode($table);
    }

    public function sSubmit(Request $request){
        // echo "reached";
        // $class = $request->get('class');
        // $cat = $request->get('cat');
        $id = $request->get('id');
        // echo $class;
        // echo $cat;
        // echo $id;
        $users = DB::table('students')
                ->select('id','name','class')
                ->where('id','=',$id)
                ->get();
        echo json_encode($users);
        // return view('center.student.student_report', compact('users'));
    }

    public function exam_view($stud_id){
        $classes = DB::table('classes')
                            ->select('*')
                            ->get();
        $users = DB::table('students')
                            ->select('id','name','class')
                            ->where('center',Auth::guard('center')->user()->id)
                            ->get();
        $find = DB::table('exam_results')
                ->select('*')
                ->where('stud_id',$stud_id)
                ->orderBy('id',DESC)
                ->get();
        return view('center.student.student_report', compact('classes','users','find','stud_id'));
    }

    public function stud_view(Request $request){
        $value = $request->get('value');
    }

	public function general(){
    	return view('center.student.report');
    }

	public function students(){
        $first_view = 0;
        $classes = DB::table('classes')
                            ->select('*')
                            ->get();
        $users = DB::table('students')
                            ->select('id','name','class')
                            ->where('center',Auth::guard('center')->user()->id)
                            ->get();
    	return view('center.student.student_report', compact('classes','users','first_view'));
        // return view('center.student.student_report');
    }



    public function exam()
    {
        $users = array();
        $user = DB::table('students')
                            ->select('id')
                            ->where('center',Auth::guard('center')->user()->id)
                            ->get();
        foreach ($user as $key => $value) {
            $ids = DB::table('students')
                        ->where('id',$value->id)
                        ->first()
                        ->id;
            array_push($users, $ids);
        }
        $pack_id = DB::table('exam_type_packages')
                        ->select('*')
                        ->get();
        // $questad = DB::table('questions')
        //                     ->select('*')
        //                     ->get();
        return view('center.exam_report.exam_report', compact('users','pack_id'));
    }



    public function question(){
    	return view('center.student.questions'); 
    }

    public function q_answer(Request $request){
        $Question_id = $request -> input('Question_id');
        $users = DB::table('questions')
                ->select('questions.*')                                         
                ->where('questions.id','=',$Question_id)
                ->get();

        return view('center.student.questions',['users'=>$users]);

    }

    // public function student_view(Request $request){
    //     $student_name = $request -> input('student_name');
    //     $users = DB::table('students')
    //             ->select('students.*')                                         
    //             ->where('students.name','=',$student_name)
    //             ->get();

    //     return view('center.student.student_report',['users'=>$users]);
       
    // }



    public function delete($id){

        $user = DB::table('students')->where('id', '=', $id)->delete();
        return view('center.manage_stud');


        // echo $id;
    }

    public function delete_unapproved($id){

        $user = DB::table('students_approvals')->where('id', '=', $id)->delete();
        return view('center.manage_stud');


        // echo $id;
    }

    public function edit(Request $request, $id){
        $name = $request -> input('name');
        $email =  $request -> input('email');
        $phone = $request -> input('phone');
        $school = $request -> input('school');
        $center = $request -> input('center');
        $class = $request -> input('class');
        $place = $request -> input('place');
        $dob = $request -> input('dob');
        $yoa =  $request -> input('yoa');
        $p_name = $request -> input('parent_name');
        $p_email = $request -> input('parent_email');
        $p_no = $request -> input('parent_phone');

        $users =DB::update('update students set name = ?,email = ?,phone = ?,school = ?,center = ?,class = ?,place = ?,dob = ?,yoa = ?,parent_name = ?,parent_email = ?,parent_phone = ? where id = ?',[$name,$email,$phone,$school,$center,$class,$place,$dob,$yoa,$p_name,$p_email,$p_no,$id]);
        return view('center.manage_stud');

        // echo $id;
    }



    public function update(Request $request){
        $id = $request -> input('que_id');
        $Question = $request -> input('Question');
        $Que_type = $request -> input('Que_type');
        $description = $request -> input('description');
        $Solution = $request -> input('Solution');
        DB::update('update test set question = ?,exam_type = ?,description = ?,solution = ? where id = ?',[$Question,$Que_type,$description,$Solution,$id]);

        return view('center.student.questions');
        
    }

    public function exam_result_submit(Request $request){
        $find = DB::table('exam_results')
                        ->select('*')
                        ->where('exam_id',$request->exam_name)
                        ->where('test_type',$request->exam_types)
                        ->get();

    return view('center.exam_report.exam_report', compact('find'));
    }


}
