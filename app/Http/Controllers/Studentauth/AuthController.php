<?php

namespace App\Http\Controllers\Studentauth;

use App\Models\Student;
use App\Models\Students_approval;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use HtmlFacade;
use Auth;
use Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Input;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/student';
	protected $guard= 'student';
	

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware($this->guestMiddleware(), ['except' => 'logout'], 'student');
    }

    protected function redirectTo(){
        return redirect('/');
    }

	//Controller function for show login form
	public function showLoginForm(){
        // if (view()->exists('auth.authenticate')) {
        //     return view('auth.authenticate');
        // }
        if (Auth::guard('student')->check()){
            if(!session()->has('url.intended')){
                session(['url.intended' => url()->previous()]);
            } else {
                return redirect('student');                
            }
        }
        
        return view('student.auth.login');
    }
	   //Controller function for show registration form
    public function showRegistrationForm(){
			return view('student.auth.register');
	}  
    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:students',
            'school' => 'required|max:255',
            'phone' => 'required|min:10|max:12|unique:students',
            'center' => 'required',
            'dob' => 'required',
            'yoa' => 'required',
            'place' => 'required|max:255',
            'class' => 'required|max:255',
            'parent_name' => 'required|max:255',
            'rws' => 'required|max:255',
            'parent_email' => 'email|max:255|unique:students',
            'parent_phone' => 'required|min:10|max:12|unique:students',
            'address' => 'required|max:255',            
            'password' => 'required|min:5|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return Student::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'school' => $data['school'],
            'phone' => $data['phone'],
            'center' => $data['center'],
            'dob' => $data['dob'],
            'yoa' => $data['yoa'],
            'class' => $data['class'],
            'place' => $data['place'],
            'parent_name' => $data['parent_name'],
            'rws' => $data['rws'],
            'parent_email' => $data['parent_email'],
            'parent_phone' => $data['parent_phone'],
            'address' => $data['address'],
            'password' => bcrypt($data['password']),
        ]);
    }

    public function a_register(Request $request)
    {
        $this->validate(request(),[
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:students',
            'school' => 'required|max:255',
            'phone' => 'required|min:10|max:12|unique:students',
            'center' => 'required',
            'dob' => 'required',
            'yoa' => 'required',
            'place' => 'required|max:255',
            'class' => 'required|max:255',
            'parent_name' => 'required|max:255',
            'rws' => 'required|max:255',
            'parent_email' => 'email|max:255',
            'parent_phone' => 'required|min:10|max:12',
            'address' => 'required|max:255',            
            // 'password' => 'required|min:5|confirmed',
        ]);
        $dobs = explode('-', $request->dob);
        $d = $dobs[2];
        $m = $dobs[1];
        $y = $dobs[0];
        $arr=array ($d,$m,$y);
        $sdob = implode("/",$arr);

        $student = new Student();
        $student->name = $request->name;
        $student->email = $request->email;
        $student->school = $request->school;
        $student->phone = $request->phone;
        $student->center = $request->center;
        $student->dob = $request->dob;
        $student->yoa = $request->yoa;
        $student->place = $request->place;
        $student->class = $request->class;
        $student->parent_name = $request->parent_name;
        $student->rws = $request->rws;
        $student->parent_email = $request->parent_email;
        $student->parent_phone = $request->parent_phone;
        $student->address = $request->address;
        $student->password = Hash::make($sdob);
        $student->status = 0;
        $student->added_by = "student";
        $student->pass_change_status = 0;
        $student->save();

        $email = $request->email;
        $subject = 'You Are Successfully Registered To Esame';
        $message = 'Dear, Esame Welcome you. Your have been registered in to our esame. Go to website (www.esame.in) and login. Email id :'. $email.' and default password is 
        '.$sdob.'. Please change accordingly. Now you can move forward with the datas. Any doubt feel free to contact us. Thanks and regards, Esame Team';
        $name = 'Esame Education';
        $esame = 'info@esame.in';
        $replayMail = '-f'.$esame;
        $header = "From:" . $name.'<info@esame.in>'."\r\n";
        $header.= "Reply-To: ". $name .'<info@esame.in>'." \r\n";
        $header.= "MIME-Version: 1.0\r\n";
        $header.= "Content-Type: text/html; charset=ISO-8859-1";
        
        mail($email,$subject,$message,$header,$replyMail);
        return redirect('/student/login')->with('message', 'Student Registered Send Successfully');
    }

	public function login(Request $request)
    {
		//echo "<pre>";print_r($_POST);die;
        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        $throttles = $this->isUsingThrottlesLoginsTrait();

        if ($throttles && $lockedOut = $this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        $credentials = $this->getCredentials($request);
		//echo "<pre>";print_r($credentials);die;
        if (Auth::guard($this->getGuard())->attempt($credentials, $request->has('remember'))) {
            
            return $this->handleUserWasAuthenticated($request, $throttles);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        if ($throttles && ! $lockedOut) {
            $this->incrementLoginAttempts($request);
        }

        return $this->sendFailedLoginResponse($request);
    }
	
	protected function validateLogin(Request $request)
    {
		
        $this->validate($request, [
            $this->loginUsername() => 'required', 'password' => 'required',
        ]);
		/*$this->validate($request, [
            'email' => 'required', 'password' => 'required',
        ]);*/
    }
	protected function handleUserWasAuthenticated(Request $request, $throttles)
    {
        if ($throttles) {
            $this->clearLoginAttempts($request);
        }

        if (method_exists($this, 'authenticated')) {
            return $this->authenticated($request, Auth::guard($this->getGuard())->user());
        }
        
        return redirect()->intended($this->redirectPath());
    }
	 protected function sendFailedLoginResponse(Request $request)
    {
        return redirect()->back()
            ->withInput($request->only($this->loginUsername(), 'remember'))
            ->withErrors([
                $this->loginUsername() => $this->getFailedLoginMessage(),
            ]);
			/*return redirect()->back()
            ->withInput($request->only('email', 'remember'))
            ->withErrors([
                'email' => $this->getFailedLoginMessage(),
            ]);*/
    }
	protected function getFailedLoginMessage()
    {
        return Lang::has('auth.failed')
                ? Lang::get('auth.failed')
                : 'These credentials do not match our records.';
    }
	 protected function getCredentials(Request $request)
    {
        return $request->only($this->loginUsername(), 'password');
    }
	
	
	 public function getLogout()
    {
        return $this->logout();
    }

    /**
     * Log the user out of the application.
     *
     * @return \Illuminate\Http\Response
     */
    public function logout()
    {
        Auth::guard($this->getGuard())->logout();

        return redirect(property_exists($this, 'redirectAfterLogout') ? $this->redirectAfterLogout : '/student');
    }

    /**
     * Get the guest middleware for the application.
     */
    public function guestMiddleware()
    {
        $guard = $this->getGuard();

        return $guard ? 'guest:'.$guard : 'guest';
    }

    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    public function loginUsername()
    {
     
        return property_exists($this, 'username') ? $this->username : 'email';
    }

    /**
     * Determine if the class is using the ThrottlesLogins trait.
     *
     * @return bool
     */
    protected function isUsingThrottlesLoginsTrait()
    {
        return in_array(
            ThrottlesLogins::class, class_uses_recursive(static::class)
        );
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return string|null
     */
    protected function getGuard()
    {
		
        return property_exists($this, 'guard') ? $this->guard : null;
    }

    function slogin(Request $request)
    {
        $rules = array (
                
                'email' => 'required',
                'password' => 'required' 
        );
        $validator = Validator::make ( Input::all (), $rules );
        if ($validator->fails ()) {
            return Redirect::back ()->withErrors ($validator)->withInput ();
        } else {
            if (Auth::attempt ( array (
                    
                    'email' => $request->get ( 'email' ),
                    'password' => $request->get ( 'password' ) 
            ) )) {
                session ( [ 
                        
                        'email' => $request->get ( 'email' ) 
                ] );
                return Redirect('/student');
            } else {
                return redirect()->back()->with('message', "Invalid Credentials , Please try again.");
            }
        }

    }
    
}
