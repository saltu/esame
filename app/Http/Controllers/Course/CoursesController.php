<?php

namespace App\Http\Controllers\Course;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class CoursesController extends Controller
{
    public function index(){
    	return view('course');
    }

    public function details($cls_id,$cat_id,$exam_id, $subj_id){
    	return view('course.details', compact('cls_id','cat_id','exam_id' ,'subj_id'));
    }
    
    public function exam($cls_id,$cat_id,$exam_id, $subj_id, $exam){
//        echo $cls_id.$cat_id.$exam_id.$subj_id.$exam ;die;
        
        /// Filter the qestion id using the details values convert it into array and pass it into the next page.,
    	return view('exam.index', compact('cls_id','cat_id','exam_id' ,'subj_id', 'exam'));
    }
    
	public function classes($cls_id){
			// echo $billno;die;
		$classes = DB::table('classes')
							->select('*')
							->get();
		return view('course.index', compact('classes','cls_id'));
	}
    
    public function exam_title($cls_id,$cat_id){
        $classes = DB::table('classes')
							->select('*')
							->get();
    	return view('course.index', compact('classes' , 'cls_id' ,'cat_id'));
    }
}