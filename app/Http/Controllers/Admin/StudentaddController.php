<?php

namespace App\Http\Controllers\Admin;

use App\Models\Student;
use App\Models\Students_approval;
use App\Models\Center;
use Validator;
use DB;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Auth;
use HtmlFacade;
use Illuminate\Http\Request;
use Hash;
use Mail;

class StudentaddController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.student.add');
    }
    public function s_report()
    {
        $users = DB::table('students')
                            ->select('id','name','class')
                            ->get();
        return view('admin.report.students_report',compact('users'));
    }
    public function exam_view($stud_id){
        $users = DB::table('students')
                            ->select('id','name','class')
                            ->where('id',$stud_id)
                            ->get();
        $attented_exams = DB::table('exam_results')
                ->select('*')
                ->where('stud_id',$stud_id)
                ->orderBy('id',DESC)
                ->get();
        return view('admin.report.student_report_list', compact('users','attented_exams'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $dobs = explode('-', $request->dob);
        $d = $dobs[2];
        $m = $dobs[1];
        $y = $dobs[0];
        $arr=array ($d,$m,$y);
        $sdob = implode("/",$arr);

        $id = $request->id;
        $student = new Student();
        $student->name = $request->name;
        $student->email = $request->email;
        $student->school = $request->school;
        $student->phone = $request->phone;
        $student->center = $request->center;
        $student->dob = $request->dob;
        $student->yoa = $request->yoa;
        $student->place = $request->place;
        $student->class = $request->class;
        $student->parent_name = $request->parent_name;
        $student->rws = $request->rws;
        $student->parent_email = $request->parent_email;
        $student->parent_phone = $request->parent_phone;
        $student->address = $request->address;
        $student->password = Hash::make($sdob);
        $student->status = "0";
        $student->added_by = $request->added_from;
        $student->pass_change_status = 0;
        $student->save();

        $email = $request->email;
        $subject = 'You Are Successfully Registered To Esame';
        $message = 'Dear, Esame Welcome you. Your have been registered in to our esame. Go to website (www.esame.in) and login. Email id :'. $email.' and default password is 
        '.$sdob.'. Please change accordingly. Now you can move forward with the datas. Any doubt feel free to contact us. Thanks and regards, Esame Team';
        $name = 'Esame Education';
        $esame = 'info@esame.in';
        $replayMail = '-f'.$esame;
        $header = "From:" . $name.'<info@esame.in>'."\r\n";
        $header.= "Reply-To: ". $name .'<info@esame.in>'." \r\n";
        $header.= "MIME-Version: 1.0\r\n";
        $header.= "Content-Type: text/html; charset=ISO-8859-1";
        
        mail($email,$subject,$message,$header,$replyMail);

        // $dup = Students_approval::findorfail($id);
        // $dup->destroy($id);

        return redirect('/admin/student_add')->with('message', 'Student Registered Successfully');
    }

    public function manage()
    {
        $students = DB::table('students_approvals')
                        ->select('*')
                        ->get();
        $a_students = DB::table('students')
                        ->select('*')
                        ->get();
        return view('admin.student.manage',compact('students','a_students'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function view($id)
    {
        $student = DB::table('students_approvals')
                        ->select('*')
                        ->where('id',$id)
                        ->get();
        return view('admin.student.student_view',compact('student'));
    }

    public function a_s_view($id)
    {
        $student = DB::table('students')
                        ->select('*')
                        ->where('id',$id)
                        ->get();
        return view('admin.student.approved_student_details',compact('student'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function approve($id)
    // {
    //     $std = DB::table('students_approvals')
    //                     ->select('*')
    //                     ->where('id',$id)
    //                     ->get();
    //     $doob = explode('-', $std[0]->dob);
    //     $d = $doob[2];
    //     $m = $doob[1];
    //     $y = $doob[0];
    //     $arr=array ($d,$m,$y);
    //     $str = implode("/",$arr);
        
    //     $student = new Student();
    //     $student->name = $std[0]->name;
    //     $student->email = $std[0]->email;
    //     $student->school = $std[0]->school;
    //     $student->phone = $std[0]->phone;
    //     $student->center = $std[0]->center;
    //     $student->dob = $std[0]->dob;
    //     $student->yoa = $std[0]->yoa;
    //     $student->place = $std[0]->place;
    //     $student->class = $std[0]->class;
    //     $student->parent_name = $std[0]->parent_name;
    //     $student->rws = $std[0]->rws;
    //     $student->parent_email = $std[0]->parent_email;
    //     $student->parent_phone = $std[0]->parent_phone;
    //     $student->address = $std[0]->address;
    //     $student->password = Hash::make($str);
    //     $student->status = "0";
    //     $student->added_by = $std[0]->added_from;
    //     $student->pass_change_status = 0;
    //     $student->save();

    //     $del = Students_approval::findorfail($id);
    //     $del->destroy($id);

    //     return redirect('/admin/students_manage')->with('message', 'Approved Student Successfully');
    // }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
        
        
    public function update(Request $request)
    {
        DB::update("UPDATE students SET name = '$request->name', email = '$request->email', school = '$request->school', phone = '$request->phone', center = '$request->center', dob = '$request->dob', yoa = '$request->yoa', place = '$request->place', class = '$request->class', parent_name = '$request->parent_name', rws = '$request->rws', parent_email = '$request->parent_email', parent_phone = '$request->parent_phone', address = '$request->address', status = '0', added_by = '$request->added_by' WHERE id = ? ",[$request->id]);
        return redirect('/admin/students_manage')->with('message', 'Updated Student Details Successfully');
    }

    public function block($id)
    {
        DB::table('students')
            ->where('id',$id)
            ->update(['status' => 1]);
        return redirect('/admin/students_manage')->with('dmessage', 'Blocked Student Successfully');
    }
    public function unblock($id)
    {
        DB::table('students')
            ->where('id',$id)
            ->update(['status' => 0]);
        return redirect('/admin/students_manage')->with('message', 'Unblocked student Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $rjct = Students_approval::findorfail($id);
        $rjct->destroy($id);

        return redirect('/admin/students_manage')->with('dmessage', 'Rejected Student Successfully');
    }
     public function a_s_delete($id)
    {
        $rjct = Student::findorfail($id);
        $rjct->destroy($id);

        return redirect('/admin/students_manage')->with('dmessage', 'Deleted Student Successfully');
    }
    public function centername(Request $request){
        
        $json=array();

        $center = Center::where('name','like', '%' . $request->term . '%')
                        ->get();

        foreach ($center as $key => $value) {
                $json[] =  $value->name;
            }

        echo json_encode($json);                        
    }
    public function fetchname(Request $request){
        $ctrname = Center::where('name',[$request->value])
                    ->get();

        foreach ($ctrname as $key => $value) {
                $json['id'] =  $value->id;
                $json['name'] =  $value->name;
            }

        echo json_encode($json);

    }
}
