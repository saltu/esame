<?php

namespace App\Http\Controllers\Admin;

use App\Models\Center;
use DB;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Auth;
use HtmlFacade;
use Illuminate\Http\Request;
use Hash;
use Mail;

class CenteraddController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.center.add');
    }

    public function manage()
    {
        $centers = DB::table('centers')
                        ->select('*')
                        ->get();
        return view('admin.center.manage',compact('centers'));
    }

    public function center_report()
    {
        $centers = DB::table('centers')
                            ->select('*')
                            ->get();
        return view('admin.report.centers_list',compact('centers'));
    }
    public function center_view($cent_id){
        $users = DB::table('students')
                            ->select('id','name','class','center')
                            ->where('id',$cent_id)
                            ->get();
        foreach ($users as $key => $value) {
        $attented_exams = DB::table('exam_results')
                            ->select('*')
                            ->where('stud_id',$value->id)
                            ->orderBy('id','DESC')
                            ->get();
        }
        return view('admin.report.center_student_exam_report', compact('users','attented_exams'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(request(),[
            'name' => 'required|max:255',
            'place' => 'required|max:255',
            'registration_no' => 'required|max:255|unique:centers',
            'email' => 'required|email|max:255|unique:centers',
            'address' => 'required|max:255',
            'phone' => 'required|min:10|max:12|unique:centers',
            // 'password' => 'required|min:5|confirmed',
        ]);


        $center = new Center();
        $center->name = $request->name;
        $center->place = $request->place;
        $center->registration_no = $request->registration_no;
        $center->email = $request->email;
        $center->address = $request->address;
        $center->phone = $request->phone;
        $center->password = Hash::make("esame123");
        $center->status = '0';
        $center->save();
        
        $email = $request->email;
        $subject = 'Your Center Added Successfully';
        $message = 'Dear, Esame Welcome you. Your Center has been included in our system. Go to Center website (www.esame.in) from login option. Email id :'. $email.' and default password is esame123. Please change accordingly. Now you can move forward with the datas. Any doubt feel free to contact us. Thanks and regards, Esame Team';
        $name = 'Esame Education';
        $esame = 'info@esame.in';
        $replayMail = '-f'.$esame;
        $header = "From:" . $name.'<info@esame.in>'."\r\n";
        $header.= "Reply-To: ". $name .'<info@esame.in>'." \r\n";
        $header.= "MIME-Version: 1.0\r\n";
        $header.= "Content-Type: text/html; charset=ISO-8859-1";
        
        mail($email,$subject,$message, $header, $replyMail);
        
        return redirect('/admin/manage_center')->with('message', 'Center Added Successfully');
    }

    public function view($id)
    {
        $centers = DB::table('centers')
                        ->select('*')
                        ->where('id',$id)
                        ->get();
        return view('admin.center.center_details',compact('centers'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function block($id)
    {
        DB::table('centers')
            ->where('id',$id)
            ->update(['status' => 1]);
        return redirect('/admin/manage_center')->with('dmessage', 'Blocked Center Successfully');
    }
    public function unblock($id)
    {
        DB::table('centers')
            ->where('id',$id)
            ->update(['status' => 0]);
        return redirect('/admin/manage_center')->with('message', 'Unblocked Center Successfully');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
       DB::update("UPDATE centers SET name = '$request->name', registration_no = '$request->registration_no', email = '$request->email', phone = '$request->phone', place = '$request->place', address = '$request->address', status = '0', WHERE id = ? ",[$request->id]);
        return redirect('/admin/manage_center')->with('message', 'Updated Center Details Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $rjct = Center::findorfail($id);
        $rjct->destroy($id);

        return redirect('/admin/manage_center')->with('dmessage', 'Deleted Center Successfully');
    }
}
