<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Auth;
use DB;
use Validator;
use App\Models\Question;
use App\Models\Question_option;
use App\Models\Question_detail;
use App\Models\Categorie;
use App\Models\Classes;
use App\Models\Exam_name;
use App\Models\Chapter;
use App\Models\Exam_fee;
use App\Models\Exam_type_package;
use App\Models\Subject;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class ExamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $classes = DB::table('classes')
							->select('*')
							->get();
		return view('admin.exams.add', compact('classes'));
    }

    public function questions()
    {
        $questad = DB::table('questions')
                            ->select('*')
                            ->orderBy('id','DESC')
                            ->get();
        return view('admin.exams.questions', compact('questad'));
    }
    
    public function exam_category($c_id)
    {
        
        $categories = DB::table('categories')
							->select('*')
                            ->where('class_id',$c_id)
							->get();
       echo json_encode($categories);  
    }

    
    public function exam_type($cat_id)
    {
        $exam_types = DB::table('exam_names')
							->select('*')
                            ->where('category_id',$cat_id)
							->get();
       echo json_encode($exam_types);  
    }
    
    public function exam_subject($type_id)
    {
        $subjects = DB::table('subjects')
							->select('*')
                            ->where('examname_id',$type_id)
							->get();
       echo json_encode($subjects);  
    }
    
    public function exams($subject_id)
    {
        $chapters = DB::table('chapters')
							->select('*')
                            ->where('subject_id',$subject_id)
							->get();
       echo json_encode($chapters);  
    }    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }
    public function package()
    {
        $exam_fees = DB::table('exam_fees')
                            ->select('*')
                            ->get();
        $package_id = Exam_fee::pluck('ex_package_id')->all();
        $packages = Exam_type_package::whereNotIn('id', $package_id)
                            ->select('*')
                            ->get();
        return view('admin.exams.exam_package', compact('exam_fees','packages'));
    }
    public function exam_package(Request $request)
    {
        $this->validate(request(),[
            'package_name' => 'required',
            'class' => 'required',
            'e_name' => 'required',
        ]);
        $package = new Exam_type_package();
        $package->title = $request->package_name;
        $package->class_ids = implode(",",$request->class);
        $package->ex_type_ids = implode(",",$request->e_name);
        $package->added_by = Auth::guard('admin')->user()->id;
        $package->status = 0;
        $package->save();
        return redirect('/admin/manage_exam_package')->with('message', 'Exam Package Added Sucessfully');
    }
    public function add_fee(Request $request)
    {
        $this->validate(request(),[
            'package' => 'required',
            'student_amount' => 'required',
            'center_amount' => 'required',
        ]);
        $title = DB::table('exam_type_packages')
                            ->select('title')
                            ->where('id', $request->package)
                            ->get();
        $fee = new Exam_fee();
        $fee->title = $title[0]->title;
        $fee->ex_package_id = $request->package;
        $fee->student_amount = $request->student_amount;
        $fee->center_amount = $request->center_amount;
        $fee->added_by = Auth::guard('admin')->user()->id;
        $fee->status = 0;
        $fee->save();
        return redirect('/admin/manage_exam_package')->with('message', 'Package Fee Added Sucessfully');
    }
    public function edit_exam_fee($id)
    {
        $e_fee = DB::table('exam_fees')
                            ->select('*')
                            ->where('id', $id)
                            ->get();
        return view('admin.exams.exam_fee_edit', compact('e_fee'));
    }
    public function fee_edit(Request $request)
    {
        $this->validate(request(),[
            'student_amount' => 'required',
            'center_amount' => 'required',
        ]);
        DB::update("UPDATE exam_fees SET student_amount = '$request->student_amount', center_amount = '$request->center_amount' WHERE id = ? ",[$request->id]);
        return redirect('/admin/manage_exam_package')->with('message', 'Exam Amount Updated Sucessfully');
    }

    public function edit_exam($id,$name_id)
    {
        $fil_id = $name_id;
        if ($fil_id == '1'){
            $disp_cls = DB::table('classes')
                            ->select('*')
                            ->where('id', $id)
                            ->get();
            return view('admin.exams.exam_edit', compact('disp_cls'));
        }
        elseif ($fil_id == '2') {
            $disp_cat = DB::table('categories')
                                ->select('*')
                                ->where('id', $id)
                                ->get();
            return view('admin.exams.exam_edit', compact('disp_cat'));
        }
        elseif ($fil_id == '3') {
            $disp_ty = DB::table('exam_types')
                                ->select('*')
                                ->where('id', $id)
                                ->get();
            return view('admin.exams.exam_edit', compact('disp_ty'));
        }
        elseif ($fil_id == '4') {
            $disp_ex = DB::table('exams')
                                ->select('*')
                                ->where('id', $id)
                                ->get();
            return view('admin.exams.exam_edit', compact('disp_ex'));
        }
    }

    public function exam_edit(Request $request)
    {
        $filter = $request->fil;
        if($filter == "1"){
            DB::table('classes')
            ->where('id',$request->id)
            ->update(['class' => $request->name]);
        }elseif ($filter == "2") {
            DB::table('categories')
            ->where('id',$request->id)
            ->update(['name' => $request->name]);
        }elseif ($filter == "3") {
            DB::table('exam_types')
            ->where('id',$request->id)
            ->update(['name' => $request->name]);
        }elseif ($filter == "4") {
            DB::table('exams')
            ->where('id',$request->id)
            ->update(['name' => $request->name]);
        }else{
            return redirect('/admin/manage_exams')->with('dmessage', 'Failed');
        }
        return redirect('/admin/manage_exams')->with('message', 'Exam Updated Sucessfully');
    }

    public function manage()
    {
        return view('admin.exams.manage');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
 
    public function store(Request $request)
    {
        $json = array();
        $flag = 0;
        /* Getting file name */
        $filename = $_FILES['iquestion']['name'];
        $file_ans = $_FILES['i_answer']['name'];
        $file_opt1 = $_FILES['i_option1']['name'];
        $file_opt2 = $_FILES['i_option2']['name'];
        $file_opt3 = $_FILES['i_option3']['name'];
        $question = $_POST['question'];
        $answer = $_POST['answer'];
        $solution = $_POST['solution'];
        $opn1 = $_POST['option1'];
        $opn2 = $_POST['option2'];
        $opn3 = $_POST['option3'];

        $cls_id = $_POST['catagories'];
        $cat_id = $_POST['exam_types'];
        $exam_id = $_POST['exams'];
        $sub_id = $_POST['chapters'];
        $chap_id = $_POST['chapter'];

        /* Location */
        $location = "public/question_images/".$filename;
        $ans_loc = "public/option_images/".$file_ans;
        $opt1_loc = "public/option_images/".$file_opt1;
        $opt2_loc = "public/option_images/".$file_opt2;
        $opt3_loc = "public/option_images/".$file_opt3;
        $uploadOk = 1;
        $imageFileType = pathinfo($location,PATHINFO_EXTENSION);
        $ext_ans = pathinfo($ans_loc,PATHINFO_EXTENSION);
        $ext_opt1 = pathinfo($opt1_loc,PATHINFO_EXTENSION);
        $ext_opt2 = pathinfo($opt2_loc,PATHINFO_EXTENSION);
        $ext_opt3 = pathinfo($opt3_loc,PATHINFO_EXTENSION);

        /* Valid Extensions */
        $valid_extensions = array("jpg","jpeg","png");
        /* Check file extension */
        if ($filename != '') {
            if( !in_array(strtolower($imageFileType),$valid_extensions)) {
                $uploadOk = 0;
            }
        }
        if ($file_ans != '') {
            if (!in_array(strtolower($ext_ans),$valid_extensions)) {
                $uploadOk = 0;
            }
        }
        if ($file_opt1 != '') {
            if (!in_array(strtolower($ext_opt1),$valid_extensions)) {
                $uploadOk = 0;
            }
        }
        if ($file_opt2 != '') {
            if (!in_array(strtolower($ext_opt2),$valid_extensions)) {
                $uploadOk = 0;
            }
        }
        if ($file_opt3 != '') {
            if (!in_array(strtolower($ext_opt3),$valid_extensions)) {
                $uploadOk = 0;
            }
        }

        if($uploadOk == 0){
            $json['status'] = 'extension error';
            $json['q_id'] = 0;
           echo json_encode($json);
        }else{
            if ( $filename == '' && $file_ans == '' && $file_opt1 == '' && $file_opt2 == '' && $file_opt3 == '') {
                $questions = new Question;
                $questions->question = $question;
                $questions->solution = $answer;
                $questions->description = $solution;
                $questions->save();
                $last_id = $questions->id;

                $question_ans = new Question_option;
                $question_ans->question_id = $last_id;
                $question_ans->options = $answer;
                $question_ans->save();
                $answer_id = $question_ans->id;

                $question_opn1 = new Question_option;
                $question_opn1->question_id = $last_id;
                $question_opn1->options = $opn1;
                $question_opn1->save();

                $question_opn2 = new Question_option;
                $question_opn2->question_id = $last_id;
                $question_opn2->options = $opn2;
                $question_opn2->save();

                $question_opn3 = new Question_option;
                $question_opn3->question_id = $last_id;
                $question_opn3->options = $opn3;
                $question_opn3->save();

                DB::table('questions')
                    ->where('id',$last_id)
                    ->update(['solution' => $answer_id]);

                $qst_dtls = new Question_detail();
                $qst_dtls->question_id = $last_id;
                $qst_dtls->chapter_id = $_POST['id_chap'];
                $qst_dtls->save();

                $json['status'] = 'Question Added Sucessfully';
                $json['q_id'] = $last_id;
                echo json_encode($json);
            }
           /* Upload file */
            else{
                $questions = new Question;
                $questions->question = $question;
                $questions->solution = $answer;
                $questions->description = $solution;
                if( $filename != '' ){
                    if(move_uploaded_file($_FILES['iquestion']['tmp_name'],$location)){
                        $questions->q_image = $location;
                    }else{
                        $flag = 1;
                    }
                }
                $questions->save();
                $last_id = $questions->id;

                $question_ans = new Question_option;
                $question_ans->question_id = $last_id;
                $question_ans->options = $answer;
                if($file_ans != ''){
                    if(move_uploaded_file($_FILES['i_answer']['tmp_name'],$ans_loc)){
                        $question_ans->opt_image = $ans_loc;
                    }else{
                        $flag = 1;
                    }
                }
                $question_ans->save();
                $answer_id = $question_ans->id;

                DB::table('questions')
                    ->where('id',$last_id)
                    ->update(['solution' => $answer_id]);

                $question_opn1 = new Question_option;
                $question_opn1->question_id = $last_id;
                $question_opn1->options = $opn1;
                if($file_opt1 != ''){
                    if(move_uploaded_file($_FILES['i_option1']['tmp_name'],$opt1_loc)){
                        $question_opn1->opt_image = $opt1_loc;
                    }else{
                        $flag = 1;
                    }
                }
                $question_opn1->save();

                $question_opn2 = new Question_option;
                $question_opn2->question_id = $last_id;
                $question_opn2->options = $opn2;
                if($file_opt2 != ''){
                    if(move_uploaded_file($_FILES['i_option2']['tmp_name'],$opt2_loc)){
                        $question_opn2->opt_image = $opt2_loc;
                    }else{
                        $flag = 1;
                    }
                }
                $question_opn2->save();

                $question_opn3 = new Question_option;
                $question_opn3->question_id = $last_id;
                $question_opn3->options = $opn3;
                if($file_opt3 != ''){
                    if(move_uploaded_file($_FILES['i_option3']['tmp_name'],$opt3_loc)){
                        $question_opn3->opt_image = $opt3_loc;
                    }else{
                        $flag = 1;
                    }
                }
                $question_opn3->save();

                if($flag == 0){
                    $qst_dtls = new Question_detail();
                    $qst_dtls->question_id = $last_id;
                    $qst_dtls->chapter_id = $_POST['id_chap'];
                    $qst_dtls->save();

                    $json['status'] = 'Question Added Sucessfully';
                    $json['q_id'] = $last_id;
                    echo json_encode($json);
                }else{
                    $delete = Question::findorfail($last_id);
                    $delete->destroy($last_id);

                    DB::table('question_options')
                        ->where('question_id', $last_id)
                        ->delete();

                    $json['status'] = 'move error';
                    $json['q_id'] = 0;
                    echo json_encode($json);
                }
            }
        }
    }

    public function category(Request $request)
    {
        $this->validate(request(),[
            'cclasses' => 'required',
            'category' => 'required',
        ]);
        $category = new Categorie();
        $category->class_id = $request->cclasses;
        $category->name = $request->category;
        $category->description = $request->c_description;
        $category->added_by = Auth::guard('admin')->user()->id;
        $category->save();
        return redirect('/admin/manage_exams');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function add_class(Request $request)
    {
        $this->validate(request(),[
            'classes' => 'required',
        ]);
        $clas = new Classes();
        $clas->class = $request->classes;
        $clas->added_by = Auth::guard('admin')->user()->id;
        $clas->save();
        return redirect('/admin/manage_exams');
    }
    public function add_name(Request $request)
    {
        $this->validate(request(),[
            'classes' => 'required',
            'category' => 'required',
            'exam_name' => 'required',
        ]);
        $type = new Exam_name();
        $type->category_id = $request->category;
        $type->name = $request->exam_name;
        $type->description = $request->e_description;
        $type->added_by = Auth::guard('admin')->user()->id;
        $type->save();
        return redirect('/admin/manage_exams');
    }
    public function add_subject(Request $request)
    {
        $this->validate(request(),[
            'class' => 'required',
            'e_name' => 'required',
            'subject' => 'required',
        ]);

        $subject = new Subject();
        $subject->class_id = $request->class;
        $subject->examname_id = implode(",",$request->e_name);
        $subject->name = $request->subject;
        $subject->description = $request->s_description;
        $subject->added_by = Auth::guard('admin')->user()->id;
        $subject->save();
        $sub_id = $subject->id;

        $ex_n = implode(",",$request->e_name);
        for ($i = 0; $i <= count($request->e_name); $i++) {
            Exam_name::where('id', $ex_n[$i])
                        ->update(['subject_ids' => DB::raw('CONCAT(subject_ids,",'.$sub_id.'")')]);
        }

        return redirect('/admin/manage_exams');
    }
    public function add_chapter(Request $request)
    {
        $this->validate(request(),[
            'subjects' => 'required',
            'subject' => 'required',
            'chapter' => 'required',
        ]);
        $chapter = new Chapter();
        $chapter->subject_id = $request->subject;
        $chapter->name = $request->chapter;
        $chapter->description = $request->c_description;
        $chapter->added_by = Auth::guard('admin')->user()->id;
        $chapter->save();
        return redirect('/admin/manage_exams');
    }

    
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function question_edit($id)
    {
        $questions = DB::table('questions')
                            ->select('*')
                            ->where('id', $id)
                            ->get();
        $q_options = DB::table('question_options')
                            ->select('*')
                            ->where('question_id', $id)
                            ->get();
        return view('admin.exams.question_edit', compact('questions','q_options'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        DB::update("UPDATE questions SET question = '$request->question', solution = '$request->danswer', description = '$request->solution' WHERE id = ? ",[$request->id]);
        DB::table('question_options')
            ->where('id',$request->danswer)
            ->update(['options' => $request->answer]);
        DB::table('question_options')
            ->where('id',$request->doption1)
            ->update(['options' => $request->option1]);
        DB::table('question_options')
            ->where('id',$request->doption2)
            ->update(['options' => $request->option2]);
        DB::table('question_options')
            ->where('id',$request->doption3)
            ->update(['options' => $request->option3]);

        return redirect('/admin/question_add')->with('message', 'Question Updated Sucessfully');
    }
    public function delete(Request $request)
    {
        $delete = Question::findorfail($request->id);
        $delete->destroy($request->id);

        DB::table('question_options')
            ->where('question_id', $request->id)
            ->delete();
        DB::table('question_details')
            ->where('question_id', $request->id)
            ->delete();
        echo "Question Deleted Succesfully";

        // return redirect('/admin/question_add')->with('dsmessage', 'Deleted Question and Options');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $del = Exam_fee::findorfail($id);
        $del->destroy($id);

        return redirect('/admin/manage_exam_package')->with('dmessage', 'Deleted Exam Fee');
    }
    public function packdestroy($id)
    {
        $del = Exam_type_package::findorfail($id);
        $del->destroy($id);

        DB::table('exam_fees')
            ->where('ex_package_id', '=', $id)
            ->delete();

        return redirect('/admin/manage_exam_package')->with('dmessage', 'Deleted Exam Package');
    }
    // public function cat_delete($id)
    // {
    //     $types= DB::table('exam_types')
    //             ->where('category_id', $id)
    //             ->get();
    //     foreach ($types as $type) {
    //     $subs=DB::table('exams')
    //             ->where('type_id', $type->id)
    //             ->get();
    //         foreach ($subs as $sub) {
    //             Exam_title::where('subject_id', $sub->id)
    //                         ->delete();
    //         }
    //         Exam::where('type_id', $type->id)
    //                         ->delete();
    //         Exam_type::where('category_id', $type->id)
    //                         ->delete();
    //     }

    //     $dele = Catagories::findorfail($id);
    //     $dele->destroy($id);

    //     return redirect('/admin/manage_exams')->with('dmessage', 'Deleted Exam Category');
    // }
    // public function title_delete($id)
    // {
    //     $dele = Exam_type::findorfail($id);
    //     $dele->destroy($id);

    //     return redirect('/admin/manage_exams')->with('dmessage', 'Deleted Exam Type');
    // }
    public function subject_delete($id)
    {
        $dele = Exam::findorfail($id);
        $dele->destroy($id);

        return redirect('/admin/manage_exams')->with('dmessage', 'Deleted Exam Subject');
    }
    public function classes(Request $request){
        
        $json=array();

        $clas = DB::table('classes')
                        ->select('*')
                        ->where('class','like', '%' . $request->term . '%')
                        ->get();

        foreach ($clas as $key => $value) {
                $val = $value->class;
                $json[] =  $val;
            }

        echo json_encode($json);                        
    }
    public function categories(Request $request){
        $value = $request->get('value');
        $category = DB::table ('categories')
                    ->select('*')
                    ->where('class_id',$value)
                    ->orderBy('name','ASC')
                    ->get();
        $output = '<option value="">-- Select Category --</option>';
        foreach ($category as $key => $value) {
                $output .= '<option value="'.$value->id.'">'.$value->name.'</option>';
            }
            echo $output;
        
    }
    public function title(Request $request){
        $select = $request->get('select');
        $db = $request->get('db');
        $value = $request->get('value');
        $dependent = $request->dependentt;
        $data = DB::table ($db)
                    ->select('*')
                    ->where($select,$value)
                    ->orderBy('name','ASC')
                    ->get();
        $output = '<option value="">-- Select '.ucfirst($dependent).' --</option>';
        foreach ($data as $key => $value) {
                $output .= '<option value="'.$value->id.'">'.$value->name.'</option>';
            }
            echo $output;
    }
    public function subject(Request $request){
        $select = $request->get('select');
        $db = $request->get('db');
        $value = $request->get('value');
        $dependent = $request->dependentt;
        $data = DB::table ($db)
                    ->select('*')
                    ->where($select,$value)
                    ->orderBy('name','ASC')
                    ->get();
        $output = '<option value="">-- Select '.ucfirst($dependent).' --</option>';
        foreach ($data as $key => $value) {
                $output .= '<option value="'.$value->id.'">'.$value->name.'</option>';
            }
            echo $output;
    } 
    public function chapter(Request $request){
        $select = $request->get('select');
        $db = $request->get('db');
        $value = $request->get('value');
        $dependent = $request->dependentt;
        $data = DB::table ($db)
                    ->select('*')
                    ->where($select,$value)
                    ->orderBy('name','ASC')
                    ->get();
        $output = '<option value="">-- Select '.ucfirst($dependent).' --</option>';
        foreach ($data as $key => $value) {
                $output .= '<option value="'.$value->id.'">'.$value->name.'</option>';
            }
            echo $output;
    }
    public function exam()
    {
        $pack_id = DB::table('exam_type_packages')
                        ->select('*')
                        ->get();
        return view('admin.report.exam_report', compact('pack_id'));
    }  
    public function append_question(Request $request)
    {
        $questad = DB::table('questions')
                        ->select('*')
                        ->orderBy('id','DESC')
                        ->take(30)
                        ->get();
        return view('admin.exams.question_list', compact('questad'));
    }
}