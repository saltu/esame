<?php
/**********************************************
 * LoginController - backend user account management
 * @author  : helena.pg@shtishtionline.com *
 * @date    : 10/03/2017                      *
 **********************************************/
namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Auth;
use DB;
use App\Models\Admin;
use App\Models\Notification;
use Session;
use Hash;
//use App\Models\UserPermission;
//use Illuminate\Html\HtmlServiceProvider;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;

class AccountController extends Controller
{     
	public function index()
    {
        return view('admin.home');
    }
    public function reset_password()
    {
        return view('admin.auth.passwords.reset');
    }
    public function reset_p( Request $request )
    {
        $this->validate(request(),[
            'c_password' => 'required|min:5',
            'password' => 'required|min:5|confirmed',
        ]);
        $admin = DB::table('admins')
                        ->select('password')
                        ->where('id',$request->user_id)
                        ->get();
        if (Hash::check($request->c_password, $admin[0]->password)) {
        DB::table('admins')
            ->where('id',$request->user_id)
            ->update(['password' => Hash::make($request->password)]);
        return redirect('/admin')->with('message', 'Password Changed Sucessfully');
        }else{
            return Redirect::back()->withInput(Input::all())->with('dmessage', 'Current password is wrong');
        }
    }
	 /*
	* Admin account view
	*/
    public function showAccount( Request $request )
    { 
		$adminUser = Auth::guard('admin')->user();
		return view('admin.account.account',['model'=>$adminUser]);
    }
    

   /*
    * Current user account edit
    */
    public function showAccountpost( Request $request )
    {
		$adminUser = Auth::guard('admin')->user();
		$request->merge( array_map('trim', $request->all()) );
		$rules = array(
            'email'      => 'required|email',
            'name'  => 'required|max:10',
        );
        
        $validator = Validator::make($request->all(), $rules);       
        //process the login
        if ($validator->fails()) 
        {
            return redirect('admin/account')->withErrors($validator)->withInput();
        } 
        else 
        {    
			//checking username address exists
			$condition = 'id != ? and email = ?';
			$duplication_check = Admin::whereRaw($condition, array( $adminUser->id , $request->input('email') ))->get();
			
			if(count($duplication_check) > 0 )
			{
			    $validator->getMessageBag()->add('email','This Email already exists');
                return redirect('admin/account')->withErrors($validator)->withInput();
            }
            else
            {
			   //updating the user data
               $adminUser->email = $request->input('email'); 
               $adminUser->name = $request->input('name');
               
               $adminUser->save();
               Session::flash('success', 'Settings updated successfully');
               return redirect('admin/account');	
			}
		}
    }
    
    
    
   /*
    * Password reset
    */
    public function showAccountpass( Request $request )
    {
		$adminUser = Auth::guard('admin')->user();
		$request->merge(array_map('trim', $request->all()));
		$rules = array(
            'old_password'  => 'required',
            'new_password'  => 'required|min:5',
            're_password'   => 'required|same:new_password',
        );
        $validator = Validator::make($request->all(), $rules);       
        //process the login
        if ($validator->fails()) 
        {	
            return redirect('admin/account')->withErrors($validator)->withInput();
        } 
        else 
        {    
            //checking old password matches with new
            if( Hash::check( $request->input('old_password') , $adminUser->password ) )
            {
				$adminUser->password   = Hash::make($request->input('new_password')); 
				$adminUser->save();
				Session::flash('success', 'Password updated successfully');
				return redirect('admin/account');
			}
			else
			{
				$validator->getMessageBag()->add('old_password', 'Old password dosent match with system data');
			    return redirect('admin/account')->withErrors($validator)->withInput();
			}	
		}
		
    }
    public function notify(){
        return view('admin.notification');
    }
    public function notify_class($classs){
        if($classs == 1){
            $class = "plus one";
        }else{
            $class = "plus two";
        }
      $users = DB::table('students')
                    ->select('*')
                    ->where('class',$class)
                    ->get();
        return view('admin.notification',['users'=>$users]);
    }
    public function notify_try(Request $request){
            if ($request->id != NULL) {
                $id = $request->id;
                $users = DB::table('students')
                            ->select('*')
                            ->where('id',$id)
                            ->get();
                return view('admin.notification',['users'=>$users]);
            }else{
                $id = $request->center;
                $users = DB::table('students')
                            ->select('*')
                            ->where('center',$id)
                            ->get();
                return view('admin.notification',['users'=>$users]);
            }
    }
    public function send(Request $request){
                $ids = $request->selector;
                $c_ids = $request->c_selector;
                // echo $ids;
                // echo $msg;
                if(count($ids) == 0 && count($c_ids) == 0){
                    echo "select all try";
                    return redirect('admin/notification')->with('dmessage', 'Select students/Centers');
                }
                else{
                    if (count($ids) > 0) {
                        foreach ($ids as $id) {
                            $notification = new Notification();
                            $notification->student_id = $id;
                            $notification->date = date('Y-m-d');
                            $notification->from = $request->from;
                            $notification->message = $request->msg;
                            $notification->view = 0;
                            $notification->save();
                        }
                    }else{
                        foreach ($c_ids as $ids) {
                            $notification = new Notification();
                            $notification->center_id = $ids;
                            $notification->date = date('Y-m-d');
                            $notification->from = "admin";
                            $notification->message = $request->msg;
                            $notification->view = 0;
                            $notification->save();
                        }
                    }
                }
                
                // alert("message send");
                return redirect('admin/notification')->with('message', 'Message Send Sucessfully');
    }
}