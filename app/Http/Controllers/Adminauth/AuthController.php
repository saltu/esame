<?php

namespace App\Http\Controllers\Adminauth;

use App\Models\Admin;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Auth;
use HtmlFacade;

class AuthController extends Controller
{

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    protected $redirectTo = 'admin/';
	protected $guard = 'admin';
	
	public function showLoginForm(){
		if (Auth::guard('admin')->check()){
			return redirect('admin');
		}
		
		return view('admin.auth.login');
	}
	
	
	// public function showRegistrationForm()
	// {
	// 	return view('admin.auth.register');
	// }
	
	public function resetPassword()
	{
		return view('admin.auth.passwords.email');
	}
	
	public function logout()
	{
		Auth::guard('admin')->logout();
		return redirect('/admin/');
	}
	
	/**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:admins',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return Admin::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }
}