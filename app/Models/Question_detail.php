<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Question_detail extends Model
{
    protected $fillable = [
    	'question_id','chapter_id',
    ];
}
