<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Exam_type_package extends Model
{
    protected $fillable = [
    	'title','class_ids','ex_type_ids','added_by','status',
    ];
}
