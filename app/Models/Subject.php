<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    protected $fillable = [
    	'class_id','examname_id','name','description','added_by','status',
    ];
}
