<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Student extends Authenticatable
{
    protected $fillable = [
        'name', 'email', 'school', 'phone', 'center', 'dob', 'yoa', 'place', 'class',  'parent_name', 'rws', 'parent_email' , 'parent_phone', 'address', 'password','status','added_by','pass_change_status',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
