<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Exam_result extends Model
{
    protected $fillable = [
        'exam_id','test_type','chapter_id','date','stud_id','q_id','op_id','result','p_id','t_attend','t_true','t_question',
    ];
}
