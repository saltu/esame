<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Student_exam_paid extends Model
{
    protected $fillable = [
    	'package_id','student_id', 'status',
    ];
}
