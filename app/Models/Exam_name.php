<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Exam_name extends Model
{
    protected $fillable = [
    	'category_id','name','description','image','added_by','subject_ids','status',
    ];
}
