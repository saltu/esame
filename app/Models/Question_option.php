<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Question_option extends Model
{
    protected $fillable = [
    	'question_id','options','opt_image',
    ];
}
