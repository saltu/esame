<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Student_payment extends Model
{
    protected $fillable = [
    	'amount','student_id', 'package_id', 'date', 'status',
    ];
}
