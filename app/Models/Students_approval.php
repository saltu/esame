<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Students_approval extends Model
{
    protected $fillable = [
        'name', 'added_from', 'email', 'school', 'phone', 'center', 'dob', 'yoa', 'place', 'class',  'parent_name', 'rws', 'parent_email' , 'parent_phone', 'address', 'password', 'status',
    ];
}
