<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Exam_fee extends Model
{
    protected $fillable = [
    	'title','ex_package_id', 'student_amount' ,'center_amount','added_by','status',
    ];
}
