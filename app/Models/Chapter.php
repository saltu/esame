<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Chapter extends Model
{
    protected $fillable = [
    	'subject_id','name','description','added_by','status',
    ];
}
